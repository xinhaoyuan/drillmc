.PHONY: debug release msvc all clean

all: debug

release:
	mkdir -p build/Release
	cd build/Release; cmake -DCMAKE_BUILD_TYPE=Release ../.. -G "Unix Makefiles"

debug:
	mkdir -p build/Debug
	cd build/Debug; cmake -DCMAKE_BUILD_TYPE=Debug ../.. -G "Unix Makefiles"

msvc:
	mkdir -p build/MSVC
	@[ ! -e MSVCConfig.cmake ] &&  cp MSVCConfig.cmake.default MSVCConfig.cmake || echo Using existing MSVCConfig.cmake
	cd build/MSVC; cmake ../../ -G 'Visual Studio 14 2015 Win64'

clean:
	rm -r build

loc:
	wc -l `find -iname "*.cpp" -or -iname "*.hpp" | grep -v build`
