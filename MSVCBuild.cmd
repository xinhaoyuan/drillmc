@echo off
@pushd %~dp0

@if not exist "MSVCConfig.cmake" (
    @echo Create MSVCConfig.cmake using MSVCConfig.cmake.default.
    @copy MSVCConfig.cmake.default MSVCConfig.cmake
)

@mkdir build\MSVC
@cd build\MSVC

rem You may want to change the generator name to fit your MSVC version
@cmake ..\.. -G "Visual Studio 14 2015 Win64"

@popd
@pause
@echo on