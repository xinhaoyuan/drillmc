# API Concepts

## Node Wrapper Object

A node wrapper object is the singleton proxy object associated to a node (usually a process in distributed system), which provides API functions to:

1. Initiailization and uninitiailization
2. Auxilary data management
3. __Schedule point__ registeration
4. __Context__ management

Related concepts will be introduced later.

## Schedule Point

schedule points denote execution events that occur in software execution. In Drill, schedule points are exposed by member functions of node wrapper objects.

There are two kinds of schedule points:

### Synchronous Schedule Point (Sync Point)

A sync point is exposed by ScheduleSync() API, defined as the returning of the API calls.
That is, every time a ScheduleSync() returns is considered a sync point.

### Asynchronous Schedule Point (Async Point)

An async point is exposed by ScheduleAsync() API. Function pointers are passed into ScheduleAsync as tasks, which will be executed later.
The async point is defined as each task get executed.

## Schedule

A schedule is a total order of schedule points. Schedules can be extracted from software execution.
Many executions may maps to the same schedules, which are considered equivalent.

## Context

A context is a running thread registered to Drill proxy object using context management API.
Only threads with contexts can register schedule points and send messages.

## Message

Messages are communication across contexts inside single operations to express thread communications and node communications.
During the execution of a single schedule point, drill explorer and proxies keep track of messages and schedule them __deterministically__.

## Quiescent State

An execution is considered quiescent state if all context are inside calls of ScheduleSync, and there's no message in-flight.
That means, no more things to do without triggering further schedule points.

## Correctness of Instrumentation

An instrumentation is correct if and only if:

If two executions have the same schedule,

1. they are considered equivalent, and
2. after reaching a quiescent state, they have the same set of successor schedule points.
	
## Guided Execution

Executions under Drill instrumentation are guided under schedules decided by explorer.

In short, given a schedule as a sequence of schedule points [S_1, S_2, ... , S_n]:

1. The explorer pick the next schedule point to schedule, notify the corresponding node proxy to trigger it.
2. The node proxy trigger the schedule point. If it's a sync point, the proxy object release the ScheduleSync call to return.
   If it's an async point, it runs the function pointer inside proxy's thread.
3. Wait the system enter quiescent again, report new schedule points to the explorer.
4. Repeat to step 1 until no more schedule point can be scheduled.

# API Functions (Incomplete)

## Init And De-init

Node Wrapper Object created locally without connected to centralized Drill explorer. To use Drill, the first thing is establishing connection:

	InitNode([EndPointName],[AuthCookie])

	[EndPointName]: Describe how to connect to explorer server, usually like "[IpAddress]:[PortNumber]". Leaving it nullptr means using environment setting.
	[AuthCookie]: The connecting cookie if StrongAuth is enabled in server. Leaving it nullptr means using environment setting.
	
	Return value: boolean - whether the connection is established.
	
Typical program terminates when its main thread eeration nnds, which will cause the Drill exploration iteration incorrectly ends.
To prevent that, developer use the following to wait for iteration to properly end.

	Join()

## Auxilary Data Management

Technically, this part is not necessary to be included with Drill. It's provided for convenience of instrumentation for resueable part across targets.

Description of auxilary data management is ignored for now.

## Schedule Point Registeration

	ScheduleSync([StateFunc],[Stub],[Data],[Name],[NByte],[Flags])

Register a sync point to explorer. The [StateFunc] is evaluated with [Data] every time.
[Stub] is evaluated with [Data] when the sync point is scheduled.

## Context Management

