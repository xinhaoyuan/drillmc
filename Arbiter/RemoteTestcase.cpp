#include "RemoteTestcase.hpp"
#include "Wrapper/cpp/DMCWrapper.hpp"
#include <thrift/concurrency/ThreadManager.h>
#include <thrift/concurrency/PlatformThreadFactory.h>
#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/server/TSimpleServer.h>
#include <thrift/server/TThreadPoolServer.h>
#include <thrift/server/TThreadedServer.h>
#include <thrift/transport/TServerSocket.h>
#include <thrift/transport/TSocket.h>
#include <thrift/transport/TTransportUtils.h>
#include <sstream>
#include <chrono>
#include <string>
#include <cstring>

#include "Utils/cpp/Formatted.hpp"

#ifdef _MSVC
#include <process.h>
#endif

#include "thrift/gen-cpp/Arbiter.h"
#include "thrift/gen-cpp/arbiter_types.h"

using namespace DMC;
using namespace DMC::Thrift;
using namespace XYHelper;

using namespace std;

using namespace apache::thrift;
using namespace apache::thrift::concurrency;
using namespace apache::thrift::protocol;
using namespace apache::thrift::transport;
using namespace apache::thrift::server;

#ifdef __APPLE__
extern char ** environ;
#endif

#define LOG_LOCK_GUARD lock_guard

static boost::shared_ptr<TTransport> GetTransport(const string & endpoint) {
    size_t pos = endpoint.find(':');
    if (pos != string::npos) {
        string addr(endpoint, 0, pos);
        int port = atoi(endpoint.c_str() + pos + 1);

        boost::shared_ptr<TSocket> socket(new TSocket(addr, port));
        socket->setNoDelay(true);
        boost::shared_ptr<TTransport> transport(new TBufferedTransport(socket));

        return transport;
    }
    else return nullptr;
}

RemoteTestcase::RemoteTestcase(RTArbiterRoot * arbiterRoot)
    : _arbiterRoot(arbiterRoot)
    , _logger(nullptr)
    , _autoNotify(false)
{
    conf.hostname = "127.0.0.1";
    conf.port = 18390;
    conf.forceExit = false;
    conf.reportTimeoutMs = 120000;
    conf.strongAuth = false;
    conf.verboseLevel = VerboseLevel::VERBOSE_DEFAULT;
    conf.nodeVerboseLevel = Wrapper::VERBOSE_DEFAULT;
    conf.localFilter = "";
    conf.localRandomSeed = 18390;
    conf.verboseStackInfo = false;
}

void RemoteTestcase::SetOptions(const map<string, string> & options) {
    auto it = options.find("LocalRandomSeed");
    if (it != end(options)) {
        conf.localRandomSeed = stoll(it->second);
    }
}

void RemoteTestcase::AddLogLine(const char * line) {
#ifdef __DMC_OUROBOROS__
    auto wrapper = GetDMCNodeInstance();
    wrapper->SendLog({ line });
#else
    if (_logger != nullptr) {
        VerboseLevel verboseLevel = VerboseLevel::VERBOSE_DEFAULT;
        if (line[0] == '<' && !!line[1] && line[2] == '>') {
            switch (line[1]) {
            case 'D':
                verboseLevel = VerboseLevel::VERBOSE_DEBUG;
                break;
            case 'I':
                verboseLevel = VerboseLevel::VERBOSE_INFO;
                break;
            case 'T':
                verboseLevel = VerboseLevel::VERBOSE_TLOG;
                break;
            case 'W':
                verboseLevel = VerboseLevel::VERBOSE_WARNING;
                break;
            case 'E':
                verboseLevel = VerboseLevel::VERBOSE_ERROR;
                break;
            }
        }
        _logger->Log(verboseLevel, line);
    }
#endif
}

void RemoteTestcase::RegisterNode(int nodeId, RTArbiter * node, const string & controllerEndpoint) {
    _unique_lock lock(_lock);

    _nodes[nodeId] = node;
    _metadata[Formatted("ctrl:%d", nodeId).CString()] = controllerEndpoint;
    
    if (_schedulePresented) {
        if (_scheduleNodeId == -1) {
            ArbiterChoice choice;
            choice.source = ChoiceSource::TO_EXIT;
            node->ExecuteSchedule(choice, lock);
            return;
        }
        
        if (_scheduleNodeId == nodeId) {
            TryTriggerScheduleWithLock(lock);
        }
    }

    lock.unlock();
}

void RemoteTestcase::LockedUnregisterNode(int nodeId) {
    _nodes.erase(nodeId);
    _activeNodes.erase(nodeId);
    _notifySet.erase(nodeId);
    _metadata.erase(Formatted("ctrl:%d", nodeId).CString());

    remove_if(begin(_messageQueue), end(_messageQueue),
              [nodeId](const tuple<int, int, string> & m) -> bool { return get<1>(m) == nodeId; }
        );
}

void RemoteTestcase::UnregisterNode(int nodeId) {
    lock_guard lock(_lock);
    LockedUnregisterNode(nodeId);
}

inline void RemoteTestcase::ReportBackInLock() {
    _reportPresented = true;
    _cv.notify_all();
}

void RemoteTestcase::ReportBack() {
    lock_guard lock(_lock);
    ReportBackInLock();
}

inline void RemoteTestcase::LockedReportBackEnd(bool isError) {
    _endPresented = true;
    _errorPresented = isError;
    _cv.notify_all();
}

void RemoteTestcase::ReportBackEnd(bool isError) {
    lock_guard lock(_lock);
    LockedReportBackEnd(isError);
}

bool RemoteTestcase::WaitReport() {
    _unique_lock lock(_lock, defer_lock);
    auto deadline = chrono::system_clock::now() + chrono::milliseconds(conf.reportTimeoutMs);
    lock.lock();
    while (true) {
        if (_reportPresented) break;
        if (_endPresented) break;
        // XXX do not consider timeout for now
#ifdef __DMC_OUROBOROS__
        _cv.wait(lock);
#else
        if (conf.reportTimeoutMs <= 0) {
            _cv.wait(lock);
        }
        else if (_cv.wait_until(lock, deadline) == cv_status::timeout) {
            // Timeout
            AddLogLine(Formatted("<W> No report for %dms. Treat it as an error report", conf.reportTimeoutMs).CString());
            _endPresented = true;
            _errorPresented = true;
            break;
        }
#endif
    }
    _reportPresented = false;
    if (_endPresented) {
        lock.unlock();
        return false;
    }

    // 1 on 1 report back
    lock.unlock();
    return true;
}

void RemoteTestcase::SetSchedule(int nodeId, ChoiceSource::type source, int globalChoice) {
    _unique_lock lock(_lock, defer_lock);
    lock.lock();
    
    _scheduleNodeId = nodeId;
    _scheduleSource = source;
    _globalChoice = globalChoice;
    TryTriggerScheduleWithLock(lock);
    
    lock.unlock();
}

void RemoteTestcase::TryTriggerScheduleWithLock(_unique_lock & lock) {
    ArbiterChoice choice;
    
    if (_scheduleNodeId >= 0) {
        auto it = _nodes.find(_scheduleNodeId);
        if (it != end(_nodes)) {
            // Nice, we have the node here. Do some state handling and schedule the work.

            // Update local view of global metadata
            choice.currentNodeCount = _nodeIdCounter;
            choice.currentIdCount = _idCounter;
            choice.currentMessageTokenCount = _messageTokenCounter;
            // Update local detach generator
            // Currently we just do at the initial time
            if (get<1>(*it)->_scheduleCount == 0) {
                choice.localRandomSeed = conf.localRandomSeed;
                choice.localFilter = conf.localFilter;
                AddLogLine(GenString("<D> pass local info to ", _scheduleNodeId,
                    ", localFilter = [", conf.localFilter, "]"
                    ", localRandomSeed = ", conf.localRandomSeed));
            }
            else {
                choice.localRandomSeed = -1;
            }
            choice.externalWeight = 0;

            _schedulePresented = false;
            if (_stateType == StateType::Input) {
                choice.source = ChoiceSource::INPUT;
                choice.choice = _globalChoice;
            }
            else if (_scheduleSource == ChoiceSource::REMOTE) {
                choice.source = ChoiceSource::REMOTE;
                // here the globalChoice is the transfer token
                choice.choice = _globalChoice;
            }
            else if (_scheduleSource == ChoiceSource::LOCAL) {
                choice.source = ChoiceSource::LOCAL;
                // translate it into local operation id
                choice.choice = get<1>(_globalToLocal[_globalChoice]);
            }
            else if (_scheduleSource == ChoiceSource::NOTIFY) {
                choice.source = ChoiceSource::NOTIFY;
                choice.choice = -1;
            }
            else {
                AddLogLine(GenString("<E> Unknown schedule source type ", _scheduleSource,
                                     ". Do notify instead. The result may be unexpected"));
                choice.source = ChoiceSource::NOTIFY;
                choice.choice = -1;
            }

            // AddLogLine(GenString("<D> Schedule rpc calling to node ", _scheduleNodeId));
            get<1>(*it)->ExecuteSchedule(choice, lock);
            // AddLogLine(GenString("<D> Schedule rpc called to node ", _scheduleNodeId));
        }
        else {
            _schedulePresented = true;
        }
    }
    else {
        // Assume it's a quit message sending to everyone
        _schedulePresented = true;
        choice.source = ChoiceSource::TO_EXIT;

        for (auto && kv : _nodes) {
            get<1>(kv)->ExecuteSchedule(choice, lock);
        }

        _activeNodes.clear();
    }
}

void RemoteTestcase::SetTracking(
    map<int, DMC::OperationDescription *> * newOps, set<int> * toggledOps, set<int> * cancelledOps,
    DependencyRecord ** dep, vector<MessageRecord *> * msgs, vector<AnnotationRecord *> * ants) {

    _newOps = newOps;
    _toggledOps = toggledOps;
    _cancelledOps = cancelledOps;
    _dep = dep;
    _msgs = msgs;
    _ants = ants;
}

int RemoteTestcase::GetInputNode() {
    if (_stateType != StateType::Input) {
        return -1;
    }
    else {
        return _inputNode;
    }
}

int RemoteTestcase::GetInputRange() {
    if (_stateType != StateType::Input) {
        return 0;
    }
    else {
        return _inputRange;
    }
}

StateType RemoteTestcase::Setup() {
    _localToGlobal.clear();
    _globalToLocal.clear();
    _inQueueCount.clear();
    _messageQueue.clear();
    _notifySet.clear();
    _activeNodes.clear();
    _desc.clear();
    _nodes.clear();
    _metadata = _initMetadata;
    _buffers.clear();
    
    _nodeIdCounter = 0;
    _opIdCounter = 0;
    _idCounter = 0;
    _messageTokenCounter = 0;
    _schedulePresented = false;
    _reportPresented = false;
    _endPresented = false;
    _errorPresented = false;
    _isError = false;
    _isEnd = false;
    _stateType = StateType::Operation;
    
    ++_nodeIdCounter;
    ++_messageTokenCounter;
    EnqueueMessage(0, 0, "<Global Start>");
    
    AddLogLine("<I> Setup() started");

    for (auto && kv : _metadata) {
        AddLogLine(GenString("<I>   [", get<0>(kv), "] => [", get<1>(kv), ']'));
    }
    
    _arbiterRoot->Start(this);

    HandleMessage();
    if (_isEnd) {
        AddLogLine("<W> Setup() terminated from HandleMessage");
        return _stateType;
    }
    
    if (_setupPrefix.size() > 0) {
        AddLogLine("<D> Setup() is executing prefix");

        set<int> cancelledOps;
        set<int> toggledOps;

        if (_cancelledOps == nullptr)
            _cancelledOps = &cancelledOps;
        
        if (_toggledOps == nullptr)
            _toggledOps = &toggledOps;

        for (int choice : _setupPrefix) {
            InternalHandle(choice, false);
            if (_isEnd) {
                AddLogLine("<W> Setup() terminated from InternalHandle");
                return _stateType;
            }
        }

        // handle cancelledOps, delete cancelled ops that occur in newOps or toggledOps.
        
        for (int opId : *_cancelledOps) {

            auto it = _toggledOps->find(opId);
            if (it != end(*_toggledOps))
                _toggledOps->erase(it);
        
            if (_newOps != nullptr) {
                auto it = _newOps->find(opId);
                if (it != end(*_newOps)) {
                    delete get<1>(*it);
                    _newOps->erase(it);
                }
            }
        }

        if (_toggledOps == &toggledOps)
            _toggledOps = nullptr;
        if (_cancelledOps == &cancelledOps)
            _cancelledOps = nullptr;
    }
    
    AddLogLine("<I> Setup() finished");
    return _stateType;
}

class EarlyListenSocket : public TServerSocket {
    bool _listen;
public:
    EarlyListenSocket(int port)
        : _listen(false), TServerSocket(port) { }

    void listen() override {
        if (!_listen) {
            _listen = true;
            
            int backoffMs = 10;
            int retry = 16;
                    
            while (true) {
                try {
                    TServerSocket::listen();
                    break;
                } catch (TException x) {
                    if (retry > 0) {
                        --retry;
#if _MSVC
                        Sleep(backoffMs);
#else
                        usleep(backoffMs * 1000);
#endif
                        backoffMs = backoffMs + (backoffMs >> 1);
                    }
                    else {
                        throw x;
                    }
                }
            }
        }
    }
};

RTArbiterRoot::RTArbiterRoot()
    : _testcase(nullptr)
    , _currentSessionId(0)
    , _serverUp(false)
    , _factory(boost::make_shared<RTArbiterFactory>(this))
    , _nSessions(0)
    , _nSessionsHighWaterMark(512)
    , _nSessionsLowWaterMark(256)
    , _serverMainThread(nullptr)
{
}

void RTArbiterRoot::StartServerThread(int port) {
    if (_serverMainThread != nullptr) {
        // XXX error message?
        return;
    }

    _port = port;

#ifdef __DMC_OUROBOROS__
    auto wrapper = GetDMCNodeInstance();
    int token = wrapper->MessageSend();
    _serverMainThread = new thread(ServerThread, this);
    wrapper->MessageReceive(false, token);
#else
    _serverMainThread = new thread(ServerThread, this);
#endif

    _unique_lock lock(_lock, defer_lock);
    lock.lock();
    while (!_serverUp)
        _serverCV.wait(lock);
    lock.unlock();
}

void RTArbiterRoot::ServerThread(RTArbiterRoot * self) {
#ifdef __DMC_OUROBOROS__
    auto wrapper = GetDMCNodeInstance();
    wrapper->MessageReceiveUnique(false);
#endif

    auto socket = boost::make_shared<EarlyListenSocket>(self->_port);
    // Create the server first
    self->_arbiterServer = new TThreadedServer(
        boost::make_shared<ArbiterProcessorFactory>(self->_factory),
        socket,
        boost::make_shared<TBufferedTransportFactory>(),
        boost::make_shared<TBinaryProtocolFactory>());
    // listen before testcase start
    socket->listen();
    {
        lock_guard lock(self->_lock);
        self->_serverUp = true;
        self->_serverCV.notify_one();
    }
#ifdef __DMC_OUROBOROS__
    wrapper->ContextDetach();
#endif
    self->_arbiterServer->serve();
}

RTArbiterRoot::~RTArbiterRoot() {
    _arbiterServer->stop();
    // Assume when main thread ends, all sessions are normally ended.
    _serverMainThread->join();
}

bool RTArbiterRoot::Start(RemoteTestcase * testcase) {
    ostringstream os;
    
    {
        lock_guard lock(_lock);
        _activeSessions = 0;
        _testcase = testcase;
        _connectedNodes.clear();
        os << _currentSessionId << "-" << rand();
        _initCookie = os.str(); os.str("");
    }

    map<string, string> env = testcase->_testcaseEnv;
    env["DMC_ARBITER_ENDPOINT"] = GenString(testcase->conf.hostname, ":", testcase->conf.port);
    env["DMC_ARBITER_COOKIE"] = _initCookie;
    
    // Generate the environ and args
    vector<string> & args = testcase->_testcaseArgs;
    vector<char *> newEnv;
    for (char ** envEntry = environ; *envEntry != nullptr; ++envEntry) {
        if (env.find(string(*envEntry,
                            strchr(*envEntry, '=') - *envEntry))
            != env.end())
            continue;
        newEnv.push_back(strdup(*envEntry));
    }

    for (auto && kv : env) {
        os << kv.first << "=" << kv.second;
        newEnv.push_back(strdup(os.str().c_str())); os.str("");
    }    
    newEnv.push_back(nullptr);
    
    char ** argv = (char **)alloca(sizeof(char *) * (1 + args.size()));
    for (int i = 0; i < args.size(); ++i) {
        argv[i] = strdup(args[i].c_str());
    }
    argv[args.size()] = nullptr;
    char ** envp = &newEnv[0];

    bool ret;

#ifdef __DMC_OUROBOROS__
    auto wrapper = GetDMCNodeInstance();
    int token = wrapper->MessageSend();
#endif
    
#ifdef _MSVC
    ret = _spawnvpe(_P_NOWAIT, argv[0], argv, envp) >= 0;
#else

#ifdef __APPLE__
    int pid = fork();

    if (pid == 0) {
        environ = envp;
        execvp(argv[0], argv);
        pid = -1;
        _exit(-1);
    }
#else
    int pid = vfork();
    
    if (pid == 0) {
        execvpe(argv[0], argv, envp);
        pid = -1;
        _exit(-1);
    }
#endif

    for (char * e : newEnv) free(e);
    for (int i = 0; i < args.size(); ++i) free(argv[i]);

    if (pid == -1) {
        cerr << "Failed to spwan a new test" << endl;
        ret = false;
    }
    else ret = true;
    
#endif

    return ret;
}

ArbiterIf * RTArbiterRoot::RTArbiterFactory::getHandler(const ::apache::thrift::TConnectionInfo & connInfo) {
#ifdef __DMC_OUROBOROS__
    auto wrapper = GetDMCNodeInstance();
    // wrapper->EnsureContext(); FIX
#endif
    TSocket * socket = reinterpret_cast<TSocket *>(connInfo.transport.get());
    if (socket != nullptr) {
        socket->setNoDelay(true);
    }

    _unique_lock lock(_root->_lock, defer_lock);
    lock.lock();
    while (_root->_nSessions >= _root->_nSessionsHighWaterMark)
        _root->_nSessionsCV.wait(lock);
    ++_root->_nSessions;
    if (_root->_testcase != nullptr) {
        return new RTArbiter(_root, _root->_testcase);
    } else {
        // Thrift cannot refuse session at this point, return a dummy.
        return new RTArbiter(nullptr, nullptr);
    }
#ifdef __DMC_OUROBOROS__
    wrapper->ContextDetach();
#endif
}

void RTArbiterRoot::RTArbiterFactory::releaseHandler(ArbiterIf * handler) {
#ifdef __DMC_OUROBOROS__
    auto wrapper = GetDMCNodeInstance();
    // wrapper->EnsureContext(); FIX
#endif

    auto && controller = ((RTArbiter *)handler)->_controllerTransport;
    if (!!controller) controller->close();
    delete handler;

    {
        lock_guard lock(_root->_lock);
        if (_root->_nSessions == _root->_nSessionsLowWaterMark) {
            _root->_nSessionsCV.notify_all();
        }
        --_root->_nSessions;
        if (_root->_nSessions == 0) {
            _root->_nSessionsCV.notify_all();
        }
    }

#ifdef __DMC_OUROBOROS__
    wrapper->ContextDetach();
#endif
}

bool RTArbiterRoot::End() {
    _unique_lock lock(_lock);

    _testcase = nullptr;
    _connectedNodes.clear();
    ++_currentSessionId;
    
    // Active sessions are session running in locally. It's reasonable (?) to assume there's no deadlock.
    while (_activeSessions > 0)
        _cleanCV.wait(lock);

    // After this point, all previous connection will not be able to modify Arbiter and RemoteTestcase, because sessionId doesn't match anymore.

    // Wait 10s for most connections to shutdown
    auto deadline = chrono::system_clock::now() + chrono::milliseconds(10000);
    while (_nSessions > 0) {
#ifdef __DMC_OUROBOROS__
        _nSessionsCV.wait(lock);
#else
        if (_nSessionsCV.wait_until(lock, deadline) == cv_status::timeout)
            return false;
#endif
    }

    return true;
}

bool RTArbiterRoot::Attach(RemoteTestcase * testcase, int sessionId) {
    lock_guard lock(_lock);
    if ((_testcase == testcase) && (_currentSessionId == sessionId)) {
        ++_activeSessions;
        return true;
    }
    else {
        return false;
    }
}

bool RTArbiterRoot::AttachWithoutSession(RemoteTestcase * testcase) {
    lock_guard lock(_lock);
    if (_testcase == testcase) {
        ++_activeSessions;
        return true;
}
    else {
        return false;
    }
}

bool RTArbiterRoot::InitNodeAttach(RemoteTestcase * testcase, const string & cookie, int nodeId, int & outSessionId, InitConfiguration & outConf) {
#ifdef __DMC_OUROBOROS__
    auto wrapper = GetDMCNodeInstance();
    // wrapper->EnsureContext(); FIX
#endif
    lock_guard lock(_lock);

    bool succ = false;
    if (_testcase == testcase && 
        (!_testcase->conf.strongAuth || _initCookie == cookie)) {
        if (_connectedNodes.find(nodeId) == end(_connectedNodes)) {
            _connectedNodes.insert(nodeId);
            succ = true;
        }
    }

    if (succ) {
        outSessionId = _currentSessionId;

        outConf.success = true;
        outConf.cookie = _initCookie;
        outConf.forceExit = testcase->conf.forceExit;
        outConf.strongAuth = testcase->conf.strongAuth;
        outConf.logLevel = testcase->conf.nodeVerboseLevel;
        outConf.verboseStackInfo = testcase->conf.verboseStackInfo;
        ++_activeSessions;
        return true;
    }
    else {
        testcase->AddLogLine(GenString("<W> Duplicated or invalid node ", nodeId, ", or auth failed (if strongAuth is enabled)"));
        outConf.success = false;
        outConf.forceExit = true;
        return false;
    }
}

void RTArbiterRoot::DetachNode(int nodeId) {
    lock_guard lock(_lock);
    _connectedNodes.erase(nodeId);
}

void RTArbiterRoot::Detach() {
    lock_guard lock(_lock);
    if (--_activeSessions == 0)
        _cleanCV.notify_one();
}

StateType RemoteTestcase::Proceed(int choice) {
    if (_isEnd) return _stateType;

    InternalHandle(choice, true);
    return _stateType;
}

void RemoteTestcase::InternalHandle(int choice, bool external) {
    if (_stateType == StateType::Operation) {
        if (_globalToLocal.find(choice) == end(_globalToLocal)) {
            _isEnd = true;
            _isError = true;
            _stateType = StateType::Error;
            AddLogLine(Formatted("<E> Handle() choose unregistered operation %d", choice).CString());
            return;
        }

        if (_cancelledOps != nullptr) {
            _cancelledOps->insert(choice);
        }

        auto && tp = _globalToLocal[choice];
        int nodeId = get<0>(tp);

        AddLogLine(Formatted("<I> %s chose operation %d => (%d, %d) [%s]",
            external ? "External" : "Internal",
            choice, get<0>(tp), get<1>(tp), _desc[choice].c_str()).CString());

        SetSchedule(nodeId, ChoiceSource::LOCAL, choice);
        if (!WaitReport()) {
            _isEnd = true;
            _isError = _errorPresented;
            if (_isError) {
                _stateType = StateType::Error;
                AddLogLine("<W> Handle() failed from local operation");
            }
            else {
                _stateType = StateType::Terminated;
                AddLogLine("<I> End from local operation");
            }
            
            return;
        }
    }
    else if (_stateType == StateType::Input) {
        AddLogLine(Formatted("<I> %s chose input %d to node %d",
            external ? "External" : "Internal", choice, _inputNode).CString());

        SetSchedule(_inputNode, ChoiceSource::INPUT, choice);
        if (!WaitReport()) {
            _isEnd = true;
            _isError = _errorPresented;
            if (_isError) {
                _stateType = StateType::Error;
                AddLogLine("<W> Handle() failed from local operation");
            }
            else {
                _stateType = StateType::Terminated;
                AddLogLine("<I> End from local operation");
            }

            return;
        }
    }
    else {
        AddLogLine(Formatted("<W> %s try to proceed with %d in unknown StateType",
            external ? "External" : "Internal", choice).CString());
        return;
    }

    HandleMessage();
    
    AddLogLine(Formatted("<D> Handle() finished normally, StatusType = %d", _stateType).CString());
}

int RemoteTestcase::PostProcessNewOperation(const Thrift::OperationDescriptionWithId & desc) {
    int nodeId = desc.desc.node;
    int localId  = desc.id;
    int globalId = _opIdCounter++;
    Formatted f;

    _localToGlobal[forward_as_tuple(nodeId, localId)] = globalId;
    _globalToLocal[globalId] = forward_as_tuple(nodeId, localId);

    AddLogLine(f("<D> operation %d => (%d, %d) [%s], weak = %d", globalId, nodeId, localId, desc.desc.text.c_str(), desc.desc.isWeak).CString());

    return globalId;
}

void RemoteTestcase::EnqueueMessage(int token, int target, const string & message) {
    ++_inQueueCount[target];
    _messageQueue.push_back(forward_as_tuple(token, target, message));
}

void RemoteTestcase::HandleMessage() {
    if (_stateType == StateType::Operation && !_messageQueue.empty()) {
        AddLogLine(GenString("<D> MsgQueue.size = ", _messageQueue.size()));
    }

    while (_stateType == StateType::Operation && !_messageQueue.empty()) {
        auto tp = _messageQueue.front();
        int  node = get<1>(tp);
        auto it = _inQueueCount.find(node);

        _messageQueue.pop_front();
        if (it == end(_inQueueCount)) {
            AddLogLine(GenString("<E> target ", node, " not found in inQueueCount. This should not happen"));
            continue;
        }

        int remaining = --it->second;

        // _messageQueue and _inQueueCount are free to mutate

        _activeNodes.insert(node);
        AddLogLine(GenString("<I> Send message to node ", node, " with text [", get<2>(tp), "], token = ", get<0>(tp)));

        // Hacky: use the sign for uniqueness
        SetSchedule(node, ChoiceSource::REMOTE, (remaining == 0) ? get<0>(tp) : -get<0>(tp));

        if (!WaitReport()) {
            _isEnd = true;
            _isError = _errorPresented;
            if (_isError) {
                _stateType = StateType::Error;
                AddLogLine("<W> Get error report in HandleMessage");
            }
            else {
                _stateType = StateType::Terminated;
                AddLogLine("<I> End in HandleMessage");
            }
            return;
        }

        AddLogLine(GenString("<D> Sending message to node ", node, " completed"));

    };

    if (_stateType == StateType::Operation) {
        if (_autoNotify || (_notifySet.size() > 0 && *begin(_notifySet) < 0))
            _notifySet.insert(begin(_activeNodes), end(_activeNodes));
        
        for (int node : _notifySet) {
            if (node < 0) continue;
            if (_activeNodes.find(node) == end(_activeNodes)) continue;

            SetSchedule(node, ChoiceSource::NOTIFY, -1);
            if (!WaitReport()) {
                _isEnd = true;
                _isError = _errorPresented;
                if (_isError) {
                    _stateType = StateType::Error;
                    AddLogLine("<W> Get error report in HandleMessage when Notifying");
                }
                else {
                    _stateType = StateType::Terminated;
                    AddLogLine("<I> End in HandleMessage when Notifying");
                }

                return;
            }
        }

        _notifySet.clear();
    }
}

void RemoteTestcase::CleanUp() {
    AddLogLine("<I> Cleanup started");
    
    SetSchedule(-1, ChoiceSource::TO_EXIT, -1);

    if (!_arbiterRoot->End()) {
        AddLogLine("<W> Some clients still remain connected. May need to inspect.");
    }

    _nodes.clear();
    _metadata.clear();
    _buffers.clear();

    AddLogLine("<I> Cleanup finished");
}

void RemoteTestcase::Sync(int nodeId, const Thrift::SyncData & syncData) {
    _unique_lock lock(_lock);

    // Make sure RemoteTestcase are waiting for us to report {{

    if (_schedulePresented || _scheduleNodeId != nodeId) {
        AddLogLine(Formatted("<W> Error: Inactive node %d tried to sync.", nodeId).CString());
        LockedReportBackEnd(true);
        return;
    }

    // }}. From now on the testcase are waiting for us.

    AddLogLine(Formatted("<D> Node %d is synchronizing", nodeId).CString());
    ostringstream os;
    bool first;

    // 1:i32 inputRange
    _inputNode = nodeId;
    if (syncData.__isset.inputRange && syncData.inputRange > 0) {
        _stateType = StateType::Input;
        _inputRange = syncData.inputRange;

        AddLogLine(Formatted("<I> Node %d request for a input, range = %d", nodeId, _inputRange).CString());
    }
    else if (syncData.__isset.inputRange && syncData.inputRange < 0) {
        _stateType = StateType::Operation;

        LockedUnregisterNode(nodeId);
        AddLogLine(GenString("<I> Node ", nodeId, " report to disconnect"));
    }
    else {
        _stateType = StateType::Operation;
        _inputRange = 0;
    }

    // 2:list<OperationDescriptionWithId> newOps
    if (syncData.__isset.newOps && syncData.newOps.size() > 0) {
        first = false;

        os << "<D> newOps = {";
        first = true;

        for (auto && iDesc : syncData.newOps) {
            int globalId = PostProcessNewOperation(iDesc);

            if (first) first = false;
            else os << ",";
            os << globalId;
            _desc[globalId] = iDesc.desc.text;

            if (_newOps != nullptr) {
                OperationDescription * desc = new OperationDescription();
                desc->node = iDesc.desc.node;
                desc->isWeak = iDesc.desc.isWeak;
                desc->text = iDesc.desc.text;

                (*_newOps)[globalId] = desc;
            }
        }

        os << "}";
        AddLogLine(os.str()); os.str("");
    }

    // 3:list<i32> toggledOps
    if (syncData.__isset.toggledOps && syncData.toggledOps.size() > 0) {

        os << "<D> toggledOps = {";
        first = true;

        for (int32_t localOpId : syncData.toggledOps) {
            auto it = _localToGlobal.find(forward_as_tuple(nodeId, localOpId));
            if (it == end(_localToGlobal)) {
                AddLogLine("<E> localOpId not registered in toggledOps");
                LockedReportBackEnd(true);
                return;
            }

            int globalId = get<1>(*it);
            if (first) first = false;
            else os << ",";
            os << globalId;

            if (_toggledOps != nullptr) {
                if (_toggledOps->find(globalId) == _toggledOps->end())
                    _toggledOps->insert(globalId);
                else _toggledOps->erase(globalId);
            }
        }

        os << "}";
        AddLogLine(os.str()); os.str("");
    }

    // 4:list<i32> cancelledOps
    if (syncData.__isset.cancelledOps && syncData.cancelledOps.size() > 0) {

        os << "<D> cancelledOps = {";
        first = true;

        for (int32_t localOpId : syncData.cancelledOps) {
            auto it = _localToGlobal.find(forward_as_tuple(nodeId, localOpId));
            if (it == end(_localToGlobal)) {
                AddLogLine("<E> localOpId not registered in cancelledOps");
                LockedReportBackEnd(true);
                return;
            }

            int globalId = get<1>(*it);
            if (first) first = false;
            else os << ",";
            os << globalId;

            if (_cancelledOps != nullptr) {
                _cancelledOps->insert(globalId);
            }
        }

        os << "}";
        AddLogLine(os.str()); os.str("");
    }

    // 5:common.OperationDependency dep
    if (syncData.__isset.dep) {
        
        os << "<D> Dep = R: {";
        {
            bool first = true;
            for (auto && ele : syncData.dep.readSet) {
                if (first)
                    first = false;
                else os << ",";
                os << ele;
            }
        }
        os << "}, W: {";
        {
            bool first = true;
            for (auto && ele : syncData.dep.writeSet) {
                if (first)
                    first = false;
                else os << ",";
                os << ele;
            }
        }
        os << "}";

        AddLogLine(os.str()); os.str("");

        if (_dep != nullptr && (syncData.dep.readSet.size() > 0 || syncData.dep.writeSet.size() > 0)) {
            if (*_dep == nullptr) *_dep = new DependencyRecord();
            for (auto && ele : syncData.dep.readSet)
                (**_dep).readSet.insert(ele);
            for (auto && ele : syncData.dep.writeSet)
                (**_dep).writeSet.insert(ele);
        }
    }

    // 6:list<string> annotations
    if (syncData.__isset.annotations && syncData.annotations.size() > 0) {
        if (_ants != nullptr) {
            for (auto && antEntry : syncData.annotations) {
                auto ant = new AnnotationRecord();
                ant->node = nodeId;
                ant->type = antEntry.type;
                ant->value = antEntry.value;
                _ants->push_back(ant);
            }
        }
    }

    // 7:list<MessageWithId> messages
    if (syncData.__isset.messages && syncData.messages.size() > 0) {
        os << "<D> MsgQueue += {";
        first = true;
        for (auto && msg : syncData.messages) {
            EnqueueMessage(msg.id, msg.message.target, msg.message.text);
            if (first) first = false;
            else os << ',';
            os << '[' << msg.id << ',' << msg.message.target << ',' << msg.message.text << ']';
        }
        os << '}';
        AddLogLine(os.str()); os.str("");
    }

    // 8:list<i32> notify
    if (syncData.__isset.notify && syncData.notify.size() > 0) {
        os << "<D> Notify {";
        first = true;
        for (int nodeId : syncData.notify) {
            if (first) first = false;
            else os << ',';
            _notifySet.insert(nodeId);
            os << nodeId;
        }
        os << "}";
        AddLogLine(os.str()); os.str("");
    }

    // 9:i32 updatedNodeCount
    if (syncData.__isset.updatedNodeCount && _nodeIdCounter <= syncData.updatedNodeCount) {
        _nodeIdCounter = syncData.updatedNodeCount;
    }

    // 10:i32 updatedMessageTokenCount
    if (syncData.__isset.updatedMessageTokenCount && _messageTokenCounter <= syncData.updatedMessageTokenCount) {
        _messageTokenCounter = syncData.updatedMessageTokenCount;
    }

    // 11:i32 updatedIdCount
    if (syncData.__isset.updatedIdCount && _idCounter <= syncData.updatedIdCount) {
        _idCounter = syncData.updatedIdCount;
    }

    ReportBackInLock();
    AddLogLine(Formatted("<D> Node %d reported.", nodeId).CString());
}

void RemoteTestcase::Report(int nodeId, const Thrift::ReportData & reportData) {
    lock_guard lock(_lock);

    string typeStr = "<T> ";

    if (reportData.isError) {
        typeStr = "<W> ";
        AddLogLine(GenString(typeStr, "Node ", nodeId, " reported error: ", reportData.description));
    }
    else {
        AddLogLine(GenString(typeStr, "Node ", nodeId, ": ", reportData.description));
    }

    if (reportData.isEnd) {
        AddLogLine(GenString(typeStr, "Testcase ended by node ", nodeId, "."));
        LockedReportBackEnd(reportData.isError);
    }

    if (reportData.__isset.info && reportData.info.size() > 0) {
        AddLogLine(GenString(typeStr, "Info:"));
        for (const string & line : reportData.info) {
            AddLogLine(GenString(typeStr, line));
        }
    }
}

void RemoteTestcase::SetVariables(int nodeId, const map<string, string> & entries) {
    lock_guard lock(_lock);
    for (auto && kv : entries) {
        AddLogLine(Formatted(
            "<I> Set var [%s] => [%s] by node %d",
            get<0>(kv).c_str(), get<1>(kv).c_str(), nodeId).CString());
        _metadata[get<0>(kv)] = get<1>(kv);
    }
}

void RemoteTestcase::GetVariables(int nodeId, vector<string> & outValues, const vector<string> & names) {
    lock_guard lock(_lock);
    for (auto && name : names) {
        auto it = _metadata.find(name);
        if (it == end(_metadata)) {
            outValues.push_back("");
            AddLogLine(Formatted("<I> var [%s] not found, queried by node %d",
                name.c_str(), nodeId).CString());
        }
        else {
            outValues.push_back(get<1>(*it));
            //AddLogLine(Formatted("<I> var [%s] => [%s], queried by node %d",
            //    name.c_str(), get<1>(*it).c_str(), nodeId).CString());
        }
    }
}

void RemoteTestcase::BufferGetInfo(int nodeId, int id, bool & isOpen, int & length) {
    lock_guard lock(_lock);

    auto it = _buffers.find(id);
    if (it == _buffers.end()) {
        AddLogLine(GenString("<W> BufferGetInfo - buffer ", id, " not found, queried by node ", nodeId));
        
        isOpen = false;
        length = 0;
    }
    else {
        isOpen = it->second.isOpen;
        length = it->second.length;
    }
}

bool RemoteTestcase::BufferOpen(int nodeId, int id) {
    lock_guard lock(_lock);

    AddLogLine(GenString("<D> BufferOpen ", id, " by node ", nodeId));

    auto it = _buffers.find(id);
    if (it == _buffers.end()) {
        _buffers[id].isOpen = true;
        return true;
    }
    else if (it->second.isOpen) {
        return false;
    }
    else {
        it->second.isOpen = true;
        return true;
    }
}

bool RemoteTestcase::BufferClose(int nodeId, int id) {
    lock_guard lock(_lock);

    AddLogLine(GenString("<D> BufferClose ", id, " by node ", nodeId));

    auto it = _buffers.find(id);
    if (it == _buffers.end()) {
        return false;
    }
    else if (it->second.isOpen) {
        it->second.isOpen = false;
        if (it->second.length == 0) {
            _buffers.erase(it);
        }
        return true;
    }
    else {
        return false;
    }
}

int RemoteTestcase::BufferWrite(int nodeId, int id, const char * data, int len) {
    lock_guard lock(_lock);

    auto it = _buffers.find(id);
    if (it == _buffers.end() || it->second.isOpen == false) {
        AddLogLine(GenString("<D> BufferWrite ", id, " by node ", nodeId, " len = ", len, " => buffer does not exist"));
        return 0;
    }
    else {
        int ret = it->second.buf.sputn(data, len);
        if (ret != len)
            AddLogLine(GenString("<W> Partial buffer write, nodeId = ", nodeId, ", id = ", id,
                                 ", expected = ", len, ", ret = ", ret));
        it->second.length += ret;
        AddLogLine(GenString("<D> BufferWrite ", id, " by node ", nodeId, " len = ", len, " => ", ret));
        return ret;
    }
}

int RemoteTestcase::BufferRead(int nodeId, int id, string & out, int len) {
    lock_guard lock(_lock);

    auto it = _buffers.find(id);
    if (it == _buffers.end() || it->second.length == 0) {
        AddLogLine(GenString("<D> BufferRead ", id, " by node ", nodeId, " len = ", len, " => buffer does not exist"));
        return 0;
    }
    else {
        int expected = it->second.length;
        if (expected > len && len >= 0) expected = len;

        out.resize(expected);
        int ret = it->second.buf.sgetn(&out[0], expected);
        out.resize(ret);
        if (ret != expected)
            AddLogLine(GenString("<W> Partial buffer read, nodeId = ", nodeId, ", id = ", id,
                                 " expected = ", expected, ", ret = ", ret));

        AddLogLine(GenString("<D> BufferRead ", id, " by node ", nodeId, " len = ", len, " => ", ret));
        it->second.length -= ret;
        return ret;
    }
}

void RTArbiter::InitNode(InitConfiguration & outConf, const string & cookie, const int32_t nodeId, const string & controllerEndpoint) {
    if (_isRunning && !_root->InitNodeAttach(_testcase, cookie, nodeId, _sessionId, outConf)) {
        _testcase->AddLogLine(GenString("<W> Rejected node ", nodeId, " to init"));
        _isRunning = false;
    }
    if (!_isRunning) return;
    else {
        RTArbiterRoot::Guard guard(_root);

        _testcase->AddLogLine(GenString("<D> Node ", nodeId, " entered init queue"));

        _nodeId = nodeId;

        if (controllerEndpoint.size() > 0) {
            _pullSchedule = false;
            _testcase->AddLogLine(GenString("<D> Connecting node ", nodeId, " controller endpoint ", controllerEndpoint));

            _controllerTransport = GetTransport(controllerEndpoint);
            try {
                _controllerTransport->open();
                _controller = new ControllerClient(boost::shared_ptr<TProtocol>(new TBinaryProtocol(_controllerTransport)));
            }
            catch (const TException &) {
                _testcase->AddLogLine(GenString("<W> Cannot connect to controller endpoint ", controllerEndpoint));

                _isRunning = false;
                _controllerTransport->close();
                _controllerTransport = nullptr;
                _controller = nullptr;

                // TODO shall we report an error?

                return;
            }
        }
        else {
            _testcase->AddLogLine(GenString("<D> Node ", nodeId, " will pull schedule"));
            _pullSchedule = true;
            _controllerTransport = nullptr;
            _controller = nullptr;
        }

        _testcase->RegisterNode(nodeId, this, controllerEndpoint);
        _testcase->AddLogLine(GenString("<I> Node ", nodeId, " connected"));
    }
}

void RTArbiter::Sync(const Thrift::SyncData & syncData) {
    if (_isRunning && !_root->Attach(_testcase, _sessionId)) _isRunning = false;
    if (!_isRunning) return;
    else {
        RTArbiterRoot::Guard guard(_root);
        
        _testcase->Sync(_nodeId, syncData);

        if (syncData.inputRange < 0) {
            // Allow the node to connect again
            _root->DetachNode(_nodeId);
            _isRunning = false;
        }
    }
}

void RTArbiter::Report(const Thrift::ReportData & reportData) {
    if (_isRunning && !_root->Attach(_testcase, _sessionId)) _isRunning = false;
    if (!_isRunning) return;
    else
    {
        RTArbiterRoot::Guard guard(_root);

        _testcase->Report(_nodeId, reportData);
    }
}

void RTArbiter::ExecuteSchedule(const ArbiterChoice & choice, _unique_lock & lock) {
    if (_controller == nullptr) {
        if (_pullSchedule) {
            _testcase->AddLogLine(Formatted("<D> Sending choice to node %d.", _nodeId).CString());
            {
                lock_guard lock(_pullLock);
                _scheduleAvailable = true;
                _choice = choice;
                _pullCV.notify_one();
            }
        }
        else {
            _testcase->AddLogLine("<W> Controller not available. Report an error.");
            _testcase->LockedReportBackEnd(true);
        }
    }
    else {
        try {
            _controller->Schedule(choice);
        }
        catch (const TException &) {
            if (choice.source != ChoiceSource::TO_EXIT) {
                _testcase->AddLogLine("<W> Failed to call schedule. Report an error.");
                _testcase->LockedReportBackEnd(true);
            }
        }
    }
    ++_scheduleCount;
}

void RTArbiter::WaitSchedule(ArbiterChoice & outChoice) {
    if (_isRunning && !_root->Attach(_testcase, _sessionId)) _isRunning = false;
    if (!_isRunning) return;
    else
    {
        RTArbiterRoot::Guard guard(_root);
        if (!_pullSchedule) {
            _testcase->AddLogLine(GenString("<W> Should not WaitSchedule() on node ", _nodeId, ", Report an error"));
            _testcase->ReportBackEnd(true);
            return;
        }

        _testcase->AddLogLine(GenString("<D> Node ", _nodeId, " is pulling schedule"));
        _unique_lock lock(_pullLock, defer_lock);
        lock.lock();
        while (true) {
            if (_scheduleAvailable) break;
            else _pullCV.wait(lock);
        }
        _scheduleAvailable = false;
        outChoice = _choice;
        lock.unlock();

        _testcase->AddLogLine(GenString("<D> Node ", _nodeId, " got schedule"));
    }
}

void RTArbiter::GetVariables(vector<string> & outValues, const vector<string> & names) {
    // We don't actually check cookies here because we want nodes to be able to get variables for init before auth.
    // We can check cookie for some sensitive variables (e.x. if we store the real cookie somewhere)
    if (_isRunning && !_root->AttachWithoutSession(_testcase)) _isRunning = false;
    if (!_isRunning) return;
    else
    {
        RTArbiterRoot::Guard guard(_root);

        _testcase->GetVariables(_nodeId, outValues, names);
    }
}

void RTArbiter::SetVariables(const map<string, string> & entries) {
    if (_isRunning && !_root->Attach(_testcase, _sessionId)) _isRunning = false;
    if (!_isRunning) return;
    else
    {
        RTArbiterRoot::Guard guard(_root);
        
        _testcase->SetVariables(_nodeId, entries);
    }
}

void RTArbiter::BufferGetInfo(BufferInfo & ret, const int32_t id) {
    if (_isRunning && !_root->Attach(_testcase, _sessionId)) _isRunning = false;
    if (!_isRunning) return;
    else
    {
        RTArbiterRoot::Guard guard(_root);
        
        _testcase->BufferGetInfo(_nodeId, id, ret.isOpen, ret.length);
    }    
}

bool RTArbiter::BufferOpen(const int32_t id) {
    if (_isRunning && !_root->Attach(_testcase, _sessionId)) _isRunning = false;
    if (!_isRunning) return false;
    else
    {
        RTArbiterRoot::Guard guard(_root);
        
        return _testcase->BufferOpen(_nodeId, id);
    }
}

bool RTArbiter::BufferClose(const int32_t id) {
    if (_isRunning && !_root->Attach(_testcase, _sessionId)) _isRunning = false;
    if (!_isRunning) return false;
    else
    {
        RTArbiterRoot::Guard guard(_root);
        
        return _testcase->BufferClose(_nodeId, id);
    }
}

int32_t RTArbiter::BufferWrite(const int32_t id, const string & data) {
    if (_isRunning && !_root->Attach(_testcase, _sessionId)) _isRunning = false;
    if (!_isRunning) return 0;
    else
    {
        RTArbiterRoot::Guard guard(_root);
        
        return _testcase->BufferWrite(_nodeId, id, data.data(), data.length());
    }
}

void RTArbiter::BufferRead(string & ret, const int32_t id, const int32_t readLen) {
    if (_isRunning && !_root->Attach(_testcase, _sessionId)) _isRunning = false;
    if (!_isRunning) {
        ret = "";
        return;
    }
    else
    {
        RTArbiterRoot::Guard guard(_root);
        _testcase->BufferRead(_nodeId, id, ret, readLen);
    }
}

RTArbiter::~RTArbiter() {
    if (_isRunning && _nodeId >= 0 && _root->Attach(_testcase, _sessionId)) {
        RTArbiterRoot::Guard guard(_root);

        _testcase->UnregisterNode(_nodeId);
    }

    // Otherwise we don't worry about clean it from nodes.
}
