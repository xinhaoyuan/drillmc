#ifndef __DMC_ARBITER_REMOTE_TESTCASE_HPP__
#define __DMC_ARBITER_REMOTE_TESTCASE_HPP__

#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/server/TThreadedServer.h>
#include <boost/make_shared.hpp>
#include "thrift/gen-cpp/Arbiter.h"
#include "thrift/gen-cpp/Controller.h"

#include "Explorer/Core/Testcase.hpp"
#include "Explorer/Core/Operation.hpp"
#include "Utils/cpp/Logger.hpp"

#include <thread>
#include <mutex>
#include <condition_variable>
#include <deque>
#include <map>
#include <iostream>
#include <sstream>

#ifdef __DMC_OUROBOROS__
#include "Wrapper/cpp/DMCWrapperEx.hpp"
#endif

LWN_START(DMC);

using namespace Thrift;
using namespace apache::thrift::transport;

LWN_EXPORT;

#ifdef __DMC_OUROBOROS__

class WrappedConditionVariable;

class WrappedUniqueLock {
    bool _locked;
    mutex & _lock;

    friend class WrappedConditionVariable;

    void DoLock() {
        _lock.lock();
        _locked = true;
    }

    void DoUnlock() {
        _lock.unlock();
        _locked = false;
    }

public:
    WrappedUniqueLock(mutex & l)
    : _lock(l), _locked(false) {
        lock();
    }

    WrappedUniqueLock(mutex & l, defer_lock_t _) 
    : _lock(l), _locked(false) {
    }

    static int EnabledPredicate(void * param) {
        WrappedUniqueLock * self = (WrappedUniqueLock *)param;
        if (self->_lock.try_lock()) {
            self->_lock.unlock();
            return true;
        }
        else {
            return false;
        }
    }

    void lock() {
        Wrapper::INodeWrapper * wrapper = GetDMCNodeInstance();
        wrapper->ScheduleSync(EnabledPredicate, nullptr, this, DMC_CODE_HERE);
        DoLock();
    }

    void unlock() {
        Wrapper::INodeWrapper * wrapper = GetDMCNodeInstance();
        wrapper->ScheduleSync(nullptr, nullptr, nullptr, DMC_CODE_HERE);
        DoUnlock();
    }

    ~WrappedUniqueLock() {
        if (_locked)
            _lock.unlock();
    }
};

typedef WrappedUniqueLock _unique_lock;
typedef WrappedUniqueLock lock_guard;

class WrappedConditionVariable {
private:
    
    struct ListNode {
        int role;
        ListNode * prev;
        ListNode * next;
    };

    ListNode guard;
    int waitCount;
    int notifyCount;

    struct WaitInfo {
        ListNode * node;
        mutex * lock;
    };

    mutex _lock;
    std::condition_variable _cv;

    static int EnabledPredicate(void * param) {
        WaitInfo * info = (WaitInfo *)param;
        ListNode * node = info->node;
        while (true) {
            if (node->role > 0)
                break;
            else if (node->role == 0)
                return false;
            node = node->next;
        }
        if (info->lock->try_lock()) {
            info->lock->unlock();
            return true;
        }
        else {
            return false;
        }
    }

public:

    WrappedConditionVariable() {
        guard.role = 0;
        guard.next = guard.prev = &guard;
        waitCount = 0;
        notifyCount = 0;
    }

    void wait(WrappedUniqueLock & lock) {
        std::unique_lock<mutex> ul(_lock, defer_lock);
        // cerr << "cv " << this << " wait start" << endl;
        ul.lock();
        lock.DoUnlock();
        
        ListNode node;
        node.role = -1;
        node.next = &guard;
        node.prev = guard.prev;
        node.next->prev = &node;
        node.prev->next = &node;
        WaitInfo info;
        info.node = &node;
        info.lock = &lock._lock;

        ++waitCount;

        ul.unlock();

        Wrapper::INodeWrapper * wrapper = GetDMCNodeInstance();
        wrapper->ScheduleSync(EnabledPredicate, nullptr, &info, DMC_CODE_HERE);

        ul.lock();

        while (true) {
            ListNode *cur = node.next;
            while (cur->role < 0) cur = cur->next;
            if (cur->role > 0) {
                if (--cur->role == 0) {
                    cur->prev->next = cur->next;
                    cur->next->prev = cur->prev;
                    delete cur;
                }
                break;
            }
            // cerr << "cv " << this << " wait check failed" << endl;
            _cv.wait(ul);
        }

        node.next->prev = node.prev;
        node.prev->next = node.next;

        --waitCount;
        --notifyCount;
        
        ul.unlock();
        lock.DoLock();
        
        // cerr << "cv " << this << " wait finished" << endl;
    }

    void notify_one() {
        Wrapper::INodeWrapper * wrapper = GetDMCNodeInstance();
        wrapper->ScheduleSync(nullptr, nullptr, nullptr, DMC_CODE_HERE);

        std::lock_guard<mutex> lock(_lock);
        // cerr << "cv " << this << " notify one. nc = " << notifyCount << ", wc = " << waitCount << endl;
        if (notifyCount >= waitCount) return;
        ++notifyCount;

        if (guard.prev->role > 0) {
            ++guard.prev->role;
        }
        else {
            ListNode * node = new ListNode();
            node->role = 1;
            node->next = &guard;
            node->prev = guard.prev;
            node->next->prev = node;
            node->prev->next = node;
        }

        _cv.notify_one();
    }

    void notify_all() {
        Wrapper::INodeWrapper * wrapper = GetDMCNodeInstance();
        wrapper->ScheduleSync(nullptr, nullptr, nullptr, DMC_CODE_HERE);

        std::lock_guard<mutex> lock(_lock);
        // cerr << "cv " << this << " notify all. nc = " << notifyCount << ", wc = " << waitCount << endl;        
        if (notifyCount >= waitCount) return;
        int role = waitCount - notifyCount;
        notifyCount = waitCount;

        if (guard.prev->role > 0) {
            guard.prev->role += role;
        }
        else {
            ListNode * node = new ListNode();
            node->role = role;
            node->next = &guard;
            node->prev = guard.prev;
            node->next->prev = node;
            node->prev->next = node;
        }

        _cv.notify_all();
    }
};

typedef WrappedConditionVariable condition_variable;

#else
typedef std::unique_lock<mutex> _unique_lock;
typedef std::lock_guard<mutex> lock_guard;
#endif

class RTArbiterRoot;
class RemoteTestcase;

class RTArbiter : public ArbiterIf {

    RTArbiterRoot * _root;
    RemoteTestcase * _testcase;
    int    _sessionId;

    bool   _init;
    int    _nodeId;
    bool   _isRunning;
    bool   _pullSchedule;

    int    _scheduleCount;

    mutex  _pullLock;
    condition_variable _pullCV;
    bool   _scheduleAvailable;
    ArbiterChoice _choice;

    boost::shared_ptr<TTransport> _controllerTransport;
    ControllerIf * _controller;

    friend class RTArbiterRoot;
    friend class RemoteTestcase;

public:

    inline RTArbiter(RTArbiterRoot * root, RemoteTestcase * testcase)
        : _root(root)
        , _testcase(testcase)
        , _init(false)
        , _nodeId(-1)
        , _isRunning(testcase != nullptr)
        , _controller(nullptr)
        , _pullSchedule(false)
        , _scheduleAvailable(false)
        , _scheduleCount(0)
    { };

    ~RTArbiter() override;

    void InitNode(Thrift::InitConfiguration & outConf, const string & cookie, const int32_t nodeId, const string & controllerEndpoint) override;

    void Sync(const Thrift::SyncData & syncData) override;
    void Report(const Thrift::ReportData & reportData) override;

    void SetVariables(const map<string, string> & kv) override;
    void GetVariables(vector<string> & outValues, const vector<string> & names) override;

    void WaitSchedule(ArbiterChoice & outChoice) override;

    void BufferGetInfo(BufferInfo & ret, const int32_t id) override;
    bool BufferOpen(const int32_t id) override;
    bool BufferClose(const int32_t id) override;
    int32_t BufferWrite(const int32_t id, const string & data) override;
    void BufferRead(string & ret, const int32_t id, const int32_t readLen) override;

    // Below functions are run with testcase lock held.
    void ExecuteSchedule(const ArbiterChoice & choice, _unique_lock & lock);
};

enum VerboseLevel {
    VERBOSE_ERROR = 0,
    VERBOSE_WARNING = 1,
    VERBOSE_TLOG = 2,
    VERBOSE_INFO = 3,
    VERBOSE_DEBUG = 4,

    VERBOSE_DEFAULT = 2,
};

class RemoteTestcase : public ITarget {
private:

    int _opIdCounter;
    int _nodeIdCounter;
    int _idCounter;
    int _messageTokenCounter;
    bool _isError;
    bool _isEnd;
    StateType _stateType;

    int  _inputNode;
    int  _inputRange;

    XYHelper::Logger * _logger;

    // Data fields used for communication between testcase and session {{

    mutex _lock;
    bool  _schedulePresented;
    bool  _reportPresented;
    bool  _endPresented;
    bool  _errorPresented;
    condition_variable _cv;

    int  _scheduleNodeId;
    ChoiceSource::type _scheduleSource;
    int  _globalChoice;

    map<int, RTArbiter *> _nodes;

    map<int, OperationDescription *> *  _newOps;
    set<int> *                          _toggledOps;
    set<int> *                          _cancelledOps;
    DependencyRecord **                 _dep;
    vector<MessageRecord *> *           _msgs;
    vector<AnnotationRecord *> *        _ants;

    // Run in testcase thread
    void ReportBack();
    void ReportBackInLock();
    void ReportBackEnd(bool isError);
    void LockedReportBackEnd(bool isError);

    // Run in arbiter thread
    bool WaitReport();
    void SetSchedule(int nodeId, ChoiceSource::type source, int globalChoice);

    void TryTriggerScheduleWithLock(_unique_lock & lock);
    // }}

    // Bookkeeping
    deque<tuple<int, int, string>> _messageQueue;
    map<int, int> _inQueueCount;

    bool _autoNotify;
    set<int> _notifySet;

    set<int> _activeNodes;
    map<int, tuple<int, int>> _globalToLocal;
    map<tuple<int, int>, int> _localToGlobal;
    map<int, string>          _desc;

    vector<string> _testcaseArgs;
    map<string, string> _testcaseEnv;
    RTArbiterRoot * _arbiterRoot;

    map<string, string> _initMetadata;
    map<string, string> _metadata;

    struct Buffer {
        bool isOpen;
        int length;
        stringbuf buf;

        Buffer()
            : isOpen(false)
            , length(0)
            { } 
    };
        
    map<int, Buffer> _buffers;

    vector<int> _setupPrefix;

    struct {
        string hostname;
        int port;
        bool forceExit;
        bool strongAuth;
        int reportTimeoutMs;
        VerboseLevel verboseLevel;
        int nodeVerboseLevel;
        string localFilter;
        int64_t localRandomSeed;
        bool verboseStackInfo;
    } conf;

    friend class RTArbiterRoot;
    friend void RTArbiter::ExecuteSchedule(const ArbiterChoice & choice, _unique_lock & lock);
    friend void RTArbiter::WaitSchedule(ArbiterChoice & _return);

    int  PostProcessNewOperation(const OperationDescriptionWithId & desc);
    void HandleMessage();
    void EnqueueMessage(int token, int target, const string & message);
    void InternalHandle(int choice, bool external);

    void LockedUnregisterNode(int nodeId);

public:

    void AddLogLine(const char * line);
    inline void AddLogLine(const string & line) { AddLogLine(line.c_str()); }
 
    RemoteTestcase(RTArbiterRoot * arbiterRoot);

    // Configuration {{
    inline void SetTestcaseArgs(const vector<string> & args) { _testcaseArgs = args; }
    inline void SetTestcaseEnv(const map<string, string> & env) { _testcaseEnv = env; }
    inline void SetLogger(XYHelper::Logger * logger) { _logger = logger; if (_logger != nullptr) { _logger->SetVerboseLevel(conf.verboseLevel); } }
    inline void SetHostname(const string & hostname) { conf.hostname = hostname; }
    inline void SetServerPort(int port) { conf.port = port; }
    inline int  GetServerPort() { return conf.port; }
    inline void SetForceExit(bool forceExit) { conf.forceExit = forceExit; }
    inline void SetReportTimeout(int reportTimeoutMs) { conf.reportTimeoutMs = reportTimeoutMs; }
    inline void SetVerboseLevel(VerboseLevel verboseLevel) { conf.verboseLevel = verboseLevel; if (_logger != nullptr) { _logger->SetVerboseLevel(verboseLevel); } }
    inline void SetNodeVerboseLevel(int verboseLevel) { conf.nodeVerboseLevel = verboseLevel; }
    inline void SetSetupPrefix(const vector<int>& prefix) {
        _setupPrefix.clear();
        _setupPrefix.insert(begin(_setupPrefix), begin(prefix), end(prefix));
    }
    inline void SetStrongAuth(bool strongAuth) { conf.strongAuth = strongAuth; }
    inline void SetInitVariables(const map<string, string> initMetadata) { _initMetadata = initMetadata; }
    inline void SetLocalFilter(const string & localFilter) { conf.localFilter = localFilter; }
    inline void SetLocalRandomSeed(int64_t localRandomSeed) { conf.localRandomSeed = localRandomSeed; }
    inline void SetAutoNotify(bool autoNotify) { _autoNotify = autoNotify; }
    inline void SetVerboseStackInfo(bool verboseStackInfo) { conf.verboseStackInfo = verboseStackInfo; }
    // }}
    
    
    // ITestcase interface, run in arbiter thread
    void SetTracking(
        map<int, OperationDescription *> * newOps, set<int> * toggledOps, set<int> * cancelledOps,
        DependencyRecord ** dep, vector<MessageRecord *> * messages, vector<AnnotationRecord *> * annotations
        ) override;
    int GetInputNode() override;
    int GetInputRange() override;
    void SetOptions(const map<string, string> & options) override;
    StateType Setup() override;
    StateType Proceed(int choice) override;
    void CleanUp() override;

    // Arbiter interface, use by RTArbiter
    void RegisterNode(int nodeId, RTArbiter * node, const string & controllerEndpoint);
    void UnregisterNode(int nodeId);

    void Sync(int nodeId, const Thrift::SyncData & syncData);
    void Report(int nodeId, const Thrift::ReportData & reportData);

    void GetVariables(int nodeId, vector<string> & outValues, const vector<string> & names);
    void SetVariables(int nodeId, const map<string, string> & entries);

    void BufferGetInfo(int nodeId, int id, bool & isOpen, int & length);
    bool BufferOpen(int nodeId, int id);
    bool BufferClose(int nodeId, int id);
    int  BufferWrite(int nodeId, int id, const char * data, int len);
    int  BufferRead(int nodeId, int id, string & out, int len);
};

class RTArbiterRoot {
private:
    
    class RTArbiterFactory : public ArbiterIfFactory {
        RTArbiterRoot * _root;
    public:
        inline RTArbiterFactory(RTArbiterRoot * root)
            : _root(root) { }
        
        // These run in RPC context 
        ArbiterIf * getHandler(const ::apache::thrift::TConnectionInfo & connInfo) override;
        void releaseHandler(ArbiterIf * handler) override;
    };
    
    RemoteTestcase * _testcase;
    string _initCookie;
    int    _port;
    int    _currentSessionId;
    mutex  _lock;
    
    thread * _serverMainThread;
    bool     _serverUp;
    apache::thrift::server::TServer * _arbiterServer;
    boost::shared_ptr<RTArbiterFactory> _factory;
    condition_variable _serverCV;

    int                _activeSessions;
    condition_variable _cleanCV;

    int _nSessionsHighWaterMark;
    int _nSessionsLowWaterMark;
    int _nSessions;
    condition_variable _nSessionsCV;

    set<int> _connectedNodes;

    static void ServerThread(RTArbiterRoot * self);
    
public:
    
    RTArbiterRoot();
    ~RTArbiterRoot();

    // These run in RemoteTestcase's context
    void StartServerThread(int port);
    bool Start(RemoteTestcase * testcase);
    bool End(); // return true if all connection is closed within 10s
    
    // Runs in sessions
    void AddNewNode(int nodeId);
    bool Attach(RemoteTestcase * testcase, int sessionId);
    bool AttachWithoutSession(RemoteTestcase * testcase);
    bool InitNodeAttach(RemoteTestcase * testcase, const string & cookie, int nodeId, int & outSessionId, InitConfiguration & outConf);
    void DetachNode(int nodeId);
    void Detach();

    class Guard {
        RTArbiterRoot * _root;
    public:
        inline Guard(RTArbiterRoot * root)
            : _root(root) { }
        inline ~Guard() {
            _root->Detach();
        }
    };
};

LWN_END;

#endif
