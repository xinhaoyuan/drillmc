#include "Arbiter/RemoteTestcase.hpp"
#include "Wrapper/cpp/DMCWrapper.hpp"
#include "Explorer/Core/LocalPathCollector.hpp"
#include "Explorer/Core/ExhaustiveExplorer.hpp"
#include "Explorer/Core/SleepSetExplorer.hpp"
#include "Explorer/Random/RandomExplorer.hpp"
#include "Explorer/Transformer/Identical.hpp"
#include "Explorer/Transformer/Modular.hpp"
#include "Utils/cpp/ArgStreamParser.hpp"
#include "Utils/cpp/Option.hpp"
#include "Utils/simpleon/cpp/simpleon.hpp"

#include "git_revision.h"

#include <cstring>
#include <string>
#include <regex>
#include <iostream>
#include <fstream>
#include <sstream>
#include <list>
#include <map>
#include <boost/filesystem.hpp>

#ifndef _MSVC
#include <signal.h>
#endif

using namespace std;
using namespace DMC;
using namespace XYHelper;
using namespace simpleon;

namespace FS = boost::filesystem;

Logger * logger = new Logger(&cerr, false);
string reportPath = "-";
string explName = "random";

map<string, string> explOptions;
map<string, string> initMetadata;
map<string, string> initEnv;

static const Option kOptionAutoNotify("auto-notify", "Auto notify all nodes to allow implicit node communication");
static const Option kOptionCheckMismatch("check-mismatch", "Check state information mismatch along exploration");
static const Option kOptionUseModular("use-modular", "Use modular explorer for more features");
static const Option kOptionNodeVerboseLevel("node-verbose-level", "The verbose level for each node");
static const Option kOptionVerboseStackInfo("verbose-stack-info", "Print the stack info at every blocking API call");  
static const Option kOptionNodeFilter("local-filter", "The local filter in each node");
static const Option kOptionRandomSeed("seed", "Use the particular random seed for random exploration");
static const Option kOptionRandomRepeat("random-repeat", "The number of repeats for random exploration");
static const Option kOptionRandomMetaSeed("meta-seed", "The meta random seed for random exploration");
static const Option kOptionRandomLogPrefix("log-prefix", "Log prefix for random exploration");
static const Option kOptionRandomSingleLog("use-single-log", "Do not use multiple log file for each exploration");
static const Option kOptionPrefixFile("prefix-file", "Use the prefix in the file at every exploration");

list<Option> explOptionInfo =
{
    kOptionAutoNotify, kOptionCheckMismatch, kOptionUseModular
  , kOptionNodeVerboseLevel, kOptionVerboseStackInfo, kOptionNodeFilter
  , kOptionRandomSeed, kOptionRandomRepeat, kOptionRandomMetaSeed, kOptionRandomLogPrefix, kOptionRandomSingleLog
  , kOptionPrefixFile
};

static void Help() {
    cout
        << "Git revision: " GIT_REVISION << endl << endl
        << "Help:" << endl << endl
        << "    This is the centralized layer that turns a distributed" << endl
        << "    software system into an abstract model." << endl << endl
        << "Arguments:" << endl << endl
        << "  -h|--help" << endl << endl
        << "    Print this message." << endl << endl
        << "  -i|--include [filename]" << endl << endl
        << "    Include [filename] into current arguments list inplace. Each line" << endl
        << "    in the file will be treated as a single argument item. Use" << endl
        << "    \\n and \\r to insert newlines, and use \\ at the end of line to" << endl
        << "    combine lines. Lines starting with # will be ignored." << endl << endl
        << "    The file will be searched from the search path." << endl
        << "    Upon the file is loaded, the file directory will be added into the search paths." << endl << endl
        << "  --push-path [path]" << endl << endl
        << "    Push [path] into search paths" << endl << endl
        << "  --pop-path" << endl << endl
        << "    Pop the head from search paths" << endl << endl
        << "  --dump-args" << endl << endl
        << "    Print out the resulting args list for debugging. Do no actions." << endl << endl
        << "  -o|--explorerOptions { key:value ... }" << endl << endl
        << "    Specify option for the explorer - " << endl << endl
        << "      explorer - Use the specified explorer. Supported explorers are" << endl
        << "        \"exhaustive\", \"sleepset\", \"random\" (default)." << endl
        << "      host-name" << endl
        << "      port" << endl
        << "      force-exit" << endl
        << "      timeout-ms" << endl
        << "      strong-auth" << endl
        << "      log-output - the filename for log, \"-\" for stderr" << endl
        << "      log-verbose(\"debug\"|\"info\") - the log verbose level" << endl
        << "      report-output - the filename for report, \"-\" for stdout" << endl;
    for (auto && option : explOptionInfo) {
        cout << "      " << option.GetName() << " - " << option.GetDescription() << endl; 
    }
    cout
        << endl
        << "  -v|--variables { name:value ... }" << endl << endl
        << "    Specify initial variables." << endl << endl
        << "  -e|--environment { name:value ... }" << endl << endl
        << "    Specify env variable for initial command" << endl << endl
        << "  --args [ cmd ... ]" << endl << endl
        << "    Specify the starting command line." << endl << endl
        ;
}

static bool FindFileInPaths(const list<string> & paths, const string & filePath, string & realPath) {
    for (auto && base : paths) {
        if (base.size() == 0) continue;
        FS::path currentPath = FS::path(base) / filePath;
        if (!FS::exists(currentPath)) continue;
        if (is_regular_file(currentPath)) {
            realPath = canonical(currentPath).string();
            return true;
        }
    }

    if (!FS::exists(filePath)) return false;
    realPath = FS::canonical(filePath).string();
    return FS::is_regular_file(filePath);
}

static void ProcessArgs(list<string> & args,
                        list<string> & includeDirs,
                        map<string, string> & explOptions,
                        RemoteTestcase * rt) {
    
    auto it = begin(args);
    IParser * parser = CreateSimpleONParser(true, false);
    bool hasStartArgs = false;
    string head;
    vector<IData *> stack;
    int items_to_push = 0;
    
    bool dump_args = false;
    
    while (it != end(args)) {
        parser->ParseLine(*it);
        ++it;

        IData * item;
        while ((item = parser->Extract()) != nullptr) {
            if (items_to_push > 0) {
                stack.push_back(item);
                --items_to_push;
            }
            else if (item->GetType() == IData::T_STRING || item->GetType() == IData::T_UQ_STRING) {
                head = item->GetString();
                delete item;

                if (head == "-h" || head == "--help") {
                    head = "-h";
                    Help();
                    _exit(0);
                }
                else if (head == "-i" || head == "--include") {
                    items_to_push = 1;
                }
                else if (head == "--push-path") {
                    items_to_push = 1;
                }
                else if (head == "-o" || head == "--explorerOptions") {
                    head == "-o";
                    items_to_push = 1;
                }
                else if (head == "-v" || head == "--variables") {
                    head = "-v";
                    items_to_push = 1;
                }
                else if (head == "-e" || head == "--environment") {
                    head = "-e";
                    items_to_push = 1;
                }
                else if (head == "--args") {
                    items_to_push = 1;
                }
            }
            else if (item->GetType() == IData::T_DICT) {
                head = "-o";
                stack.push_back(item);
            }
            else if (item->GetType() == IData::T_LIST) {
                head = "--args";
                stack.push_back(item);
            }
            else {
                cerr << "option can only start with string" << endl;
                Help();
                _exit(-1);
            }

            if (items_to_push > 0) continue;

            if (head == "-i") {
                string argsFilePath = stack[0]->GetString();
                string argsFileRealPath;
                
                if (FindFileInPaths(includeDirs, argsFilePath, argsFileRealPath)) {                    
                    FS::path filePath = FS::path(argsFileRealPath);
                    FS::path basePath = filePath.parent_path();

                    auto savedPos = args.insert(it, "--push-path");
                    args.insert(it, basePath.string());
                                
                    ifstream inf(filePath.string());
                    ParseArgStream(inf, [&](const string & arg) {
                            args.insert(it, arg);
                        });
                    args.insert(it, "--pop-path");
                    it = savedPos;
                }
                else {
                    cerr << "Warning: failed to include \"" << argsFilePath << "\"" << endl;
                }
            }
            else if (head == "--push-path") {
                if (FS::exists(stack[0]->GetString())) {
                    includeDirs.push_front(FS::canonical(stack[0]->GetString()).string());
                }
                else {
                    includeDirs.push_front("");
                }
            }
            if (head == "--pop-path") {
                if (includeDirs.size() > 0) {
                    includeDirs.pop_front();
                }
            }
            else if (head == "--dump-args") {
                dump_args = true;
            }
            else if (head == "-o") {
                if (stack[0]->GetType() != IData::T_DICT) {
                    cerr << "explorer options: expect a dict but got ";
                    Dump(cerr, stack[0]);
                    cerr << endl;
                    _exit(-1);
                }

                for (auto && kv : stack[0]->GetDict()) {
                    auto && key = get<0>(kv);
                    auto && value = get<1>(kv);
                    if (key == "explorer") {
                        explName = value->GetString();
                    }
                    else if (key == "log-output") {
                        if (logger != nullptr)
                            delete logger;

                        if (value->GetString() == "-") {
                            logger = new Logger(&cerr, false);
                        }
                        else {
                            logger = new Logger(new ofstream(value->GetString()), true);
                        }
                    }
                    else if (key == "log-verbose") {
                        if (value->GetString() == "info") {
                            rt->SetVerboseLevel(VerboseLevel::VERBOSE_INFO);
                        }
                        else if (value->GetString() == "debug") {
                            rt->SetVerboseLevel(VerboseLevel::VERBOSE_DEBUG);
                        }
                        else {
                            cerr << "log-verbose: unknown verbose level" << endl;
                        }
                    }
                    else if (key == "report-output") {
                        reportPath = value->GetString();
                    }
                    else if (key == "force-exit") {
                        rt->SetForceExit(stoi(value->GetString()));
                    }
                    else if (key == "timeout-ms") {
                        rt->SetReportTimeout(stoi(value->GetString()));
                    }
                    else if (key == "strong-auth") {
                        rt->SetStrongAuth(stoi(value->GetString()));
                    }
                    else if (key == "host-name") {
                        rt->SetHostname(value->GetString());
                    }
                    else if (key == "port") {
                        rt->SetServerPort(stoi(value->GetString()));
                    }
                    else {
                        bool documented = false;
                        for (auto && option : explOptionInfo) {
                            if (key == option.GetName()) {
                                documented = true;
                                break;
                            }
                        }

                        if (!documented) {
                            cerr << "Warning: undocumented option " << key << ", the result may be affected" << endl;
                        }
                
                        explOptions[key] = value->GetString();
                    }
                }
            }
            else if (head == "-e") {
                if (stack[0]->GetType() != IData::T_DICT) {
                    cerr << "env vars: expect a dict but got";
                    Dump(cerr, stack[0]);
                    cerr << endl;
                    _exit(-1);
                }

                for (auto && kv : stack[0]->GetDict()) {
                    initEnv[get<0>(kv)] = get<1>(kv)->GetString();
                }
            }
            else if (head == "-v") {
                if (stack[0]->GetType() != IData::T_DICT) {
                    cerr << "vars: expect a dict but got ";
                    Dump(cerr, stack[0]);
                    cerr << endl;
                    _exit(-1);
                }

                for (auto && kv : stack[0]->GetDict()) {
                    initMetadata[get<0>(kv)] = get<1>(kv)->GetString();
                }
            }
            else if (head == "--args") {
                if (stack[0]->GetType() != IData::T_LIST) {
                    cerr << "args: expect a list but got ";
                    Dump(cerr, stack[0]);
                    cerr << endl;
                    _exit(-1);
                }
                
                vector<string> testcaseArgs;
                for (auto && i : stack[0]->GetList()) {
                    testcaseArgs.push_back(i->GetString());
                }

                if (testcaseArgs.size() > 0) {
                    hasStartArgs = true;
                    rt->SetTestcaseArgs(testcaseArgs);
                }
            }
            
            for (auto arg : stack)
                delete arg;
            stack.clear();
        }
    }

    if (items_to_push > 0) goto err_out;

    if (dump_args) {
        for (auto && arg : args) {
            cerr << arg << endl;
        }
    }

    if (!hasStartArgs) {
        cout << "Starting args are missing! Show the help message." << endl << endl;
        Help();
        _exit(-1);
    }

    return;

  err_out:

    if (items_to_push > 0) {
        cerr << "more parameters needed: [" << head;
        for (auto && s : stack) {
            cerr << ',';
            Dump(cerr, s);
        }
        cerr << endl;
        _exit(-1);
    }
}


static void RegisterOptions() {
    ModularTransformer::Register(explOptionInfo);
}

int main(int argc, char ** argv) {
#ifndef _MSVC
    struct sigaction sigchld_action;
    sigchld_action.sa_handler = SIG_DFL;
    sigchld_action.sa_flags = SA_NOCLDWAIT;
    sigaction(SIGCHLD, &sigchld_action, nullptr);
#endif
    
#ifdef __DMC_OUROBOROS__
    Wrapper::INodeWrapper * wrapper = GetDMCNodeInstance();
    wrapper->InitNode(nullptr, nullptr, nullptr);
    wrapper->NodeStart(0);
#endif

    RegisterOptions();

    RTArbiterRoot * arbiterRoot = new RTArbiterRoot();
    RemoteTestcase * rt = new RemoteTestcase(arbiterRoot);
    ITarget * transformer = nullptr;
    function<bool(const string &)> * predicate = nullptr;
    ITarget * target = rt;
    LocalPathCollector * pathCollector;
    vector<int> setupPrefix;
    
    list<string> args;
    list<string> paths;
    for (int i = 1; i < argc; ++i) args.push_back(argv[i]);

    ProcessArgs(args, paths, explOptions, rt);

    if (reportPath.length() == 0) {
        pathCollector = new LocalPathCollector(nullptr);
    }
    else if (reportPath == "-") {
        pathCollector = new LocalPathCollector(&cout);
    }
    else {
        pathCollector = new LocalPathCollector(reportPath.c_str());
    }

    decltype(explOptions)::iterator optionIt;

    optionIt = explOptions.find(kOptionUseModular.GetName());
    if (optionIt != end(explOptions)) {
        transformer = new ModularTransformer(rt, logger);
        transformer->SetOptions(explOptions);
        
        target = transformer;
    }

    optionIt = explOptions.find(kOptionNodeVerboseLevel.GetName()); 
    if (optionIt != end(explOptions)) {
        string verboseLevelStr = optionIt->second;
        int verboseLevel;
        if (verboseLevelStr == "debug") {
            verboseLevel = Wrapper::VERBOSE_DEBUG;
        }
        else if (verboseLevelStr == "info") {
            verboseLevel = Wrapper::VERBOSE_INFO;
        }
        else {
            istringstream is(verboseLevelStr);
            if (!(is >> verboseLevel)) {
                verboseLevel = Wrapper::VERBOSE_DEFAULT;
            }
        }

        rt->SetNodeVerboseLevel(verboseLevel);
    }

    optionIt = explOptions.find(kOptionNodeFilter.GetName());
    if (optionIt != end(explOptions)) {
        rt->SetLocalFilter(optionIt->second);
    }

    optionIt = explOptions.find(kOptionAutoNotify.GetName());
    if (optionIt != end(explOptions)) {
        rt->SetAutoNotify(atoi(optionIt->second.c_str()));
    }

    optionIt = explOptions.find(kOptionVerboseStackInfo.GetName());
    if (optionIt != end(explOptions)) {
        rt->SetVerboseStackInfo(true);
    }

    optionIt = explOptions.find(kOptionPrefixFile.GetName());
    if (optionIt != end(explOptions)) {
        ifstream inf(optionIt->second);
        int choice;
        while (inf >> choice) setupPrefix.push_back(choice);
        if (explName != "random") {
            // Warning message?
            explName = "random";
        }
    }

    rt->SetLogger(logger);
    rt->SetInitVariables(initMetadata);
    rt->SetTestcaseEnv(initEnv);
    arbiterRoot->StartServerThread(rt->GetServerPort());

    if (explName == "exhaustive") {
        auto * ex = new ExhaustiveExplorer(target, pathCollector);
        ex->ExploreAll(logger);
        delete ex;
    }
    else if (explName == "sleepset") {
        auto * ex = new SleepSetExplorer(target, pathCollector);
        ex->ExploreAll(logger);
        delete ex;
    }
    else if (explName == "random") {
        auto * ex = new RandomExplorer(target, pathCollector, setupPrefix);

        optionIt = explOptions.find(kOptionCheckMismatch.GetName());
        if (optionIt != end(explOptions)) {
            ex->SetCheckMismatch(true);
        }


        auto repeatIt = explOptions.find(kOptionRandomRepeat.GetName());
        int repeat = (repeatIt == end(explOptions)) ? -1 : stoi(repeatIt->second);
        auto seedIt = explOptions.find(kOptionRandomSeed.GetName());
        
        if (seedIt != end(explOptions)) {
            unsigned long seed = stoul(seedIt->second);

            if (repeat == -1) repeat = 1;
            
            logger->LogFormat(-1, "Random explore with seed %lu for %d times", seed, repeat);
            while (repeat == -1 || repeat > 0) {
                ex->ExploreWithSeed(seed, logger);
                if (repeat > 0) 
                    --repeat;
            }
        }
        else {
            string logPrefix = "";

            optionIt = explOptions.find(kOptionRandomLogPrefix.GetName());
            if (optionIt != end(explOptions)) {
                logPrefix = optionIt->second;
            }
            
            unsigned long sSeed = (unsigned)time(nullptr);

            optionIt = explOptions.find(kOptionRandomMetaSeed.GetName());
            if (optionIt != end(explOptions)) {
                sSeed = stoul(optionIt->second);
            }

            Logger * itLogger = nullptr;

            optionIt = explOptions.find(kOptionRandomSingleLog.GetName());
            if (optionIt == end(explOptions)) {
                rt->SetLogger(itLogger = new Logger(nullptr, false));
            }

            ex->StartExplore(repeat, logPrefix, sSeed, logger, itLogger);

            delete itLogger;
        }
        delete ex;
    }
    
    if (logger != nullptr) {
        logger->Log(-1, "Exploration finished.");
        delete logger;
    }

    arbiterRoot->End();

    delete rt;
    delete transformer;
    delete predicate;
    delete arbiterRoot;
    delete pathCollector;

#ifdef __DMC_OUROBOROS__
    cerr << "Gonna exit!" << endl;

    wrapper->SendLog("RTE ends.");
    wrapper->ContextDetach();
    wrapper->Join();
#endif

    return 0;
}
