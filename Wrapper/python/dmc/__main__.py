from dmc import DriverNode

def RunTest():
    node = DriverNode()
    node.Connect(None)
    node.NodeStart(0)
    node.Yield()
    node.Yield()
    node.SetVariable("a", "b")
    node.Barrier()
    print(node.GetVariable("a"))
    print(node.GetVariable("b"))
    node.Yield()
    node.End()
    
if __name__ == "__main__":
    RunTest()
