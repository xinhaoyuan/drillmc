import thriftpy
from thriftpy.rpc import make_client
import os
import re

local_path = os.path.dirname(os.path.abspath(__file__))
dmc_thrift_path = os.getenv("DMC_THRIFT_PATH", os.path.join(local_path, "thrift"))

dmc_arbiter_thrift = thriftpy.load(
    os.path.join(dmc_thrift_path, "arbiter.thrift"),
    module_name = "dmc_arbiter_thrift", include_dirs = [dmc_thrift_path])

class DriverNode:

    def __init__(self):
        self.client = None
        pass

    def Connect(self, endpoint):
        if endpoint is None:
            endpoint = os.getenv("DMC_ARBITER_ENDPOINT")
        if endpoint is None:
            raise Exception("missing endpoint.")
        
        m = re.match('([^:]+):([0-9]+)', endpoint)
        if m:
            ip = m.group(1)
            port = int(m.group(2))
            self.client = make_client(dmc_arbiter_thrift.Arbiter, ip, port)

    def EnsureConnected(self):
        if self.client is None:
            raise Exception("not connected")
        

    def Log(self, logStr):
        self.EnsureConnected()
        self.client.Report(dmc_arbiter_thrift.ReportData(False, False, logStr, None))
        
    def NodeStart(self, node_id):
        self.EnsureConnected()
        
        conf = self.client.InitNode("", node_id, "")
        if not conf.success:
            raise Exception("cannot attach to explorer")
        self.node_id = node_id
        self.op_id = 0
        self.conf = conf
        self.WaitSchedule()

    def NewNode(self):
        self.EnsureConnected()
        
        ret = self.node_counter
        self.node_counter = self.node_counter + 1

        return ret
        
    def MessageSend(self, node_id, text):
        self.EnsureConnected()
        
        token = self.token_counter
        self.token_counter = self.token_counter + 1
        self.messages.append(
            dmc_arbiter_thrift.MessageWithId(
                token,
                dmc_arbiter_thrift.common.Message(node_id, text)))

        return token

    def Yield(self, op_name = "yield", async = False):
        self.EnsureConnected()
        
        op_id = self.op_id
        self.op_id = self.op_id + 1

        ops = [
            dmc_arbiter_thrift.OperationDescriptionWithId(
                op_id, dmc_arbiter_thrift.common.OperationDescription(
                    self.node_id, False, op_name))
            ]

        syncData = dmc_arbiter_thrift.SyncData(
            0, ops, [], [], None, [], self.messages, list(self.notify),
            self.token_counter, self.node_counter, self.id_counter)
        
        self.client.Sync(syncData)
        if not async:
            self.WaitSchedule()

    def Notify(self, node_id):
        self.notify.add(node_id)
        
    def Barrier(self, priority = 100, async = False):
        self.Yield("<Barrier:{0}>".format(int(priority)), async = async)

    def WaitSchedule(self):
        self.EnsureConnected()

        while True:
            info = self.client.WaitSchedule()

            if info.source != dmc_arbiter_thrift.ChoiceSource.NOTIFY:
                break
            
            self.client.Sync(dmc_arbiter_thrift.SyncData(
                0, [], [], [], None, [], [], [],
                -1, -1, -1))
            
        self.node_counter = info.currentNodeCount
        self.token_counter = info.currentMessageTokenCount
        self.id_counter = info.currentIdCount;
        self.messages = []
        self.notify = set()

    def End(self, async = False):
        self.EnsureConnected()
        
        syncData = dmc_arbiter_thrift.SyncData(
            0, [], [], [], None, [], self.messages, list(self.notify),
            self.token_counter, self.node_counter, self.id_counter)
        
        self.client.Sync(syncData)
        if not async:
            self.WaitSchedule()

    def Close(self):
        self.client = None

    def GetVariable(self, name):
        self.EnsureConnected()
        
        return self.client.GetVariables([name])[0]

    def SetVariable(self, name, value):
        self.EnsureConnected()
        
        self.client.SetVariables({ name : value })
