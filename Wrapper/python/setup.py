from distutils.core import setup

setup(name = 'dmc',
      version ='0.0.1',
      packages = ['dmc'],
      requires = ['thriftpy'],
      package_data = {'dmc' : ['thrift/*.thrift']}
      )
