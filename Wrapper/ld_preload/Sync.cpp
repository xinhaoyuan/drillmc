/*
  Sync.cpp - interposition for pthread_mutex_* and pthread_cond_*
 */

#include "ld_preload/inst.h"
#include "DMCI.hpp"
#include <pthread.h>
#include <map>
#include <iostream>
#include <set>

using namespace std;
using namespace DMC;
using namespace DMC::Wrapper;
using namespace DMC::Wrapper::Helper;
using namespace DMCI;

#define DMCI_PTR_LOCK_STATUS_FREE 0
#define DMCI_PTR_LOCK_STATUS_ACQUIRED 1

static int _syncOpFlags = 0;
static int _strictSyncChecks = 0;

static int _ptrLockCounter = 0;

struct PtrLockInfo {
    int  id;
    bool permanent;
    int  status;
    int  rc;
    int  ownThreadId;
    int  wrRecCount; // < 0 means non-recursive
    int  rdCount;
    PtrLockInfo(bool _permanent)
        : permanent(_permanent)
        , status(DMCI_PTR_LOCK_STATUS_FREE)
        , ownThreadId(-1)
        , rc(0)
        , wrRecCount(-1)
        , rdCount(0)
        { }
};

struct PtrLockWaiter {
    PtrLockInfo * info;
    int write;
};

struct PthreadCondInfo {
    // List for waiters and signals
    struct ListNode {
        int role; // > 0 - signal; -1 : waiting node; == 0 : guard
        ListNode * prev;
        ListNode * next;
    };
    
    bool permanent;
    int  rc;
    int  blockingCount;
    ListNode list;

    PthreadCondInfo(bool _permanent)
        : permanent(_permanent)
        , rc(0)
        , blockingCount(0)
        {
            list.role = 0;
            list.prev = list.next = &list;
        }
};

struct PthreadCondWaiterInfo {
    PthreadCondInfo * cond;
    PtrLockInfo * ptrLock;

    PthreadCondInfo::ListNode * listNode;
};

static map<void *, PtrLockInfo *> _ptrLockMap;
static map<pthread_cond_t *, PthreadCondInfo *> _condMap;

void LWN(DMCI)::SetSyncOpFlags(int flags) {
    _syncOpFlags = flags;
}

void LWN(DMCI)::SetStrictSyncChecks(int enabled) {
    _strictSyncChecks = enabled;
}

static PtrLockInfo * getPtrLockInfo(void * ptr, bool canCreate) {
    auto it = _ptrLockMap.find(ptr);
    if (it == _ptrLockMap.end()) {
        if (canCreate) {
            auto info = new PtrLockInfo(true);
            info->id = _ptrLockCounter++;
            _ptrLockMap[ptr] = info;
            return info;
        }
        else {
            return nullptr;
        }
    }
    else {
        return it->second;
    }
}

static PthreadCondInfo * getCondInfo(pthread_cond_t * cond, bool canCreate) {
    auto it = _condMap.find(cond);
    if (it == _condMap.end()) {
        if (canCreate) {
            auto info = new PthreadCondInfo(true);
            _condMap[cond] = info;
            return info;
        }
        else {
            return nullptr;
        }
    }
    else {
        return it->second;
    }
}

static int PtrLockInit(void * ptr, int recursive) {
    auto info = getPtrLockInfo(ptr, false);
    if (info != nullptr) {
        if (info->rc > 0 || info->status != DMCI_PTR_LOCK_STATUS_FREE) {
            _wrapper->CheckAssert(false, "PtrLock re-initialization on referred ptr");
            return EBUSY;
        }
        else {
            return 0;
        }
    }
    else {
        info = new PtrLockInfo(false);
        info->id = _ptrLockCounter++;
        info->wrRecCount = recursive ? 0 : -1;
        _ptrLockMap[ptr] = info;
        return 0;
    }
}

static int PtrLockAcquire(void * ptr, int write) {
    auto info = getPtrLockInfo(ptr, true);

    if (write &&
        info->status == DMCI_PTR_LOCK_STATUS_ACQUIRED &&
        info->ownThreadId == _wrapper->GetContextId() &&
        info->wrRecCount > 0) {
        ++info->wrRecCount;
        return 0;
    }
    
    PtrLockWaiter waiter;
    waiter.info = info;
    waiter.write = write;
    
    ++info->rc;
    _wrapper->ScheduleSync([](void * param) {
            auto waiter = (PtrLockWaiter *)param;
            if (waiter->info->status == DMCI_PTR_LOCK_STATUS_FREE) {
                return STATE_ENABLED;
            }
            else if (!waiter->write && waiter->info->rdCount > 0) {
                return STATE_ENABLED;
            }
            else {
                return STATE_DISABLED;
            }
        }, nullptr, &waiter, GenString("ITP|PtrLockAcquire ", info->id).c_str(), -1, _syncOpFlags);
    --info->rc;


    info->status = DMCI_PTR_LOCK_STATUS_ACQUIRED;
    if (!write) {
        ++info->rdCount;
    }
    else {
        info->ownThreadId = _wrapper->GetContextId();
        if (info->wrRecCount >= 0) info->wrRecCount = 1;
    }
    return 0;
}

static int PtrLockTryAcquire(void * ptr, int write) {
    auto info = getPtrLockInfo(ptr, true);
    
    if (write &&
        info->status == DMCI_PTR_LOCK_STATUS_ACQUIRED &&
        info->ownThreadId == _wrapper->GetContextId() &&
        info->wrRecCount > 0) {
        ++info->wrRecCount;
        return 0;
    }
        
    ++info->rc;
    _wrapper->ScheduleSync(nullptr, nullptr, nullptr, GenString("ITP|PtrLockTryAcquire ", info->id).c_str(), -1, _syncOpFlags);
    --info->rc;

    if (info->status == DMCI_PTR_LOCK_STATUS_ACQUIRED) {
        return EBUSY;
    }

    info->status = DMCI_PTR_LOCK_STATUS_ACQUIRED;
    if (!write) {
        ++info->rdCount;
    }
    else {
        info->ownThreadId = _wrapper->GetContextId();
        if (info->wrRecCount >= 0) info->wrRecCount = 1;
    }
    return 0;
}

static int PtrLockRelease(void * ptr) {
    auto info = getPtrLockInfo(ptr, false);
    auto threadId = _wrapper->GetContextId();
    
    if (info == nullptr) {
        _wrapper->CheckAssert(false, "PtrLockRelease with invalid ptr");
        return EINVAL;
    }

    // ++info->rc;
    // _wrapper->ScheduleSync(nullptr, nullptr, nullptr, GenString("ITP|PtrLockRelease ", info->id).c_str(), -1, _syncOpFlags);
    // --info->rc;

    if (info->status != DMCI_PTR_LOCK_STATUS_ACQUIRED) {
        _wrapper->SendLog("PtrLockRelease on non-acquired lock", DMC_VERBOSE_WARNING);
        return EPERM;
    }
    else if (info->rdCount == 0) {
        // release write lock
        if (info->ownThreadId != threadId) {
            _wrapper->SendLog("PtrLockRelease on locked owned by other thread", DMC_VERBOSE_WARNING);
            return EPERM;
        }
        else if (info->wrRecCount > 1) {
            --info->wrRecCount;
            return 0;
        }
        else {
            info->status = DMCI_PTR_LOCK_STATUS_FREE;
            info->ownThreadId = -1;
            if (info->wrRecCount >= 0) info->wrRecCount = 0;
            return 0;
        }
    }
    else {
        // release read lock
        if (info->rdCount > 1) {
            // TODO: One may want to check if current thread own the read lock
            --info->rdCount;
            return 0;
        }
        else {
            info->status = DMCI_PTR_LOCK_STATUS_FREE;
            info->rdCount = 0;
            return 0;
        }
    }
}

static int PtrLockDestroy(void * ptr) {
    auto info = getPtrLockInfo(ptr, false);
    if (info == nullptr) {
        _wrapper->CheckAssert(false, "PtrLockDestroy on invalid ptr");
        return EINVAL;
    }
    else if (info->permanent) {
        if (_strictSyncChecks) {
            _wrapper->CheckAssert(false, "PtrLockDestroy on permanent ptr");
            return EINVAL;
        }
        else {
            _wrapper->SendLog("PtrLockDestroy on permanent ptr (or without init), try remove it anyway", DMC_VERBOSE_WARNING);
        }
    }

    if (info->rc > 0 || info->status != DMCI_PTR_LOCK_STATUS_FREE) {
        _wrapper->CheckAssert(false, "PtrLockDestroy on currently referred ptr");
        return EBUSY;
    }

    delete _ptrLockMap[ptr];
    _ptrLockMap.erase(ptr);
    return 0;
}

int dmci_inst_pthread_mutex_init(pthread_mutex_t * mutex, const pthread_mutexattr_t * attr) {
    int ret = dmci_orig_pthread_mutex_init(mutex, attr);
    if (_wrapper == NULL || ret != 0) return ret;

    int type;
    int recursive = 0;
    if (attr != nullptr && pthread_mutexattr_gettype(attr, &type) == 0) {
        if (type == PTHREAD_MUTEX_RECURSIVE) {
            recursive = 1;
        }
    }
    
    return PtrLockInit(mutex, recursive);
}

int dmci_inst_pthread_mutex_lock(pthread_mutex_t * mutex) {
    if (_wrapper == nullptr)
        return dmci_orig_pthread_mutex_lock(mutex);

    int ret = PtrLockAcquire(mutex, 1);
    if (ret == 0) {
        dmci_orig_pthread_mutex_lock(mutex);
    }
    return ret;
}

int dmci_inst_pthread_mutex_trylock(pthread_mutex_t * mutex) {
    if (_wrapper == nullptr)
        return dmci_orig_pthread_mutex_trylock(mutex);

    int ret = PtrLockTryAcquire(mutex, 1);
    if (ret == 0) {
        dmci_orig_pthread_mutex_trylock(mutex);
    }
    return ret;
}

int dmci_inst_pthread_mutex_unlock(pthread_mutex_t * mutex) {
    if (_wrapper == nullptr)
        return dmci_orig_pthread_mutex_unlock(mutex);

    int ret = PtrLockRelease(mutex);
    if (ret == 0) {
        dmci_orig_pthread_mutex_unlock(mutex);
    }
    return ret;
}

int dmci_inst_pthread_mutex_destroy(pthread_mutex_t * mutex) {
    int ret = dmci_orig_pthread_mutex_destroy(mutex);
    if (_wrapper == NULL || ret != 0) return ret;
    
    return PtrLockDestroy(mutex);
}

// rwlock

int dmci_inst_pthread_rwlock_init(pthread_rwlock_t * lock, const pthread_rwlockattr_t * attr) {
    int ret = dmci_orig_pthread_rwlock_init(lock, attr);
    if (_wrapper == NULL || ret != 0) return ret;

    return PtrLockInit(lock, 0);
}

int dmci_inst_pthread_rwlock_wrlock(pthread_rwlock_t * lock) {
    if (_wrapper == nullptr)
        return dmci_orig_pthread_rwlock_wrlock(lock);

    int ret = PtrLockAcquire(lock, 1);
    if (ret == 0) {
        dmci_orig_pthread_rwlock_wrlock(lock);
    }
    return ret;
}

int dmci_inst_pthread_rwlock_timedwrlock(
    pthread_rwlock_t * lock, const struct timespec * abstime) {
    return dmci_inst_pthread_rwlock_wrlock(lock);
}

int dmci_inst_pthread_rwlock_trywrlock(pthread_rwlock_t * lock) {
    if (_wrapper == nullptr)
        return dmci_orig_pthread_rwlock_trywrlock(lock);

    int ret = PtrLockTryAcquire(lock, 1);
    if (ret == 0) {
        dmci_orig_pthread_rwlock_trywrlock(lock);
    }
    return ret;
}

int dmci_inst_pthread_rwlock_rdlock(pthread_rwlock_t * lock) {
    if (_wrapper == nullptr)
        return dmci_orig_pthread_rwlock_rdlock(lock);

    int ret = PtrLockAcquire(lock, 0);
    if (ret == 0) {
        dmci_orig_pthread_rwlock_rdlock(lock);
    }
    return ret;
}

int dmci_inst_pthread_rwlock_timedrdlock(
    pthread_rwlock_t * lock, const struct timespec * abstime) {
    return dmci_inst_pthread_rwlock_rdlock(lock);
}

int dmci_inst_pthread_rwlock_tryrdlock(pthread_rwlock_t * lock) {
    if (_wrapper == nullptr)
        return dmci_orig_pthread_rwlock_tryrdlock(lock);

    int ret = PtrLockTryAcquire(lock, 0);
    if (ret == 0) {
        dmci_orig_pthread_rwlock_tryrdlock(lock);
    }
    return ret;
}

int dmci_inst_pthread_rwlock_unlock(pthread_rwlock_t * lock) {
    if (_wrapper == nullptr)
        return dmci_orig_pthread_rwlock_unlock(lock);

    int ret = PtrLockRelease(lock);
    if (ret == 0) {
        dmci_orig_pthread_rwlock_unlock(lock);
    }
    return ret;
}

int dmci_inst_pthread_rwlock_destroy(pthread_rwlock_t * lock) {
    int ret = dmci_orig_pthread_rwlock_destroy(lock);
    if (_wrapper == NULL || ret != 0) return ret;
    
    return PtrLockDestroy(lock);
}


// HACKY -- redirect spin_* to mutex_*

int dmci_inst_pthread_spin_init(pthread_spinlock_t * lock, int pshared) {
    int ret = dmci_orig_pthread_spin_init(lock, pshared);
    if (_wrapper == NULL || ret != 0) return ret;

    return PtrLockInit((void *)lock, 0);
}

int dmci_inst_pthread_spin_lock(pthread_spinlock_t * lock) {
    if (_wrapper == nullptr)
        return dmci_orig_pthread_spin_lock(lock);

    int ret = PtrLockAcquire((void *)lock, 1);
    if (ret == 0) {
        dmci_orig_pthread_spin_lock(lock);
    }
    return ret;
}

int dmci_inst_pthread_spin_trylock(pthread_spinlock_t * lock) {
    if (_wrapper == nullptr)
        return dmci_orig_pthread_spin_trylock(lock);

    int ret = PtrLockTryAcquire((void *)lock, 1);
    if (ret == 0) {
        dmci_orig_pthread_spin_trylock(lock);
    }
    return ret;
}

int dmci_inst_pthread_spin_unlock(pthread_spinlock_t * lock) {
    if (_wrapper == nullptr)
        return dmci_orig_pthread_spin_unlock(lock);

    int ret = PtrLockRelease((void *)lock);
    if (ret == 0) {
        dmci_orig_pthread_spin_unlock(lock);
    }
    return ret;
}

int dmci_inst_pthread_spin_destroy(pthread_spinlock_t * lock) {
    int ret = dmci_orig_pthread_spin_destroy(lock);
    if (_wrapper == NULL || ret != 0) return ret;

    return PtrLockDestroy((void *)lock);
}

static int
PthreadCondWait(pthread_cond_t * cond, pthread_mutex_t * mutex, bool hasTimeout) {
    PthreadCondInfo * condInfo = getCondInfo(cond, true);
    PtrLockInfo * ptrLockInfo = getPtrLockInfo(mutex, false);

    if (ptrLockInfo == nullptr ||
        ptrLockInfo->status != DMCI_PTR_LOCK_STATUS_ACQUIRED ||
        ptrLockInfo->ownThreadId != _wrapper->GetContextId() ||
        ptrLockInfo->wrRecCount == 0) {
        return EINVAL;
    }
    else {
        _wrapper->ScheduleSync(nullptr, nullptr, nullptr, "ITP|pthread_cond_wait-release_mutex", -1, _syncOpFlags);
        ptrLockInfo->status = DMCI_PTR_LOCK_STATUS_FREE;
        int wrRecCount = ptrLockInfo->wrRecCount;
        ptrLockInfo->wrRecCount = wrRecCount >= 0 ? 0 : -1;

        dmci_orig_pthread_mutex_unlock(mutex);
        
        PthreadCondInfo::ListNode listNode;
        PthreadCondWaiterInfo waiter;
        
        ++condInfo->blockingCount;

        // insert at the end
        listNode.role = -1;
        listNode.next = &condInfo->list;
        listNode.prev = condInfo->list.prev;
        listNode.next->prev = &listNode;
        listNode.prev->next = &listNode;
        
        waiter.cond = condInfo;
        waiter.ptrLock = ptrLockInfo;
        waiter.listNode = &listNode;

        ++condInfo->rc;
        ++ptrLockInfo->rc;
        _wrapper->ScheduleSync([](void * param) {
                auto waiter = (PthreadCondWaiterInfo *)param;
                auto cur = waiter->listNode;
                while (true) {
                    if (cur->role > 0)
                        break;
                    else if (cur->role == 0) // reach the guard -- cannot find a signal
                        return STATE_DISABLED;
                    cur = cur->next;
                }
                // find wakening signal -- see if we can grab the lock
                if (waiter->ptrLock->status != DMCI_PTR_LOCK_STATUS_FREE)
                    return STATE_DISABLED;
                else return STATE_ENABLED;
            }, nullptr, &waiter,
            hasTimeout ? "ITP|pthread_cond_wait-timeout" : "ITP|pthread_cond_wait", -1,
            _syncOpFlags);
        --condInfo->rc;
        --ptrLockInfo->rc;

        // in our turn, find the signal and erase it.
        auto cur = &listNode;
        while (true) {
            if (cur->role >= 0)
                break;
            cur = cur->next;
        }
        
        if (cur->role <= 0) {
            // impossible ..
            _wrapper->SendLog(GenString("impossible - ", DMC_CODE_HERE).c_str());
        }
        else if (--cur->role == 0) {
            cur->prev->next = cur->next;
            cur->next->prev = cur->prev;
            delete cur;
        }

        listNode.next->prev = listNode.prev;
        listNode.prev->next = listNode.next;
        
        // then acquire the lock
        ptrLockInfo->status = DMCI_PTR_LOCK_STATUS_ACQUIRED;
        ptrLockInfo->ownThreadId = _wrapper->GetContextId();
        ptrLockInfo->wrRecCount = wrRecCount;

        dmci_orig_pthread_mutex_lock(mutex);

        return 0;
    }
}

int dmci_inst_pthread_cond_wait(pthread_cond_t * cond, pthread_mutex_t * mutex) {
    if (_wrapper == nullptr)
        return dmci_orig_pthread_cond_wait(cond, mutex);

    return PthreadCondWait(cond, mutex, false);
}

int dmci_inst_pthread_cond_timedwait(pthread_cond_t * cond, pthread_mutex_t * mutex, const struct timespec * abstime) {
    if (_wrapper == nullptr)
        return dmci_orig_pthread_cond_timedwait(cond, mutex, abstime);
    
    // no timeout for now
    return PthreadCondWait(cond, mutex, true);
}

void DMCIPthreadCondSignal(PthreadCondInfo * condInfo, bool broadcast) {
    ++condInfo->rc;
    _wrapper->ScheduleSync(nullptr, nullptr, nullptr,
                           broadcast ? "ITP|pthread_cond_broadcast" : "ITP|pthread_cond_signal",
                           -1, _syncOpFlags);
    --condInfo->rc;

    if (condInfo->blockingCount > 0) {
        int signal = broadcast ? condInfo->blockingCount : 1;
        condInfo->blockingCount -= signal;
        
        PthreadCondInfo::ListNode * node = condInfo->list.prev;
        if (node->role <= 0) {
            node = new PthreadCondInfo::ListNode();
            node->role = signal;
            node->next = &condInfo->list;
            node->prev = condInfo->list.prev;
            node->next->prev = node;
            node->prev->next = node;
        }
        else {
            node->role += signal;
        }
    }

}

int dmci_inst_pthread_cond_signal(pthread_cond_t * cond) {
    if (_wrapper == nullptr)
        return dmci_orig_pthread_cond_signal(cond);
    
    PthreadCondInfo * info = getCondInfo(cond, true);
    DMCIPthreadCondSignal(info, false);
    return 0;
}

int dmci_inst_pthread_cond_broadcast(pthread_cond_t * cond) {
    if (_wrapper == nullptr)
        return dmci_orig_pthread_cond_broadcast(cond);

    PthreadCondInfo * info = getCondInfo(cond, true);
    DMCIPthreadCondSignal(info, true);
    return 0;
}

int dmci_inst_pthread_cond_init(pthread_cond_t * cond, const pthread_condattr_t * attr) {
    int ret = dmci_orig_pthread_cond_init(cond, attr);
    if (_wrapper == nullptr || ret != 0) return ret;

    auto info = getCondInfo(cond, false);
    if (info != nullptr) {
        _wrapper->CheckAssert(false, "pthread_cond_init re-initialization");
        return EBUSY;
    }
    else {
        info = new PthreadCondInfo(false);
        _condMap[cond] = info;
        return 0;
    }
    return 0;
}

int dmci_inst_pthread_cond_destroy(pthread_cond_t * cond) {
    int ret = dmci_orig_pthread_cond_destroy(cond);
    if (_wrapper == nullptr || ret != 0) return ret;

    
    auto info = getCondInfo(cond, false);
    if (info == nullptr) {
        _wrapper->CheckAssert(false, "pthread_cond_destroy on invalid cond var");
        return EINVAL;
    }
    else if (info->permanent) {
        if (_strictSyncChecks) {
             _wrapper->CheckAssert(false, "pthread_cond_destroy on permanent cond var");
            return EINVAL;
        }
        else {
            _wrapper->SendLog(
                "pthread_cond_destroy on permanent cond var (or without init), try remove it anyway",
                DMC_VERBOSE_WARNING);
        }
    }
    else if (info->rc > 0) {
        _wrapper->CheckAssert(false, "pthread_cond_destroy on currently referred cond var");
        return EBUSY;
    }

    delete _condMap[cond];
    _condMap.erase(cond);
    return 0;
}
