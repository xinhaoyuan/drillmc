#include "ld_preload/inst.h"

void * dmci_inst_dlopen(const char * filename, int flags) {
    return dmci_orig_dlopen(filename, flags);
}

int dmci_inst_dlclose(void * handle) {
    return dmci_orig_dlclose(handle);
}

void * dmci_inst_dlsym(void * handle, const char * name) {
    return dmci_orig_dlsym(handle, name);
}

char * dmci_inst_dlerror(void) {
    return dmci_orig_dlerror();
}
