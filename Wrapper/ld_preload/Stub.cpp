#include "dmci_stub.h"
#include <chrono>

void dmci_inst_on(void) {}
void dmci_inst_off(void) {}
int  dmci_inst_save(int) { return 0; }
void dmci_inst_restore(int) {}
dmc_wrapper_t dmci_get_wrapper(void) { return 0; }

dmc_wrapper_t dmc_wrapper_get_default() {
    return 0;
}

dmc_wrapper_t dmc_wrapper_create() {
    return 0;
}

void dmc_wrapper_destroy(dmc_wrapper_t wrapper) { }

int dmc_wrapper_is_connected(dmc_wrapper_t wrapper) { return false; }

int dmc_wrapper_init_node(dmc_wrapper_t wrapper, const char * endpoint, const char * cookie, struct context_ptr_store_ops_s * cps_ops, void * cps) {
    return 0;
}

int dmc_wrapper_node_start(dmc_wrapper_t wrapper, int node_id) {
    return 0;
}

int dmc_wrapper_get_node_id(dmc_wrapper_t wrapper) {
    return -1;
}

const char * dmc_wrapper_get_variable(dmc_wrapper_t wrapper, const char * name) {
    return "";
}

void dmc_wrapper_set_variable(dmc_wrapper_t wrapper, const char * name, const char * value) {
}

int dmc_wrapper_message_send(dmc_wrapper_t wrapper, int node_id, const char * desc, int nbytes) {
    return -1;
}

void dmc_wrapper_message_receive(dmc_wrapper_t wrapper, int remote, int token) {
}

int dmc_wrapper_message_receive_unique(dmc_wrapper_t wrapper, int remote) {
}

int dmc_wrapper_schedule_sync(dmc_wrapper_t wrapper, get_state_f state_func, stub_f stub_func, void * data, const char * desc, int nbytes, unsigned flags) {
    return 0;
}

void dmc_wrapper_schedule_async(dmc_wrapper_t wrapper, get_state_f state_func, stub_f stub_func, void * data, const char * desc, int nbytes, unsigned flags) {
    if (stub_func) stub_func(0, data);
}

dmc_timestamp_t dmc_wrapper_get_time_offset_ns(dmc_wrapper_t wrapper) {
    auto now = std::chrono::steady_clock::now();
    return std::chrono::duration_cast<std::chrono::nanoseconds>(now.time_since_epoch()).count(); 
}

void dmc_wrapper_insert_time_event_at(dmc_wrapper_t wrapper, dmc_timestamp_t ts) {
}

void dmc_wrapper_insert_time_event_after(dmc_wrapper_t wrapper, dmc_timestamp_t ts_after) {
}

void dmc_wrapper_context_detach(dmc_wrapper_t wrapper) {
}

void dmc_wrapper_send_log(dmc_wrapper_t wrapper, const char * log, int level) {
}

void dmc_wrapper_send_stack(dmc_wrapper_t wrapper, const char * message, int level) {
}

void dmc_wrapper_check_assert(dmc_wrapper_t wrapper, int should_be_non_zero, const char * message) {
}

void dmc_wrapper_node_shutdown(dmc_wrapper_t wrapper) {
}

void dmc_wrapper_end(dmc_wrapper_t wrapper, int error, const char * comment) {
}

void dmc_wrapper_join(dmc_wrapper_t wrapper) {
}
