#ifndef __DMCI_STUB_H__
#define __DMCI_STUB_H__

#include "DMCWrapper.h"

#if __cplusplus
extern "C" {
#endif
    
    void dmci_inst_on(void);
    void dmci_inst_off(void);
    int  dmci_inst_save(int);
    void dmci_inst_restore(int);
    dmc_wrapper_t dmci_get_wrapper(void);

#define DMCI_BEGIN(W, F) do { dmc_wrapper_t W = dmci_get_wrapper(); if (!W) break; int F = dmci_inst_save(0); do
#define DMCI_END(W, F) while (0); dmci_inst_restore(F); } while(0)

#if __cplusplus
}
#endif

#endif
