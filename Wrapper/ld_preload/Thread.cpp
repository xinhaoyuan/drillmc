#include "ld_preload/inst.h"
#include "DMCI.hpp"
#include <pthread.h>
#include <map>
#include <iostream>

using namespace std;
using namespace DMC;
using namespace DMC::Wrapper;
using namespace DMC::Wrapper::Helper;
using namespace DMCI;

#define PTHREAD_HELPER_STATUS_INIT    0
#define PTHREAD_HELPER_STATUS_RUNNING 1
#define PTHREAD_HELPER_STATUS_EXITED  2

struct PthreadHelper {
    void *(* entry)(void *);
    void *   data;
    
    int dmci_token;
    int status;
    // Used only in internal thread, application thread will be controlled and serialized 
    pthread_mutex_t  status_lock;
    pthread_cond_t   status_cond;
    // Used only in application thread for pending join request
    int joined;
};

static map<pthread_t, PthreadHelper *> _pthreadHMap;
static int _application_thread_count = 1;
static __thread PthreadHelper * _pthreadHelperSelf = nullptr;

void * dmci_pthread_create_helper_entry(void * _h) {
    PthreadHelper * h = (PthreadHelper *)_h;
    void *(* entry)(void *);
    void * data;

    /* h is guarenteed to be valid in dmci mode */
    entry = h->entry;
    data = h->data;
    
    if (h->dmci_token == -1) {
        pthread_mutex_lock(&h->status_lock);
        h->status = PTHREAD_HELPER_STATUS_RUNNING;
        pthread_cond_signal(&h->status_cond);
        pthread_mutex_unlock(&h->status_lock);

        pthread_setname_np(pthread_self(), "dmci");
        
        void * result = entry(data);
        return result;
    }
    else {
        _pthreadHelperSelf = h;
        AppThreadEnter();
                
        dmci_inst_on();
        void * result = entry(data);
        dmci_inst_off();
        
        AppThreadExit();
        return result;
    }
}

void LWN(DMCI)::AppThreadEnter() {
    if (_wrapper != nullptr) {
        if (_pthreadHelperSelf == nullptr) {
            // main thread case
            _pthreadHelperSelf = new PthreadHelper();
            _pthreadHelperSelf->entry = nullptr;
            _pthreadHelperSelf->data = nullptr;
            _pthreadHelperSelf->dmci_token = -1;
            _pthreadHelperSelf->joined = 0;
            _pthreadHMap[pthread_self()] = _pthreadHelperSelf;
        }
        else {
            _wrapper->MessageReceive(false, _pthreadHelperSelf->dmci_token);
        }

        pthread_setname_np(pthread_self(), GenString(_wrapper->GetContextId(), ':').c_str());
        _pthreadHelperSelf->status = PTHREAD_HELPER_STATUS_RUNNING;
    }
}

void LWN(DMCI)::AppThreadExit() {
    if (_wrapper != nullptr && _pthreadHelperSelf != nullptr) {

        _pthreadHelperSelf->status = PTHREAD_HELPER_STATUS_EXITED;
        if (_pthreadHelperSelf->joined == 0) {
            _pthreadHMap.erase(pthread_self());
            delete _pthreadHelperSelf;
        }
        
        _wrapper->ContextDetach();
    }
}

int pthread_create(pthread_t * p, const pthread_attr_t * pa, void *(*e)(void *), void * d) {
    dmci_inst_try_init();
    
    if (_wrapper == nullptr)
        return dmci_orig_pthread_create(p, pa, e, d);

    int ret = -1;    
    int inst_on = dmci_inst_save(0);
    if (!inst_on) {
        // pthread_helper data will not be needed once the internal
        // thread is created, so create it on the stack
        PthreadHelper h;
           
        // pthread_create inside inst_off
        h.entry = e;
        h.data = d;
        h.dmci_token = -1;
        h.status = PTHREAD_HELPER_STATUS_INIT;
        h.joined = 0;
        pthread_mutex_init(&h.status_lock, NULL);
        pthread_cond_init(&h.status_cond, NULL);

        ret = dmci_orig_pthread_create(p, pa, dmci_pthread_create_helper_entry, &h);

        if (ret == 0) {
            pthread_mutex_lock(&h.status_lock);
            while (h.status == PTHREAD_HELPER_STATUS_INIT) {
                pthread_cond_wait(&h.status_cond, &h.status_lock);
            }
            pthread_mutex_unlock(&h.status_lock);
        }

        pthread_mutex_destroy(&h.status_lock);
        pthread_cond_destroy(&h.status_cond);
    }
    else {
        // pthread_create called by application
        ++ _application_thread_count;
        
        PthreadHelper * h = new PthreadHelper();
        h->entry = e;
        h->data = d;
        h->dmci_token = _wrapper->MessageSend(-1, "ITP|pthread_create", -1);
        h->joined = 0;
        pthread_t _p;
        ret = dmci_orig_pthread_create(&_p, pa, dmci_pthread_create_helper_entry, h);
        
        if (ret == 0) {
            if (p != nullptr) *p = _p;
            _pthreadHMap[_p] = h;
            _wrapper->LocalYield();
        }
    }
    dmci_inst_restore(inst_on);
    
    return ret;
}

int dmci_inst_pthread_join(pthread_t thread, void ** value_ptr) {
    if (_wrapper == nullptr)
        return dmci_orig_pthread_join(thread, value_ptr);
    
    auto it = _pthreadHMap.find(thread);
    if (it == _pthreadHMap.end()) {
        // thread not found -- maybe already exited
    }
    else if (it->second->joined != 0) {
        _wrapper->CheckAssert(false, GenString("pthread_join to a joined thread ", thread).c_str());
        return -1;
    }
    else if (it->second->status == PTHREAD_HELPER_STATUS_EXITED) {
        // already exited
        delete it->second;
        _pthreadHMap.erase(it);
    }
    else {
        it->second->joined = 1;
        _wrapper->ScheduleSync([](void * param)->int {
                PthreadHelper * h = (PthreadHelper *)param;
                if (h->status == PTHREAD_HELPER_STATUS_EXITED)
                    return STATE_ENABLED;
                else return STATE_DISABLED;
            }, nullptr, it->second, "ITP|pthread_join_wait", -1, OP_FLAG_WEAK);
        delete it->second;
        _pthreadHMap.erase(it);
    }

    return dmci_orig_pthread_join(thread, value_ptr);
}

int dmci_inst_pthread_setname_np(pthread_t thread, const char * name) {
    char buf[20];
    int len = snprintf(buf, 20, "%d:%s", _wrapper->GetContextId(), name);
    buf[len] = 0;
    
    return dmci_orig_pthread_setname_np(thread, buf);
}
