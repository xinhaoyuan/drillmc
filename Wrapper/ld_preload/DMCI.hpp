#ifndef __DMC_INTERPOSITION_HPP__
#define __DMC_INTERPOSITION_HPP__

#include "Wrapper/cpp/DMCWrapper.hpp"
#include "Wrapper/cpp/DMCWrapperEx.hpp"

#define DMC_WRAPPER_IMPL
typedef DMC::Wrapper::INodeWrapper * dmc_wrapper_t;
#include "dmci.h"
#include "Utils/cpp/LWN.hpp"
#include <string>

LWN_START(DMCI);

using namespace DMC::Wrapper;

LWN_EXPORT;

extern INodeWrapper * _wrapper;

void AppThreadEnter();
void AppThreadExit();
void SetSyncOpFlags(int flags);
void SetStrictSyncChecks(int enabled);
std::string GetCmdlineVarName(int argc, char * const * args);

LWN_END;

#endif
