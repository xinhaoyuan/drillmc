#include "ld_preload/inst.h"
#include "DMCI.hpp"
#include "File.hpp"
#include <pthread.h>
#include <map>
#include <iostream>
#include <cstring>
#include <sys/stat.h>
#include <fcntl.h>

using namespace std;
using namespace DMC;
using namespace DMC::Wrapper;
using namespace DMC::Wrapper::Helper;
using namespace DMCI;

string LWN(DMCI)::PollBitsToString(int pollBits) {
    ostringstream os;
    if (pollBits & PollBits::IN) os << "In";
    if (pollBits & PollBits::OUT) os << "Out";
    if (pollBits & PollBits::ERR) os << "Err";
    if (pollBits & PollBits::RDHUP) os << "Rdhup";
    if (pollBits & PollBits::HUP) os << "Hup";
    return os.str();
}

pthread_mutex_t _fileMapLock = PTHREAD_MUTEX_INITIALIZER;
map<int, IFileHandler *> _fileMap;

ssize_t dmci_inst_write(int fd, const void * buf, size_t count) {
    pthread_mutex_lock(&_fileMapLock);
    auto it = _fileMap.find(fd);
    if (it == _fileMap.end()) {
        pthread_mutex_unlock(&_fileMapLock);
        return dmci_orig_write(fd, buf, count);
    }
    pthread_mutex_unlock(&_fileMapLock);
    
    return it->second->Write(fd, (const char *)buf, count);
}

ssize_t dmci_inst_read(int fd, void * buf, size_t count) {
    pthread_mutex_lock(&_fileMapLock);
    auto it = _fileMap.find(fd);
    if (it == _fileMap.end()) {
        pthread_mutex_unlock(&_fileMapLock);
        return dmci_orig_read(fd, buf, count);
    }
    pthread_mutex_unlock(&_fileMapLock);

    return it->second->Read(fd, (char *)buf, count);
}

ssize_t dmci_inst_writev(int fd, const struct iovec * iov, int iovcnt) {
    pthread_mutex_lock(&_fileMapLock);
    auto it = _fileMap.find(fd);
    if (it == _fileMap.end()) {
        pthread_mutex_unlock(&_fileMapLock);
        return dmci_orig_writev(fd, iov, iovcnt);
    }
    pthread_mutex_unlock(&_fileMapLock);

    size_t totalLength = 0;
    for (int i = 0; i < iovcnt; ++i)
        totalLength += iov[i].iov_len;

    char * buf = (char *)malloc(totalLength);
    size_t cur_cpy = 0;

    for (int i = 0; i < iovcnt; ++ i) {
        memcpy(buf + cur_cpy, iov[i].iov_base, iov[i].iov_len);
        cur_cpy += iov[i].iov_len;
    }

    ssize_t ret = it->second->Write(fd, buf, totalLength);
    free(buf);

    return ret;
}

ssize_t dmci_inst_readv(int fd, const struct iovec * iov, int iovcnt) {
    pthread_mutex_lock(&_fileMapLock);
    auto it = _fileMap.find(fd);
    if (it == _fileMap.end()) {
        pthread_mutex_unlock(&_fileMapLock);
        return dmci_orig_readv(fd, iov, iovcnt);
    }
    pthread_mutex_unlock(&_fileMapLock);

    size_t totalLength = 0;
    for (int i = 0; i < iovcnt; ++i)
        totalLength += iov[i].iov_len;

    char * buf = (char *)malloc(totalLength); 

    ssize_t ret = it->second->Read(fd, buf, totalLength);
    if (ret < 0) return ret;
    if (ret > totalLength) return -1;
    
    size_t remain = ret;
    const struct iovec * cur_dst = iov;
    char * cur_src = buf;
    while (remain > 0) {
        int cpy = cur_dst->iov_len;
        if (cpy > remain) cpy = remain;
        
        memcpy(iov->iov_base, cur_src, cpy);

        ++ iov;
        cur_src += cpy;
        remain -= cpy;
    }

    free(buf);

    return ret;
}

int dmci_inst_close(int fd) {
    pthread_mutex_lock(&_fileMapLock);
    auto it = _fileMap.find(fd);
    if (it == _fileMap.end()) {
        pthread_mutex_unlock(&_fileMapLock);
        return dmci_orig_close(fd);
    }
    pthread_mutex_unlock(&_fileMapLock);

    int ret = it->second->Close(fd);
    pthread_mutex_lock(&_fileMapLock);
    _fileMap.erase(it);
    pthread_mutex_unlock(&_fileMapLock);
}

void LWN(DMCI)::RegisterFD(int fd, IFileHandler * handler) {
    pthread_mutex_lock(&_fileMapLock);
    auto it = _fileMap.find(fd);
    if (it == _fileMap.end()) {
        _fileMap[fd] = handler;
    }
    pthread_mutex_unlock(&_fileMapLock);
}

IFileHandler * LWN(DMCI)::GetFileHandler(int fd) {
    pthread_mutex_lock(&_fileMapLock);
    auto it = _fileMap.find(fd);
    if (it == _fileMap.end()) {
        pthread_mutex_unlock(&_fileMapLock);
        return NULL;
    }
    pthread_mutex_unlock(&_fileMapLock);
    return it->second;
}
