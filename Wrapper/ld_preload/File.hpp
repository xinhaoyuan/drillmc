#ifndef __DMC_INTERPOSITION_FILE_HPP__
#define __DMC_INTERPOSITION_FILE_HPP__

#include <sys/epoll.h>
#include <string>

LWN_START(DMCI);

using namespace DMC::Wrapper;
using namespace std;

LWN_EXPORT;

enum PollBits {
    IN = EPOLLIN,
    OUT = EPOLLOUT,
    ERR = EPOLLERR,
    RDHUP = EPOLLRDHUP,
    HUP = EPOLLHUP,
};

string PollBitsToString(int pollBits);

class IFileHandler {
public:
        
    virtual ~IFileHandler() = default;
    virtual int Write(int fd, const char * buf, int len) = 0;
    virtual int Read(int fd, char * buf, int len) = 0;
    virtual int Poll(int fd) = 0; 
    // virtual int Fcntl(int fd, int cmd, va_list_t args) = 0;
    virtual int Close(int fd) = 0;
};

void RegisterFD(int fd, IFileHandler * handler);
IFileHandler * GetFileHandler(int fd);

LWN_END;

#endif
