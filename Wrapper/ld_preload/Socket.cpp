#include "ld_preload/inst.h"
#include "DMCI.hpp"
#include "File.hpp"
#include <pthread.h>
#include <map>
#include <iostream>
#include <cstring>
#include <sys/stat.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <sys/un.h>

using namespace std;
using namespace DMC;
using namespace DMC::Wrapper;
using namespace DMC::Wrapper::Helper;
using namespace DMCI;

class LocalBuffer {
    bool   _open;
    string _buffer;

public:

    LocalBuffer() : _open(false) { }

    void Open() {
        if (!_open) {
            _open = true;
            _buffer.clear();
        }
    }

    int Write(const char * buf, int len) {
        if (_open) {
            _buffer.append(buf, len);
            return len;
        }
        else {
            return 0;
        }
    }

    int Read(char * buf, int len) {
        if (_buffer.size() < len) {
            len = _buffer.size();
        }

        memcpy(buf, _buffer.data(), len);
        for (int i = 0; i < _buffer.size() - len; ++i) {
            _buffer[i] = _buffer[i + len];
        }

        _buffer.resize(_buffer.size() - len);
        return len;
    }

    void Close() {
        _open = false;
    }

    void GetInfo(bool & isOpen, int & len) {
        isOpen = _open;
        len = _buffer.size();
    }
};

static int _localBufferIdCounter = 0;
static map<int, LocalBuffer> _localBuffers;

struct ConnHelper {
    bool async;
    int  domain;
    
    int  inBufferId;
    int  outBufferId;
    int  peerNodeId;
    
    enum Type {
        INIT,
        LISTENING,
        CONNECTED,
        ERROR,
    } type;
    
    bool bind;
    
    sockaddr_storage local_addr;
    socklen_t local_addrlen;
    
    sockaddr_storage peer_addr;
    socklen_t peer_addrlen;

    bool Listen() {
        if (type != INIT) return false;

        _wrapper->BufferOpen(inBufferId);
        type = ConnHelper::Type::LISTENING;
    }

    ConnHelper * Accept(bool newAsync) {
        if (type != ConnHelper::Type::LISTENING) return nullptr;
                                    
        char data[12];
        if (_wrapper->BufferRead(inBufferId, data, 12) != 12) {
            // supposed to receive 3 32-bit integers -- what happened?
            return nullptr;
        }
        int newOutBufferId = *(int *)(data);
        int newPeerNodeId = *(int *)(data + 4);
        int newInBufferId = *(int *)(data + 8);

        auto ret = new ConnHelper();
        
        ret->domain = domain;
        ret->async = newAsync;
        ret->inBufferId = newInBufferId;
        ret->outBufferId = newOutBufferId;
        ret->peerNodeId = newPeerNodeId;
        ret->type = ConnHelper::Type::CONNECTED;

        return ret;
    }

    bool Connect(int listenerId, int listenerNodeId) {
        if (type != ConnHelper::Type::INIT) return false;
        
        int len;
        bool isOpen;
        _wrapper->BufferGetInfo(listenerId, isOpen, len);
        if (!isOpen) return false;
        
        outBufferId = _wrapper->CreateGlobalUniqueId();
        char data[12];

        *(int *)(data) = inBufferId;
        *(int *)(data + 4) = _wrapper->GetNodeId();  
        *(int *)(data + 8) = outBufferId;

        _wrapper->BufferOpen(inBufferId);
        _wrapper->BufferOpen(outBufferId);

        if (_wrapper->BufferWrite(listenerId, data, 12) != 12) {
            return false;
        }

        peerNodeId = listenerNodeId;
        type = ConnHelper::Type::CONNECTED;
        _wrapper->Notify(listenerNodeId);

        return true;
    }
    
    int Write(const char * data, int len) {
        if (len < 0 || type != ConnHelper::Type::CONNECTED) return -1;
        if (len == 0) return 0;
        // then write all data at once
        if (peerNodeId >= 0) {
            // Remote write
            int ret = _wrapper->BufferWrite(outBufferId, data, len);
            if (ret > 0) _wrapper->Notify(peerNodeId);
            return ret;
        }
        else {
            // Local write
            auto && buf = _localBuffers[outBufferId];
            return buf.Write(data, len);
        }
    }

    int Read(char * buffer, int len) {
        if (len < 0 || type != ConnHelper::Type::CONNECTED) return -1;
        if (len == 0) return 0;
        if (peerNodeId >= 0) {
            // Remote read
            return _wrapper->BufferRead(inBufferId, buffer, len);
        }
        else {
            // Local read
            auto && buf = _localBuffers[inBufferId];
            return buf.Read(buffer, len);
        }
    }

    void Shutdown(bool read, bool write) {
        {
            ostringstream os;
            os << "Socket shutdown buffer";
            if (peerNodeId >= 0)
                os << " (remote)";
            else os << " (local)";
            if (read) os << ' ' << inBufferId;
            if (write) os << ' ' << outBufferId;

            _wrapper->SendLog(os.str().c_str(), VERBOSE_DEBUG);
        }

        if (type == ConnHelper::Type::LISTENING) {
            if (read)_wrapper->BufferClose(inBufferId);
        }
        else if (type == ConnHelper::Type::CONNECTED) {
            if (peerNodeId >= 0) {
                if (read) {
                    _wrapper->BufferClose(inBufferId);
                }
                if (write) {
                    _wrapper->BufferClose(outBufferId);
                }
                _wrapper->Notify(peerNodeId);
            }
            else {
                if (read) {
                    _localBuffers[inBufferId].Close();
                }
                if (write) {
                    _localBuffers[outBufferId].Close();
                }
            }
        }
    }

    void Close() {
        Shutdown(true, true);
        type = ConnHelper::Type::INIT;
    }

    int Poll() {
        int ret = 0;
        
        if (type == ConnHelper::Type::LISTENING) {
            bool isOpen;
            int len;
            _wrapper->BufferGetInfo(inBufferId, isOpen, len);

            if (len > 0) ret |= PollBits::IN;
        }
        else if (type == ConnHelper::Type::CONNECTED) {
            int  len;
            bool isOpen;
            int  peerLen;
            bool peerIsOpen;

            if (peerNodeId >= 0) {
                _wrapper->BufferGetInfo(inBufferId, isOpen, len);
                _wrapper->BufferGetInfo(outBufferId, peerIsOpen, peerLen);
            }
            else {
                auto it = _localBuffers.find(inBufferId);
                
                if (it != end(_localBuffers)) {
                    it->second.GetInfo(isOpen, len);
                }
                else {
                    isOpen = false;
                    len = 0;
                }

                it = _localBuffers.find(outBufferId);
                if (it != end(_localBuffers)) {
                    it->second.GetInfo(peerIsOpen, peerLen);
                }
                else {
                    peerIsOpen = false;
                    peerLen = 0;
                }
            }

            if (len > 0) ret |= PollBits::IN;
            if (!isOpen) ret |= PollBits::RDHUP | PollBits::HUP;
            if (!peerIsOpen) ret |= PollBits::RDHUP | PollBits::HUP;
            else ret |= PollBits::OUT;
        }
        else if (type == ConnHelper::Type::ERROR) {
            ret |= PollBits::ERR | PollBits::HUP;
        }
        else {
            ret |= PollBits::HUP;
        }

        return ret;
    }
};

map<int, ConnHelper *> _connMap;

class SocketFileHandler : public IFileHandler {
    ConnHelper * _conn;

public:

    SocketFileHandler(ConnHelper * conn)
        : _conn(conn) {
    }

    int Write(int fd, const char * buf, int len) override {
        return dmci_inst_send(fd, buf, len, 0);
    }

    int Read(int fd, char * buf, int len) override {
        return dmci_inst_recv(fd, buf, len, 0);
    }

    int Poll(int fd) override {
        return _conn->Poll();
    }

    int Close(int fd) override {
        _connMap.erase(fd);
        _conn->Close();
        close(fd);
        // delete _conn; no proper management yet
    }
};

string StringifySockaddr(const sockaddr * addr, socklen_t addrlen) {
    if (addr->sa_family == AF_INET) {
        const sockaddr_in * sin = (const sockaddr_in *)addr;
        char buf[INET_ADDRSTRLEN + 1];
        inet_ntop(AF_INET, &sin->sin_addr.s_addr, buf, sizeof(buf));
        return GenString("inet:", buf, ":", ntohs(sin->sin_port));
    }
    else if (addr->sa_family == AF_UNIX) {
        const sockaddr_un * sun = (const sockaddr_un *)addr;
        return GenString("unix:", sun->sun_path);
    }
    else {
        return "unrecognized_sockaddr";
    }
}

void dmci_autobind_socket(int sockfd) {
    auto it = _connMap.find(sockfd);
    if (it == _connMap.end()) return;
    auto conn = it->second;
    
    if (conn->bind) return;

    if (conn->domain == AF_INET) { 
        struct sockaddr_in sin;
        sin.sin_family = AF_INET;
        sin.sin_addr.s_addr = htonl(INADDR_ANY);
        sin.sin_port = 0;
        dmci_inst_bind(sockfd, (const struct sockaddr *)&sin, sizeof(sin));
    }
    else if (conn->domain == AF_UNIX) {
        struct sockaddr_un sun;
        sun.sun_family = AF_UNIX;
        strcpy(sun.sun_path, GenString("unix-unbind:", conn->inBufferId).c_str());
        // Problem? we do not really bind
        conn->local_addrlen = sizeof(sun);
        memcpy(&conn->local_addr, &sun, sizeof(sun));
    }

    conn->bind = true;
}

int dmci_inst_socket(int domain, int type, int protocol) {
    bool async = false;
    int real_type = type;
    int ret = dmci_orig_socket(domain, type, protocol);
    ConnHelper * conn;

    if (ret < 0) return ret;

    if (_wrapper == nullptr)
        return ret;

    if (domain != AF_UNIX &&
        domain != AF_INET
        )
        return ret;
    
    if (!!(real_type & SOCK_NONBLOCK)) {
        async = true;
        real_type ^= SOCK_NONBLOCK;
    }

    if (!!(real_type & SOCK_CLOEXEC)) {
        real_type ^= SOCK_CLOEXEC;
    }
    
    if (real_type != SOCK_STREAM)
        return ret;
    
    conn = new ConnHelper();
    conn->domain = domain;
    conn->async = async;
    conn->inBufferId = _wrapper->CreateGlobalUniqueId();
    conn->outBufferId = -1;
    conn->type = ConnHelper::Type::INIT;
    conn->bind = false;
    
    _connMap[ret] = conn;
    RegisterFD(ret, new SocketFileHandler(conn));

    _wrapper->SendLog(GenString("Register socket fd ", ret).c_str(), VERBOSE_DEBUG);
    
    return ret;
}

int dmci_inst_socketpair(int domain, int type, int protocol, int sv[2]) {
    bool async = false;
    int real_type = type;
    int ret = dmci_orig_socketpair(domain, type, protocol, sv);
        
    ConnHelper * s0conn;
    ConnHelper * s1conn;
    int s0id, s1id;
    
    if (ret < 0) return ret;

    if (_wrapper == nullptr)
        return ret;

    if (domain != AF_UNIX &&
        domain != AF_INET
        )
        return ret;
    
    if (!!(real_type & SOCK_NONBLOCK)) {
        async = true;
        real_type ^= SOCK_NONBLOCK;
    }

    if (!!(real_type & SOCK_CLOEXEC)) {
        real_type ^= SOCK_CLOEXEC;
    }
    
    if (real_type != SOCK_STREAM)
        return ret;

    s0id = _localBufferIdCounter++;
    _localBuffers[s0id].Open();
    s1id = _localBufferIdCounter++;
    _localBuffers[s1id].Open();
    
    s0conn = new ConnHelper();
    s0conn->domain = domain;
    s0conn->async = async;
    s0conn->inBufferId = s0id;
    s0conn->outBufferId = s1id;
    s0conn->peerNodeId = -1;
    s0conn->type = ConnHelper::Type::CONNECTED;
    s0conn->bind = true;
    s0conn->local_addrlen = 0;
    s0conn->peer_addrlen = 0;
    
    _connMap[sv[0]] = s0conn;
    RegisterFD(sv[0], new SocketFileHandler(s0conn));

    s1conn = new ConnHelper();
    s1conn->domain = domain;
    s1conn->async = async;
    s1conn->inBufferId = s1id;
    s1conn->outBufferId = s0id;
    s1conn->peerNodeId = -1;
    s1conn->type = ConnHelper::Type::CONNECTED;
    s1conn->bind = true;
    s1conn->local_addrlen = 0;
    s1conn->peer_addrlen = 0;
    
    _connMap[sv[1]] = s1conn;
    RegisterFD(sv[1], new SocketFileHandler(s1conn));

    _wrapper->SendLog(GenString(
                          "Register socketpair fds ", sv[0], "<=>", sv[1],
                          " ", s0id, "<=>", s1id).c_str(),
                      VERBOSE_DEBUG);

    // Ceveats:
    // We assume the socket is bind here, but its local address is not recorded.
    // This may be problematic when someone use getsockname.
    
    return 0;
}

int dmci_inst_bind(int sockfd, const struct sockaddr * addr, socklen_t addrlen) {
    auto it = _connMap.find(sockfd);
    if (it == _connMap.end()) return dmci_orig_bind(sockfd, addr, addrlen);
    auto conn = it->second;
    
    if (conn->bind) {
        errno = EINVAL;
        return -1;
    }

    if (addr->sa_family == AF_INET) {
        struct sockaddr_in * sin = (struct sockaddr_in *)addr;
        if (sin->sin_addr.s_addr == htonl(INADDR_ANY)) {
            // change the inaddr_any to local ip address, if it exists
            const char * local_addr =
                _wrapper->GetVariable(GenString("local-addr:", _wrapper->GetNodeId()).c_str());
            if (*local_addr) {
                sin->sin_addr.s_addr = inet_addr(local_addr);
            }
        }
    }

    int ret = dmci_orig_bind(sockfd, addr, addrlen);
    int errno_saved = errno;
    
    _wrapper->SendLog(GenString("Bind fd = ", sockfd, ' ',
                                StringifySockaddr(addr, addrlen), " ret = ", ret).c_str(),
                      VERBOSE_DEBUG);
    if (ret != 0) {
        errno = errno_saved;
        return ret;
    }
    
    conn->local_addrlen = sizeof(conn->local_addr);
    // Assume success
    getsockname(sockfd, (struct sockaddr *)&conn->local_addr, &conn->local_addrlen);

    conn->bind = true;
    
    return ret;
}

int dmci_inst_listen(int sockfd, int backlog) {
    auto it = _connMap.find(sockfd);
    if (it == _connMap.end()) return dmci_orig_listen(sockfd, backlog);
    auto conn = it->second;

    if (!conn->bind) {
        errno = EADDRINUSE;
        return -1;
    }

    int ret = dmci_orig_listen(sockfd, backlog);
    int errno_saved = errno;
    
    _wrapper->SendLog(GenString("Listen fd = ", sockfd, ' ',
                                StringifySockaddr((const sockaddr *)&conn->local_addr, conn->local_addrlen),
                                " ret = ", ret).c_str(),
                      VERBOSE_DEBUG);
    if (ret != 0) {
        errno = errno_saved;
        return ret;
    }

    conn->Listen();

    string addrBufKey = GenString(
        "sockaddr-buf:",
        StringifySockaddr(
            (const struct sockaddr *)&conn->local_addr,
            conn->local_addrlen));
    string addrNodeIdKey = GenString(
        "sockaddr-nodeId:",
        StringifySockaddr(
            (const struct sockaddr *)&conn->local_addr,
            conn->local_addrlen)); 
    _wrapper->SetVariable(addrBufKey.c_str(), GenString(conn->inBufferId).c_str());
    _wrapper->SetVariable(addrNodeIdKey.c_str(), GenString(_wrapper->GetNodeId()).c_str());
    
    return 0;
}

int dmci_inst_accept(int sockfd, struct sockaddr * addr, socklen_t * addrlen) {
    auto it = _connMap.find(sockfd);
    if (it == _connMap.end())
        return dmci_orig_accept(sockfd, addr, addrlen);

    auto conn = it->second;

    if (!conn->async) {
        int poll = conn->Poll();
        if (!(poll & (PollBits::IN | PollBits::ERR))) {
            errno = EWOULDBLOCK;
            return -1;
        }
    }
    else {
        _wrapper->ScheduleSync([](void * param) {
                auto conn = (ConnHelper *)param;
                int poll = conn->Poll();
                if (poll & (PollBits::IN | PollBits::ERR)) {
                    return STATE_ENABLED;
                }
                else {
                    return STATE_DISABLED;
                }
            }, nullptr, conn, "ITP|blocking-accept");
    }

    auto newConn = conn->Accept(false);

    if (newConn == nullptr) {
        _wrapper->SendLog(GenString("Accept fd = ", sockfd, " failed").c_str(), VERBOSE_DEBUG);
        errno = EINVAL;
        return -1;
    }

    char addrlenBuf[4];
    if (_wrapper->BufferRead(conn->inBufferId, addrlenBuf, 4) != 4) {
        errno = EINVAL;
        return -1;
    }
    socklen_t newAddrlen = *(int *)(addrlenBuf);

    char sockaddr_buf[newAddrlen];
    if (_wrapper->BufferRead(conn->inBufferId, sockaddr_buf, newAddrlen) != newAddrlen) {
        _wrapper->SendLog(GenString("Accept fd = ", sockfd, " failed on reading peer addr").c_str(), VERBOSE_DEBUG);
        errno = EINVAL;
        return -1;
    }

    newConn->peer_addrlen = newAddrlen;
    if (newConn->peer_addrlen > sizeof(newConn->peer_addr)) newConn->peer_addrlen = sizeof(newConn->peer_addr);
    memcpy((char *)&newConn->peer_addr, sockaddr_buf, newConn->peer_addrlen);

    if (addr != nullptr && addrlen != nullptr) {
        int copyLen = newAddrlen;
        if (copyLen > *addrlen) copyLen = *addrlen;
        *addrlen = newAddrlen;
        
        memcpy(addr, sockaddr_buf, copyLen);
    }

    int newFd = dmci_orig_socket(conn->domain, SOCK_STREAM, 0);
    if (newFd < 0) {
        _wrapper->SendLog(GenString("Accept fd = ", sockfd, " failed on allocating new fd").c_str(), VERBOSE_DEBUG);
        errno = ENOMEM;
        return -1;
    }

    _wrapper->SendLog(GenString("Accept on fd = ", sockfd, " from ",
                                StringifySockaddr((const sockaddr *)sockaddr_buf, newAddrlen),
                                " new fd = ", newFd,
                                " in buf Id = ", newConn->inBufferId,
                                " out buf id = ", newConn->outBufferId,
                                " peer node id = ", newConn->peerNodeId).c_str(),
                      VERBOSE_DEBUG);

    _connMap[newFd] = newConn;
    RegisterFD(newFd, new SocketFileHandler(newConn));

    dmci_autobind_socket(newFd);
    
    return newFd;
}

int dmci_inst_connect(int sockfd, const struct sockaddr * addr, socklen_t addrlen) {
    auto it = _connMap.find(sockfd);
    if (it == _connMap.end()) return dmci_orig_connect(sockfd, addr, addrlen);
    auto conn = it->second;

    if (!conn->bind) {
        dmci_autobind_socket(sockfd);
    }

    struct sockaddr_in sin;

    if (addr->sa_family == AF_INET) {
        memcpy(&sin, addr, addrlen > sizeof(sin) ? sizeof(sin) : addrlen);
        if (sin.sin_addr.s_addr == htonl(INADDR_LOOPBACK)) {
            const char * local_addr =
                _wrapper->GetVariable(GenString("local-addr:", _wrapper->GetNodeId()).c_str());
            if (*local_addr) {
                sin.sin_addr.s_addr = inet_addr(local_addr);
                addr = (const struct sockaddr *)&sin;
                addrlen = sizeof(sin);
            }
        }
    }
    
    string addrStr = StringifySockaddr((const sockaddr *)&conn->local_addr, conn->local_addrlen);
    string peerAddrStr = StringifySockaddr(addr, addrlen);
    string addrBufKey = GenString("sockaddr-buf:", peerAddrStr);
    string addrNodeIdKey = GenString("sockaddr-nodeId:", peerAddrStr);
    const char * idStr = _wrapper->GetVariable(addrBufKey.c_str());
    int id = -1;
    if (*idStr == 0) {
        _wrapper->SendLog(GenString("Connect fd = ", sockfd,
                                    " local addr [", addrStr,
                                    "], remote addr [", peerAddrStr,
                                    "] failed - endpoint not registered").c_str(),
                          VERBOSE_DEBUG);
        errno = ECONNREFUSED;
        return -1;
    }
    else {
        id = atoi(idStr);
    }

    int nodeId = atoi(_wrapper->GetVariable(addrNodeIdKey.c_str()));

    if (!conn->Connect(id, nodeId)) {
        int errno_saved = errno;
        _wrapper->SendLog(GenString("Connect fd = ", sockfd,
                                    " local addr [", addrStr,
                                    "], remote addr [", peerAddrStr,
                                    "] failed").c_str(),
                          VERBOSE_DEBUG);
        errno = errno_saved;
        return -1;
    }

    _wrapper->SendLog(GenString("Connect fd = ", sockfd,
                                " local addr [", addrStr,
                                "], remote addr [", peerAddrStr,
                                "] done, buffer ",
                                conn->inBufferId, " <=> ", conn->outBufferId).c_str(),
                      VERBOSE_DEBUG);

    char addrlenBuf[4];
    *(int *)addrlenBuf = conn->local_addrlen;
    
    _wrapper->BufferWrite(id, addrlenBuf, 4);
    _wrapper->BufferWrite(id, (const char *)&conn->local_addr, conn->local_addrlen);

    conn->peer_addrlen = addrlen;
    if (conn->peer_addrlen > sizeof(conn->peer_addr)) conn->peer_addrlen = sizeof(conn->peer_addr);
    memcpy((char *)&conn->peer_addr, (const char *)addr, conn->peer_addrlen);

    return 0;
}

ssize_t dmci_inst_send(int sockfd, const void * buf, size_t len, int flags) {
    auto it = _connMap.find(sockfd);
    if (it == _connMap.end()) return dmci_orig_send(sockfd, (const char *)buf, len, flags);
    auto conn = it->second;
    // send doesn't really block ... we are simulating ideal socket with unlimited buf
    return conn->Write((const char *)buf, len);
}

ssize_t dmci_inst_sendmsg(int sockfd, const struct msghdr * msg, int flags) {
    auto it = _connMap.find(sockfd);
    if (it == _connMap.end()) return dmci_orig_sendmsg(sockfd, msg, flags);

    auto conn = it->second;
    size_t totalLength = 0;
    for (int i = 0; i < msg->msg_iovlen; ++i)
        totalLength += msg->msg_iov[i].iov_len;

    char * buf = (char *)malloc(totalLength);
    size_t cur_cpy = 0;

    for (int i = 0; i < msg->msg_iovlen; ++ i) {
        memcpy(buf + cur_cpy, msg->msg_iov[i].iov_base, msg->msg_iov[i].iov_len);
        cur_cpy += msg->msg_iov[i].iov_len;
    }

    ssize_t ret = conn->Write(buf, totalLength);
    int errno_saved = errno;
    free(buf);
    errno = errno_saved;
    
    return ret;
}

ssize_t dmci_inst_recv(int sockfd, void * buf, size_t len, int flags) {
    auto it = _connMap.find(sockfd);
    if (it == _connMap.end()) return dmci_orig_recv(sockfd, (char *)buf, len, flags);
    auto conn = it->second;

    int ret = conn->Read((char *)buf, len);
    if (ret == 0 && len > 0) {
        if (conn->async || (flags & MSG_DONTWAIT)) {
            errno = EAGAIN;
            return -1;
        }
        else {
            _wrapper->ScheduleSync([](void * param) {
                    auto conn = (ConnHelper *)param;
                    int poll = conn->Poll();
                    if (poll & (PollBits::IN | PollBits::ERR)) {
                        return STATE_ENABLED;
                    }
                    else {
                        return STATE_DISABLED;
                    }
                }, nullptr, conn, "ITP|blocking-recv");
            ret = conn->Read((char *)buf, len);
        }
    }

    // TODO MSG_PEEK and MSG_WAITALL?

    return ret;
}

int dmci_inst_getsockname(int sockfd, struct sockaddr *addr, socklen_t *addrlen) {
    auto it = _connMap.find(sockfd);
    if (it == _connMap.end()) return dmci_orig_getsockname(sockfd, addr, addrlen);
    auto conn = it->second;

    if (!conn->bind) {
        errno = EINVAL;
        return -1;
    }

    if (conn->peerNodeId < 0) {
        _wrapper->SendLog(GenString("getsockname on local sock fd ", sockfd).c_str(),
                          VERBOSE_DEBUG);
    }

    if (conn->local_addrlen == 0) {
        _wrapper->SendLog(GenString("getsockname ", sockfd, " return 0-length data").c_str(), VERBOSE_WARNING);
    }
    else {
        _wrapper->SendLog(
            GenString("getsockname ", sockfd, " return ",
                      StringifySockaddr((const sockaddr *)&conn->local_addr, conn->local_addrlen),
                      " addrlen = ", conn->local_addrlen).c_str(),
            VERBOSE_DEBUG);
    }

    if (*addrlen >= conn->local_addrlen) {
        memcpy(addr, &conn->local_addr, conn->local_addrlen);
    }
    else {
        memcpy(addr, &conn->local_addr, *addrlen);
    }

    *addrlen = conn->local_addrlen;
    return 0;
}

int dmci_inst_getpeername(int sockfd, struct sockaddr * addr, socklen_t * addrlen) {
    auto it = _connMap.find(sockfd);
    if (it == _connMap.end()) return dmci_orig_getpeername(sockfd, addr, addrlen);
    auto conn = it->second;

    if (conn->type != ConnHelper::Type::CONNECTED) {
        errno = ENOTCONN;
        return -1;
    }

    if (conn->peerNodeId < 0) {
        _wrapper->SendLog(GenString("getpeername on local sock fd ", sockfd).c_str(),
                          VERBOSE_DEBUG);
    }

    if (conn->peer_addrlen == 0) {
        _wrapper->SendLog(GenString("getpeername ", sockfd, " return 0-length data").c_str(), VERBOSE_WARNING);
    }
    else {
        _wrapper->SendLog(
            GenString("getpeername ", sockfd, " return ",
                      StringifySockaddr((const sockaddr *)&conn->peer_addr, conn->peer_addrlen),
                      " addrlen = ", conn->peer_addrlen).c_str(),
            VERBOSE_DEBUG);
    }

    if (*addrlen >= conn->peer_addrlen) {
        memcpy(addr, &conn->peer_addr, conn->peer_addrlen);
    }
    else {
        memcpy(addr, &conn->peer_addr, *addrlen);
    }

    *addrlen = conn->peer_addrlen;
    return 0;
}

int dmci_inst_shutdown(int sockfd, int how) {
    auto it = _connMap.find(sockfd);
    if (it == _connMap.end()) return dmci_orig_shutdown(sockfd, how);
    auto conn = it->second;

    if (conn->type != ConnHelper::Type::CONNECTED) {
        errno = ENOTCONN;
        return -1;
    }

    if (how == SHUT_RD) {
        conn->Shutdown(true, false);
    }
    else if (how == SHUT_WR) {
        conn->Shutdown(false, true);
    }
    else if (how == SHUT_RDWR) {
        conn->Shutdown(true, true);
    }
    else {
        errno = EINVAL;
        return -1;
    }

    return 0;
}
