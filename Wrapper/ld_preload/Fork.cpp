#include "ld_preload/inst.h"
#include "DMCI.hpp"

using namespace std;
using namespace DMC;
using namespace DMC::Wrapper;
using namespace DMCI;

pid_t fork(void) {
    dmci_inst_try_init();

    int s = dmci_inst_save(0);
    int ret = dmci_orig_fork();
    if (ret == 0)
        return 0;

    dmci_inst_restore(s);
    return ret;
}

int execv(const char * path, char * const * argv) {
    dmci_inst_try_init();

    int s = dmci_inst_save(0);
    if (_wrapper) {
        string cmdline_varname = GetCmdlineVarName(-1, argv);
        const char * nodeIdStr = _wrapper->GetVariable(cmdline_varname.c_str());
        if (*nodeIdStr) {
            int nodeId = atoi(nodeIdStr);
            _wrapper->MessageSend(nodeId, "execv");
        }
        else {
            fprintf(stderr, "cannot find %s\n", cmdline_varname.c_str());
        }
        _wrapper->NodeShutdown();
    }
    int ret = dmci_orig_execv(path, argv);

    dmci_inst_restore(s);
    return ret;
}
