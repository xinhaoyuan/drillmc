#include "dmci_stub.h"
#include <stdio.h>

int main() {
    DMCI_BEGIN(wrapper, p)
    {
        dmc_wrapper_send_log(wrapper, "Hello DMC!", 0);
    }
    DMCI_END(wrapper, p);

    return 0;
}
