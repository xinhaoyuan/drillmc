#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char ** argv) {
    if (argc == 1) {
        printf("first time\n");
        
        char * new_argv[3];
        new_argv[0] = argv[0];
        new_argv[1] = "second";
        new_argv[2] = NULL;

        execv(argv[0], new_argv);
    }
    else {
        printf("second time\n");
    }
    return 0;
}
