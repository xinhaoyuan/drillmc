#include <pthread.h>
#include <stdio.h>

pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER;

void * thread_entry() {
    return NULL;
}

int main() {
    pthread_t t;
    pthread_create(&t, NULL, &thread_entry, NULL);
    pthread_join(t, NULL);
    
    return 0;
}
