#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <errno.h>
#include <string.h>

int sv[2];

void * thread_entry(void * param) {
    char send_buf[10] = "hello";
    int send_ret = send(sv[1], send_buf, 6, 0);

    close(sv[1]);
}

int main()
{
    int ret = socketpair(AF_UNIX, SOCK_STREAM, 0, sv);
    if (ret != 0) {
        printf("socketpair() error - %s\n", strerror(errno));
        return 0;
    }
    
    pthread_t t;
    pthread_create(&t, NULL, &thread_entry, NULL);
    
    char recv_buf[10];

    int recv_ret = recv(sv[0], recv_buf, sizeof(recv_buf), 0);
    printf("recv ret = %d, buf = %s\n", recv_ret, recv_buf);

    close(sv[0]);
    
    return 0;
}
