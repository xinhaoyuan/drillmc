#include <unistd.h>
#include <pthread.h>
#include <stdio.h>

int lock_acquired = 0;
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

// Since all threads sleep. Sleepset will be ignored.

void * pentry(void * param) {
    sleep(1);
    pthread_mutex_lock(&lock);
    lock_acquired = 1;
    pthread_mutex_unlock(&lock);
}

int main() {
    pthread_t p;
    pthread_create(&p, NULL, pentry, NULL);
    sleep(1);
    pthread_mutex_lock(&lock);
    if (!lock_acquired) {
        printf("main get lock first!\n");
    }
    pthread_mutex_unlock(&lock);
    pthread_join(p, NULL);
    return 0;
}
