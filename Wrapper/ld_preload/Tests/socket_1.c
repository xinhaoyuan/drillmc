#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <pthread.h>

int sd;
struct sockaddr_in name;

void * thread_entry(void * param) {
    int csd = socket(AF_INET, SOCK_STREAM, 0);
    inet_aton("127.0.0.1", &name.sin_addr);
    connect(csd, (const struct sockaddr *)&name, sizeof(name));

    char send_buf[10] = "hello";
    int send_ret = send(csd, send_buf, 6, 0);

    close(csd);
}

int main()
{
    int psd;

    sd = socket(AF_INET, SOCK_STREAM, 0);
    name.sin_family = AF_INET;
    name.sin_addr.s_addr = htonl(INADDR_ANY);
    name.sin_port = htons(12345);

    bind(sd, (const struct sockaddr *)&name, sizeof(name));
    listen(sd,1);

    pthread_t t;
    pthread_create(&t, NULL, &thread_entry, NULL);
    
    psd = accept(sd, 0, 0);
    close(sd);

    char recv_buf[10];

    int recv_ret = recv(psd, recv_buf, sizeof(recv_buf), 0);
    printf("recv ret = %d, buf = %s\n", recv_ret, recv_buf);

    close(psd);
    
    return 0;
}
