#include <pthread.h>
#include <stdio.h>

pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t c = PTHREAD_COND_INITIALIZER;

volatile int x = 0;

void * thread_entry() {

    pthread_mutex_lock(&m);
    x = 1;
    pthread_cond_signal(&c);
    pthread_mutex_unlock(&m);

    return NULL;
}

int main() {
    pthread_t t;
    pthread_create(&t, NULL, &thread_entry, NULL);

    pthread_mutex_lock(&m);
    while (x == 0) {
        pthread_cond_wait(&c, &m);
    }
    pthread_mutex_unlock(&m);
    
    pthread_join(t, NULL);
    
    return 0;
}
