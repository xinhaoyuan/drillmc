#include "ld_preload/inst.h"
#include "DMCI.hpp"
#include <sstream>

using namespace std;
using namespace DMC;
using namespace DMC::Wrapper;
using namespace DMC::Wrapper::Helper;
using namespace DMCI;

unsigned int dmci_inst_sleep(unsigned int sec) {
    if (_wrapper == nullptr)
        return dmci_orig_sleep(sec);
    
    _wrapper->ScheduleSync(nullptr, nullptr, nullptr, "ITP|<Sleep>", -1, 0);
    
    return 0;
}

string LWN(DMCI)::GetCmdlineVarName(int argc, char * const * argv) {
    ostringstream os;
    os << "cmd:";
    for (int i = 0; i != argc && argv[i] && i < 5; ++ i) {
        if (i > 0) os << ' ';
        os << argv[i];
    }
    return os.str();
}
