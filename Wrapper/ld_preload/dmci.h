#ifndef __DMCI_H__
#define __DMCI_H__

#include "DMCWrapper.h"

#if __cplusplus
extern "C" {
#endif

    dmc_wrapper_t dmci_get_wrapper(void);

#if __cplusplus
}
#endif

#endif
