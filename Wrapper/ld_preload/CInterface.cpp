#include "DMCWrapper.hpp"
#include "ld_preload/inst.h"

using namespace DMC;
using namespace DMC::Wrapper;

#define DMC_WRAPPER_IMPL
typedef INodeWrapper * dmc_wrapper_t;
#include "DMCWrapper.h"

// Basically is same as cpp/CInterface.cpp, with interposition auto-off

int dmc_wrapper_is_connected(dmc_wrapper_t wrapper) {
    int f = dmci_inst_save(0);
    int ret = wrapper->IsConnected();
    dmci_inst_restore(f);
    return ret;
}

int dmc_wrapper_get_node_id(dmc_wrapper_t wrapper) {
    int f = dmci_inst_save(0);
    int ret = wrapper->GetNodeId();
    dmci_inst_restore(f);
    return ret;
}

const char * dmc_wrapper_get_variable(dmc_wrapper_t wrapper, const char * name) {
    int f = dmci_inst_save(0);
    const char * ret = wrapper->GetVariable(name);
    dmci_inst_restore(f);
    return ret;
}

void dmc_wrapper_set_variable(dmc_wrapper_t wrapper, const char * name, const char * value) {
    int f = dmci_inst_save(0);
    wrapper->SetVariable(name, value);
    dmci_inst_restore(f);
}

int dmc_wrapper_message_send(dmc_wrapper_t wrapper, int node_id, const char * desc, int nbytes) {
    int f = dmci_inst_save(0);
    int ret = wrapper->MessageSend(node_id, desc, nbytes);
    dmci_inst_restore(f);
    return ret;
}

void dmc_wrapper_message_receive(dmc_wrapper_t wrapper, int remote, int token) {
    int f = dmci_inst_save(0);
    wrapper->MessageReceive(remote, token);
    dmci_inst_restore(f);
}

int dmc_wrapper_message_receive_unique(dmc_wrapper_t wrapper, int remote) {
    int f = dmci_inst_save(0);
    int ret = wrapper->MessageReceiveUnique(remote);
    dmci_inst_restore(f);
    return ret;
}

int dmc_wrapper_schedule_sync(dmc_wrapper_t wrapper, get_state_f state_func, stub_f stub_func, void * data, const char * desc, int nbytes, unsigned flags) {
    int f = dmci_inst_save(0);
    int ret = wrapper->ScheduleSync(state_func, stub_func, data, desc, nbytes, flags);
    dmci_inst_restore(f);
    return ret;
}

void dmc_wrapper_schedule_async(dmc_wrapper_t wrapper, get_state_f state_func, stub_f stub_func, void * data, const char * desc, int nbytes, unsigned flags) {
    int f = dmci_inst_save(0);
    wrapper->ScheduleAsync(state_func, stub_func, data, desc, nbytes, flags);
    dmci_inst_restore(f);
}

void dmc_wrapper_context_detach(dmc_wrapper_t wrapper) {
    int f = dmci_inst_save(0);
    wrapper->ContextDetach();
    dmci_inst_restore(f);
}

void dmc_wrapper_send_log(dmc_wrapper_t wrapper, const char * log, int level) {
    int f = dmci_inst_save(0);
    wrapper->SendLog(log, level);
    dmci_inst_restore(f);
}

void dmc_wrapper_send_stack(dmc_wrapper_t wrapper, const char * message, int level) {
    int f = dmci_inst_save(0);
    wrapper->SendStack(message, level);
    dmci_inst_restore(f);
}

dmc_timestamp_t dmc_wrapper_get_time_offset_ns(dmc_wrapper_t wrapper) {
    int f = dmci_inst_save(0);
    dmc_timestamp_t ret = wrapper->GetTimeOffsetNs();
    dmci_inst_restore(f);
    return ret;
}

void dmc_wrapper_insert_time_event_at(dmc_wrapper_t wrapper, dmc_timestamp_t ts) {
    int f = dmci_inst_save(0);
    wrapper->InsertTimeEventAt(ts);
    dmci_inst_restore(f);
}

void dmc_wrapper_insert_time_event_after(dmc_wrapper_t wrapper, dmc_timestamp_t ts_after) {
    int f = dmci_inst_save(0);
    wrapper->InsertTimeEventAfter(ts_after);
    dmci_inst_restore(f);
}

void dmc_wrapper_check_assert(dmc_wrapper_t wrapper, int should_be_non_zero, const char * message) {
    int f = dmci_inst_save(0);
    wrapper->CheckAssert(should_be_non_zero != 0, message);
    dmci_inst_restore(f);
}

void dmc_wrapper_end(dmc_wrapper_t wrapper, int error, const char * comment) {
    int f = dmci_inst_save(0);
    wrapper->End(error, comment);
    dmci_inst_restore(f);
}
