#include "ld_preload/inst.h"
#include "DMCI.hpp"
#include "Wrapper/cpp/WrapperImpl.hpp"

#include <cstdlib>
#include <iostream>
#include <sstream>
 
using namespace std;
using namespace DMC;
using namespace DMC::Wrapper;
using namespace DMC::Wrapper::Impl;
using namespace DMC::Wrapper::Helper;
using namespace DMCI;

INodeWrapper * DMCI::_wrapper = nullptr;

void dmci_inst_init() {}

void dmci_inst_thread_init() {}

extern "C" {
    int __libc_start_main(int (*main) (int, char **, char **),
                          int argc, char **ubp_av, void (*init) (void),
                          void (*fini) (void), void (*rtld_fini) (void),
                          void (*stack_end));
}

static int(* real_main)(int, char **, char **) = nullptr;

int main_wrapper(int argc, char ** argv, char ** envp) {
    _wrapper = new NodeWrapperImpl();

    string cmdline_varname = GetCmdlineVarName(argc, argv);
    
    const char * blacklist = getenv("DMCI_BLACKLIST");
    if (blacklist) {
        int start = 0;
        int end = 0;
        while (1) {
            while (blacklist[end] && blacklist[end] != '|') ++end;

            if (start < end) {
                string keyword(blacklist + start, blacklist + end);
                if (cmdline_varname.find(keyword) != string::npos) {
                    unsetenv("LD_PRELOAD");
                    _wrapper = nullptr;
                    break;
                }
            }
                
            if (!blacklist[end]) break;
            start = end + 1;
            end = start;
        }
    }

    if (_wrapper == nullptr) {
        // do nothing
    }
    else if (!_wrapper->InitNode(nullptr, nullptr, nullptr)) {
        delete _wrapper;
        _wrapper = nullptr;
    }
    else {
        const char * nodeIdStr = _wrapper->GetVariable(cmdline_varname.c_str());
        if (nodeIdStr == nullptr || *nodeIdStr == 0) {
            fprintf(stderr, "Cannot retrieve node id by %s\n", cmdline_varname.c_str());
            _wrapper = nullptr;
        }
        else {
            int nodeId = atoi(nodeIdStr);
            if (!_wrapper->NodeStart(nodeId)) {
                fprintf(stderr, "Failed at NodeStart, nodeId = %d\n", nodeId);
                _wrapper = nullptr;
            }
            else {
                _wrapper->SendLog(GenString(
                                      "Process ", getpid(), " attached to node ", nodeId).c_str(),
                                  VERBOSE_DEBUG);
            }
        }

        if (_wrapper != nullptr) {
            string varName = "sync-op-flags";
            ostringstream os;
            
            os.str(""); os << '|' << _wrapper->GetVariable(varName.c_str()) << '|';
            string flagsStr = os.str();
            
            int flags = 0;
            if (flagsStr.find("|detach|") != string::npos) {
                flags |= DMC_OP_FLAG_LOCAL_DETACH;
            }

            SetSyncOpFlags(flags);

            varName = "strict-sync-checks";
            const char * valueCStr = _wrapper->GetVariable(varName.c_str());
            if (*valueCStr) {
                SetStrictSyncChecks(1);
            }
        }
    }

    AppThreadEnter();

    dmci_inst_on();
    int ret = real_main(argc, argv, envp);
    dmci_inst_off();
    
    AppThreadExit();

    if (_wrapper)
        _wrapper->Join();
    
    return ret;
}

int __libc_start_main(int (*main) (int, char **, char **),
                      int argc, char **ubp_av, void (*init) (void),
                      void (*fini) (void), void (*rtld_fini) (void),
                      void (*stack_end)) {
    dmci_inst_try_init();
    real_main = main;
    return dmci_orig___libc_start_main(main_wrapper, argc, ubp_av, init, fini, rtld_fini, stack_end);
}

dmc_wrapper_t dmci_get_wrapper(void) {
    return _wrapper;
}
