#include "ld_preload/inst.h"
#include "DMCI.hpp"
#include "File.hpp"
#include <pthread.h>
#include <map>
#include <iostream>
#include <vector>
#include <cstring>
#include <sys/stat.h>
#include <fcntl.h>

using namespace std;
using namespace DMC;
using namespace DMC::Wrapper;
using namespace DMC::Wrapper::Helper;
using namespace DMCI;

struct EpollEntry {
    int fd;
    int order;
    
    struct epoll_event event;
    IFileHandler * handler;
    int registered_bits;
    int current_bits;
    int previous_bits;

    EpollEntry()
        : handler(nullptr)
        , fd(-1)
        , order(-1)
        , registered_bits(0)
        , current_bits(0)
        , previous_bits(0)
        { }
};

inline int EpollEventsToPollBits(int events) {
    return events;
}

inline int PollBitsToEpollEvents(int bits) {
    return bits;
}

struct EpollHelper {
    int orderCounter;
    map<int, EpollEntry> entriesByFd;
    map<int, int> entriesByOrder;

    EpollHelper()
        : orderCounter(0)
        { }
};

pthread_mutex_t _epollMapLock = PTHREAD_MUTEX_INITIALIZER;
map<int, EpollHelper *> _epollMap;

class DummyFileHandler : public IFileHandler {
public:

    int Write(int fd, const char * buf, int len) {
        return 0;
    }

    int Read(int fd, char * buf, int len) {
        return 0;
    }

    int Poll(int fd) {
        return 0;
    }

    int Close(int fd) {
        return 0;
    }
} dummyFileHandler;

int dmci_inst_epoll_create(int size) {
    return dmci_inst_epoll_create1(0);
}

int dmci_inst_epoll_create1(int flags) {
    int fd = dmci_orig_epoll_create1(flags);
    if (fd < 0) return fd;
    
    RegisterFD(fd, &dummyFileHandler);

    pthread_mutex_lock(&_epollMapLock);
    _epollMap[fd] = new EpollHelper();
    pthread_mutex_unlock(&_epollMapLock);
    
    return fd;
}

int dmci_inst_epoll_ctl(int epfd, int op, int fd, struct epoll_event *event) {
    pthread_mutex_lock(&_epollMapLock);
    auto it = _epollMap.find(epfd);
    if (it == _epollMap.end()) {
        pthread_mutex_unlock(&_epollMapLock);
        return dmci_orig_epoll_ctl(epfd, op, fd, event);
    }
    auto epoll = it->second;
    pthread_mutex_unlock(&_epollMapLock);

    auto handler = GetFileHandler(fd);
    if (handler == NULL)
        return dmci_orig_epoll_ctl(epfd, op, fd, event);

    auto eit = epoll->entriesByFd.find(fd);
    
    switch (op) {
    case EPOLL_CTL_ADD:
    case EPOLL_CTL_MOD:
    {
        bool created = false;
        if (eit == end(epoll->entriesByFd)) {
            eit = epoll->entriesByFd.emplace(fd, EpollEntry()).first;
            created = true;
        }
        auto && entry = eit->second;
        
        entry.event = *event;
        entry.handler = handler;
        entry.registered_bits = EpollEventsToPollBits(
            event->events & (EPOLLIN | EPOLLOUT | EPOLLERR | EPOLLHUP | EPOLLRDHUP));
        entry.registered_bits |= EPOLLERR | EPOLLHUP;
        
        if (created) {
            entry.fd = fd;
            entry.order = epoll->orderCounter++;
            epoll->entriesByOrder[entry.order] = entry.fd;
        }
        
        break;
    }
    case EPOLL_CTL_DEL:
    {
        if (eit != end(epoll->entriesByFd)) {
            epoll->entriesByOrder.erase(eit->second.order);
            epoll->entriesByFd.erase(eit);
        }
        break;
    }
    default:
        errno = EINVAL;
        return -1;
    }

    return 0;
}

int epoll_wait_nb(EpollHelper * epoll, struct epoll_event * events, int maxevents) {
    vector<int> triggeredOneshot;
        
    int idx = 0;
    for (auto && tp : epoll->entriesByOrder) {
        if (idx >= maxevents) {
            break;
        }
        auto && entry = epoll->entriesByFd[get<1>(tp)];
        entry.current_bits = entry.handler->Poll(get<1>(tp));
        int observed_bits = entry.current_bits & ~entry.previous_bits & entry.registered_bits;
        if (observed_bits) {
            events[idx] = entry.event;
            if (observed_bits & (PollBits::ERR | PollBits::HUP | PollBits::RDHUP)) {
                _wrapper->SendLog(
                    GenString(
                        "epoll returns error condition on fd ",
                        get<1>(tp),
                        " err = ", (bool)(observed_bits & PollBits::ERR),
                        " hup = ", (bool)(observed_bits & PollBits::HUP),
                        " rdhup = ", (bool)(observed_bits & PollBits::RDHUP)
                        ).c_str(), VERBOSE_DEBUG);
            }
            events[idx].events = PollBitsToEpollEvents(observed_bits);
                
            if (entry.event.events & EPOLLET)
                entry.previous_bits = entry.current_bits;
            if (entry.event.events & EPOLLONESHOT) {
                epoll->entriesByFd.erase(get<1>(tp));
                triggeredOneshot.push_back(get<0>(tp));
            }
            ++idx;
        }
    }

    for (int order : triggeredOneshot) {
        epoll->entriesByOrder.erase(order);
    }

    return idx;
}

int dmci_inst_epoll_wait(int epfd, struct epoll_event *events,
                         int maxevents, int timeout) {
    pthread_mutex_lock(&_epollMapLock);
    auto it = _epollMap.find(epfd);
    if (it == _epollMap.end()) {
        pthread_mutex_unlock(&_epollMapLock);
        return dmci_orig_epoll_wait(epfd, events, maxevents, timeout);
    }
    auto epoll = it->second;
    pthread_mutex_unlock(&_epollMapLock);

    if (maxevents <= 0) {
        return 0;
    }

    int ret = dmci_orig_epoll_wait(epfd, events, maxevents, 0);
    if (ret != 0) {
        _wrapper->SendLog(GenString("epoll_wait returns from orig call ", ret).c_str(), VERBOSE_INFO);
        return ret;
    }

    if (timeout != 0) {
        _wrapper->ScheduleSync([](void * param) {
                auto epoll = (EpollHelper *)param;
                int ret = STATE_DISABLED;
                for (auto && tp : epoll->entriesByFd) {
                    auto && entry = get<1>(tp);
                    entry.current_bits = entry.handler->Poll(get<0>(tp));
                    if (entry.current_bits & ~entry.previous_bits & entry.registered_bits) {
                        ret = STATE_ENABLED;
                    }
                    if (entry.event.events & EPOLLET)
                        entry.previous_bits &= ~entry.current_bits; 
                }

                return ret;
            }, nullptr, epoll,
            (timeout > 0) ? "ITP|epoll_wait-timeout" : "ITP|epoll_wait-forever", -1,
            OP_FLAG_WEAK);
    }

    return epoll_wait_nb(epoll, events, maxevents); 
}

int dmci_inst_epoll_pwait(int epfd, struct epoll_event *events,
                          int maxevents, int timeout,
                          const sigset_t *sigmask) {
    return dmci_inst_epoll_wait(epfd, events, maxevents, timeout);
}

struct PollEntry {
    int fd;
    IFileHandler * handler;
    int event_mask;
};

struct PollHelper {
    struct pollfd * fds;
    nfds_t nfds;
    
    map<int, PollEntry> entries;
};

int dmci_inst_poll(struct pollfd * fds, nfds_t nfds, int timeout) {
    PollHelper helper;
    helper.fds = fds;
    helper.nfds = nfds;
    
    int ret = 0;
    
    for (int i = 0; i < nfds; ++ i) {
        if (fds[i].fd < 0) {
            fds[i].revents = 0;
            continue;
        }
        
        auto handler = GetFileHandler(fds[i].fd);
        
        if (handler == nullptr)
            continue;

        auto && h = helper.entries[i];
        h.fd = fds[i].fd;
        h.handler = handler;
        h.event_mask |= PollBits::ERR | PollBits::HUP;
        if (fds[i].events & POLLIN) h.event_mask |= PollBits::IN;
        if (fds[i].events & POLLOUT) h.event_mask |= PollBits::OUT;
        if (fds[i].events & POLLRDHUP) h.event_mask |= PollBits::RDHUP;
        
        fds[i].fd = -1;
        fds[i].revents = 0;
    }

    if (timeout != 0) {
        _wrapper->ScheduleSync([](void * param) {
                auto helper = (PollHelper *)param;
                
                if (dmci_orig_poll(helper->fds, helper->nfds, 0) > 0)
                    return STATE_ENABLED;
                
                for (auto && tp : helper->entries) {
                    auto && entry = get<1>(tp);
                    int rbits = entry.handler->Poll(entry.fd);
                    if (rbits & entry.event_mask) {
                        return STATE_ENABLED;
                    }
                }
                
                return STATE_DISABLED;
            }, nullptr, &helper,
            (timeout > 0) ? "ITP|poll_wait-timeout" : "ITP|poll_wait-forever", -1,
            OP_FLAG_WEAK);
    }

    ret = dmci_orig_poll(fds, nfds, 0);
    for (auto && tp : helper.entries) {
        auto && entry = get<1>(tp);
        int index = get<0>(tp);
        int rbits = entry.handler->Poll(entry.fd) & entry.event_mask;
        helper.fds[index].revents = 0;
        if (rbits & PollBits::IN) helper.fds[index].revents |= POLLIN;
        if (rbits & PollBits::OUT) helper.fds[index].revents |= POLLOUT;
        if (rbits & PollBits::ERR) helper.fds[index].revents |= POLLERR;
        if (rbits & PollBits::HUP) helper.fds[index].revents |= POLLHUP;
        if (rbits & PollBits::RDHUP) helper.fds[index].revents |= POLLRDHUP;
        if (helper.fds[index].revents) ++ret;
    }

    for (auto && kv : helper.entries) {
        helper.fds[get<0>(kv)].fd = get<1>(kv).fd;
    }

    return ret;
}

int dmci_inst___poll_chk(struct pollfd * fds, nfds_t nfds, int timeout, size_t fds_size) {
    return dmci_inst_poll(fds, nfds, timeout);
}
