#include <jni.h>
#include "dmc_wrapper_JNIWrapper.h"
#include "Wrapper/cpp/DMCWrapper.hpp"

static jfieldID nativeHandleField;

void JNICALL Java_dmc_wrapper_JNIWrapper_InstanceInitialize(JNIEnv * env, jobject self) {
    env->SetLongField(self, nativeHandleField, (jlong)NewDMCNodeInstance());
}

void JNICALL Java_dmc_wrapper_JNIWrapper_InstanceDestroy(JNIEnv * env, jobject self) {
    delete (DMC::Wrapper::INodeWrapper *)env->GetLongField(self, nativeHandleField);
}

void JNICALL Java_dmc_wrapper_JNIWrapper_LibraryInitialize(JNIEnv * env, jclass klass) {
    nativeHandleField = env->GetFieldID(klass, "_nativeHandle", "J");
}

jboolean JNICALL Java_dmc_wrapper_JNIWrapper_InitNode(JNIEnv * env, jobject jself, jstring jendpoint) {
    auto self = (DMC::Wrapper::INodeWrapper *)env->GetLongField(jself, nativeHandleField);
    const char * endpoint = env->GetStringUTFChars(jendpoint, nullptr);

    bool result = self->InitNode(endpoint, nullptr, nullptr);
    
    env->ReleaseStringUTFChars(jendpoint, endpoint);

    return result;
}


jint JNICALL Java_dmc_wrapper_JNIWrapper_RegisterNode(JNIEnv * env, jobject jself) {
    return -1;
}

void JNICALL Java_dmc_wrapper_JNIWrapper_StartNode(JNIEnv *, jobject jself, jint nodeId) {
}
