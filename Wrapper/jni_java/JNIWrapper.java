package dmc.wrapper;

public class JNIWrapper {
    static {
        System.loadLibrary("DMCCppWrapperJNI");
        LibraryInitialize();
    }

    private native void InstanceInitialize();
    private native void InstanceDestroy();
    private native static void LibraryInitialize();

    long _nativeHandle;

    public JNIWrapper() {
        InstanceInitialize();
    }

    public void Close() {
        InstanceDestroy();
    }

    public native boolean InitNode(int nodeId, String endpoint);
    public native int     RegisterNode();
    public native void    StartNode(int nodeId);
}
