package dmc.wrapper;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.Map;
import java.util.TreeMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;

import dmc.thrift.*;

interface IOperation {
    public boolean IsEnabled();
    public void Execute();
}

class SyncOperation implements IOperation {
    private IPredicate _isEnabled;
    private boolean _isScheduled;

    public SyncOperation(IPredicate isEnabled) {
        _isEnabled = isEnabled;
        _isScheduled = false;
    }

    @Override
    public boolean IsEnabled() {
        return _isEnabled.Evaluate();
    }

    @Override
    public void Execute() {
        _isScheduled = true;
    }

    public boolean IsScheduled() {
    	return _isScheduled;    
    }
}

class AsyncOperation implements IOperation {
    private TestcaseWrapper _wrapper;
    private IPredicate      _isEnabled;
    private IStub           _task;

    public AsyncOperation(TestcaseWrapper wrapper, IPredicate isEnabled, IStub task) {
        _wrapper = wrapper;
        _isEnabled = isEnabled;
        _task = task;
    }

    @Override
    public boolean IsEnabled() {
        return _isEnabled.Evaluate();
    }

    @Override
    public void Execute() {
        _task.Run();
    }
}

class RemoteOperationStub implements IOperation {
	private IStub _localStub;
	
	public RemoteOperationStub(IStub localStub) {
		_localStub = localStub;
	}
	
	@Override
	public boolean IsEnabled() { return true; }
	
	@Override
	public void Execute() { _localStub.Run(); }
}

public class TestcaseWrapper implements Runnable, ITestcaseWrapper {

    private TTransport     _transport;
    private Arbiter.Client _client;
    private String         _cookie;

    private int _nodeId;
    private int _opIdCounter;
    private int _detachedOpIdCounter;

    private boolean _isRunning = false;
    private boolean _init = false;

    private final Lock      _lock = new ReentrantLock();
    private final Condition _quiescentCV = _lock.newCondition();
    private final Condition _scheduleCV = _lock.newCondition();
    private final Condition _contextForkCV = _lock.newCondition();
    
    private final Lock      _logLock = new ReentrantLock();
    private boolean         _logBlocked = false;
    private final ArrayList<String> _logBuffer = new ArrayList<String>();
    
    private int _contextLevel;
    private int _activeContexts;

    private Thread _wrapperThread;

    private final TreeMap<Integer, IOperation> _detachedOperations = new TreeMap<Integer, IOperation>(); 
    private final TreeMap<Integer, IOperation> _newOperations = new TreeMap<Integer, IOperation>();
    private final TreeMap<Integer, IOperation> _operations = new TreeMap<Integer, IOperation>();
    private final TreeMap<Integer, IOperation> _disabledOperations = new TreeMap<Integer, IOperation>();
    private final TreeMap<Integer, Integer>    _remoteTarget = new TreeMap<Integer, Integer>();
    
    // In Target Context ========================================
    
    @Override
    public void InitNode(int nodeId) {
        if (_init) throw new RuntimeException("Double init");
        _init = true;
        _nodeId = nodeId;
        
        if (nodeId < 0) return;
        
        _isRunning = true;
        _activeContexts = 1;
        _contextLevel = 0;
        _detachedOpIdCounter = 0;

        _transport = new TSocket("localhost", 18390);
        try {
            _transport.open();
            _client = new Arbiter.Client(new TBinaryProtocol(_transport));
            _cookie = _client.InitNode(nodeId);
        }
        catch (Exception x) {
        	_isRunning = false;
            return;
        }

        _wrapperThread = new Thread(this);
        _wrapperThread.start();
    }
    
    @Override
    public void ScheduleSync(IPredicate isEnabled, boolean detached) {
        CheckNonStub();
        
        _lock.lock();
		if (_isRunning) {
	        SyncOperation sop = new SyncOperation(isEnabled);
	        
			int opId;
			if (detached) {
				opId = _detachedOpIdCounter++;
				_detachedOperations.put(opId, sop);
			} else {
				opId = _opIdCounter++;
				_newOperations.put(opId, sop);
			}
			
			while (_activeContexts == 0 && _isRunning) {
				try {
					_scheduleCV.await();
				} catch (InterruptedException e) {
					// Retry
				}
			}

			if (--_activeContexts == 0) {
				_quiescentCV.signal();
			}
			
			while (!sop.IsScheduled()) {
				try {
					_scheduleCV.await();
				} catch (InterruptedException e) {
					// Retry
				}
			}

			_lock.unlock();
		}
    }
    
    @Override
    public void ScheduleAsync(IPredicate isEnabled, IStub task, boolean detached) {
        CheckNonStub();

        _lock.lock();
        if (_isRunning) {
        	AsyncOperation aop = new AsyncOperation(this, isEnabled, task);
        	
			int opId;
			if (detached) {
				opId = _detachedOpIdCounter++;
				_detachedOperations.put(opId, aop);
			} else {
				opId = _opIdCounter++;
				_newOperations.put(opId, aop);
			}
		}
        else {
        	task.Run();
        }
        _lock.unlock();
    }
    
    @Override
    public int BeforeContextFork() {
        CheckNonStub();
        
        _lock.lock();
        int level;
        if (_isRunning) {
        	level = _contextLevel++;
        }
        else {
        	level = 0;
        }
        _lock.unlock();
        return level;
    }
    
    @Override
    public void AfterContextFork(int level) {
        CheckNonStub();
        
        _lock.lock();
		if (_isRunning) {
			while (_contextLevel > level && _isRunning) {
				try {
					_scheduleCV.await();
				} catch (Exception x) {
					// Retry
				}
			}
		}
        _lock.unlock();
    }
    
    @Override
    public void ContextDestroy() {
        CheckNonStub();
        
        _lock.lock();
        if (_isRunning) {
        	
        	while (_activeContexts == 0 && _isRunning) {
				try {
					_scheduleCV.await();
				} catch (InterruptedException e) {
					// Retry
				}
			}
        	
        	if (--_activeContexts == 0) {
                _quiescentCV.signal();
        	}
        }
        _lock.unlock();
    }
    
    @Override
    public void Join() {
        CheckNonStub();
        
        while (true) {
            try {
                _wrapperThread.join();
                break;
            }
            catch (InterruptedException x) {}
        }
    }
    
    @Override
    public int RegisterNewNode() {
    	CheckNonStub();
    	
		int ret;
		_lock.lock();
		if (_isRunning) {
			try {
				ret = _client.RegisterNewNode(_cookie);
			} catch (TException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				ret = -1;
			}
		} else {
			ret = -1;
		}
		_lock.unlock();
		return ret;
    }
    
    @Override
    public void ScheduleRemote(IStub localStub, int target) {
    	CheckNonStub();
    	
		_lock.lock();
		if (_isRunning) {
			RemoteOperationStub rop = new RemoteOperationStub(localStub);

			int opId = _opIdCounter++;
			_remoteTarget.put(opId, target);
			_newOperations.put(opId, rop);
		} else {
			localStub.Run();
		}
		_lock.unlock();
    }
    
    public void BlockLog() {
    	_logLock.lock();
    	_logBlocked = true;
    	_logLock.unlock();
    }
    
    public void UnblockLog(boolean sendToArbiter) {
    	_logLock.lock();
    	if (_logBuffer.size() > 0) {
    		if (sendToArbiter) {
    			// XXX
    		}
    		else {
    			for (String line : _logBuffer) {
    				System.err.print("Missed log: ");
    				System.err.println(line);
    			}
    		}
    	}
    	_logBlocked = false;
    	_logLock.unlock();
    }
    
    @Override
    public void SendLog(String log) {
    	_logLock.lock();
    	if (!_logBlocked) {
    		if (_isRunning) {
    			try {
					_client.SendLog(log);
				} catch (TException e) {
					// Should not happen
					e.printStackTrace();
				}
    		}
    		else {
    			System.err.print("Missing log:");
    			System.err.println(log);
    		}
    	}
    	else {
    		_logBuffer.add(log);
    	}
    	_logLock.unlock();
    }
    
    @Override
    public void SendLogLines(List<String> logLines) {
    	_logLock.lock();
    	if (!_logBlocked) {
    		if (_isRunning) {
    			try {
					_client.SendLogLines(logLines);
				} catch (TException e) {
					// Should not happen
					e.printStackTrace();
				}
    		}
    		else {
				for (String line : logLines) {
					System.err.print("Missing log:");
					System.err.println(line);
				}
    		}
    	}
    	else {
    		_logBuffer.addAll(logLines);
    	}
    	_logLock.unlock();
    }
    
    // In Wrapper Context ========================================

    @Override
    public void run() {
    	_lock.lock();
    	
        while (true) {
            
            while (_activeContexts > 0) {
                try {
                    _quiescentCV.await();
                }
                catch (Exception e) {}
            }
            
            // Handle context creation
            if (_contextLevel > 0) {
                ++_activeContexts;
                --_contextLevel;
                _contextForkCV.signal();
                _lock.unlock();
                continue;
            }

            IOperation op = null;
            int choice = -1;
            
            // Try to select from detached operations first
            for (Map.Entry<Integer, IOperation> kv : _detachedOperations.entrySet()) {
            	if (kv.getValue().IsEnabled()) {
            		op = kv.getValue();
            		choice = kv.getKey();
            		break;
            	}
            }

            if (op == null) {
	            ArrayList<Integer> toggledOps = new ArrayList<Integer>();
	            ArrayList<OperationDescriptionWithId> newOps = new ArrayList<OperationDescriptionWithId>();
	
	            for (Map.Entry<Integer, IOperation> kv : _newOperations.entrySet()) {
	                _operations.put(kv.getKey(), kv.getValue());
	                OperationDescriptionWithId desc = new OperationDescriptionWithId();
	                desc.id = kv.getKey();
	                desc.desc.fromNode = _nodeId;
	                desc.desc.text = kv.getKey().toString();
	                if (_remoteTarget.containsKey(kv.getKey())) {
	                    desc.desc.toNode = _remoteTarget.get(kv.getKey());
	                }
	                else {
	                    desc.desc.toNode = _nodeId;
	                }
	                newOps.add(desc);
	            }
	
	            _newOperations.clear();
	            for (Map.Entry<Integer, IOperation> kv : _operations.entrySet()) {
	                if (!kv.getValue().IsEnabled()) {
	                    toggledOps.add(kv.getKey());
	                }
	            }
	
	            for (Map.Entry<Integer, IOperation> kv : _disabledOperations.entrySet()) {
	                if (kv.getValue().IsEnabled()) {
	                    toggledOps.add(kv.getKey());
	                }
	            }
	
	            for (int id : toggledOps) {
	                if (_operations.containsKey(id)) {
	                    _disabledOperations.put(id, _operations.get(id));
	                    _operations.remove(id);
	                }
	                else {
	                    _operations.put(id, _disabledOperations.get(id));
	                    _disabledOperations.remove(id);
	                }
	            }
	            
	            BlockLog();
	
	            try {
	                _client.Report(_cookie,
	                               newOps,
	                               toggledOps,
	                               new ArrayList<Integer>(),
	                               new OperationDependency(),
	                               false);
	            }
	            catch (TException x) {
	            	_isRunning = false;
	            	UnblockLog(false);
	                break;
	            }
	            
	            ArbiterChoice arbiterChoice = null;
	
	            while (true) {
	                try {
	                    arbiterChoice = _client.Schedule(_cookie);
	                }
	                catch (TException x) {
	                    arbiterChoice.source = ChoiceSource.TO_EXIT;
	                }
	
	                if (arbiterChoice.source == ChoiceSource.REMOTE_STUB && 
	                    _remoteTarget.containsKey(arbiterChoice.choice)) {
	
	                	op = _operations.get(arbiterChoice.choice);
	                	op.Execute();
	                    _operations.remove(arbiterChoice.choice);
	                }
	                else break;
	                
	                // Flush log to remote
	                UnblockLog(true);
	                BlockLog();
	            }
	
	            if (arbiterChoice.source == ChoiceSource.REMOTE) {
	                _lock.lock();
	                ++_activeContexts;
	                _scheduleCV.signalAll();
	                _lock.unlock();
	                continue;
	            }
	            else if (arbiterChoice.source == ChoiceSource.TO_EXIT) {	            	
	            	_isRunning = false;
	            	UnblockLog(false);
	                break;
	            }
	
	            choice = arbiterChoice.choice;
	            op = _operations.get(choice);
	            _operations.remove(choice);
	        }
            else {
            	_detachedOperations.remove(choice);
            }

            op.Execute();

            ++_activeContexts;
            _scheduleCV.signalAll();
        }

        _transport.close();
        
        for (Collection<IOperation> ops : Arrays.asList(
        		_detachedOperations.values(),
        		_operations.values(),
        		_disabledOperations.values())) {
        	for (IOperation op : ops) {
        		op.Execute();
        	}
        }
        
        _scheduleCV.signalAll();
        _lock.unlock();
    }

    // In any context ===================================

    private void CheckNonStub() {
        if (Thread.currentThread() == _wrapperThread) {
            throw new RuntimeException("CheckNonStub failed");
        }
    }
}
