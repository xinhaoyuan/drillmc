package dmc.wrapper;

import java.util.List;

public interface ITestcaseWrapper {
    void  InitNode(int nodeId);
    int   RegisterNewNode();
    void  ScheduleSync(IPredicate isEnabled, boolean detached);
    void  ScheduleAsync(IPredicate isEnabled, IStub task, boolean detached);
    int   BeforeContextFork();
    void  AfterContextFork(int level);
    void  ContextDestroy();
    void  ScheduleRemote(IStub localStub, int nodeId);
    void  SendLog(String log);
    void  SendLogLines(List<String> logLines);
    void  Join();
}
