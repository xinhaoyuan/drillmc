#include "DMCWrapper.hpp"
#include "WrapperImpl.hpp"

DMC::Wrapper::NodeWrapperImpl _wrapper;

DMC::Wrapper::INodeWrapper * GetDMCNodeInstance() {
    return &_wrapper;
}

DMC::Wrapper::INodeWrapper * NewDMCNodeInstance() {
    return new DMC::Wrapper::NodeWrapperImpl();
}