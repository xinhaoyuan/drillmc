#ifndef __DMC_TESTCASE_WRAPPER_HPP__
#define __DMC_TESTCASE_WRAPPER_HPP__

namespace DMC {
    namespace Wrapper {

        // GetStateFunc will be called with wrapper lock held
        typedef int(*  GetStateFunc)(void * data);
        // Stub function will be called with wrapper lock held
        typedef void(* StubFunc)(int cancelled, void * data);
        // Command handler will be called in separated thread managed by thrift (for now)
        typedef void(* CommandHandlerFunc)(void * data, int fromNodeId, const char * message, int nbytes);
        
        // Flags for operations
        // Weak operation is ignored for deadlock condition (i.e. it's fine to leave weak operations as disabled in the end)
        const unsigned OP_FLAG_WEAK = 0x1;
        // Local detached operations are handled by each node internally and not exposed to explorer
        const unsigned OP_FLAG_LOCAL_DETACH = 0x2;
        // Not detached even under detach mode
        const unsigned OP_FLAG_NONDETACHABLE = 0x4;
        // Async job is executed in a seperate thread context instead of inside wrapper context. This is only for local async.
        const unsigned OP_FLAG_ASYNC_JOB = 0x8;

        // Flags for data dependencies
        const unsigned DEP_FLAG_READ = 0x1;
        const unsigned DEP_FLAG_WRITE = 0x2;

        // Operation states, returned by GetStateFunc
        // Enabled state means the operation can be scheduled without blocking internally (e.g. for a lock acquiring, it's enabled when the lock is available).
        const int STATE_ENABLED = 0x0;
        // Disabled state means the operation cannot be done without blocking (e.g. for a lock acquiring, it's enabled when the lock is already acquired).
        const int STATE_DISABLED = 0x1;
        // Cancelled state means the operation is semantically ignored, this state is unrecoverable.
        const int STATE_CANCELLED = 0x2;

        // Verbose level for client logging
        const int VERBOSE_ERROR = 0x0;
        const int VERBOSE_WARNING = 0x1;
        const int VERBOSE_DEFAULT = 0x1;
        const int VERBOSE_INFO = 0x02;
        const int VERBOSE_DEBUG = 0x03;

        // Use at least 64 bits for timestamps
        typedef unsigned long long timestamp_t;

        // Local resource id is monotonically increasingly assigned.
        typedef int local_res_t;
        // The wrapper itself take the Id 0
        const local_res_t wrapperId = 0;

        class IContextPtrStore {
        private:
            // Non-copyable
            IContextPtrStore(const IContextPtrStore &) = delete;
            IContextPtrStore & operator=(const IContextPtrStore &) = delete;
        public:
            IContextPtrStore() = default;
            virtual void * Get() = 0;
            virtual void Reset(void * ptr, void(*removeHook)(void * ptr)) = 0;
            virtual ~IContextPtrStore() = default;
        };

        class IContextPtrStoreFactory {
        public:
            virtual IContextPtrStore * Create() = 0;
            virtual ~IContextPtrStoreFactory() = default;
        };

        // A generic interface for target to expose concurrent
        // operations of a single node in distributed environemnt
        class INodeWrapper {
        public:
            virtual ~INodeWrapper() = default;
            
            // Local help functions for interception convenience
            // Wrapper locks
            virtual void Enter() = 0;
            virtual void Leave() = 0;

            virtual bool IsRunning() = 0;
            virtual bool IsConnected() = 0;
            virtual int  GetNodeId() = 0;
            virtual int  GetContextId() = 0;

            // Local resource && dependency management

            // All allocated id starts from 1. ids that <= 0 are ignored in ReleaseId
            // < 0 are used for GetIds for error

            virtual local_res_t GetIdForPtr(void * ptr, const char * decoration = nullptr, bool canExist = true, bool canCreate = true) = 0;
            virtual local_res_t GetIdForSymbol(const char * symbol, int nbytes = -1, bool canExist = true, bool canCreate = true) = 0;
            virtual local_res_t CreateUniqueId() = 0;
            // One can only release ids in user context (not in stub)!
            virtual void ReleaseId(local_res_t id) = 0;

            virtual void * GetLocalAttr(local_res_t id, const char * attrName) = 0;
            // TODO - Support for destructor
            virtual void   SetLocalAttr(local_res_t id, const char * attrName, void * attr) = 0;
            virtual void   DeleteLocalAttr(local_res_t id, const char * attrName) = 0;

            virtual int CreateGlobalUniqueId() = 0;
            
            virtual void BufferGetInfo(int id, bool & isOpen, int & length) = 0;
            virtual bool BufferOpen(int id) = 0;
            virtual bool BufferClose(int id) = 0;
            virtual int  BufferWrite(int id, const char * data, int len) = 0;
            virtual int  BufferRead(int id, char * outBuffer, int bufferLen) = 0;

            // The command handler for remote command.
            // Each command is executed in individual thread context so it's ok to be blocking.
            virtual void SetCommandHandler(CommandHandlerFunc commandHandler, void * data) = 0;
            // Register a new node in arbiter, return the global id. 
            virtual int  RegisterNewNode() = 0;
            virtual void Notify(int nodeId) = 0;
            virtual int  MessageSend(int nodeId = -1, const char * message = nullptr, int nbytes = -1) = 0;
            virtual void LocalYield() = 0;
            virtual void MessageReceive(bool fromRemote, int token) = 0;
            // A convenient message ack when you are sure there is only one message on the fly.
            virtual int  MessageReceiveUnique(bool fromRemote) = 0;
            // Init wrapper and connect to the arbiter
            virtual bool InitNode(const char * endpoint, const char * cookie, IContextPtrStore * cps) = 0;
            virtual bool NodeStart(int nodeId) = 0;
            // Synchronized operations. e.g. locks, conditional variables, etc. Returns whether this operation is cancelled.
            virtual bool ScheduleSync(GetStateFunc getStateFunc, StubFunc stub, void * data, const char * info = nullptr, int nbytes = -1, unsigned flags = 0) = 0;
            // Asynchronized operations. e.g. async IO, event handling, etc.
            virtual void ScheduleAsync(GetStateFunc getStateFunc, StubFunc stub, void * data, const char * info = nullptr, int nbytes = -1, unsigned flags = 0) = 0;
            // Ask for a integer data input within [0..range - 1]
            virtual int  Choose(int range) = 0;
            virtual void NodeShutdown() = 0;
            virtual void BeginDetachMode() = 0;
            virtual void EndDetachMode() = 0;
            virtual timestamp_t GetTimeOffsetNs(bool addDependency = true) = 0;
            virtual timestamp_t InsertTimeEventAfter(timestamp_t delayNs) = 0;
            virtual void InsertTimeEventAt(timestamp_t timeNs) = 0;

            virtual void ContextDetach() = 0;
            // Execute user-defined command in remote nodes.
            virtual void RemoteCommand(int toNodeId, const char * command) = 0;
            // Data dependency tracking
            virtual void AddDependency(int dependencyFlags, int id, const char * decoration = nullptr) = 0;
            virtual void AddGlobalDependency(int dependencyFlags, const char * name) = 0;
            // Access to the global state variables
            virtual void SetVariable(const char * name, const char * value) = 0;
            virtual const char * GetVariable(const char * name) = 0;
            // Diagnose
            virtual void CheckAssert(bool shouldBeTrue, const char * comment) = 0;
            virtual void SendLog(const char * log, int level = VERBOSE_INFO) = 0;
            virtual void SendLogLines(const char * headerLine, const char * const * logLines, int level = VERBOSE_INFO) = 0;
            virtual void SendStack(const char * desc = "", int level = VERBOSE_INFO) = 0;
            // End the iteration forcely
            virtual void End(int error, const char * comment) = 0; 
            // Wait for wrapper to finish
            virtual void Join() = 0;
        };

        // Check Develop/Wrapper for API details
    }
}

#ifdef _MSVC
#ifdef DMCCppWrapper_EXPORTS
#define __API __declspec(dllexport)
#else
#define __API __declspec(dllimport)
#endif
#else
#define __API extern
#endif

extern "C" {
    __API DMC::Wrapper::INodeWrapper * GetDMCNodeInstance();
    __API DMC::Wrapper::INodeWrapper * NewDMCNodeInstance();
}
    
#endif
