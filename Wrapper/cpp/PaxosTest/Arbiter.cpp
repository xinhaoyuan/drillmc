#include "Arbiter.hpp"
#include "Utils/cpp/Formatted.hpp"

#include <tuple>
#include <stdexcept>
#include <iostream>

using namespace std;
using namespace XYHelper;
using namespace Paxos;
using namespace DMC::Wrapper;

IProxy::IProxy() {
    _wrapper = NewDMCNodeInstance();
    _wrapper->InitNode(nullptr, nullptr, nullptr);
}

int IProxy::GetId () {
    throw logic_error("Unimplemented");
}

Proposer* IProxy::GetProposer() {
    throw logic_error("Unimplemented");
}

Acceptor* IProxy::GetAcceptor() {
    throw logic_error("Unimplemented");
}

Learner* IProxy::GetLearner() {
    throw logic_error("Unimplemented");
}

void IProxy::Report() {
}

ProposerProxy::ProposerProxy(int nId, IArbiter * arbiter)
    : _nId(nId), _arbiter(arbiter), _proposer(NULL) {
}

int ProposerProxy::GetId() {
    return _nId;
}

void ProposerProxy::Connect(Proposer * proposer) {
    _proposer = proposer;
    _wrapper->NodeStart(GetId());
    _wrapper->ContextDetach();
}

Proposer* ProposerProxy::GetProposer() {
    return _proposer;
}

void ProposerProxy::SendPrepare(const ProposalId & proposalId) {
    _arbiter->InsertPrepare(_wrapper, proposalId);
}

void ProposerProxy::SendAccept(const ProposalId & proposalId, int value) {
    _arbiter->InsertAccept(_wrapper, proposalId, value); 
}

AcceptorProxy::AcceptorProxy(int nId, IArbiter * arbiter)
    : _nId(nId), _arbiter(arbiter), _acceptor(NULL) {
}

int AcceptorProxy::GetId() {
    return _nId;
}

void AcceptorProxy::Connect(Acceptor * acceptor) {
    _acceptor = acceptor;
    _wrapper->NodeStart(GetId());
    _wrapper->ContextDetach();
}

Acceptor* AcceptorProxy::GetAcceptor() {
    return _acceptor;
}

void AcceptorProxy::SendPromise(int nId, const ProposalId & proposalId,
                                const ProposalId & prevProposalId, int value) {
    _arbiter->InsertPromise(_wrapper, nId, proposalId, prevProposalId, value);
}

void AcceptorProxy::SendAccepted(const ProposalId & proposalId, int value) {
    _arbiter->InsertAccepted(_wrapper, proposalId, value);
}

LearnerProxy::LearnerProxy(int nId, IArbiter * arbiter)
    : _nId(nId), _arbiter(arbiter), _learner(NULL), _proposalId(), _value(-1) {
}

int LearnerProxy::GetId() {
    return _nId;
}

void LearnerProxy::Connect(Learner * learner) {
    _learner = learner;
    _wrapper->NodeStart(GetId());
    _wrapper->ContextDetach();
}

Learner* LearnerProxy::GetLearner() {
    return _learner;
}

void LearnerProxy::OnResolution(const ProposalId & proposalId, int value) {
    _proposalId = proposalId;
    _value = value;
}

void LearnerProxy::Report() {
    _wrapper->SendLog(DMC_GENCSTR("Resolution: ", _value));
}

// == PaxosArbiter ==

PaxosArbiter::PaxosArbiter() : _wrapper(nullptr) {
}

PaxosArbiter::~PaxosArbiter() {
}

void PaxosArbiter::Setup() {
    _wrapper = GetDMCNodeInstance();
    _wrapper->InitNode(nullptr, nullptr, nullptr);
    _wrapper->NodeStart(0);
}

void PaxosArbiter::CleanUp() {
}

Proposer * PaxosArbiter::CreateProposer() {
    int nId = _wrapper->RegisterNewNode();
    _wrapper->MessageSend(nId, "CreateProposer");
    _wrapper->MessageSend(0, "Ack back");
    _wrapper->ContextDetach();

    ProposerProxy * proxy = new ProposerProxy(nId, this);
    Proposer * proposer = new Proposer(proxy, nId);
    _proxies[nId] = proxy;
    proxy->Connect(proposer);
    _proposerProxies.push_back(proxy);

    _wrapper->MessageReceiveUnique(true);

    return proposer;
}

Acceptor * PaxosArbiter::CreateAcceptor() {
    int nId = _wrapper->RegisterNewNode();
    _wrapper->MessageSend(nId, "CreateAcceptor");
    _wrapper->MessageSend(0, "Ack back");
    _wrapper->ContextDetach();

    AcceptorProxy * proxy = new AcceptorProxy(nId, this);
    Acceptor * acceptor = new Acceptor(proxy);
    _proxies[nId] = proxy;
    proxy->Connect(acceptor);
    _acceptorProxies.push_back(proxy);

    _wrapper->MessageReceiveUnique(true);

    return acceptor;
}

Learner * PaxosArbiter::CreateLearner() {
    int nId = _wrapper->RegisterNewNode();
    _wrapper->MessageSend(nId, "CreateLearner");
    _wrapper->MessageSend(0, "Ack back");
    _wrapper->ContextDetach();
    
    LearnerProxy * proxy = new LearnerProxy(nId, this);
    Learner * learner = new Learner(proxy);
    _proxies[nId] = proxy;
    proxy->Connect(learner);
    _learnerProxies.push_back(proxy);

    _wrapper->MessageReceiveUnique(true);

    return learner;
}

static void JoinThread(PaxosArbiter * arbiter) {
    arbiter->GetWrapper()->Join();
    arbiter->InsertTask(nullptr);
}

void PaxosArbiter::Start() {
    
    for (auto pxy : _proposerProxies) {
        auto task = new function<void()>([=]() {
            pxy->GetWrapper()->MessageReceiveUnique(true);
            pxy->GetProposer()->Prepare();
            pxy->GetWrapper()->ContextDetach();
        });

        _wrapper->MessageSend(pxy->GetId(), "Prepare");
        InsertTask(task);
    }
    
    _wrapper->ContextDetach();

    auto t = new thread(JoinThread, this);

    while (true) {
        auto task = GetTask();
        if (task == nullptr) break;
        (*task)();
        delete task;
    }

    t->join();
}

void PaxosArbiter::InsertPrepare(INodeWrapper * node, const ProposalId & proposalId) {
    for (auto pxy : _acceptorProxies) {
        
        auto task = new function<void()>([=]() {
            pxy->GetWrapper()->MessageReceiveUnique(true);
            pxy->GetAcceptor()->OnPrepare(node->GetNodeId(), proposalId);
            pxy->GetWrapper()->ContextDetach();
        });

        auto stub = new Helper::StubClosure([=](int cancelled) {
            node->MessageSend(pxy->GetId(), "OnPrepare");
            InsertTask(task);
        });

        node->ScheduleAsync(nullptr, &Helper::Stub, stub);
    }
}

void PaxosArbiter::InsertPromise(INodeWrapper * node, int toNId, const ProposalId & proposalId,
                                 const ProposalId & prevProposalId, int value) {
    auto pxy = _proxies[toNId];

    auto task = new function<void()>([=]() {
        pxy->GetWrapper()->MessageReceiveUnique(true);
        pxy->GetProposer()->OnPromise(node->GetNodeId(), proposalId, prevProposalId, value);
        pxy->GetWrapper()->ContextDetach();
    });

    auto stub = new Helper::StubClosure([=](int cancelled) {
        node->MessageSend(toNId, "OnPromise");
        InsertTask(task);
    });

    node->ScheduleAsync(nullptr, &Helper::Stub, stub);
}

void PaxosArbiter::InsertAccept(INodeWrapper * node, const ProposalId & proposalId, int value) {
    for (auto pxy : _acceptorProxies) {

        auto task = new function<void()>([=]() {
            pxy->GetWrapper()->MessageReceiveUnique(true);
            pxy->GetAcceptor()->OnAcceptRequest(node->GetNodeId(), proposalId, value);
            pxy->GetWrapper()->ContextDetach();
        });

        auto stub = new Helper::StubClosure([=](int cancelled) {
            node->MessageSend(pxy->GetId(), "OnAcceptRequest");
            InsertTask(task);
        });

        node->ScheduleAsync(nullptr, &Helper::Stub, stub);
    }
}

void PaxosArbiter::InsertAccepted(INodeWrapper * node, const ProposalId &proposalId, int value) {
    for (auto pxy : _learnerProxies) {

        auto task = new function<void()>([=]() {
            pxy->GetWrapper()->MessageReceiveUnique(true);
            pxy->GetLearner()->OnAccepted(node->GetNodeId(), proposalId, value);
            pxy->GetWrapper()->ContextDetach();
        });

        auto stub = new Helper::StubClosure([=](int cancelled) {
            node->MessageSend(pxy->GetId(), "OnAccepted");
            InsertTask(task);
        });

        node->ScheduleAsync(nullptr, &Helper::Stub, stub);
    }
}

void PaxosArbiter::InsertTask(function<void()> * task) {
    lock_guard<mutex> lock(_taskLock);
    _tasks.push_back(task);
    if (_tasks.size() == 1) 
        _taskCV.notify_all();
}

function<void()> * PaxosArbiter::GetTask() {
    unique_lock<mutex> lock(_taskLock);
    while (_tasks.size() == 0)
        _taskCV.wait(lock);
    auto task = _tasks.front();
    _tasks.pop_front();
    return task;
}
