#include "Utils/cpp/Formatted.hpp"

#include "Arbiter.hpp"

#include <list>
#include <exception>
#include <stdexcept>
#include <iostream>
#include <fstream>

#include <stdlib.h>
#include <string.h>

using namespace std;
using namespace DMC;
using namespace DMC::Wrapper;
using namespace Paxos;
using namespace XYHelper;

class SimpleTestcase {
private:
    int _numProposer;
    int _numAcceptor;
    int _numLearner;
    vector<Proposer *> proposers;
    vector<Acceptor *> acceptors;
    vector<Learner *>  learners;
    PaxosArbiter * _arbiter;
    
public:

    SimpleTestcase(int numProposer, int numAcceptor, int numLearner, PaxosArbiter* arbiter)
        : _numProposer(numProposer), _numAcceptor(numAcceptor),
          _numLearner(numLearner), _arbiter(arbiter) {
    }

    ~SimpleTestcase() {
        assert(proposers.size() == 0);
        assert(acceptors.size() == 0);
        assert(learners.size() == 0);
    }
    
    void Setup() {
        _arbiter->Setup();
        
        for (int i = 0; i < _numProposer; ++ i)
            proposers.push_back(_arbiter->CreateProposer());

        for (int i = 0; i < _numAcceptor; ++ i)
            acceptors.push_back(_arbiter->CreateAcceptor());

        for (int i = 0; i < _numLearner; ++ i)
            learners.push_back(_arbiter->CreateLearner());
    
        for (auto proposer : proposers)
            proposer->SetQuorumSize(_numAcceptor / 2 + 1);

        for (auto learner : learners)
            learner->SetQuorumSize(_numAcceptor / 2 + 1);

        for (int i = 0; i < _numProposer; ++i) {
            proposers[i]->SetProposal(i);
        }

        _arbiter->Start();
    }

    void CleanUp() {
        _arbiter->CleanUp();
    }
};

int main(int argc, char ** argv) {
    int numProposer = 2;
    int numAcceptor = 2;
    int numLearner = 2;    
    
    for (int i = 1; i < argc; ++ i) {
        if (!strcmp(argv[i], "-p") && i + 1 < argc) {
            numProposer = atoi(argv[i + 1]); ++i;
        }
        else if (!strcmp(argv[i], "-a") && i + 1 < argc) {
            numAcceptor = atoi(argv[i + 1]); ++i;
        }
        else if (!strcmp(argv[i], "-l") && i + 1 < argc) {
            numLearner = atoi(argv[i + 1]); ++i;
        }
    }

    auto arbiter = new PaxosArbiter();
    auto tc = new SimpleTestcase(numProposer, numAcceptor, numLearner, arbiter);

    tc->Setup();
    tc->CleanUp();

    return 0;    
}
