#ifndef __PAXOS_ARBITER_HPP__
#define __PAXOS_ARBITER_HPP__

#include "Utils/cpp/LWN.hpp"
#include "Actors.hpp"
#include "Wrapper/cpp/DMCWrapperEx.hpp"

#include <map>
#include <vector>
#include <tuple>
#include <list>
#include <deque>
#include <thread>
#include <mutex>
#include <condition_variable>

LWN_START(Paxos);

using namespace std;
using namespace DMC::Wrapper;

LWN_EXPORT;

class IArbiter {
public:
    virtual void Setup() = 0;
    virtual void CleanUp() = 0;

    virtual Proposer * CreateProposer() = 0;
    virtual Acceptor * CreateAcceptor() = 0;
    virtual Learner *  CreateLearner() = 0;

    virtual void InsertPrepare(INodeWrapper * node, const ProposalId & proposalId) = 0;
    virtual void InsertPromise(INodeWrapper * node, int toNId, const ProposalId & proposalId,
                               const ProposalId & prevProposalId, int value) = 0;
    virtual void InsertAccept(INodeWrapper * node, const ProposalId & proposalId, int value) = 0;
    virtual void InsertAccepted(INodeWrapper * node, const ProposalId & proposalId, int value) = 0;

    virtual ~IArbiter() = default;
};

class IProxy : public IMessenger {
protected:
    INodeWrapper * _wrapper;

public:
    IProxy();
    inline INodeWrapper * GetWrapper() { return _wrapper; }
    virtual int GetId();
    virtual Proposer * GetProposer();
    virtual Acceptor * GetAcceptor();
    virtual Learner *  GetLearner();
    virtual void Report();
    virtual ~IProxy() = default;
};

struct Msg {

    enum MsgType {
        PREPARE,
        PROMISE,
        ACCEPT,
        ACCEPTED
    };

public:
    int        mid;
    MsgType    mtype;
    int        fromNId, toNId;
    ProposalId proposalId[2];
    int        value;
};

class ProposerProxy : public IProxy {
private:
    int        _nId;
    IArbiter * _arbiter;
    Proposer * _proposer;
public:
    ProposerProxy(int nId, IArbiter * arbiter);
    void Connect(Proposer * proposer);
    int GetId() override;
    Proposer * GetProposer() override; 
    void SendPrepare(const ProposalId & proposalId) override;
    void SendAccept(const ProposalId & proposalId, int value) override;
};

class AcceptorProxy : public IProxy {
private:
    int        _nId;
    IArbiter * _arbiter;
    Acceptor * _acceptor;
public:
    AcceptorProxy(int nId, IArbiter * arbiter);
    void Connect(Acceptor * acceptor);
    int GetId() override;
    Acceptor *GetAcceptor() override;
    void SendPromise(int nId, const ProposalId & proposalId,
                     const ProposalId & prevProposalId, int value) override;
    void SendAccepted(const ProposalId & proposalId, int value) override;
};

class LearnerProxy : public IProxy {
private:
    int         _nId;
    IArbiter  * _arbiter;
    Learner   * _learner;
    ProposalId  _proposalId;
    int         _value;
public:
    LearnerProxy(int nId, IArbiter * arbiter); 
    void Connect(Learner * learner);
    int GetId() override;
    Learner * GetLearner() override;
    void OnResolution(const ProposalId & proposalId, int value) override;
    void Report() override;
};

class PaxosArbiter : public IArbiter {
private:

    int            _nIdCounter;
    int            _mIdCounter;
    list<IProxy *> _proposerProxies;
    list<IProxy *> _acceptorProxies;
    list<IProxy *> _learnerProxies;
    map<int, IProxy *> _proxies;

    vector<int>         _currentTrace;
    vector<vector<int>> _traces;

    INodeWrapper * _wrapper;

    mutex _taskLock;
    condition_variable _taskCV;
    deque<function<void()> *> _tasks;

public:

    PaxosArbiter();
    ~PaxosArbiter();

    inline INodeWrapper * GetWrapper() { return _wrapper; }
    
    void Setup() override;
    void CleanUp() override;
    
    Proposer * CreateProposer() override;
    Acceptor * CreateAcceptor() override;
    Learner *  CreateLearner() override;

    void Start();

    void InsertPrepare(INodeWrapper * node, const ProposalId & proposalId) override;
    void InsertPromise(INodeWrapper * node, int toNId, const ProposalId & proposalId,
                       const ProposalId & prevProposalId, int value) override;
    void InsertAccept(INodeWrapper * node, const ProposalId & proposalId, int value) override;
    void InsertAccepted(INodeWrapper * node, const ProposalId & proposalId, int value) override;

    void InsertTask(function<void()> * task);
    function<void()> * GetTask();
};

LWN_END;

#endif
