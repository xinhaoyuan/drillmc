#ifndef __PAXOS_ACTORS_HPP__
#define __PAXOS_ACTORS_HPP__

#include "Utils/cpp/LWN.hpp"
#include <cassert>
#include <set>
#include <map>
#include <array>
#include <stdexcept>

LWN_START(Paxos);

using namespace std;

LWN_EXPORT;

// A proposal Id is a comparable object combined proposal number with
// node id.

class ProposalId {
private:
    int _nodeId;
    int _proposalNumber;

public:
    inline ProposalId() : _nodeId(-1), _proposalNumber(-1) {
    }
    
    inline ProposalId(int proposalNumber, int nodeId) :
        _proposalNumber(proposalNumber), _nodeId(nodeId) {
        assert(proposalNumber >= 0);
    }

    inline bool operator < (const ProposalId &other) const {
        if (_proposalNumber < other._proposalNumber) return true;
        else if (_proposalNumber > other._proposalNumber) return false;
        else return (_nodeId < other._nodeId);
    }

    inline bool operator == (const ProposalId &other) const {
        return
            (_proposalNumber == other._proposalNumber) &&
            (_nodeId == other._nodeId);
    }

    inline bool operator > (const ProposalId &other) const {
        if (_proposalNumber > other._proposalNumber) return true;
        else if (_proposalNumber < other._proposalNumber) return false;
        else return (_nodeId > other._nodeId);
    }

    inline bool operator <= (const ProposalId &other) const {
        return !(*this > other);
    }

    inline bool operator >= (const ProposalId &other) const {
        return !(*this < other);
    }

    inline bool IsValid() const {
        return _proposalNumber >= 0;
    }

    inline tuple<int, int> GetRawId() const {
        return make_tuple(_proposalNumber, _nodeId); 
    }
};

class IMessenger {
public:
    virtual void SendPrepare(const ProposalId & proposalId);
    virtual void SendPromise(int nodeId,
                             const ProposalId & proposalId,
                             const ProposalId & prevProposalId,
                             int acceptedValue);
    virtual void SendAccept(const ProposalId & proposalId,
                            int acceptedValue);
    virtual void SendAccepted(const ProposalId & proposalId,
                              int acceptedValue);
    virtual void OnResolution(const ProposalId & proposalId,
                              int value);
    virtual ~IMessenger() = default;
};

class Proposer {
public:
    Proposer(IMessenger * messenger, int nodeId);

    void SetQuorumSize(int quorumSize);
    void SetProposal(int value);
    void Prepare();
    void OnPromise(int fromNId,
                   const ProposalId & proposalId, const ProposalId & prevProposalId,
                   int prevAcceptedValue);
    
private:
    int             _quorumSize;
    int             _nodeId;
    int             _nextProposalNumber;
    ProposalId      _proposalId;
    ProposalId      _lastAcceptedId;
    int             _proposedValue;
    IMessenger *    _messenger;
    set<int>        _promisesRecieved;
};

class Acceptor {
public:
    Acceptor(IMessenger * messenger);

    void OnPrepare(int fromNId,
                   const ProposalId & proposalId);
    void OnAcceptRequest(int fromNId,
                         const ProposalId & proposalId,
                         int value);
    
private:
    ProposalId   _promisedId;
    IMessenger * _messenger;
    ProposalId   _acceptedId;
    int          _acceptedValue;
};

class Learner {
public:
    Learner(IMessenger * messenger);
    void SetQuorumSize(int quorumSize);

    void OnAccepted(int fromNId,
                    const ProposalId & proposalId,
                    int value);
private:
    int                                 _quorumSize;
    map<tuple<int, int>, array<int, 3>> _proposals;
    map<int, ProposalId>                _acceptors;
    int                                 _finalValue;
    ProposalId                          _finalProposalId;
    IMessenger *                        _messenger;
};

LWN_END;

#endif
