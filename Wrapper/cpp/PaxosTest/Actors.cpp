#include "Actors.hpp"
#include <iostream>

using namespace std;
using namespace Paxos;

// == IMessenger ==

void IMessenger::SendPrepare(const ProposalId & proposalId) {
    throw logic_error("Unimplemented");
}

void IMessenger::SendPromise(int nodeId,
                             const ProposalId & proposalId,
                             const ProposalId & prevProposalId,
                             int acceptedValue) {
    throw logic_error("Unimplemented");
}

void IMessenger::SendAccept(const ProposalId & proposalId,
                            int acceptedValue) {
    throw logic_error("Unimplemented");
}

void IMessenger::SendAccepted(const ProposalId & proposalId,
                              int acceptedValue) {
    throw logic_error("Unimplemented");
}

void IMessenger::OnResolution(const ProposalId & proposalId,
                              int value) {
    throw logic_error("Unimplemented");
}

// == Proposer ==

Proposer::Proposer(IMessenger * messenger, int nodeId) {
    _proposedValue = -1;
    _nextProposalNumber = 0;
    _messenger = messenger;
    _nodeId = nodeId;
}

void Proposer::SetQuorumSize(int quorumSize) {
    _quorumSize = quorumSize;
}

void Proposer::SetProposal(int value) {
    if (_proposedValue < 0)
        _proposedValue = value;
}

void Proposer::Prepare() {
    _promisesRecieved.clear();
    _proposalId = ProposalId(_nextProposalNumber, _nodeId);
    ++ _nextProposalNumber;
    _messenger->SendPrepare(_proposalId);
}

void Proposer::OnPromise(int fromNId,
                         const ProposalId & proposalId,
                         const ProposalId & prevAcceptedId,
                         int prevAcceptedValue) {
    // We ignore the comparision here {{{
    if (!(proposalId == _proposalId) ||
        _promisesRecieved.find(fromNId) != end(_promisesRecieved))
        return;
    
    _promisesRecieved.insert(fromNId);
    // }}}
    
    if (prevAcceptedId.IsValid() &&
        (!_lastAcceptedId.IsValid() ||
         prevAcceptedId > _lastAcceptedId)) {
        _lastAcceptedId = prevAcceptedId;

        if (prevAcceptedValue >= 0)
            _proposedValue = prevAcceptedValue;

    }

    if (_promisesRecieved.size() == _quorumSize) {
        if (_proposedValue >= 0) {
            _messenger->SendAccept(_proposalId, _proposedValue);
        }
    }
}

// == Acceptor ==

Acceptor::Acceptor(IMessenger * messenger) {
    _messenger = messenger;
    _acceptedValue = -1;
}

void Acceptor::OnPrepare(int fromNId,
                         const ProposalId & proposalId) {
    assert(proposalId.IsValid());
    if (_promisedId.IsValid() && proposalId == _promisedId) {
        _messenger->SendPromise(fromNId, proposalId, _acceptedId, _acceptedValue);
    }
    else if (!_promisedId.IsValid() || proposalId > _promisedId) {
        _promisedId = proposalId;
        _messenger->SendPromise(fromNId, proposalId, _promisedId, _acceptedValue);
    }
}

void Acceptor::OnAcceptRequest(int fromNId,
                               const ProposalId & proposalId,
                               int value) {
    if (!_promisedId.IsValid() || proposalId >= _promisedId) {
        _promisedId = proposalId;
        _acceptedId = proposalId;
        _acceptedValue = value;
        _messenger->SendAccepted(proposalId, _acceptedValue);
    }
}

// == Learner ==

Learner::Learner(IMessenger * messenger) {
    _messenger = messenger;
    _finalValue = -1;
}

void Learner::SetQuorumSize(int quorumSize) {
    _quorumSize = quorumSize;
}

void Learner::OnAccepted(int fromNId,
                         const ProposalId & proposalId,
                         int acceptedValue) {
    if (_finalValue >= 0)
        return;

    ProposalId lastProposalId = _acceptors[fromNId];
    if (proposalId <= lastProposalId)
        return;

    _acceptors[fromNId] = proposalId;
    if (lastProposalId.IsValid()) {
        auto oldp = _proposals[lastProposalId.GetRawId()];
        oldp[1] -= 1;
        if (oldp[1] == 0) {
            _proposals.erase(lastProposalId.GetRawId()); 
        }
    }

    if (_proposals.find(proposalId.GetRawId()) == end(_proposals)) { 
        _proposals[proposalId.GetRawId()] = std::array<int, 3>{ {0, 0, acceptedValue} };        
    }

    auto &&p = _proposals[proposalId.GetRawId()];
    p[0] ++;
    p[1] ++;

    if (p[0] == _quorumSize) {
        _finalValue = acceptedValue;
        _finalProposalId = proposalId;
        _proposals.clear();
        _acceptors.clear();

        _messenger->OnResolution(proposalId, acceptedValue);
    }
}
