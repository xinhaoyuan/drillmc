#include "DMCWrapper.hpp"

using namespace DMC;
using namespace DMC::Wrapper;

#define DMC_WRAPPER_IMPL
typedef INodeWrapper * dmc_wrapper_t;
#include "DMCWrapper.h"

dmc_wrapper_t dmc_wrapper_get_default() {
    return GetDMCNodeInstance();
}

dmc_wrapper_t dmc_wrapper_create() {
    return NewDMCNodeInstance();
}

void dmc_wrapper_destroy(dmc_wrapper_t wrapper) {
    delete wrapper;
}

class ContextPtrStore : public IContextPtrStore {
private:
    context_ptr_store_ops_s * _ops;
    void * _ptrStore;
public:
    ContextPtrStore(context_ptr_store_ops_s * ops, void * ptr_store)
        : _ops(ops)
        , _ptrStore(ptr_store) {
    }
    
    void * Get() override {
        return _ops->get(_ptrStore);
    }

    void Reset(void * ptr, void(* removeHook)(void *)) override {
        return _ops->reset(_ptrStore, ptr, removeHook);
    }
};

int dmc_wrapper_is_connected(dmc_wrapper_t wrapper) {
    return wrapper->IsConnected();
}

int dmc_wrapper_init_node(dmc_wrapper_t wrapper, const char * endpoint, const char * cookie, struct context_ptr_store_ops_s * cps_ops, void * cps) {
    ContextPtrStore * cpsObj = cps_ops == nullptr ?
        nullptr : new ContextPtrStore(cps_ops, cps);
    int ret = wrapper->InitNode(endpoint, cookie, cpsObj);
    return ret;
}

int dmc_wrapper_node_start(dmc_wrapper_t wrapper, int node_id) {
    return wrapper->NodeStart(node_id);
}

int dmc_wrapper_get_node_id(dmc_wrapper_t wrapper) {
    return wrapper->GetNodeId();
}

const char * dmc_wrapper_get_variable(dmc_wrapper_t wrapper, const char * name) {
    return wrapper->GetVariable(name);
}

void dmc_wrapper_set_variable(dmc_wrapper_t wrapper, const char * name, const char * value) {
    wrapper->SetVariable(name, value);
}

int dmc_wrapper_message_send(dmc_wrapper_t wrapper, int node_id, const char * desc, int nbytes) {
    return wrapper->MessageSend(node_id, desc, nbytes);
}

void dmc_wrapper_message_receive(dmc_wrapper_t wrapper, int remote, int token) {
    wrapper->MessageReceive(remote, token);
}

int dmc_wrapper_message_receive_unique(dmc_wrapper_t wrapper, int remote) {
    return wrapper->MessageReceiveUnique(remote);
}

int dmc_wrapper_schedule_sync(dmc_wrapper_t wrapper, get_state_f state_func, stub_f stub_func, void * data, const char * desc, int nbytes, unsigned flags) {
    return wrapper->ScheduleSync(state_func, stub_func, data, desc, nbytes, flags);
}

void dmc_wrapper_schedule_async(dmc_wrapper_t wrapper, get_state_f state_func, stub_f stub_func, void * data, const char * desc, int nbytes, unsigned flags) {
    return wrapper->ScheduleAsync(state_func, stub_func, data, desc, nbytes, flags);
}

void dmc_wrapper_context_detach(dmc_wrapper_t wrapper) {
    wrapper->ContextDetach();
}

void dmc_wrapper_send_log(dmc_wrapper_t wrapper, const char * log, int level) {
    wrapper->SendLog(log, level);
}

void dmc_wrapper_send_stack(dmc_wrapper_t wrapper, const char * message, int level) {
    wrapper->SendStack(message, level);
}

dmc_timestamp_t dmc_wrapper_get_time_offset_ns(dmc_wrapper_t wrapper) {
    return wrapper->GetTimeOffsetNs();
}

void dmc_wrapper_insert_time_event_at(dmc_wrapper_t wrapper, dmc_timestamp_t ts) {
    wrapper->InsertTimeEventAt(ts);
}

void dmc_wrapper_insert_time_event_after(dmc_wrapper_t wrapper, dmc_timestamp_t ts_after) {
    wrapper->InsertTimeEventAfter(ts_after);
}

void dmc_wrapper_check_assert(dmc_wrapper_t wrapper, int should_be_non_zero, const char * message) {
    wrapper->CheckAssert(should_be_non_zero != 0, message);
}

void dmc_wrapper_node_shutdown(dmc_wrapper_t wrapper) {
    wrapper->NodeShutdown();
}

void dmc_wrapper_end(dmc_wrapper_t wrapper, int error, const char * comment) {
    wrapper->End(error, comment);
}

void dmc_wrapper_join(dmc_wrapper_t wrapper) {
    wrapper->Join();
}
