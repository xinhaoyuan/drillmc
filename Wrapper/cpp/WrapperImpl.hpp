#ifndef __DMC_TESTCASE_WRAPPER_IMPL_HPP__
#define __DMC_TESTCASE_WRAPPER_IMPL_HPP__

#include <boost/thread/tss.hpp>
#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/transport/TSocket.h>
#include <thrift/transport/TTransportUtils.h>
#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/server/TSimpleServer.h>
#include <thrift/server/TThreadPoolServer.h>
#include <thrift/server/TThreadedServer.h>
#include <thrift/transport/TServerSocket.h>
#include "thrift/gen-cpp/Arbiter.h"
#include "thrift/gen-cpp/Controller.h"
#include "DMCWrapper.hpp"
#include "Utils/cpp/ThreadPool.hpp"
#include <mutex>
#include <condition_variable>
#include <map>
#include <thread>
#include <set>
#include <string>
#include <deque>
#include <stack>
#include <random>
#include <regex>

namespace DMC {
    namespace Wrapper {

        namespace Impl {
            class IOperation {
            public:
                virtual ~IOperation() = default;
                // Will be called with Wrapper lock hold.
                virtual int  GetState() = 0;
                virtual void Execute() = 0;
                virtual void Describe(std::string & outDesc) = 0;
                virtual bool IsWeak() = 0;
            };

            class SyncOperation;
            class AsyncOperation;
        }

        class NodeWrapperImpl;

        enum ContextType {
            Normal,
            WrapperLoop,
            Stub,
            PureFunc
        };

        struct ContextInfo {
            bool managed;
            ContextType contextType;
            int wrapperLockLevel;
            std::unique_lock<std::mutex> wrapperLock;

            std::deque<Impl::IOperation *> offlineOperationQueue;

            // Directly reading this info is invalid, should call GetContextId
            int  contextId;

            ContextInfo(NodeWrapperImpl * wrapper);
        };

        class WrapperController : public DMC::Thrift::ControllerIf {
        private:
            NodeWrapperImpl * _wrapper;

        public:
            WrapperController(NodeWrapperImpl * wrapper)
                : _wrapper(wrapper) 
            { }

            void Schedule(const DMC::Thrift::ArbiterChoice & choice) override;
            void ExecuteCommand(const int fromNodeId, const std::string & command) override;
        };

        class BoostSpecificPtrStore : public IContextPtrStore {

            class PtrWithDtorWrapper {
            private:
                void * _ptr;
                void(* _dtor)(void * ptr);
            public:
                void * Get() { return _ptr; }
                PtrWithDtorWrapper(void * ptr, void(*dtor)(void *))
                    : _ptr(ptr), _dtor(dtor) { }
                ~PtrWithDtorWrapper() { _dtor(_ptr); }
            };

            boost::thread_specific_ptr<PtrWithDtorWrapper> _spec;

        public:

            BoostSpecificPtrStore() = default;

            void * Get() override {
                auto ret = _spec.get();
                if (ret == nullptr) return nullptr;
                else return ret->Get();
            }

            void Reset(void * ptr, void(*dtor)(void *)) override {
                _spec.reset(ptr == nullptr ? nullptr : new PtrWithDtorWrapper(ptr, dtor));
            }
        };


        class NodeWrapperImpl : public INodeWrapper {
        private:

            XYHelper::ThreadPool _asyncJobPool;
            // For thread pool
            static void BeforeAsyncJob(void * param);
            static void AfterAsyncJob(void * param);

            int  _nodeId;

            bool _isRunning;
            bool _isConnected;

            int  _opIdCounter;
            int  _detachedOpIdCounter;
            int  _contextIdCounter;
            int  _globalNodeCounter;
            int  _globalIdCounter;
            int  _globalMessageTokenCounter;
            int64_t _localRandomSeed;
            std::mt19937_64 _localRandom;
            std::string  _localFilter;
            std::regex * _localFilterRegEx;
            int  _externalWeight;

            timestamp_t _nodeClock;
            std::set<timestamp_t> _timeEvents;
            timestamp_t _startSystemTime;
            int _advanceClockOpId;

            std::mutex               _logLock;
            int                      _logVerboseLevel;
            bool                     _logBlocked;
            std::vector<std::string> _logBuffer;

            bool _ignoreTokenMatching;
            ContextInfo             * _wrapperThreadInfo;
            IContextPtrStore        * _localInfoPtrStore;
            BoostSpecificPtrStore     _defaultCPS;

            friend struct ContextInfo;
            friend class LockGuard;

            std::mutex _lock;
            int        _activeContexts;
            std::condition_variable _quiescentCV;
            std::condition_variable _transferCV;

            // Pri 0 - local messages
            int  _localTokenQueueHead;
            int  _localTokenQueueTail;
            // We need this to ensure every token is taken timely
            int  _pendingLocalToken;
            // Pri 1 - detached local ops
            std::map<int, Impl::IOperation *> _detachedLocalOps;
            bool _detachMode;
            // Pri 2 - detach mode barrier
            Impl::IOperation * _detachModeBarrierOp;

            // Pri (><) - modelled ops
            std::map<int, Impl::IOperation *> _newOperations;
            std::map<int, Impl::IOperation *> _operations;
            std::map<int, Impl::IOperation *> _disabledOps;
            std::vector<std::tuple<int, int, std::string>> _remoteMessages;
            std::set<int> _notify;
            std::map<int, int> _remote;
            int  _pendingRemoteToken;
            bool _pendingRemoteTokenIsUnique;
            std::thread * _wrapperThread;
            std::thread * _controllerThread;
            bool _isError;
            bool _isEnd;
            bool _printMissedLog;
            bool _verboseStackInfo;
            std::string _lastError;

            boost::shared_ptr<apache::thrift::transport::TTransport> _arbiterTransport;
            std::string         _arbiterCookie;
            bool                _forceExit;
            Thrift::ArbiterIf * _arbiter;

            // Local resource meta-data {{
            local_res_t _localIdCounter;

            std::map<std::string, local_res_t> _idMap;
            std::map<local_res_t, std::string> _idDesc;

            std::map<local_res_t, std::map<std::string, void *>> _localAttr;
            // }}

            boost::shared_ptr<apache::thrift::transport::TServerSocket> _controllerSocket;
            apache::thrift::server::TServer * _controllerServer;

            DMC::Thrift::OperationDependency _currentDependency;

            std::ostream * _miscLog;

            std::string _variableValue;

            friend class Impl::SyncOperation;
            friend class Impl::AsyncOperation;
            friend class WrapperController;

            void(*_commandHandler)(void * data, int fromNodeId, const char * command, int nbytes);
            void * _commandHandlerData;

            // These predicates are thread-safe without lock {{
            bool InWrapper();
            bool InStub();
            bool IsPure();
            // }}

            // Invariant:
            //  - Return value == !_isError
            //  - If it returns true, then InStub() || !_isRunning || _activeContexts == 1 holds
            //  - If requireManaged is true, then InStub() || GetContextInfo()->managed == true also holds (will not set managed)
            bool LockedEnsureFocus(bool requireManaged = false);

            ContextInfo * GetContextInfo();

            static void WrapperThreadEntryHelper(NodeWrapperImpl * self);
            static void ControllerThreadEntryHelper(NodeWrapperImpl * self);

            void ControllerThreadEntry();
            void WrapperThreadEntry();

            std::mutex              _choiceLock;
            bool                    _choiceAvailable;
            std::condition_variable _choiceCV;
            Thrift::ArbiterChoice   _choice;

            void LockedReport(int inputRange);
            void LockedWaitSchedule(Thrift::ArbiterChoice & outChoice);
            void LockedUpdateLocalMetadata(const Thrift::ArbiterChoice & choice);

            void LockedSendLog(const char * log, int level = VERBOSE_INFO);
            void LockedSendLogLines(const char * headerLine, const char * const * logLines, int level = VERBOSE_INFO);

            void LockedReportEnd(bool isError, const char * message);
            void LockedSetEnd(bool isError);

            void LockedReportShutdown();

            void ForceExit();

            void HandleAsyncOpInAppContext(Impl::IOperation * op);

            void GetStack(int skip, std::vector<std::string> & outStack);

            void Lock();
            void Unlock();

            timestamp_t GetSystemTimestamp();

        public:

            NodeWrapperImpl();

            void Enter() override;
            void Leave() override;

            bool IsRunning() override;
            bool IsConnected() override;
            int  GetNodeId() override;
            int  GetContextId() override;

            local_res_t GetIdForPtr(void * ptr, const char * decoration, bool canExist, bool canCreate) override;
            local_res_t GetIdForSymbol(const char * symbol, int nbytes, bool canExist, bool canCreate) override;
            local_res_t CreateUniqueId() override;
            void ReleaseId(local_res_t id) override;

            void * GetLocalAttr(local_res_t id, const char * attrName) override;
            void   SetLocalAttr(local_res_t id, const char * attrName, void * attr) override;
            void   DeleteLocalAttr(local_res_t id, const char * attrName) override;

            int CreateGlobalUniqueId() override;

            void BufferGetInfo(int id, bool & isOpen, int & length) override;
            bool BufferOpen(int id) override;
            bool BufferClose(int id) override;
            int  BufferWrite(int id, const char * data, int len) override;
            int  BufferRead(int id, char * outBuffer, int bufferLen) override;

            void SetCommandHandler(void(* commandHandler)(void *, int, const char *, int), void * data) override;

            int  RegisterNewNode() override;
            void Notify(int nodeId) override;
            int  MessageSend(int nodeId, const char * message, int nbytes) override;
            void LocalYield() override;
            void MessageReceive(bool fromRemote, int token) override;
            int  MessageReceiveUnique(bool fromRemote) override;
            
            bool InitNode(const char * endpoint, const char * cookie, IContextPtrStore * cps) override;
            bool NodeStart(int nodeId) override;

            bool ScheduleSync(GetStateFunc getStateFunc, StubFunc stub, void * data, const char * info, int nbytes, unsigned flags) override;
            void ScheduleAsync(GetStateFunc getStateFunc, StubFunc stub, void * data, const char * info, int nbytes, unsigned flags) override;
            int  Choose(int choice) override;
            void NodeShutdown() override;

            void BeginDetachMode() override;
            void EndDetachMode() override;

            timestamp_t GetTimeOffsetNs(bool addDependency) override;
            timestamp_t InsertTimeEventAfter(timestamp_t delayNs) override;
            void InsertTimeEventAt(timestamp_t timeNs) override;

            void ContextDetach() override;

            void RemoteCommand(int toNodeId, const char * command) override;

            void AddDependency(int dependencyFlags, int id, const char * decoration) override;
            void AddGlobalDependency(int dependencyFlags, const char * name) override;

            void SetVariable(const char * name, const char * value) override;
            const char * GetVariable(const char * name) override;

            void CheckAssert(bool shouldBeTrue, const char * comment) override;

            void SendLog(const char * log, int level) override;
            void SendLogLines(const char * headerLine, const char * const * logLines, int level) override;
            void SendStack(const char * desc, int level) override;

            void End(int error, const char * comment) override;
            void Join() override;
        };

        class LockGuard {
            NodeWrapperImpl * _wrapper;
        public:
            inline LockGuard(NodeWrapperImpl * wrapper)
                : _wrapper(wrapper)
            {
                _wrapper->Lock();
            }

            inline ~LockGuard() {
                _wrapper->Unlock();
            }
        };

        namespace Impl {
            class SyncOperation : public IOperation {
                NodeWrapperImpl * _wrapper;
                bool   _scheduled;
                std::condition_variable _scheduleCV;
                int(*_getStateFunc)(void *);
                void * _data;
                bool   _internal;
                bool   _cancelled;
                int    _opId;
                int    _contextId;
                std::string _info;
                bool   _weak;

            public:
                inline SyncOperation(NodeWrapperImpl * wrapper, int(*getStateFunc)(void *), void * data, const char * info, int nbytes, bool weak)
                    : _wrapper(wrapper)
                    , _scheduled(false)
                    , _getStateFunc(getStateFunc)
                    , _data(data)
                    , _internal(true)
                    , _cancelled(false)
                    , _contextId(-1)
                    , _opId(-1)
                    , _weak(weak)
                {
                    if (info == nullptr)
                        _info = "";
                    else if (nbytes < 0)
                        _info.assign(info);
                    else _info.assign(info, nbytes);
                }

                inline void SetDescription(bool isInternal, int contextId, int opId) {
                    _internal = isInternal;
                    _contextId = contextId;
                    _opId = opId;
                }

                int  GetState() override;
                void Execute() override;
                void Describe(std::string & outDesc) override;
                bool IsWeak() override;

                void LockedWait();
            };

            class AsyncOperation : public IOperation {
                NodeWrapperImpl * _wrapper;
                GetStateFunc _getState;
                StubFunc     _stub;
                void *       _data;
                bool         _cancelled;
                bool         _internal;
                int          _opId;
                std::string  _info;
                bool         _job;
                bool         _weak;

                static void RunAsJob(void * param);

            public:
                inline AsyncOperation(NodeWrapperImpl * wrapper,
                    GetStateFunc getState, StubFunc stub, void * data,
                    const char * info, int nbytes,
                    bool job, bool weak)
                    : _wrapper(wrapper)
                    , _getState(getState)
                    , _stub(stub)
                    , _data(data)
                    , _internal(true)
                    , _cancelled(false)
                    , _opId(-1)
                    , _job(job)
                    , _weak(weak)
                {
                    if (info == nullptr)
                        _info = "";
                    else if (nbytes < 0)
                        _info.assign(info);
                    else _info.assign(info, nbytes);
                }

                inline void SetDescription(bool isInternal, int opId) { _internal = isInternal; _opId = opId; }

                int  GetState() override;
                void Execute() override;
                void Describe(std::string & outDesc) override;
                bool IsWeak() override;
            };
        }
    }
}

#endif
