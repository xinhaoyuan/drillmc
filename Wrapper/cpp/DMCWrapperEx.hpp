#ifndef __DMC_TESTCASE_WRAPPER_EX_HPP__
#define __DMC_TESTCASE_WRAPPER_EX_HPP__

// DMC wrapper related helper definitions

#include "DMCWrapper.hpp"

#define DMC_STRINGIFY(a) #a
#define DMC_CODE_LOCATION(f,l) f ":" DMC_STRINGIFY(l)
#define DMC_CODE_HERE DMC_CODE_LOCATION(__FILE__, __LINE__)

#include <string>
#include <sstream>
#include <tuple>
#include <functional>
#include <map>

namespace DMC {
    namespace Wrapper {
        namespace Helper {

            // Copy from Utils/cpp/Formatted.hpp for convenience in target.

            template<typename Sink>
            inline void DumpTo(Sink & sink) { }

            template<typename Sink, typename First, typename ... Args>
            inline void DumpTo(Sink & sink, First && first, Args && ... args) {
                sink << first;
                DumpTo(sink, args ...);
            }

            template<typename... Args>
            inline std::string GenString(Args && ... args) {
                std::ostringstream os;
                DumpTo(os, args ...);
                return os.str();
            }

            // Helper definitions for operations descriptor
            typedef std::function<int()> GetStateClosure;
            typedef std::function<void(int)> StubClosure;
            typedef std::tuple<GetStateClosure, StubClosure> OpDesc;

            // Convenient functions to forward operation stubs
            inline int GetState(void * param) {
                return (*(GetStateClosure *)param)();
            }

            inline void Stub(int cancelled, void * param) {
                auto closure = (StubClosure *)param;
                (*closure)(cancelled);
                delete closure;
            }

            // Ex. if you have a OpDesc structure op, the typical usage is:
            // wrapper->ScheduleSync(&DescGetState, &DescGetStub, op, ...);

            inline int DescGetState(void * param) {
                return std::get<0>(*(OpDesc *)param)();
            }

            inline void DescStub(int cancelled, void * param) {
                auto closures = (OpDesc *)param;
                std::get<1>(*closures)(cancelled);
                delete closures;
            }

            class WrapperGuard {
            private:
                INodeWrapper * _wrapper;
            public:
                WrapperGuard(INodeWrapper * wrapper) : _wrapper(wrapper) { _wrapper->Enter(); }
                ~WrapperGuard() { _wrapper->Leave(); }
            };

            // Operation transformer that serialize the execution of an operation ``original'' in FIFO resource ``id'' by call-time ordering.
            // That is, given two operations transfromed by SerializeByIdEnter with the same ``id'', whoever calls SerializeByIdEnter will be executed first.
            // The operation starts after the resource is acquired.
            inline OpDesc * SerializeByIdEnter(INodeWrapper * wrapper, int id, OpDesc * original) {
                void * ticket;

                {
                    WrapperGuard guard(wrapper);
                    ticket = wrapper->GetLocalAttr(id, "ticket-tail");
                    wrapper->SetLocalAttr(id, "ticket-tail", (void *)((char *)ticket + 1));
                }

                return new OpDesc([=]()->int {
                    if (wrapper->GetLocalAttr(id, "current-ticket") == ticket) {
                        if (original == nullptr)
                            return STATE_ENABLED;
                        else return std::get<0>(*original)();
                    }
                    else return STATE_DISABLED;
                }, [=](int cancelled) {
                    if (original != nullptr)
                        std::get<1>(*original)(cancelled);
                    delete original;
                });
            }

            // Release the FIFO resource.
            inline void SerializeByIdLeave(INodeWrapper * wrapper, int id) {
                WrapperGuard guard(wrapper);
                void * ticket = wrapper->GetLocalAttr(id, "current-ticket");
                wrapper->SetLocalAttr(id, "current-ticket", (void *)((char *)ticket + 1));
            }

            // Serialize the operation ``original'' by a (potentially recursive) read-write lock resource ``id''.
            inline OpDesc * LockEnter(INodeWrapper * wrapper, int id, int threadId, bool exclusive, bool recursive, OpDesc * original) {
                std::map<int, std::tuple<int, int>> * lockinfo;
                {
                    WrapperGuard guard(wrapper);
                    lockinfo = (decltype(lockinfo))wrapper->GetLocalAttr(id, "lock-info");
                    if (lockinfo == nullptr) 
                        wrapper->SetLocalAttr(id, "lock-info", 
                            (lockinfo = new std::map<int, std::tuple<int, int>>()));
                }

                return new OpDesc([=]()->int {
                    if (lockinfo->size() == 0 || 
                        (lockinfo->size() == 1 && lockinfo->begin()->first == threadId)) {

                        if (original == nullptr)
                            return STATE_ENABLED;
                        else return std::get<0>(*original)();
                    }
                    // at least 1 locker, which is not the thread itself.
                    if (exclusive) {
                        return STATE_DISABLED;
                    }
                    else if (lockinfo->size() > 1 || std::get<1>(lockinfo->begin()->second) == 0) {
                        // Shared locked case
                        if (original == nullptr)
                            return STATE_ENABLED;
                        else return std::get<0>(*original)();
                    }
                    else {
                        // exclusive locked by the other one
                        return STATE_DISABLED;
                    }
                }, [=](int cancelled) {

                    if (!cancelled) {
                        if (exclusive) {
                            auto && info = (*lockinfo)[threadId];
                            if (std::get<1>(info) > 0 && !recursive) {
                                wrapper->CheckAssert(false, "self-deadlock on non-recursive lock");
                            }
                            ++std::get<1>(info);
                        }
                        else {
                            ++std::get<0>((*lockinfo)[threadId]);
                        }
                    }

                    if (original != nullptr)
                        std::get<1>(*original)(cancelled);
                    delete original;
                });
            }

            // Release the lock resource.
            inline void LockLeave(INodeWrapper * wrapper, int id, int threadId, bool exclusive) {
                WrapperGuard guard(wrapper);

                auto lockinfo = (std::map<int, std::tuple<int, int>> *)wrapper->GetLocalAttr(id, "lock-info");

                if (lockinfo == nullptr) {
                    wrapper->CheckAssert(false, "invalid LockLeave - no info");
                    return;
                }

                auto it = lockinfo->find(threadId);

                if (it == lockinfo->end()) {
                    wrapper->CheckAssert(false, "invalid LockLeave - entry not exist");
                    return;
                }

                if (exclusive) {
                    if (std::get<1>(it->second) == 0) {
                        wrapper->CheckAssert(false, "invalid LockLeave - lock count overflow");
                        return;
                    }
                    --std::get<1>(it->second);
                }
                else {
                    if (std::get<0>(it->second) == 0) {
                        wrapper->CheckAssert(false, "invalid LockLeave - lock count overflow");
                        return;
                    }
                    --std::get<0>(it->second);
                }

                if (std::get<0>(it->second) + std::get<1>(it->second) == 0) {
                    lockinfo->erase(threadId);
                }
            }
        }
    }
}

#define DMC_GENCSTR(...) (DMC::Wrapper::Helper::GenString(__VA_ARGS__).c_str())

#endif
