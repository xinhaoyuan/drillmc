#include "WrapperImpl.hpp"
#include "thrift/gen-cpp/arbiter_types.h"
#include <vector>
#include <iostream>
#include <cstdlib>
#include <sstream>
#include <random>
#include <tuple>
#include <chrono>
#include "Utils/cpp/Formatted.hpp"
#include <boost/make_shared.hpp>
#include <thrift/server/TThreadedServer.h>

#ifdef _MSVC

#include <Windows.h>
#pragma warning(push)
#pragma warning(disable: 4091)
#include <DbgHelp.h>
#pragma warning(pop)

#pragma comment(lib, "DbgHelp.lib")

#else

#include <execinfo.h>

#endif

using namespace XYHelper;
using namespace DMC::Thrift;
using namespace DMC::Wrapper;
using namespace DMC::Wrapper::Impl;
using namespace std;
using namespace apache::thrift;
using namespace apache::thrift::protocol;
using namespace apache::thrift::transport;
using namespace apache::thrift::server;

class EarlyListenSocket : public TServerSocket {
    bool _listen;
public:
    EarlyListenSocket(int port)
        : _listen(false), TServerSocket(port) { }

    void listen() override {
        if (!_listen) {
            _listen = true;
            TServerSocket::listen();
        }
    }
};

ContextInfo::ContextInfo(NodeWrapperImpl * wrapper)
    : contextType(ContextType::Normal)
    , managed(false)
    , contextId(-1)
    , wrapperLockLevel(0)
    , wrapperLock(wrapper->_lock, defer_lock)
{ }


void WrapperController::Schedule(const ArbiterChoice & choice) {
    lock_guard<mutex> lock(_wrapper->_choiceLock);
    _wrapper->_choice = choice;
    _wrapper->_choiceAvailable = true;
    _wrapper->_choiceCV.notify_one();
}

void WrapperController::ExecuteCommand(const int fromNodeId, const string & command) {
    void(* handler)(void *, int, const char *, int);
    void * data;

    // TODO - Seems that thrift will create new thread for every command,
    // but we don't want to create too many thread id and ContextInfo ...

    // Current we use a hacky way to prevent increase of thread ids.
    _wrapper->GetContextInfo()->contextId = 0;

    {
        LockGuard guard(_wrapper);
        
        handler = _wrapper->_commandHandler;
        data = _wrapper->_commandHandlerData;

        if (_wrapper->_isConnected && !_wrapper->_isEnd) {
            if (handler != nullptr) {
                _wrapper->LockedSendLog(Formatted("Execute command from node %d [%s].", fromNodeId, command.c_str()).CString());
            }
            else {
                _wrapper->LockedSendLog(Formatted("Ignore command from node %d [%s].", fromNodeId, command.c_str()).CString());
            }
        }
    }

    if (handler != nullptr) {    
        handler(data, fromNodeId, command.data(), (int)command.size());
    }
}

NodeWrapperImpl::NodeWrapperImpl()
    : _asyncJobPool(&BeforeAsyncJob, &AfterAsyncJob, this, -1)
    , _nodeId(-1)
    , _isRunning(false)
    , _isConnected(false)
    , _printMissedLog(false)
    , _activeContexts(0)
    , _opIdCounter(0)
    , _detachedOpIdCounter(0)
    , _localIdCounter(1)
    , _contextIdCounter(1)
    , _globalNodeCounter(-1)
    , _globalMessageTokenCounter(-1)
    , _localRandomSeed(0)
    , _localRandom(_localRandomSeed)
    , _localFilterRegEx(nullptr)
    , _localTokenQueueHead(0)
    , _localTokenQueueTail(0)
    , _pendingLocalToken(-1)
    , _pendingRemoteToken(-1)
    , _pendingRemoteTokenIsUnique(false)
    , _nodeClock(0)
    , _advanceClockOpId(-1)
    , _startSystemTime(GetSystemTimestamp())
    , _wrapperThread(nullptr)
    , _arbiter(nullptr)
    , _logVerboseLevel(VERBOSE_DEFAULT)
    , _logBlocked(false)
    , _forceExit(false)
    , _detachMode(false)
    , _detachModeBarrierOp(nullptr)
    , _ignoreTokenMatching(false)
    , _isEnd(false)
    , _miscLog(&cerr)
    , _controllerServer(nullptr)
    , _choiceAvailable(false)
    , _commandHandler(nullptr)
    , _commandHandlerData(nullptr)
    , _localInfoPtrStore(&_defaultCPS)
{ }

void NodeWrapperImpl::Lock() {
    auto ctxInfo = GetContextInfo();
    if (++ctxInfo->wrapperLockLevel == 1) {
        ctxInfo->wrapperLock.lock();
    }
}

void NodeWrapperImpl::Unlock() {
    auto ctxInfo = GetContextInfo();
    if (--ctxInfo->wrapperLockLevel == 0) {
        ctxInfo->wrapperLock.unlock();
    }
}

void NodeWrapperImpl::Enter() {
    Lock();
}

void NodeWrapperImpl::Leave() {
    Unlock();
}

bool NodeWrapperImpl::IsRunning() { return _isRunning; }

bool NodeWrapperImpl::IsConnected() { return _isConnected; }

local_res_t NodeWrapperImpl::GetIdForPtr(void * ptr, const char * decoration, bool canExist, bool canCreate) {
    LockGuard lock(this);

    if (_isEnd) return -1;
    
    if (IsPure()) {
        canCreate = false;
    }

    string key;

    if (decoration == nullptr) {
        key.assign(Formatted("P:%p", ptr).CString());
    }
    else {
        key.assign(Formatted("P:%p:%s", ptr, decoration).CString());
    }

    auto it = _idMap.find(key);
    if (it == end(_idMap)) {
        if (canCreate) {
            local_res_t id = _localIdCounter++;
            _idMap[key] = id;
            _idDesc[id] = key;
            return id;
        }
        else return -1;
    }
    else if (canExist) {
        return get<1>(*it);
    }
    else return -1;
}

local_res_t NodeWrapperImpl::GetIdForSymbol(const char * symbol, int nbytes, bool canExist, bool canCreate) {
    LockGuard lock(this);

    if (_isEnd) return -1;

    if (IsPure()) {
        canCreate = false;
    }

    string key("S:");
    if (nbytes >= 0) {
        key.append(symbol, nbytes);
    }
    else {
        key.append(symbol);
    }

    auto it = _idMap.find(key);
    if (it == end(_idMap)) {
        if (canCreate) {
            local_res_t id = _localIdCounter++;
            _idMap[key] = id;
            _idDesc[id] = key;
            return id;
        }
        else return -1;
    }
    else if (canExist) {
        return get<1>(*it);
    }
    else return -1;
}

local_res_t NodeWrapperImpl::CreateUniqueId() {
    LockGuard lock(this);

    if (_isEnd) return -1;

    if (IsPure()) {
        LockedReportEnd(true, "CreateUniqueId in pure context");
        return -1;
    }

    local_res_t id = _localIdCounter++;
    _idDesc[id] = "U";

    return id;
}

void NodeWrapperImpl::ReleaseId(local_res_t id) {
    if (id <= 0) return;

    {
        LockGuard lock(this);

        if (_isEnd) return;

        if (InStub()) {
            LockedReportEnd(true, "Cannot ReleaseId in stub");
            return;
        }

        // 1. remote all local attr with the id
        // TODO - invoke the destructor
        _localAttr.erase(id);

        // 2. release the in-class resource (e.g. _idMap, _idDesc, etc.)
        // HACKY - use the first char of desc to get the type
        switch (_idDesc[id][0]) {
        case 'U':
            _idDesc.erase(id);
            break;
        default:
            _idMap.erase(_idDesc[id]);
            _idDesc.erase(id);
            break;
        }
    }
}

void * NodeWrapperImpl::GetLocalAttr(local_res_t id, const char * attrName) {
    LockGuard lock(this);

    if (_isEnd) return nullptr;
    if (attrName == nullptr) attrName = "";

    auto it = _localAttr.find(id);
    if (it == end(_localAttr))
        return nullptr;
    auto eit = get<1>(*it).find(attrName);
    if (eit == end(get<1>(*it)))
        return nullptr;

    return get<1>(*eit);
}

void NodeWrapperImpl::SetLocalAttr(local_res_t id, const char * attrName, void * attr) {
    LockGuard lock(this);

    if (_isEnd) return;

    if (IsPure()) {
        LockedReportEnd(true, "Cannot SetLocalAttr in pure context");
        return;
    }

    if (attrName == nullptr) attrName = "";

    // TODO - call destructor to handle overwritten values

    _localAttr[id][attrName] = attr;
}

void NodeWrapperImpl::DeleteLocalAttr(local_res_t id, const char * attrName) {
    LockGuard lock(this);

    if (_isEnd) return;

    if (InStub()) {
        LockedReportEnd(true, "Cannot DeleteLocalAttr in stub");
        return;
    }

    if (attrName == nullptr) attrName = "";

    auto it = _localAttr.find(id);
    if (it == end(_localAttr))
        return;
    auto eit = get<1>(*it).find(attrName);
    if (eit == end(get<1>(*it)))
        return;

    get<1>(*it).erase(eit);
}

void NodeWrapperImpl::Notify(int nodeId) {
    LockGuard lock(this);

    if (!LockedEnsureFocus())
        return;

    if (!_isRunning)
        return;

    if (GetContextInfo()->contextType == ContextType::PureFunc) {
        LockedReportEnd(true, "Notify in pure func");
        return;
    }

    _notify.insert(nodeId);
}

int NodeWrapperImpl::CreateGlobalUniqueId() {
    LockGuard lock(this);

    if (_isEnd) return -1;

    if (!_isConnected || _globalIdCounter < 0) return -1;

    if (IsPure()) {
        LockedReportEnd(true, "CreateGlobalUniqueId in pure context");
        return -1;
    }

    return _globalIdCounter++;    
}
            
void NodeWrapperImpl::BufferGetInfo(int id, bool & isOpen, int & length) {
    LockGuard lock(this);

    isOpen = false;
    length = 0;
    
    if (!LockedEnsureFocus())
        return;

    if (!_isConnected)
        return;

    Thrift::BufferInfo bufferInfo;
    _arbiter->BufferGetInfo(bufferInfo, id);

    isOpen = bufferInfo.isOpen;
    length = bufferInfo.length;
    
    return;
}

bool NodeWrapperImpl::BufferOpen(int id) {
    LockGuard lock(this);

    if (!LockedEnsureFocus())
        return false;

    if (!_isRunning)
        return false;

    if (GetContextInfo()->contextType == ContextType::PureFunc) {
        LockedReportEnd(true, "BufferWrite in pure func");
        return false;
    }

    return _arbiter->BufferOpen(id);
}

bool NodeWrapperImpl::BufferClose(int id) {
    LockGuard lock(this);

    if (!LockedEnsureFocus())
        return false;

    if (!_isRunning)
        return false;

    if (GetContextInfo()->contextType == ContextType::PureFunc) {
        LockedReportEnd(true, "BufferWrite in pure func");
        return false;
    }

    return _arbiter->BufferClose(id);
}

int NodeWrapperImpl::BufferWrite(int id, const char * data, int len) {
    LockGuard lock(this);

    if (!LockedEnsureFocus())
        return 0;

    if (!_isRunning)
        return 0;

    if (len < 0)
        return 0;

    if (GetContextInfo()->contextType == ContextType::PureFunc) {
        LockedReportEnd(true, "BufferWrite in pure func");
        return 0;
    }

    return _arbiter->BufferWrite(id, string(data, len));
}

int NodeWrapperImpl::BufferRead(int id, char * outBuffer, int bufferLen) {
    LockGuard lock(this);

    if (!LockedEnsureFocus())
        return 0;

    if (!_isConnected)
        return 0;

    if (bufferLen < 0)
        return 0;

    string data;
    _arbiter->BufferRead(data, id, bufferLen);

    int ret = data.size();
    if (ret > bufferLen) ret = bufferLen;
    memcpy(outBuffer, data.data(), ret); 

    return ret;
}

int NodeWrapperImpl::GetNodeId() { return _nodeId; }

void NodeWrapperImpl::ForceExit() {
#ifdef _MSVC
    // Disable annoying alert dialogs
    SetErrorMode(SEM_FAILCRITICALERRORS | SEM_NOGPFAULTERRORBOX);
    _set_abort_behavior(0, _WRITE_ABORT_MSG | _CALL_REPORTFAULT);
#endif
    _exit(0);
}

static boost::shared_ptr<TTransport> GetTransport(const string & endpoint) {
    size_t pos = endpoint.find(':');
    if (pos != string::npos) {
        string addr(endpoint, 0, pos);
        int port = atoi(endpoint.c_str() + pos + 1);

        boost::shared_ptr<TSocket> socket(new TSocket(addr, port));
        socket->setNoDelay(true);
        boost::shared_ptr<TTransport> transport(new TBufferedTransport(socket));

        return transport;
    }
    else return nullptr;
}

void NodeWrapperImpl::LockedWaitSchedule(ArbiterChoice & outChoice) {
    if (_isEnd) {
        outChoice.source = ChoiceSource::TO_EXIT;
        return;
    }
    // Will temporarily release the wrapper lock
    GetContextInfo()->wrapperLock.unlock();
    {
        unique_lock<mutex> lock(_choiceLock);
        while (true) {
            if (_choiceAvailable) break;
            else _choiceCV.wait(lock);
        }
        _choiceAvailable = false;
        outChoice = _choice;
    }
    GetContextInfo()->wrapperLock.lock();
    if (_isEnd) {
        outChoice.source = ChoiceSource::TO_EXIT;
    }
    else {
        LockedUpdateLocalMetadata(outChoice);
    }

    if (_isRunning) {
        ostringstream os;
        os << "Got schedule: " << outChoice;
        LockedSendLog(os.str().c_str(), VERBOSE_DEBUG);
    }
}

void NodeWrapperImpl::LockedUpdateLocalMetadata(const ArbiterChoice & choice) {
    _globalNodeCounter = choice.currentNodeCount;
    _globalIdCounter = choice.currentIdCount;
    _globalMessageTokenCounter = choice.currentMessageTokenCount;
    if (choice.__isset.localRandomSeed && choice.localRandomSeed >= 0) {
        _localRandomSeed = choice.localRandomSeed;
        _localRandom.seed(_localRandomSeed);

        LockedSendLog(GenString("Update localRandomSeed to ", _localRandomSeed).c_str(), VERBOSE_DEBUG);

        if (choice.__isset.localFilter) {
            _localFilter = choice.localFilter;
            delete _localFilterRegEx;
            if (_localFilter.size() == 0) {
                _localFilterRegEx = nullptr;
            }
            else {
                _localFilterRegEx = new regex(_localFilter);
            }

            LockedSendLog(GenString("Update localFilter to [", _localFilter, ']').c_str(), VERBOSE_DEBUG);
        }
    }
    if (choice.__isset.externalWeight) {
        _externalWeight = choice.externalWeight;
    }
    else {
        _externalWeight = 0;
    }
    
}

bool NodeWrapperImpl::InitNode(const char * endpoint, const char * cookie, IContextPtrStore * cps) {
    if (endpoint == nullptr) endpoint = getenv("DMC_ARBITER_ENDPOINT");
    if (cookie == nullptr) cookie = getenv("DMC_ARBITER_COOKIE");
    
    if (endpoint == nullptr) {
        *_miscLog << "missing endpoint." << endl;
        return false;
    }

    {
        lock_guard<mutex> lock(_lock);
        if (cps) {
            _localInfoPtrStore = cps;
        }


        if (_isRunning || _isConnected)
            return false;

        _isEnd = false;
        _detachMode = false;
        _printMissedLog = false;
        _activeContexts = 1;
        _opIdCounter = 0;
        _detachedOpIdCounter = 0;
        _localIdCounter = 1; // 0 is for wrapper itself
        _contextIdCounter = 1; // 0 is for wrapper thread
        _globalNodeCounter = -1;
        _globalIdCounter = -1;
        _globalMessageTokenCounter = -1;
        _localTokenQueueHead = 0;
        _localTokenQueueTail = 0;
        _pendingLocalToken = -1;
        _pendingRemoteTokenIsUnique = false;
        _pendingRemoteToken = -1;
        _nodeClock = 0;
        _advanceClockOpId = -1;
        _nodeId = -1;
        _detachModeBarrierOp = nullptr;
        _arbiterCookie = cookie == nullptr ? "" : cookie;
        _verboseStackInfo = false;

        _arbiterTransport = GetTransport(endpoint);

        try {
            _arbiterTransport->open();
            _arbiter = new ArbiterClient(boost::shared_ptr<TProtocol>(new TBinaryProtocol(_arbiterTransport)));
        }
        catch (const TException &) {
            *_miscLog << "Failed to connect arbiter" << endl;
            _arbiterTransport->close();
            _arbiterTransport.reset();
            _isEnd = true;

            return false;
        }

#ifdef _MSVC
        // Disable annoying alert dialogs
        // SetErrorMode(SEM_FAILCRITICALERRORS | SEM_NOGPFAULTERRORBOX);
        // _set_abort_behavior(0, _WRITE_ABORT_MSG | _CALL_REPORTFAULT);

        // For stack trace
        SymInitialize(GetCurrentProcess(), NULL, TRUE);
#endif

        _isConnected = true;

        LockedSendLog("Connected", VERBOSE_DEBUG);
    }

    return true;
}

bool NodeWrapperImpl::NodeStart(int nodeId) {
    LockGuard lock(this);
    
    if (_isEnd)
        return false;

    if (nodeId < 0) {
        // TODO Disconnect instead of error?
        _isEnd = true;
        return false;
    }

    if (!_isConnected) {
        LockedReportEnd(true, "Not connected");
        return false;
    }

    if (InStub()) {
        LockedReportEnd(true, "Cannot NodeStartBarrier in stub");
        return false;
    }

    if (_isRunning) {
        LockedReportEnd(true, "Cannot call NodeStart multiple times");
        return false;
    }

    _nodeId = nodeId;

#ifdef _MSVC
    // Current thrift (0.97) has some problems with binding on port 0 (select by OS).
    // So pick a listenable port by retrying.
    int portBase = 18399;
    int randRange = 64;
    int maxRandRange = 1024;
    int tryCount = 32;
    minstd_rand randGen((unsigned)time(NULL));
    randGen.seed(randGen() + GetCurrentProcessId());
    randGen.seed(randGen() + nodeId);
    while (true) {
        int port = portBase + randGen() % randRange;
        _controllerSocket = boost::make_shared<EarlyListenSocket>(port);
        try {
            _controllerSocket->listen();
            break;
        }
        catch (const TException &) {
            if (randRange < maxRandRange) randRange <<= 1;
            if (tryCount == 0) {
                _controllerSocket = nullptr;
                break;
            }
        }
        --tryCount;
    }

    if (!_controllerSocket) {
        *_miscLog << "failed to pick a port for controller" << endl;
        _isEnd = true;
        return false;
    }
#else
    _controllerSocket = boost::make_shared<EarlyListenSocket>(0);
    try {
        _controllerSocket->listen();
        // For some reason thrift may fail to get the port (returns 0).
        if (_controllerSocket->getPort() == 0)
            throw TException("getPort() returns 0");
    }
    catch (const TException &) {
        *_miscLog << "failed to listen for controller" << endl;
        _controllerSocket.reset();
        return false;
    }
#endif

    _controllerThread = new thread(ControllerThreadEntryHelper, this);
    // TODO Try to get public hostname?
    string controllerEndpoint = Formatted("127.0.0.1:%d", _controllerSocket->getPort()).CString();

    InitConfiguration conf;

    try {
        // *_miscLog << Formatted("Before InitNode call by node %d", _nodeId).CString() << endl;
        _arbiter->InitNode(conf, _arbiterCookie, _nodeId, controllerEndpoint);
    }
    catch (const TException &) {
        *_miscLog << "failed InitNode" << endl;
        _isEnd = true;

        return false;
    }

    if (!conf.success) {
        *_miscLog << "Arbiter rejected our init request, detaching" << endl;
        _arbiterTransport->close();
        _arbiterTransport.reset();
        _isEnd = true;

        if (conf.forceExit)
            ForceExit();

        return false;
    }

    _arbiterCookie = conf.cookie;
    _forceExit = conf.forceExit;
    _logVerboseLevel = conf.logLevel;
    _printMissedLog = conf.printMissedLog;
    _verboseStackInfo = conf.verboseStackInfo;

    ArbiterChoice tempChoice;
    LockedWaitSchedule(tempChoice);
    if (tempChoice.source != ChoiceSource::REMOTE) {
        // should be a message
        _isEnd = true;
        return false;
    }
    
    _isRunning = true;

    LockedSendLog(GenString("Hello from node ", _nodeId,
        ", localRandomSeed = ", _localRandomSeed,
        ", localFilter = [", _localFilter ,']'
        ).c_str(), VERBOSE_DEBUG);
    _wrapperThread = new thread(WrapperThreadEntryHelper, this);

    // Set the calling thread managed
    GetContextInfo()->managed = true;

    return true;
}

int NodeWrapperImpl::RegisterNewNode() {
    LockGuard lock(this);

    if (!LockedEnsureFocus(true)) {
        return -1;
    }

    if (GetContextInfo()->contextType == ContextType::PureFunc) {
        LockedReportEnd(true, "RegisterNewNode in pure func");
        return -1;
    }

    if (_isRunning) {
        int nodeId = _globalNodeCounter++;
        return nodeId;
    }
    else {
        // node init with id -1 will not connect to arbiter.
        return -1;
    }
}

void NodeWrapperImpl::WrapperThreadEntryHelper(NodeWrapperImpl * self) {
    self->WrapperThreadEntry();
}

void NodeWrapperImpl::ControllerThreadEntryHelper(NodeWrapperImpl * self) {
    self->ControllerThreadEntry();
}

ContextInfo * NodeWrapperImpl::GetContextInfo() {
    ContextInfo * ret = (ContextInfo *)_localInfoPtrStore->Get();
    if (ret == nullptr) {
        auto info = new ContextInfo(this);
        _localInfoPtrStore->Reset(info, [](void * ptr_) { delete (ContextInfo *)ptr_; });
        return info;
    }
    else return ret;
}

int NodeWrapperImpl::GetContextId() {
    ContextInfo * ci = GetContextInfo();
    if (ci->contextId >= 0)
        return ci->contextId;

    if (_wrapperThread != nullptr && this_thread::get_id() == _wrapperThread->get_id())
        ci->contextId = 0;
    else {
        LockGuard lock(this);
        ci->contextId = _contextIdCounter++;
    }

    return ci->contextId;
}

bool NodeWrapperImpl::InWrapper() {
    return _wrapperThread != nullptr && this_thread::get_id() == _wrapperThread->get_id();
}

bool NodeWrapperImpl::InStub() {
    return GetContextInfo()->contextType != ContextType::Normal;
}

bool NodeWrapperImpl::IsPure() {
    auto ct = GetContextInfo()->contextType;
    return ct == ContextType::PureFunc;
}

bool NodeWrapperImpl::LockedEnsureFocus(bool requireManaged) {
    if (InStub()) {
        return !_isEnd;
    }

    if (requireManaged && _isRunning && !_isEnd) {
        if (!GetContextInfo()->managed) {
            LockedReportEnd(true, "Managed thread is required.");
            return false;
        }
    }
   
    if (_activeContexts == 0 && _isRunning && !_isEnd) {
        do {
            _transferCV.wait(GetContextInfo()->wrapperLock);
        } while (_activeContexts == 0 && _isRunning && !_isEnd);
    }

    if (_isEnd) {
        return false;
    }
    else if (_isRunning && _activeContexts != 1) {
        LockedReportEnd(true, "_isRuning && _activeContexts != 1");
        return false;
    }
    else {
        return true;
    }
}

class ControllerFactory: public ControllerIfFactory {
    NodeWrapperImpl * _wrapper;
    boost::shared_ptr<WrapperController> _controller;

public:
    inline ControllerFactory(NodeWrapperImpl * wrapper)
        : _wrapper(wrapper)
        , _controller(boost::make_shared<WrapperController>(wrapper))
    { }

    ControllerIf * getHandler(const ::apache::thrift::TConnectionInfo & connInfo) override {
        TSocket * socket = reinterpret_cast<TSocket *>(connInfo.transport.get());
        if (socket != nullptr) {
            socket->setNoDelay(true);
        }
        return _controller.get();
    }

    void releaseHandler(ControllerIf * handler) override {
    }
};

void NodeWrapperImpl::ControllerThreadEntry() {
    _controllerServer = new TThreadedServer(
        boost::make_shared<ControllerProcessorFactory>(
            boost::make_shared<ControllerFactory>(this)),
        _controllerSocket,
        boost::make_shared<TBufferedTransportFactory>(),
        boost::make_shared<TBinaryProtocolFactory>());

    _controllerServer->serve();
}

#define BREAK_IF_END { if (_isEnd) break; }
#define RETURN_IF_END { if (_isEnd) return; }

void NodeWrapperImpl::LockedReport(int inputRange) {
    Thrift::SyncData data;

    data.inputRange = inputRange;

    for (auto && kv : _newOperations) {
        _operations.emplace(get<0>(kv), get<1>(kv));
        OperationDescriptionWithId desc;
        desc.id = get<0>(kv);
        desc.desc.node = _nodeId;
        get<1>(kv)->Describe(desc.desc.text);
        desc.desc.isWeak = get<1>(kv)->IsWeak();
        data.newOps.push_back(desc);
    }

    _newOperations.clear();

    // Insert abstract operation of advance local clock
    bool hasTimeEvent = false;
    
    if (_timeEvents.size() > 0) {
        hasTimeEvent = true;
        
        const char * barrierStr = GetVariable("clock-barrier");
        if (barrierStr && barrierStr[0]) {
            timestamp_t barrier = atoll(barrierStr);
            if (*begin(_timeEvents) > barrier) {
                // ignore advancing clock if all events are behind the barrier
                hasTimeEvent = false;
            }
        }
    }

    if (hasTimeEvent && _advanceClockOpId == -1) {
        OperationDescriptionWithId desc;
        desc.id = _advanceClockOpId = _opIdCounter;
        desc.desc.node = _nodeId;
        desc.desc.text = "<AdvanceClock>";
        data.newOps.push_back(desc);

        ++_opIdCounter;
    }
    
    for (auto && kv : _operations) {
        switch (get<1>(kv)->GetState()) {
        case STATE_DISABLED:
            data.toggledOps.push_back(get<0>(kv));
            break;
        case STATE_CANCELLED:
            data.cancelledOps.push_back(get<0>(kv));
            break;
        default:
            break;
        }
    }

    RETURN_IF_END;

    // we don't care warning here.
    int toggledBound = (int)data.toggledOps.size();
    int cancelledBound = (int)data.cancelledOps.size();

    for (auto && kv : _disabledOps)
    {
        switch (get<1>(kv)->GetState()) {
        case STATE_ENABLED:
            data.toggledOps.push_back(get<0>(kv));
            break;
        case STATE_CANCELLED:
            data.cancelledOps.push_back(get<0>(kv));
            break;
        default:
            break;
        }
    }

    RETURN_IF_END;

    // Post handling toggled ops and cancelled ops

    for (int idx = 0; idx < data.toggledOps.size(); ++idx) {
        if (idx < toggledBound) {
            auto it = _operations.find(data.toggledOps[idx]);
            _disabledOps.emplace(*it);
            _operations.erase(it);
        }
        else {
            auto it = _disabledOps.find(data.toggledOps[idx]);
            _operations.emplace(*it);
            _disabledOps.erase(it);
        }
    }

    // convert cancelled ops to detached ops
    // NOTE - the ops' description may be stale, but seems that it doesn't matter.
    for (int idx = 0; idx < data.cancelledOps.size(); ++idx) {
        if (idx < cancelledBound) {
            auto it = _operations.find(data.cancelledOps[idx]);
            _detachedLocalOps.emplace(_detachedOpIdCounter++, get<1>(*it));
            _operations.erase(it);
        }
        else {
            auto it = _disabledOps.find(data.cancelledOps[idx]);
            _detachedLocalOps.emplace(_detachedOpIdCounter++, get<1>(*it));
            _disabledOps.erase(it);
        }
    }

    // Cancel timer event after handling normal cancelled events
    if (!hasTimeEvent && _advanceClockOpId >= 0) {
        data.cancelledOps.push_back(_advanceClockOpId);
        _advanceClockOpId = -1;
    }

    for (auto && t : _remoteMessages) {
        Thrift::MessageWithId msg;
        msg.id = get<0>(t);
        msg.message.target = get<1>(t);
        msg.message.text = get<2>(t);
        data.messages.push_back(msg);
    }

    for (int nodeId : _notify) {
        data.notify.push_back(nodeId);
    }

    data.updatedNodeCount = _globalNodeCounter;
    data.updatedIdCount = _globalIdCounter;
    data.updatedMessageTokenCount = _globalMessageTokenCounter;
    data.dep = _currentDependency;
            
    try {
        _arbiter->Sync(data);
    }
    catch (const TException &) {
        LockedSetEnd(true);
        return;
    }

    // Clean up
    _currentDependency = OperationDependency();
    _remoteMessages.clear();
    _notify.clear();
}

void NodeWrapperImpl::LockedReportShutdown() {
    Thrift::SyncData data;

    data.inputRange = -1;

    // Cancel all ops
    if (_advanceClockOpId != -1) {
        data.cancelledOps.push_back(_advanceClockOpId);
    }
    
    for (auto && kv : _operations) {
        data.cancelledOps.push_back(get<0>(kv));
    }

    // But still send out messages
    for (auto && t : _remoteMessages) {
        Thrift::MessageWithId msg;
        msg.id = get<0>(t);
        msg.message.target = get<1>(t);
        msg.message.text = get<2>(t);
        data.messages.push_back(msg);
    }

    for (int nodeId : _notify) {
        data.notify.push_back(nodeId);
    }

    data.updatedNodeCount = _globalNodeCounter;
    data.updatedIdCount = _globalIdCounter;
    data.updatedMessageTokenCount = _globalMessageTokenCounter;
    data.dep = _currentDependency;
            
    try {
        _arbiter->Sync(data);
    }
    catch (const TException &) {
        LockedSetEnd(true);
        return;
    }

    // Clean up
    _currentDependency = OperationDependency();
    _remoteMessages.clear();
    _notify.clear();
}


void NodeWrapperImpl::WrapperThreadEntry() {
    {
        LockGuard lock(this);

        _wrapperThreadInfo = GetContextInfo();
        _wrapperThreadInfo->contextType = ContextType::WrapperLoop;
        _wrapperThreadInfo->managed = true;

        while (true) {
            while (_activeContexts > 0 && !_isEnd) {
                auto status = _quiescentCV.wait_for(
                    _wrapperThreadInfo->wrapperLock, chrono::milliseconds(1000));
                if (status == cv_status::timeout) {
                    LockedSendLog("not quiescent for 1s.", VERBOSE_WARNING);
                }
            }

            BREAK_IF_END;

            if (!_ignoreTokenMatching) {
                if (_pendingLocalToken >= 0) {
                    LockedReportEnd(true, 
                        Formatted("Pending local transfer not taken after quiescent, token = %d",
                            _pendingLocalToken).CString());
                    break;
                }

                if (_pendingRemoteToken >= 0) {
                    LockedReportEnd(true, 
                        Formatted("Pending remote transfer not taken after quiescent, token = %d",
                            _pendingRemoteToken).CString());
                    break;
                }
            }

            BREAK_IF_END;

            IOperation * op = nullptr;
            int choice = -1;

            // Caution: we assume the head and tail will never overflow in a single run
            if (op == nullptr && _localTokenQueueHead < _localTokenQueueTail) {
                ++_activeContexts;
                _pendingLocalToken = _localTokenQueueHead++;
                _transferCV.notify_all();
                continue;
            }

            if (op == nullptr) {
                // Try to schedule a detached operation by deterministic random generator
                vector<tuple<int, IOperation *>> choices;

                for (auto && kv : _detachedLocalOps) {
                    if (get<1>(kv)->GetState() != STATE_DISABLED) {
                        choices.push_back(make_tuple(get<0>(kv), get<1>(kv)));
                    }
                }

                BREAK_IF_END;

                if (choices.size() > 0) {
                    // Assuem it's OK to down-cast
                    int choiceIndex = (int)(_localRandom() % (choices.size() + _externalWeight));
                    if (choiceIndex < choices.size()) {
                        choice = get<0>(choices[choiceIndex]);
                        op = get<1>(choices[choiceIndex]);
                    }
                }

                if (op != nullptr) {
                    string desc;
                    op->Describe(desc);
                    LockedSendLog(GenString("run detach op [", desc, "]").c_str(), VERBOSE_DEBUG);
                    _detachedLocalOps.erase(choice);
                }
            }

            BREAK_IF_END;

            if (op == nullptr) {
                op = _detachModeBarrierOp;
                _detachModeBarrierOp = nullptr;
            }

            if (op == nullptr) {

                LockedReport(0);

                ArbiterChoice arbiterChoice;

                LockedWaitSchedule(arbiterChoice);

                BREAK_IF_END;

                if (arbiterChoice.source == ChoiceSource::TO_EXIT) {
                    break;
                }
                else if (arbiterChoice.source == ChoiceSource::NOTIFY) {
                    // recompute state of operations
                    LockedSendLog("Get notify choice, recompute operation states", VERBOSE_INFO); 
                    continue;
                }
                else if (arbiterChoice.source == ChoiceSource::REMOTE) {
                    ++_activeContexts;
                    // Hacky - use sign of choice to represent uniqueness.
                    _pendingRemoteTokenIsUnique = arbiterChoice.choice >= 0;
                    _pendingRemoteToken = _pendingRemoteTokenIsUnique ? arbiterChoice.choice : -arbiterChoice.choice;
                    _transferCV.notify_all();
                    continue;
                }
                
                choice = arbiterChoice.choice;

                if (choice == _advanceClockOpId && _advanceClockOpId != -1) {
                    // Advance the clock to nearest event
                    auto it = begin(_timeEvents);
                    if (it == end(_timeEvents) || *it <= _nodeClock) {
                        LockedReportEnd(true, "Error: No time event available, or time event falls behind node clock");
                        break;
                    }
                    else {
                        LockedSendLog(GenString("Clock = ", *it).c_str(), VERBOSE_INFO);
                    }
                    _nodeClock = *it;
                    _timeEvents.erase(it);
                    _advanceClockOpId = -1;
                    {
                        _wrapperThreadInfo->contextType = ContextType::Stub;
                        AddDependency(DEP_FLAG_WRITE, 0, "NodeClock");
                        _wrapperThreadInfo->contextType = ContextType::WrapperLoop;
                    }
                    // no user operations to do. Re-evaluate operation states.
                    continue;
                }

                auto it = _operations.find(choice);
                if (it == end(_operations)) {
                    LockedReportEnd(true, Formatted("Error: choice %d not available, report error.", choice).CString());
                    break;
                }
                op = get<1>(*it);
                _operations.erase(it);
            }

            BREAK_IF_END;

            if (op == nullptr) {
                LockedReportEnd(true, "No operation to schedule.");
                break;
            }

            op->Execute();

            BREAK_IF_END;
        }

        _isRunning = false;
        _isConnected = false;
        _arbiterTransport->close();

        if (_isEnd) {
            LockedSendLog(GenString("LastError: ", _lastError).c_str(), VERBOSE_WARNING);
        }
        LockedSendLog("Farewell.", VERBOSE_DEBUG);

        if (_forceExit) {
            ForceExit();
        }
        else {
#if 0
            // Fire all remaining operation
            if (_detachModeBarrierOp != nullptr) {
                _detachModeBarrierOp->Execute();
            }
            for (auto && ops : { _newOperations, _detachedLocalOps, _operations, _disabledOps }) {
                for (auto && kv : ops) {
                    get<1>(kv)->Execute();
                }
            }
            _transferCV.notify_all();
#endif
        }
    }

    _controllerServer->stop();
    _controllerThread->join();
    _controllerSocket->close();
}

bool NodeWrapperImpl::ScheduleSync(GetStateFunc getStateFunc,
                                   StubFunc stub,
                                   void * data,
                                   const char * info,
                                   int nbytes,
                                   unsigned flags) {
    LockGuard lock(this);

    if (!LockedEnsureFocus(true)) {
        return true;
    }

    if (InStub()) {
        LockedReportEnd(true, "ScheduleSync in stub");
        return true;
    }

    if (_isRunning) {
        SyncOperation sop(this, getStateFunc, data, info, nbytes, !!(flags & OP_FLAG_WEAK));
        bool detach = false;
        
        if (!(flags & OP_FLAG_NONDETACHABLE)) {
            if ((flags & OP_FLAG_LOCAL_DETACH) || _detachMode)
                detach = true;
            else if (_localFilterRegEx != nullptr && info != nullptr &&
                !std::regex_search(nbytes >= 0 ? string(info, nbytes) : string(info), *_localFilterRegEx)) {
                detach = true;
            }
        }

        int opId;
        if (detach) {
            opId = _detachedOpIdCounter++;
            sop.SetDescription(true, GetContextId(), opId);
            _detachedLocalOps.emplace(opId, &sop);
        }
        else {
            opId = _opIdCounter++;
            sop.SetDescription(false, GetContextId(), opId);
            _newOperations.emplace(opId, &sop);
        }
        
        --_activeContexts;
        _quiescentCV.notify_one();

        if (_verboseStackInfo)
            SendStack("ScheduleSync block", VERBOSE_DEBUG);
        // if _isRunning become false, the wrapper thread will eventually schedule this
        sop.LockedWait();
        if (_verboseStackInfo)
            LockedSendLog("ScheduleSync unblock", VERBOSE_DEBUG);

        int state = sop.GetState();
        if (stub != nullptr)
            stub(state == STATE_CANCELLED, data);
        return state == STATE_CANCELLED;
    }
    else {
        if (stub != nullptr)
            stub(false, data);
        return false;
    }
}

void NodeWrapperImpl::ScheduleAsync(GetStateFunc getStateFunc,
                                    StubFunc stub,
                                    void * data,
                                    const char * info,
                                    int nbytes,
                                    unsigned flags) {
    LockGuard lock(this);
    auto ctxInfo = GetContextInfo();

    if (LockedEnsureFocus(true)) {
        if (ctxInfo->contextType == ContextType::PureFunc) {
            LockedReportEnd(true, "ScheduleAsync in pure func");
        }
    }

    if (stub == nullptr) {
        LockedReportEnd(true, "stub must be valid for async");
        return;
    }

    if (_verboseStackInfo)
        SendStack("ScheduleAsync", VERBOSE_DEBUG);

    string infoWithContextId =
        GenString(info == nullptr ? "" : (nbytes >= 0 ? string(info, nbytes) : string(info)),
            "@", GetContextId());

    AsyncOperation * aop = new AsyncOperation(this,
        getStateFunc, stub, data,
        infoWithContextId.data(), (int)infoWithContextId.size(),
        !!(flags & OP_FLAG_ASYNC_JOB), !!(flags & OP_FLAG_WEAK));

    if (_isRunning && !_isEnd) {
        int opId;
        bool detach = false;

        if (!(flags & OP_FLAG_NONDETACHABLE)) {
            if ((flags & OP_FLAG_LOCAL_DETACH) || _detachMode)
                detach = true;
            else if (_localFilterRegEx != nullptr && info != nullptr &&
                !std::regex_search(nbytes >= 0 ? string(info, nbytes) : string(info), *_localFilterRegEx)) {
                detach = true;
            }
        }

        if (detach) {
            opId = _detachedOpIdCounter++;
            aop->SetDescription(true, opId);
            _detachedLocalOps.emplace(opId, aop);
        }
        else {
            opId = _opIdCounter++;
            aop->SetDescription(false, opId);
            _newOperations.emplace(opId, aop);
        }
    }
    else {
        HandleAsyncOpInAppContext(aop);
    }
}

int NodeWrapperImpl::Choose(int range) {
    LockGuard lock(this);

    if (range <= 0) {
        LockedReportEnd(true, "Range must be greater than zero");
        return 0;
    }

    auto ctxInfo = GetContextInfo();
    if (LockedEnsureFocus(true)) {
        if (ctxInfo->contextType == ContextType::PureFunc) {
            LockedReportEnd(true, "Choose in pure func");
        }
    }

    if (_isRunning && !_isEnd) {
        // Use report inplace
        if (ctxInfo->contextType == ContextType::Stub || ctxInfo->contextType == ContextType::Normal) {
            LockedReport(range);
            ArbiterChoice choice;
            LockedWaitSchedule(choice);

            if (choice.source != ChoiceSource::INPUT || choice.choice < 0 || 
                (range > 0 && choice.choice >= range)) {
                LockedReportEnd(true, GenString("Get unexpected choice ", choice, ", range = ", range).c_str());
            }

            return choice.choice;
        }
        else {
            LockedReportEnd(true, GenString("Do not know how to Choose in the ContextType of ", ctxInfo->contextType).c_str());
        }
    }

    return (range > 0) ? (rand() % range) : 0;
}

void NodeWrapperImpl::NodeShutdown() {
    {
        LockGuard lock(this);

        if (!LockedEnsureFocus(true)) {
            return;
        }

        if (InStub()) {
            LockedReportEnd(true, "NodeShutdown in stub");
            return;
        }

        // TODO: how to ensure that the caller is not from asyncJobPool? Otherwise it could be deadlocked.

        if (_isRunning) {
            LockedReportShutdown();
            LockedSetEnd(false);
        }
        else {
            return;
        }
    }

    Join();
}

void NodeWrapperImpl::HandleAsyncOpInAppContext(IOperation * op) {
    // We don't want to call execute nestedly, so we maintain a local queue to do the async operations.
    auto contextInfo = GetContextInfo();
    contextInfo->offlineOperationQueue.push_back(op);
    // Only handle the queue in top level
    if (contextInfo->contextType == ContextType::Normal || contextInfo->contextType == ContextType::WrapperLoop) {
        auto && queue = contextInfo->offlineOperationQueue;
        while (!queue.empty()) {
            auto headOp = queue.front();
            queue.pop_front();
            // ops will take care of them self
            headOp->Execute();
        }
    }
}

void NodeWrapperImpl::BeginDetachMode() {
    LockGuard lock(this);

    if (_isEnd || !_isRunning || _detachMode)
        return;

    if (IsPure()) {
        LockedReportEnd(true, "BeginDetachMode in pure context");
        return;
    }

    LockedSendLog("Enter detach-mode.", VERBOSE_DEBUG);

    _detachMode = true;
}

void NodeWrapperImpl::EndDetachMode() {
    LockGuard lock(this);

    if (!LockedEnsureFocus(true)) {
        return;
    }

    if (!_isRunning || !_detachMode)
        return;

    if (InStub()) {
        LockedReportEnd(true, "EndDetachMode in stub");
        return;
    }

    if (_detachModeBarrierOp != nullptr) {
        LockedReportEnd(true, "Detach barrier already exists.");
        return;
    }

    {
        SyncOperation sop(this, nullptr, nullptr, nullptr, 0, false);

        _detachModeBarrierOp = &sop;

        --_activeContexts;
        _quiescentCV.notify_one();

        // if wrapper stop, the wrapper thread will eventually schedule this
        sop.LockedWait();
    }

    LockedSendLog("Leave detach-mode.", VERBOSE_DEBUG);

    _detachMode = false;
}

timestamp_t NodeWrapperImpl::GetTimeOffsetNs(bool addDependency) {
    LockGuard lock(this);

    if (!LockedEnsureFocus()) {
        return GetSystemTimestamp() - _startSystemTime;
    }

    if (!_isRunning) {
        return GetSystemTimestamp() - _startSystemTime;
    }

    if (!IsPure() && addDependency) {
        AddDependency(DEP_FLAG_READ, 0, "NodeClock");
    }

    return _nodeClock;
}

timestamp_t NodeWrapperImpl::InsertTimeEventAfter(timestamp_t delayNs) {
    LockGuard lock(this);

    timestamp_t timeout = _nodeClock + delayNs;
    InsertTimeEventAt(timeout);

    return timeout;
}

void NodeWrapperImpl::InsertTimeEventAt(timestamp_t timeNs) {
    LockGuard lock(this);

    if (!LockedEnsureFocus(true))
        return;

    if (!_isRunning)
        return;

    if (timeNs == _nodeClock) {
        return;
    }
    else if (timeNs < _nodeClock) {
        LockedSendLog(Formatted("Insert stale time event at %llu, current clock = %llu", timeNs, _nodeClock).CString(), VERBOSE_INFO); 
        // LockedReportEnd(true, Formatted("Time event point %ull is not advancing to %ull", timeNs, _nodeClock).CString());
        return;
    }

    auto ct = GetContextInfo()->contextType;
    if (ct != Normal && ct != Stub) {
        LockedReportEnd(true, "InsertTimeEvent in place other than normal and local stub");
        return;
    }

    _timeEvents.insert(timeNs);
}


timestamp_t NodeWrapperImpl::GetSystemTimestamp() {
    auto now = chrono::steady_clock::now();
    return chrono::duration_cast<chrono::nanoseconds>(now.time_since_epoch()).count();
}

void NodeWrapperImpl::AddDependency(int dependencyFlags, int id, const char * decoration) {
    LockGuard lock(this);

    if (!LockedEnsureFocus(true))
        return;

    if (!_isRunning)
        return;

    auto ctxInfo = GetContextInfo();
    if (IsPure()) {
        LockedReportEnd(true, "AddDependency in pure context");
        return;
    }
    
    string symbol;
    if (decoration == nullptr)
        symbol.assign(Formatted("%d:%d", _nodeId, id).CString());
    else {
        symbol.assign(Formatted("%d:%d-%s", _nodeId, id, decoration).CString());
    }

    if (dependencyFlags & DEP_FLAG_READ) {
        _currentDependency.readSet.insert(symbol);
    }

    if (dependencyFlags & DEP_FLAG_WRITE) {
        _currentDependency.writeSet.insert(symbol);
    }
}

void NodeWrapperImpl::AddGlobalDependency(int dependencyFlags, const char * symbol) {
    LockGuard lock(this);

    if (!LockedEnsureFocus(true))
        return;

    if (!_isRunning)
        return;

    auto ctxInfo = GetContextInfo();
    if (IsPure()) {
        LockedReportEnd(true, "AddGlobalDependency in pure context");
        return;
    }
    
    string globalSymbol;
    globalSymbol.assign(Formatted("G:%s", symbol).CString());

    if (dependencyFlags & DEP_FLAG_READ) {
        _currentDependency.readSet.insert(globalSymbol);
    }

    if (dependencyFlags & DEP_FLAG_WRITE) {
        _currentDependency.writeSet.insert(globalSymbol);
    }
}

int NodeWrapperImpl::MessageSend(int nodeId, const char * message, int nbytes) {
    LockGuard lock(this);

    if (!LockedEnsureFocus(true))
        return -1;

    if (!_isRunning)
        return -1;

    auto ct = GetContextInfo()->contextType;
    if (ct == ContextType::PureFunc) {
        LockedReportEnd(true, "ContextTransfer in pure func");
        return -1;
    }

    int token = -1;

    if (nodeId < 0) {
        token = _localTokenQueueTail++;
    }
    else {
        token = _globalMessageTokenCounter++;
        if (message == nullptr) {
            _remoteMessages.push_back(forward_as_tuple(token, nodeId, ""));
        }
        else if (nbytes < 0) {
            _remoteMessages.push_back(forward_as_tuple(token, nodeId, string(message)));
        }
        else {
            _remoteMessages.push_back(forward_as_tuple(token, nodeId, string(message, nbytes)));
        }
    }

    return token;
}

void NodeWrapperImpl::MessageReceive(bool fromRemote, int token) {
    LockGuard lock(this);

    if (!LockedEnsureFocus())
        return;

    if (!_isRunning)
        return;

    if (InStub()) {
        LockedReportEnd(true, "MessageReceive in stub");
        return;
    }
    
    auto ctxInfo = GetContextInfo();
    if (ctxInfo->managed) {
        LockedReportEnd(true, "MessageReceive in managed thread");
        return;
    }

    if (fromRemote) {
        if (_verboseStackInfo)
            SendStack("MessageReceive block", VERBOSE_DEBUG);
        while (token > _pendingRemoteToken && _isRunning) {
            _transferCV.wait(GetContextInfo()->wrapperLock);
        }
        if (_verboseStackInfo)
            LockedSendLog("MessageReceive unblock", VERBOSE_DEBUG);
        
        if (token != _pendingRemoteToken) {
            LockedReportEnd(true, "Pending remote token mismatch");
            return;
        }
        
        _pendingRemoteToken = -1;
    }
    else {
        if (token >= _localTokenQueueTail) {
            LockedReportEnd(true, "Invalid local token");
            return;
        }

        if (_verboseStackInfo)
            SendStack("MessageReceive block", VERBOSE_DEBUG);
        while (token > _pendingLocalToken && _isRunning) {
            _transferCV.wait(GetContextInfo()->wrapperLock);
        }
        if (_verboseStackInfo)
            LockedSendLog("MessageReceive unblock", VERBOSE_DEBUG);
        
        if (token != _pendingLocalToken) {
            LockedReportEnd(true, "Pending local token mismatch");
            return;
        }

        _pendingLocalToken = -1;
    }

    // Now the thread are managed after context transferred
    ctxInfo->managed = true;
}


int NodeWrapperImpl::MessageReceiveUnique(bool fromRemote) {
    LockGuard lock(this);

    if (!LockedEnsureFocus())
        return -1;

    if (!_isRunning)
        return -1;

    if (InStub()) {
        LockedReportEnd(true, "MessageReceiveUnique in stub");
        return -1;
    }

    auto ctxInfo = GetContextInfo();
    if (ctxInfo->managed) {
        LockedReportEnd(true, "MessageReceiveUnique in managed thread");
        return -1;
    }

    int ret;

    if (fromRemote) {
        if (_verboseStackInfo)
            SendStack("MessageReceiveUnique block", VERBOSE_DEBUG);
        while (_pendingRemoteToken < 0 && _isRunning) {
            _transferCV.wait(GetContextInfo()->wrapperLock);
        }
        if (_verboseStackInfo)
            LockedSendLog("MessageReceiveUnique unblock", VERBOSE_DEBUG);

        if (!_pendingRemoteTokenIsUnique) {
            LockedReportEnd(true, "More than one remote tokens pending in MessageReceiveUnique.");
            return -1;
        }

        ret = _pendingRemoteToken;
        _pendingRemoteToken = -1;
    }
    else {
        if (_verboseStackInfo)
            SendStack("MessageReceiveUnique block", VERBOSE_DEBUG);
        while (_pendingLocalToken < 0 && _isRunning) {
            _transferCV.wait(GetContextInfo()->wrapperLock);
        }
        if (_verboseStackInfo)
            LockedSendLog("MessageReceiveUnique unblock", VERBOSE_DEBUG);

        if (_localTokenQueueTail - _localTokenQueueHead > 1) {
            LockedReportEnd(true, "More than one local tokens pending in MessageReceiveUnique.");
            return -1;
        }

        ret = _pendingLocalToken;
        _pendingLocalToken = -1;
    }

    // Now the thread are managed after message delivered
    ctxInfo->managed = true;
    return ret;
}

void NodeWrapperImpl::RemoteCommand(int toNodeId, const char * command) {
    // Currently we assume that thrift will create a dedicate thread for each connection. 
    // So each command will execute in a individual thread and can block without deadlock.
    // If this breaks, we need to use fully async mechanism, which is too complicated.
    vector<string> endpointRet;

    {
        LockGuard lock(this);
        if (_isConnected) {
            // Getting the controller address -- since it's immutable, we could keep a cache on that.
            {
                ostringstream os;
                os << "ctrl:" << toNodeId;
                try {
                    _arbiter->GetVariables(endpointRet, { os.str() });
                }
                catch (const TException &) {
                    LockedSetEnd(true);
                    return;
                }
            }

            if (endpointRet.size() == 0 || endpointRet[0].size() == 0)
                return;
        }
    }

    auto transport = GetTransport(endpointRet[0]);

    try {
        transport->open();
        auto controller = new ControllerClient(boost::shared_ptr<TProtocol>(new TBinaryProtocol(transport)));
        controller->ExecuteCommand(_nodeId, command);
    }
    catch (const TException &) {
        // Ignore any errors.
    }
    transport->close();
}

void NodeWrapperImpl::SetVariable(const char * name, const char * value) {
    LockGuard lock(this);

    // Set local metadata in unmanaged thread is fine
    if (!LockedEnsureFocus())
        return;

    if (!_isRunning)
        return;

    if (GetContextInfo()->contextType == ContextType::PureFunc) {
        LockedReportEnd(true, "SetVariable in pure func");
        return;
    }

    map<string, string> tempMap;
    tempMap[name] = value;

    try {
        _arbiter->SetVariables(tempMap);
    }
    catch (const TException &) {
        LockedSetEnd(true);
    }
}

const char * NodeWrapperImpl::GetVariable(const char * name) {
    LockGuard lock(this);

    if (!LockedEnsureFocus())
        return "";

    if (!_isConnected)
        return "";

    vector<string> ret;
    try {
        _arbiter->GetVariables(ret, { name });
    }
    catch (const TException &) {
        LockedSetEnd(true);
        return "";
    }

    if (ret.size() > 0) {
        _variableValue = ret[0];
        return _variableValue.c_str();
    }
    else {
        return "";
    }
}

void NodeWrapperImpl::SendLog(const char * log, int level) {
    LockGuard lock(this);

    LockedSendLog(log, level);
}

void NodeWrapperImpl::LockedSendLog(const char * log, int level) {
    if (level > _logVerboseLevel) return;

    if (_isRunning && !_isEnd) {
        if (_logBlocked)
            _logBuffer.push_back(Formatted("(%d) %s", GetContextId(), log).CString());
        else {
            ReportData data;
            data.isEnd = false;
            data.description = Formatted("(%d) %s", GetContextId(), log).CString();
            try {
                _arbiter->Report(data);
            }
            catch (const TException &) {
                LockedSetEnd(true);
            }
        }
    }
    else if (_printMissedLog) {
        *_miscLog << Formatted("Missed log [%d]: (%d) %s", _nodeId, GetContextId(), log).CString() << endl;
    }
}

void NodeWrapperImpl::SendLogLines(const char * headerLine, const char * const * logLines, int level) {
    LockGuard lock(this);

    LockedSendLogLines(headerLine, logLines, level);
}

void NodeWrapperImpl::GetStack(int skip, vector<string> & outStack) {
#ifdef _MSVC
    // Credit: http://stackoverflow.com/questions/5693192/win32-backtrace-from-c-code
    unsigned int   i;
    void         * stack[256];
    unsigned short frames;
    SYMBOL_INFO  * symbol;
    HANDLE         proc = GetCurrentProcess();

    frames = CaptureStackBackTrace(skip + 1, 256, stack, NULL);
    symbol = (SYMBOL_INFO *)calloc(sizeof(SYMBOL_INFO) + 256 * sizeof(char), 1);
    symbol->MaxNameLen = 255;
    symbol->SizeOfStruct = sizeof(SYMBOL_INFO);

    for (i = 0; i < frames; i++)
    {
        SymFromAddr(proc, (DWORD64)(stack[i]), 0, symbol);
        outStack.push_back(GenString(frames - i - 1, ": ", symbol->Name, " - 0x", symbol->Address));
    }

    free(symbol);
#else
    void * stack[256];
    int n = backtrace(stack, 256);
    char ** symbols = nullptr;

    if (n <= skip) return;

    if (getenv("DMC_NO_BACKTRACE_SYMBOL") == nullptr)
        symbols = backtrace_symbols(stack, n - skip);

    for (int i = 0; i < n - skip; ++i) {
        if (symbols) { 
            outStack.push_back(GenString(i, ':', symbols[i]));
        }
        else {
            outStack.push_back(GenString(i, ':', stack[i]));
        }
    }

    free(symbols);
    
#endif
}

void NodeWrapperImpl::SendStack(const char * desc, int level) {
    if (level > _logVerboseLevel) return;

    vector<string> stack;
    vector<const char *> logLines;

    stack.push_back(GenString("Stack of thread ", GetContextId(), ":"));
    GetStack(1, stack);

    for (auto && level : stack) {
        logLines.push_back(level.c_str());
    }
    logLines.push_back(nullptr);
    
    SendLogLines(desc, logLines.data(), level);
}

void NodeWrapperImpl::LockedSendLogLines(const char * headerLine, const char * const * logLines, int level) {
    if (level > _logVerboseLevel) return;
    int contextId = GetContextId();

    if (_isRunning && !_isEnd) {
        if (_logBlocked) {
            _logBuffer.push_back(Formatted("(%d) %s", contextId, headerLine).CString());
            for (auto cur = logLines; *cur != nullptr; ++cur) {
                _logBuffer.push_back(Formatted("(%d) %s", contextId, *cur).CString());
            }
        }
        else {
            ReportData data;
            data.isEnd = false;
            data.description = headerLine;
            for (auto cur = logLines; *cur != nullptr; ++cur) {
                data.info.push_back(Formatted("(%d) %s", contextId, *cur).CString());
            }
            try {
                _arbiter->Report(data);
            }
            catch (const TException &) {
                LockedSetEnd(true);
            }
        }
    }
    else if (_printMissedLog) {
        for (auto cur = logLines; *cur != nullptr; ++cur) {
            *_miscLog << "Missed log: " << Formatted("(%d) %s", contextId, *cur).CString() << endl;
        }
    }
}

void NodeWrapperImpl::LocalYield() {
    ScheduleSync(nullptr, nullptr, nullptr, "<LocalYield>", -1, OP_FLAG_LOCAL_DETACH);
}

void NodeWrapperImpl::SetCommandHandler(void(* commandHandler)(void *, int, const char *, int), void * data) {
    LockGuard lock(this);

    if (InStub()) {
        LockedReportEnd(true, "SetCommandHandler in stub");
        return;
    }

    _commandHandler = commandHandler;
    _commandHandlerData = data;
}

void NodeWrapperImpl::LockedReportEnd(bool isError, const char * message) {
    // We are running as a stub, report the error directly and exit.
    // Assume the lock is held.
    ReportData data;
    data.isEnd = true;
    data.isError = isError;
    data.description = message;
    data.info.push_back(GenString("Stack of context ", GetContextId(), ":"));
    GetStack(1, data.info);

    _lastError = message;

    if (!_isConnected) {
        for (auto && line : data.info) {
            *_miscLog << line << endl;
        }
        *_miscLog << "end without connection (error=" << isError << "): " << message << endl;
    }
    else if (!_isEnd) {
        try {
            _arbiter->Report(data);
        }
        catch (const TException &) { }
        LockedSetEnd(isError);
    }
    else if (_printMissedLog) {
        for (auto && line : data.info) {
            *_miscLog << line << endl;
        }
        *_miscLog << "Post-end report (error=" << isError << "): " << message << endl;
    }
}

void NodeWrapperImpl::LockedSetEnd(bool isError) {
    auto ctxInfo = GetContextInfo();
    
    _isEnd = true;
    
    ctxInfo->wrapperLock.unlock();
    // notify LockedWaitSchedule()
    _choiceLock.lock();
    _choiceAvailable = true;
    _choiceCV.notify_one();
    _choiceLock.unlock();
    ctxInfo->wrapperLock.lock();

    _quiescentCV.notify_one();
}

// TODO: regular routines should be aware of _isEnd
void NodeWrapperImpl::CheckAssert(bool shouldBeTrue, const char * comment) {
    LockGuard lock(this);
    if (!shouldBeTrue && _isRunning && !_isEnd) {
        LockedReportEnd(true, Formatted("Assert \"%s\" failed.", comment).CString());
    }
}

void NodeWrapperImpl::End(int error, const char * comment) {
    LockGuard lock(this);
    if (_isRunning && !_isEnd) {
        LockedReportEnd(error != 0, Formatted("End (error=%d): %s.", error, comment).CString()); 
    }
}


void NodeWrapperImpl::ContextDetach() {
    LockGuard lock(this);

    if (!LockedEnsureFocus(true))
        return;

    if (!_isRunning)
        return;
    
    if (InStub()) {
        LockedReportEnd(true, "ContextDestroy in stub");
        return;
    }

    --_activeContexts;
    _quiescentCV.notify_one();

    // Now the context is unmanaged.
    GetContextInfo()->managed = false;
}

void NodeWrapperImpl::Join() {
    if (_wrapperThread != nullptr) 
        _wrapperThread->join();
    _asyncJobPool.Join();
}

int SyncOperation::GetState() {
    if (_getStateFunc == nullptr) return STATE_ENABLED;
    else if (_cancelled) return STATE_CANCELLED;
    else {
        auto ctxInfo = _wrapper->GetContextInfo();
        auto ct = ctxInfo->contextType;
        ctxInfo->contextType = ContextType::PureFunc;
        int ret = (*_getStateFunc)(_data);
        ctxInfo->contextType = ct;
        _cancelled = ret == STATE_CANCELLED;
        return ret;
    }
}

void SyncOperation::Execute() {
    // Only case that GetState() could return STATE_DISABLE is when wrapper exits
    ++_wrapper->_activeContexts;
    _scheduled = true;
	_scheduleCV.notify_one();
}

void SyncOperation::LockedWait() {
	auto && lock = _wrapper->GetContextInfo()->wrapperLock;
	while (!_scheduled)
		_scheduleCV.wait(lock);
}

void SyncOperation::Describe(string & outDesc) {
    outDesc = Formatted("%c%d[%s]@%d", _internal ? 'I' : 'L', _opId, _info.c_str(), _contextId).CString();
}

bool SyncOperation::IsWeak() { return _weak; }

int AsyncOperation::GetState() {
    if (_getState == nullptr) return STATE_ENABLED;
    else if (_cancelled) return STATE_CANCELLED;
    else {
        auto ctxInfo = _wrapper->GetContextInfo();
        auto ct = ctxInfo->contextType;
        ctxInfo->contextType = ContextType::PureFunc;
        int ret = (*_getState)(_data);
        ctxInfo->contextType = ct;
        _cancelled = ret == STATE_CANCELLED;
        return ret;
    }
}

void NodeWrapperImpl::BeforeAsyncJob(void * param) {
    auto wrapper = (NodeWrapperImpl *)param;
    wrapper->MessageReceiveUnique(false);
}

void NodeWrapperImpl::AfterAsyncJob(void * param) {
    auto wrapper = (NodeWrapperImpl *)param;
    wrapper->ContextDetach();
}

void AsyncOperation::RunAsJob(void * param) {
    auto op = (AsyncOperation *)param;
    op->_stub(op->GetState() == STATE_CANCELLED, op->_data);
    delete op;
}

void AsyncOperation::Execute() {
    auto ctxInfo = _wrapper->GetContextInfo();
    auto ct = ctxInfo->contextType;
    ctxInfo->contextType = ContextType::Stub;
    if (!_job) {
        // Only case that GetState() could return STATE_DISABLE is when wrapper exits
        _stub(GetState() == STATE_CANCELLED, _data);
        delete this;
    }
    else {
        _wrapper->MessageSend(-1, "AsyncJob", -1);
        _wrapper->_asyncJobPool.Start(RunAsJob, this);
    }
    ctxInfo->contextType = ct;
}

void AsyncOperation::Describe(string & outDesc) {
   outDesc = Formatted("%c%d[%s]", _internal ? 'I' : 'L', _opId, _info.c_str()).CString();
}

bool AsyncOperation::IsWeak() { return _weak; }
