#ifndef __DMC_TESTCASE_WRAPPER_H__
#define __DMC_TESTCASE_WRAPPER_H__

#if __cplusplus
extern "C" {
#endif

    typedef int (* get_state_f)(void * data);
    typedef void(* stub_f)(int cancelled, void * data);
    typedef void(* command_handler_f)(void * data, int from_node_id, const char * message, int nbytes);
        
#define DMC_OP_FLAG_WEAK 0x1
#define DMC_OP_FLAG_LOCAL_DETACH 0x2
#define DMC_OP_FLAG_NONDETACHABLE 0x4
#define DMC_OP_FLAG_ASYNC_JOB 0x8

#define DMC_DEP_FLAG_READ 0x1
#define DMC_DEP_FLAG_WRITE 0x2

#define DMC_STATE_ENABLED 0x0
#define DMC_STATE_DISABLED 0x1
#define DMC_STATE_CANCELLED 0x2

#define DMC_VERBOSE_ERROR 0x0
#define DMC_VERBOSE_WARNING 0x1
#define DMC_VERBOSE_DEFAULT 0x1
#define DMC_VERBOSE_INFO 0x02
#define DMC_VERBOSE_DEBUG 0x03

    struct context_ptr_store_ops_s {
        void *(* get)(void * ptr_store);
        void  (* reset)(void * ptr_store, void * ptr, void (* remove_hook)(void * ptr));
    };

#ifndef DMC_WRAPPER_IMPL
    typedef struct dmc_wrapper_s { } * dmc_wrapper_t;
#endif

    typedef unsigned long long dmc_timestamp_t;

    dmc_wrapper_t dmc_wrapper_get_default();
    dmc_wrapper_t dmc_wrapper_create();
    void dmc_wrapper_destroy(dmc_wrapper_t wrapper);

    int dmc_wrapper_is_connected(dmc_wrapper_t wrapper);

    int  dmc_wrapper_init_node(dmc_wrapper_t wrapper, const char * endpoint, const char * cookie, struct context_ptr_store_ops_s * cps_ops, void * cps);
    int  dmc_wrapper_node_start(dmc_wrapper_t wrapper, int node_id);
    
    int dmc_wrapper_get_node_id(dmc_wrapper_t wrapper);
    
    const char * dmc_wrapper_get_variable(dmc_wrapper_t wrapper, const char * name);
    void dmc_wrapper_set_variable(dmc_wrapper_t wrapper, const char * name, const char * value);

    int  dmc_wrapper_message_send(dmc_wrapper_t wrapper, int node_id, const char * desc, int nbytes);
    void dmc_wrapper_message_receive(dmc_wrapper_t wrapper, int remote, int token);
    int  dmc_wrapper_message_receive_unique(dmc_wrapper_t wrapper, int remote);

    int dmc_wrapper_schedule_sync(dmc_wrapper_t wrapper, get_state_f state_func, stub_f stub_func, void * data, const char * desc, int nbytes, unsigned flags);
    void dmc_wrapper_schedule_async(dmc_wrapper_t wrapper, get_state_f state_func, stub_f stub_func, void * data, const char * desc, int nbytes, unsigned flags);

    dmc_timestamp_t dmc_wrapper_get_time_offset_ns(dmc_wrapper_t wrapper);
    void dmc_wrapper_insert_time_event_at(dmc_wrapper_t wrapper, dmc_timestamp_t ts);
    void dmc_wrapper_insert_time_event_after(dmc_wrapper_t wrapper, dmc_timestamp_t ts_after);

    void dmc_wrapper_send_log(dmc_wrapper_t wrapper, const char * log, int level);
    void dmc_wrapper_send_stack(dmc_wrapper_t wrapper, const char * message, int level);

    void dmc_wrapper_check_assert(dmc_wrapper_t wrapper, int should_be_non_zero, const char * message); 
    
    void dmc_wrapper_context_detach(dmc_wrapper_t wrapper);
    void dmc_wrapper_node_shutdown(dmc_wrapper_t wrapper);
    void dmc_wrapper_end(dmc_wrapper_t wrapper, int error, const char * comment);
    void dmc_wrapper_join(dmc_wrapper_t wrapper);
    
#if __cplusplus
}
#endif

#endif
