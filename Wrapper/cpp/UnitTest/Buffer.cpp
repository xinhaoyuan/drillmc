#include "UnitTest.hpp"
#include "Utils/cpp/Formatted.hpp"

using namespace DMC;
using namespace DMC::Wrapper;
using namespace std;
using namespace XYHelper;

void BufferTest::Run() {
    _wrapper->InitNode(nullptr, nullptr, nullptr);
    _wrapper->NodeStart(0);

    int id1 = _wrapper->CreateGlobalUniqueId();

    _wrapper->ScheduleSync(nullptr, nullptr, nullptr);
    
    int id2 = _wrapper->CreateGlobalUniqueId();

    _wrapper->SendLog(GenString("id1 = ", id1, ", id2 = ", id2).c_str());

    char buf[10];
    int ret;

    _wrapper->BufferOpen(id1);
    _wrapper->BufferOpen(id2);

    ret = _wrapper->BufferWrite(id1, "Hello", 5);
    _wrapper->SendLog(GenString("write ret = ", ret).c_str());

    ret = _wrapper->BufferWrite(id1, "World", 5);
    _wrapper->SendLog(GenString("write ret = ", ret).c_str());

    ret = _wrapper->BufferRead(id1, buf, 10);
    _wrapper->SendLog(GenString("read ret = ", ret , "[", string(buf, ret), "]").c_str());
    
    ret = _wrapper->BufferRead(id1, buf, 10);
    _wrapper->SendLog(GenString("read ret = ", ret , "[", string(buf, ret), "]").c_str());

    ret = _wrapper->BufferWrite(id2, "Hello", 5);
    _wrapper->SendLog(GenString("write ret = ", ret).c_str());

    ret = _wrapper->BufferRead(id2, buf, 10);
    _wrapper->SendLog(GenString("read ret = ", ret , "[", string(buf, ret), "]").c_str());
    
    ret = _wrapper->BufferWrite(id2, "World", 5);
    _wrapper->SendLog(GenString("write ret = ", ret).c_str());
    
    ret = _wrapper->BufferRead(id2, buf, 10);
    _wrapper->SendLog(GenString("read ret = ", ret , "[", string(buf, ret), "]").c_str());

    _wrapper->BufferClose(id1);
    _wrapper->BufferClose(id2);

    ret = _wrapper->BufferWrite(id2, "World", 5);
    _wrapper->SendLog(GenString("write ret = ", ret).c_str());
    
    ret = _wrapper->BufferRead(id2, buf, 10);
    _wrapper->SendLog(GenString("read ret = ", ret , "[", string(buf, ret), "]").c_str());


    _wrapper->ContextDetach();
    _wrapper->Join();
}
