#include "UnitTest.hpp"
#include <iostream>
#include <thread>
#include <mutex>

using namespace std;

namespace DMC {
    using namespace Wrapper;
    using namespace Helper;
}

using namespace DMC;

void LocalAsyncTest::Run() {
    _wrapper->InitNode(nullptr, nullptr, nullptr);
    _wrapper->NodeStart(0);

    // Enable this will lead a error to report
#if 0
    _wrapper->ScheduleAsync(nullptr, &Stub, new StubClosure([=](int cancelled) {
        _wrapper->SendLog("First time in async stub");
        _wrapper->ScheduleSync(nullptr, nullptr);
        _wrapper->SendLog("Second time in async stub");
    }));
#endif

    auto ac1 = new OpDesc([]()->int { return STATE_ENABLED; }, [=](int cancelled) {
        _wrapper->SendLog("First time in async 1");
        _wrapper->ScheduleSync(nullptr, nullptr, nullptr);
        _wrapper->SendLog("Second time in async 1");
        SerializeByIdLeave(_wrapper, 0);
    });

    auto ac2 = new OpDesc([]()->int { return STATE_ENABLED; }, [=](int cancelled) {
        _wrapper->SendLog("First time in async 2");
        _wrapper->ScheduleSync(nullptr, nullptr, nullptr);
        _wrapper->SendLog("Second time in async 2");
        SerializeByIdLeave(_wrapper, 0);
    });

    _wrapper->ScheduleAsync(DescGetState, DescStub, SerializeByIdEnter(_wrapper, 0, ac1), nullptr, -1, OP_FLAG_ASYNC_JOB);

    _wrapper->ScheduleAsync(DescGetState, DescStub, SerializeByIdEnter(_wrapper, 0, ac2), nullptr, -1, OP_FLAG_ASYNC_JOB);

    _wrapper->ContextDetach();
    _wrapper->Join();
}
