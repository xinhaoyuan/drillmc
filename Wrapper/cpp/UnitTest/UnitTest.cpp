#include "UnitTest.hpp"
#include <iostream>
#include <map>

using namespace std;
using namespace DMC;
using namespace DMC::Wrapper;
using namespace DMC::Wrapper::Helper;

void UnitTest::Assert(bool condition, const string & description) {
    if (!condition)
        throw runtime_error(description);
}

static void PrintTests(const map<string, UnitTest *> & tests) {
    cout << "Available tests:" << endl;
    for (auto && kv : tests) {
        cout << '\t' << get<0>(kv) << endl;
    }
}

// Some simple tests go here.

void RealDeadlockTest::Run() {
    _wrapper->InitNode(nullptr, nullptr, nullptr);
    _wrapper->NodeStart(0);

    _wrapper->ScheduleSync([](void * param)->int {
        return STATE_DISABLED;
    }, nullptr, nullptr);

    _wrapper->ContextDetach();
    _wrapper->Join();
}

void FakeDeadlockTest::Run() {
    _wrapper->InitNode(nullptr, nullptr, nullptr);
    _wrapper->NodeStart(0);

    _wrapper->ScheduleSync([](void * param)->int {
        return STATE_DISABLED;
    }, nullptr, nullptr, "", -1, OP_FLAG_WEAK);

    _wrapper->ContextDetach();
    _wrapper->Join();
}

void NotifyTest::Run() {
    _wrapper->InitNode(nullptr, nullptr, nullptr);
    _wrapper->NodeStart(0);

    _wrapper->Notify(0);

    _wrapper->ScheduleSync(nullptr, nullptr, nullptr);

    _wrapper->Notify(0);
    
    _wrapper->ContextDetach();
    _wrapper->Join();
}

void BarrierTest::Run() {
    _wrapper->InitNode(nullptr, nullptr, nullptr);
    _wrapper->NodeStart(0);

    _wrapper->ScheduleAsync(nullptr, [](int, void *) {}, nullptr, "<Barrier>");
    _wrapper->ScheduleAsync(nullptr, [](int, void *) {}, nullptr, "<Barrier:99>");
    _wrapper->ScheduleAsync(nullptr, [](int, void *) {}, nullptr, "<Barrier:101>");
    _wrapper->ScheduleAsync(nullptr, [](int, void *) {}, nullptr, "<Barrier>");
    _wrapper->ScheduleAsync(nullptr, [](int, void *) {}, nullptr, "<Barrier:99>");

    _wrapper->ContextDetach();
    _wrapper->Join();
}

void NormalEndTest::Run() {
    _wrapper->InitNode(nullptr, nullptr, nullptr);
    _wrapper->NodeStart(0);

    _wrapper->ScheduleAsync(nullptr, [](int, void *) {}, nullptr, "Foo");
    _wrapper->ScheduleSync(nullptr, nullptr, nullptr, "Bar");
    _wrapper->ScheduleAsync(nullptr, [](int, void *) {}, nullptr, "Foo2");
    
    _wrapper->End(0, "NormalEnd");

    _wrapper->ContextDetach();
    _wrapper->Join();
}

void ErrorEndTest::Run() {
    _wrapper->InitNode(nullptr, nullptr, nullptr);
    _wrapper->NodeStart(0);

    _wrapper->ScheduleAsync(nullptr, [](int, void *) {}, nullptr, "Foo");
    _wrapper->ScheduleSync(nullptr, nullptr, nullptr, "Bar");
    _wrapper->ScheduleAsync(nullptr, [](int, void *) {}, nullptr, "Foo2");
    
    _wrapper->End(1, "ErrorEnd");

    _wrapper->ContextDetach();
    _wrapper->Join();
}

int main(int argc, char ** argv) {
    map<string, UnitTest *> tests = {
        { "CancelledAsync", new CancelledAsyncTest() },
        { "Time", new TimeTest() },
        { "PingPong", new PingPongTest() },
        { "LocalSync", new LocalSyncTest() },
        { "LocalAsync", new LocalAsyncTest() },
        { "LocalError1", new LocalErrorTest1() },
        { "Command", new CommandTest() },
        { "Choose", new ChooseTest() },
        { "RealDeadlock", new RealDeadlockTest() },
        { "FakeDeadlock", new FakeDeadlockTest() },
        { "Buffer", new BufferTest() },
        { "Notify", new NotifyTest() },
        { "Barrier", new BarrierTest() },
        { "NormalEnd", new NormalEndTest() },
        { "ErrorEnd", new ErrorEndTest() },
    };

    if (argc < 2) {
        cerr << "Must specify test name" << endl;
        PrintTests(tests);
        return -1;
    }

    auto it = tests.find(argv[1]);
    if (it != end(tests)) {
        get<1>(*it)->Run();
    }
    else {
        cerr << "Cannot find the test - " << argv[1] << endl;
        PrintTests(tests);
        return -1;
    }

    return 0;
}
