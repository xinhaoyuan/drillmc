#include "UnitTest.hpp"
#include <iostream>
#include <tuple>
#include <functional>

using namespace std;
using namespace DMC;
using namespace DMC::Wrapper;

void TimeTest::Run() {
    _wrapper->InitNode(nullptr, nullptr, nullptr);
    _wrapper->NodeStart(0);

    _wrapper->InsertTimeEventAt(10);
    _wrapper->InsertTimeEventAt(20);

    {
        auto tp = new tuple<function<int()>, function<void(int)>>([this]()->int {
            if (_wrapper->GetTimeOffsetNs() < 10)
                return STATE_DISABLED;
            else if (_wrapper->GetTimeOffsetNs() < 20)
                return STATE_ENABLED;
            else return STATE_CANCELLED;
        }, [this](int cancelled) {
            cerr << "TimeOffset: " << _wrapper->GetTimeOffsetNs() << ", cancelled: " << cancelled << endl;
        });

        _wrapper->ScheduleAsync([](void * param)->int {
            auto tp = (tuple<function<int()>, function<void(int)>> *)param;
            return get<0>(*tp)();
        }, [](int cancelled, void * param) {
            auto tp = (tuple<function<int()>, function<void(int)>> *)param;
            get<1>(*tp)(cancelled);
            delete tp;
        }, tp);
    }

    {
        auto getState = new function<int()>([this]()->int {
            if (_wrapper->GetTimeOffsetNs() < 10)
                return STATE_DISABLED;
            else return STATE_ENABLED;
        });

        _wrapper->ScheduleSync([](void * param)->int {
            auto func = (function<int()> *)param;
            return (*func)();
        }, nullptr, getState);

        delete getState;
    }
    
    cout << _wrapper->GetTimeOffsetNs() << endl;

    _wrapper->ContextDetach();
    _wrapper->Join();
}
