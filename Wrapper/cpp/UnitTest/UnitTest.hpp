#ifndef __DMC_WRAPPER_UNIT_TEST_HPP__
#define __DMC_WRAPPER_UNIT_TEST_HPP__

#include "Utils/cpp/LWN.hpp"
#include "Wrapper/cpp/DMCWrapperEx.hpp"
#include <thread>
#include <string>
#include <mutex>
#include <condition_variable>

LWN_START(DMC)

using namespace std;
using namespace Wrapper;

LWN_EXPORT;

class UnitTest {

protected:

    INodeWrapper * _wrapper;
    
    void Assert(bool condition, const string & description);

public:

    UnitTest() : _wrapper(GetDMCNodeInstance()) {}

    virtual void Run() = 0;
};

class CancelledAsyncTest : public UnitTest {
    
    bool opCancelled;

public:
    CancelledAsyncTest()
        : opCancelled(false) 
    {}
    
    void Run() override;
};

class TimeTest : public UnitTest {
    
    bool opCancelled;

public:
    TimeTest()
        : opCancelled(false)
    {}

    void Run() override;
};

class PingPongTest : public UnitTest {
public:

    enum Message {
        None,
        Ping,
        Pong
    };

    mutex curMessageLock;
    condition_variable curMessageCV;
    Message curMessage = Message::None;

    void Run() override;
};

class LocalSyncTest : public UnitTest {

    thread * t;
    mutex _lock1;
    mutex _lock2;
    int _lock1Id;
    int _lock2Id;

public:

    LocalSyncTest()
        : t(nullptr)
    {}

    void ThreadEntry();
    void Run() override;
};

class LocalAsyncTest : public UnitTest {
public:
    void Run() override;
};


class LocalErrorTest1 : public UnitTest {

    thread * t;
    int counter;
    mutex _lock;

public:

    LocalErrorTest1()
        : t(nullptr)
        , counter(0)
    {}

    void ThreadEntry();
    void Run() override;
};

class CommandTest : public UnitTest {
public:

    void Run() override;
};

class ChooseTest : public UnitTest {
public:

    void Run() override;
};

class RealDeadlockTest : public UnitTest {
public:

    void Run() override;
};

class FakeDeadlockTest : public UnitTest {
public:

    void Run() override;
};

class BufferTest : public UnitTest {
public:

    void Run() override;
};

class NotifyTest : public UnitTest {
public:

    void Run() override;
};

class BarrierTest : public UnitTest {
public:

    void Run() override;
};

class NormalEndTest : public UnitTest {
public:

    void Run() override;
};

class ErrorEndTest : public UnitTest {
public:

    void Run() override;
};

LWN_END;

#endif
