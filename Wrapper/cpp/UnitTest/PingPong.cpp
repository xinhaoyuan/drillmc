#include "UnitTest.hpp"
#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>
#include "Utils/cpp/Closure.hpp"
#include <string>

using namespace std;
using namespace DMC;
using namespace DMC::Wrapper;
using namespace XYHelper;

class Node {
    PingPongTest *_test;
    int _nodeId;
    INodeWrapper * _wrapper;
    thread * _thread;
    
public:

    Node(PingPongTest * test, int nodeId)
        : _test(test)
        , _nodeId(nodeId)
        , _wrapper(nullptr)
        , _thread(nullptr) {
    }

    static void ThreadEntryHelper(void * param) {
        ((Node *)param)->ThreadEntry();
    }

    void ThreadEntry() {
        _wrapper = NewDMCNodeInstance();
        _wrapper->InitNode("localhost:18390", nullptr, nullptr);
        _wrapper->NodeStart(_nodeId);
        
        if (_nodeId == 0) {
            int slaveId = _wrapper->RegisterNewNode();
            Node * slave;

            _wrapper->ScheduleAsync(nullptr, &Helper::Stub, new Helper::StubClosure([&](int canclled) {
                cerr << "transfer start" << endl;
                _wrapper->MessageSend(slaveId, "start slave");
                _wrapper->MessageSend(_nodeId, "notify back");
                cerr << "transfer done" << endl;
                slave = new Node(_test, slaveId);
                slave->Start();
            }), DMC_CODE_HERE);

            _wrapper->ContextDetach();

            _wrapper->MessageReceiveUnique(true);

            _wrapper->ScheduleAsync(nullptr, &Helper::Stub, new Helper::StubClosure([=](int cancelled) {
                lock_guard<mutex> guard(_test->curMessageLock);
                _wrapper->MessageSend(slaveId, "PING");
                _test->curMessage = PingPongTest::Message::Ping;
                _test->curMessageCV.notify_all();
                cerr << "Ping sent" << endl;
            }), DMC_CODE_HERE);

            _wrapper->ContextDetach();

            unique_lock<mutex> ul(_test->curMessageLock, defer_lock);
            ul.lock();
            while (_test->curMessage != PingPongTest::Message::Pong)
                _test->curMessageCV.wait(ul);
            ul.unlock();

            _wrapper->MessageReceiveUnique(true);

            cerr << "Master logically finished" << endl;
            
            _wrapper->ContextDetach();
            _wrapper->Join();

            slave->Join();
            
            cerr << "Master thread finished" << endl;

        }
        else {
            _wrapper->ContextDetach();

            unique_lock<mutex> ul(_test->curMessageLock, defer_lock);
            ul.lock();
            while (_test->curMessage != PingPongTest::Message::Ping)
                _test->curMessageCV.wait(ul);
            ul.unlock();

            _wrapper->MessageReceiveUnique(true);

            _wrapper->ScheduleAsync(nullptr, &Helper::Stub, new Helper::StubClosure([=](int cancelled) {
                lock_guard<mutex> guard(_test->curMessageLock);
                _wrapper->MessageSend(0, "PONG");
                _test->curMessage = PingPongTest::Message::Pong;
                _test->curMessageCV.notify_all();
                cerr << "Pong sent" << endl;
            }), DMC_CODE_HERE);

            cerr << "Slave logically finished" << endl;
            _wrapper->ContextDetach();
            _wrapper->Join();
            cerr << "Slave thread finished" << endl;
        }
    }

    void Start() {
        _thread = new thread(ThreadEntryHelper, this);
    }

    void Join() {
        _thread->join();
    }
};

void PingPongTest::Run() {
    cerr << "testcase starts" << endl;

    Node * master = new Node(this, 0);
    master->Start();
    master->Join();
    cerr << "testcase ends" << endl;
}
