#include "UnitTest.hpp"
#include <string>
#include <iostream>
#include <sstream>

using namespace DMC;
using namespace DMC::Wrapper;
using namespace std;

static void CommandHandler(void * param, int fromNodeId, const char * command, int nbytes) {
    cerr << "Got command " << string(command, nbytes)  << " from " << fromNodeId << endl;
}

void CommandTest::Run() {
    _wrapper->InitNode(nullptr, nullptr, nullptr);

    {
        ostringstream os;
        os << "Hello before start, cfg:foo = " << _wrapper->GetVariable("cfg:foo");
        _wrapper->SendLog(os.str().c_str());
    }
    
    _wrapper->NodeStart(0);
    _wrapper->SetCommandHandler(CommandHandler, nullptr);
    _wrapper->RemoteCommand(0, "Hello world");
    
    _wrapper->ContextDetach();
    _wrapper->Join();
}

