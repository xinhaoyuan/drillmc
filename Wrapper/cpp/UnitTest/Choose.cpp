#include "UnitTest.hpp"
#include <iostream>

using namespace std;
using namespace DMC;
using namespace DMC::Wrapper::Helper;

void ChooseTest::Run() {
    _wrapper->InitNode(nullptr, nullptr, nullptr);
    _wrapper->NodeStart(0);

    auto tp = new OpDesc([=]()->int {
        return STATE_ENABLED;
    }, [=](int cancelled) {
        // cerr << "Choose in async gives me " << _wrapper->Choose(2) << endl;

        auto cl = new StubClosure([=](int) {
            // cerr << "Choose in remote gives me " << _wrapper->Choose(2) << endl;
            cerr << "Choose in A-async gives me " << _wrapper->Choose(2) << endl;
        });

        _wrapper->ScheduleAsync(nullptr, &Stub, cl, "A-async");

        // cerr << "Choose in async gives me " << _wrapper->Choose(2) << endl;
    });

    // cerr << "Choose gives me " << _wrapper->Choose(2) << endl;

    _wrapper->ScheduleAsync(&DescGetState, &DescStub, tp, "Async");

    // cerr << "Choose gives me " << _wrapper->Choose(2) << endl;

    _wrapper->ScheduleSync(nullptr, nullptr, nullptr, "Sync1");

    _wrapper->ScheduleSync(nullptr, nullptr, nullptr, "Sync2");

    //cerr << "Choose gives me " << _wrapper->Choose(2) << endl;

    //cerr << "Choose gives me " << _wrapper->Choose(2) << endl;

    //_wrapper->ScheduleSync(nullptr, nullptr);

    //_wrapper->ScheduleSync(nullptr, nullptr);

    //cerr << "Choose gives me " << _wrapper->Choose(2) << endl;

    //cerr << "Choose gives me " << _wrapper->Choose(2) << endl;

    _wrapper->ContextDetach();
    _wrapper->Join();
}
