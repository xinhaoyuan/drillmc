#include "UnitTest.hpp"
#include <iostream>
#include <tuple>
#include <functional>

using namespace std;
using namespace DMC;
using namespace DMC::Wrapper;

void CancelledAsyncTest::Run() {
    _wrapper->InitNode(nullptr, nullptr, nullptr);
    _wrapper->NodeStart(0);

    {
        auto tp = new tuple<function<int()>, function<void(int)>>([this]()->int {
            if (opCancelled) return STATE_CANCELLED;
            else return STATE_ENABLED;
        }, [](int cancelled) {
            if (cancelled) {
                cerr << "op 1 cancelled" << endl;
            }
            else {
                cerr << "op 1 executed" << endl;
            }
        });

        _wrapper->ScheduleAsync([](void * param)->int {
            auto tp = (tuple<function<int()>, function<void(int)>> *)param;
            return get<0>(*tp)();
        }, [](int cancelled, void * param) {
            auto tp = (tuple<function<int()>, function<void(int)>> *)param;
            get<1>(*tp)(cancelled);
            delete tp;
        }, tp);
    }

    {
        auto tp = new tuple<function<int()>, function<void(int)>>([this]()->int {
            return STATE_ENABLED;
        }, [this](int cancelled) {
            if (cancelled) {
                cerr << "op 2 cancelled" << endl;
            }
            else {
                opCancelled = true;
                cerr << "op 2 executed, cancelled op 1" << endl;
            }
        });

        _wrapper->ScheduleAsync([](void * param)->int {
            auto tp = (tuple<function<int()>, function<void(int)>> *)param;
            return get<0>(*tp)();
        }, [](int cancelled, void * param) {
            auto tp = (tuple<function<int()>, function<void(int)>> *)param;
            get<1>(*tp)(cancelled);
            delete tp;
        }, tp);
    }

    _wrapper->ContextDetach();
    _wrapper->Join();
}
