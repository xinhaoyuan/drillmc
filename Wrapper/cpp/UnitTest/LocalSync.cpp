#include "UnitTest.hpp"
#include <iostream>
#include <thread>
#include <mutex>

using namespace std;
using namespace DMC;
using namespace DMC::Wrapper;
using namespace DMC::Wrapper::Helper;

static void threadEntry(void * param) {
    LocalSyncTest * test = (LocalSyncTest *)param;
    test->ThreadEntry();
}

// TODO - Test the shared lock

void LocalSyncTest::ThreadEntry() {
    int tid = _wrapper->GetContextId();

    _wrapper->MessageReceiveUnique(false);
    _wrapper->ScheduleSync(&DescGetState, &DescStub, LockEnter(_wrapper, _lock1Id, tid, true, false, nullptr), DMC_CODE_HERE);
    _wrapper->AddDependency(DEP_FLAG_WRITE, _wrapper->GetIdForPtr(&_lock1));
    _lock1.lock();
    _wrapper->ScheduleSync(&DescGetState, &DescStub, LockEnter(_wrapper, _lock2Id, tid, true, false, nullptr), DMC_CODE_HERE);
    _wrapper->AddDependency(DEP_FLAG_WRITE, _wrapper->GetIdForPtr(&_lock2));
    _lock2.lock();

    _wrapper->ScheduleSync(nullptr, nullptr, nullptr, "HERE");
    _wrapper->SendLog("the other thread");

    _wrapper->ScheduleSync(nullptr, nullptr, nullptr, DMC_CODE_HERE);
    LockLeave(_wrapper, _lock2Id, tid, true);
    _wrapper->AddDependency(DEP_FLAG_WRITE, _lock2Id);
    _lock2.unlock();
    _wrapper->ScheduleSync(nullptr, nullptr, nullptr, DMC_CODE_HERE);
    LockLeave(_wrapper, _lock1Id, tid, true);
    _wrapper->AddDependency(DEP_FLAG_WRITE, _lock1Id);
    _lock1.unlock();

    // cerr << "thread exited" << endl;
    _wrapper->ContextDetach();
}

void LocalSyncTest::Run() {
    // cerr << "testcase starts" << endl;

    _wrapper->InitNode(nullptr, nullptr, nullptr);
    _wrapper->NodeStart(0);

    _lock1Id = _wrapper->GetIdForPtr(&_lock1);
    _lock2Id = _wrapper->GetIdForPtr(&_lock2);

    int tid = _wrapper->GetContextId();

    _wrapper->SendLog(DMC_GENCSTR("tid = ", tid, ", lock 1 id = ", _lock1Id, ", lock 2 id = ", _lock2Id));

    int token = _wrapper->MessageSend();
    t = new thread(threadEntry, this);
    _wrapper->LocalYield();

    _wrapper->ScheduleSync(&DescGetState, &DescStub, LockEnter(_wrapper, _lock1Id, tid, true, false, nullptr), DMC_CODE_HERE);
    _wrapper->AddDependency(DEP_FLAG_WRITE, _wrapper->GetIdForPtr(&_lock1));
    _lock1.lock();
    _wrapper->ScheduleSync(&DescGetState, &DescStub, LockEnter(_wrapper, _lock2Id, tid, true, false, nullptr), DMC_CODE_HERE);
    _wrapper->AddDependency(DEP_FLAG_WRITE, _wrapper->GetIdForPtr(&_lock2));
    _lock2.lock();

    _wrapper->ScheduleSync(nullptr, nullptr, nullptr, "HERE");
    _wrapper->SendLog("main thread");

    _wrapper->ScheduleSync(nullptr, nullptr, nullptr, DMC_CODE_HERE);
    LockLeave(_wrapper, _lock2Id, tid, true);
    _wrapper->AddDependency(DEP_FLAG_WRITE, _lock2Id);
    _lock2.unlock();
    _wrapper->ScheduleSync(nullptr, nullptr, nullptr, DMC_CODE_HERE);
    LockLeave(_wrapper, _lock1Id, tid, true);
    _wrapper->AddDependency(DEP_FLAG_WRITE, _lock1Id);
    _lock1.unlock();
    
    _wrapper->ContextDetach();
    _wrapper->Join();
    t->join();
}
