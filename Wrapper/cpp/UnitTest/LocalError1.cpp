#include "UnitTest.hpp"
#include <iostream>
#include <thread>
#include <mutex>

using namespace std;
using namespace DMC;
using namespace DMC::Wrapper;

static int lockStateObserver(void * __lock) {
    mutex * lock = (mutex *)__lock;
    if (lock->try_lock()) {
        lock->unlock();
        return DMC::Wrapper::STATE_ENABLED;
    }
    else {
        return DMC::Wrapper::STATE_DISABLED;
    }
}

static void threadEntry(void * param) {
    LocalErrorTest1 * test = (LocalErrorTest1 *)param;
    test->ThreadEntry();
}

void LocalErrorTest1::ThreadEntry() {
    _wrapper->MessageReceiveUnique(false);
    _wrapper->ScheduleSync(lockStateObserver, nullptr, &_lock, DMC_CODE_HERE);
    _lock.lock();
    ++counter;
    _wrapper->ScheduleSync(nullptr, nullptr, nullptr, DMC_CODE_HERE);
    _lock.unlock();
    _wrapper->ContextDetach();
}

void LocalErrorTest1::Run() {
    cerr << "testcase starts" << endl;
    
    _wrapper->InitNode(nullptr, nullptr, nullptr);
    _wrapper->NodeStart(0);
    
    _wrapper->MessageSend();
    t = new thread(threadEntry, this);
    _wrapper->LocalYield();
    
    _wrapper->ScheduleSync(lockStateObserver, nullptr, &_lock, DMC_CODE_HERE);
    _lock.lock();
    _wrapper->CheckAssert(counter == 0, "counter == 0");
    _wrapper->ScheduleSync(nullptr, nullptr, nullptr, DMC_CODE_HERE);
    _lock.unlock();
    
    _wrapper->ContextDetach();
    _wrapper->Join();
    t->join();
}
