#ifndef __DMC_TRANSFORMER_IDENTICAL_HPP__
#define __DMC_TRANSFORMER_IDENTICAL_HPP__

#include "Utils/cpp/LWN.hpp"
#include "Explorer/Core/Testcase.hpp"
#include "Explorer/Core/Operation.hpp"
#include <map>
#include <set>
#include <vector>

LWN_START(DMC);

using namespace std;

LWN_EXPORT;

// The identical transformer does nothing except forwarding information. Just an example.

class IdenticalTransformer : public ITarget {

    ITarget * _source;
    StateType _stateType;

    // Upper layer tracking info
    map<int, OperationDescription *> * _newOps;
    set<int> * _toggledOps;
    set<int> * _cancelledOps;
    DependencyRecord ** _dep;
    vector<MessageRecord *> * _msgs;
    vector<AnnotationRecord *> * _ants;

    // Internal bookkeeping
    map<int, OperationDescription *> _allOps;
    map<int, OperationDescription *> _enabledOps;

    void ProceedSource(int choice);

public:

    IdenticalTransformer(ITarget * source);

    void SetTracking(
        map<int, OperationDescription *> * newOps,
        set<int> * toggledOps,
        set<int> * cancelledOps,
        DependencyRecord ** dep,
        vector<MessageRecord *> * msgs,
        vector<AnnotationRecord *> * ants) override;

    virtual int GetInputNode() override;
    virtual int GetInputRange() override;

    StateType Setup() override;
    StateType Proceed(int choice) override;
    void CleanUp() override;

};

LWN_END;

#endif