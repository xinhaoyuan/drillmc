#include "Modular.hpp"
#include "Explorer/Core/ExplContext.hpp"
#include "Arbiter/RemoteTestcase.hpp"
#include <regex>

using namespace std;
using namespace DMC;
using namespace XYHelper;

const Option ModularTransformer::kOptionModularFocus = Option("modular.focus", "The regular expression for filtering operations");
const Option ModularTransformer::kOptionModularWaitOnCleanup = Option("modular.wait-on-cleanup", "Wait forever before clean up for debugging");

const regex ModularTransformer::_barrierRegEx = regex("<Barrier(:[0-9]+)?>");

void ModularTransformer::Register(list<Option> & options) {
    options.push_back(kOptionModularFocus);
    options.push_back(kOptionModularWaitOnCleanup);
}

void ModularTransformer::GetPriorityInfo(const string & name, bool & barrier, int & priority) {
    smatch match;
    if (regex_search(name, match, _barrierRegEx)) {
        if (match.size() == 2 && match[1].length() > 1) {
            // skip the ':'
            barrier = true;
            priority = atoi(match[1].str().c_str() + 1);
        }
        else {
            barrier = true;
            priority = 100;
        }
    }
    else if (name.find("<AdvanceClock>") != string::npos) {
        barrier = false;
        priority = 80;
    }
    else {
        barrier = false;
        priority = 0;
    }
}

bool ModularTransformer::IsSleepOp(const string & name) {
    return name.find("<Sleep>") != string::npos;
}

ModularTransformer::ModularTransformer(ITarget * source, Logger * logger)
    : _source(source)
    , _logger(logger)
    , _predicate(nullptr)
    , _initialSeed(18390)
    , _stateType(StateType::Terminated)
    , _currentRand(nullptr)
    , _waitOnCleanup(false)
{
}

void ModularTransformer::SetOptions(const map<string, string> & options) {
    auto it = options.find(kOptionModularFocus.GetName());

    if (it != end(options))
    {
        if (_predicate != nullptr)
            delete _predicate;

        if (it->second.size() > 0) {
            regex focusRegEx(it->second);
            _predicate = new function<bool(const string &)>([=](const string & name) {
                    return regex_search(name, focusRegEx);
                });
        }
        else {
            _predicate = nullptr;
        }
    }

    it = options.find("LocalRandomSeed");
    if (it != end(options)) {
        _initialSeed = stoul(it->second);
    }

    it = options.find(kOptionModularWaitOnCleanup.GetName());
    if (it != end(options)) {
        _waitOnCleanup = stoi(it->second);
    }

    _source->SetOptions(options);
}

ModularTransformer::~ModularTransformer() {
    if (_predicate != nullptr)
        delete _predicate;
}

void ModularTransformer::SetTracking(
    map<int, OperationDescription *> * newOps,
    set<int> * toggledOps,
    set<int> * cancelledOps,
    DependencyRecord ** dep,
    vector<MessageRecord *> * msgs,
    vector<AnnotationRecord *> * ants) {

    _newOps = newOps;
    _toggledOps = toggledOps;
    _cancelledOps = cancelledOps;
    _dep = dep;
    _msgs = msgs;
    _ants = ants;
}

int ModularTransformer::GetInputNode() {
    return _source->GetInputNode();
}

int ModularTransformer::GetInputRange() {
    return _source->GetInputRange();
}

void ModularTransformer::ClearInternal() {
    _internalNewOps.clear();
    _internalToggledOps.clear();
    _internalCancelledOps.clear();
    _internalDep = nullptr;
    _internalMsgs.clear();
    _internalAnts.clear();
}

void ModularTransformer::ProceedSourceStep(int choice) {
    ClearInternal();

    _source->SetTracking(&_internalNewOps, &_internalToggledOps, &_internalCancelledOps, &_internalDep, &_internalMsgs, &_internalAnts);
    if (choice < 0) {
        _stateType = _source->Setup();
    }
    else {
        bool toCleanSleepOps = true;
        
        if (_stateType == StateType::Operation) {
            auto it = _internalEnabledOps.find(choice);
            if (it != end(_internalEnabledOps) &&
                IsSleepOp(get<1>(*it)->text)) {
                toCleanSleepOps = false;
            }
        }

        if (toCleanSleepOps) {
            _sleepOps.clear();
        }
        
        _stateType = _source->Proceed(choice);
        
    }
    _source->SetTracking(nullptr, nullptr, nullptr, nullptr, nullptr, nullptr);

    if (_internalDep != nullptr) {
        if (_dep != nullptr) {
            if (*_dep == nullptr) {
                *_dep = new DependencyRecord();
            }
            (*_dep)->readSet.insert(begin(_internalDep->readSet), end(_internalDep->readSet));
            (*_dep)->writeSet.insert(begin(_internalDep->writeSet), end(_internalDep->writeSet));
        }
    }
    else {
        delete _internalDep;
    }

    if (_msgs != nullptr) {
        for (auto message : _internalMsgs)
            _msgs->push_back(message);
    }
    else {
        for (auto message : _internalMsgs)
            delete message;
    }

    if (_ants != nullptr) {
        for (auto annotation : _internalAnts)
            _ants->push_back(annotation);
    }
    else {
        for (auto annotation : _internalAnts)
            delete annotation;
    }

    ArrangeOpSets(_internalAllOps, _internalEnabledOps, _internalNewOps, _internalToggledOps, _internalCancelledOps, true);

    for (auto && kv : _internalNewOps) {
        if (IsSleepOp(get<1>(kv)->text)) {
            _sleepOps.insert(get<0>(kv));
        }
    }

    // Prioritized Scheduler
    //
    // It only enable operations with lowest priority. Others will be
    // masked.  Ops in sleepset are skipped when calculating the
    // lowest priority, then they have the lowest priority calculated.
    //
    // If there are only barriers left for the lowest priority, they
    // will be queued to schedule together. Otherwise they will be
    // masked too.
    //
    // Using clear text is not good ... will change it in future
    _maskedOps.clear();

    int lowestPri = -1;
    bool onlyBarrier;
    bool hasSleepOpsEnabled = false;
    
    for (auto && kv : _internalEnabledOps) {
        if (_sleepOps.find(get<0>(kv)) != end(_sleepOps)) {
            hasSleepOpsEnabled = true;
            continue;
        }
        bool barrier;
        int pri;
        GetPriorityInfo(get<1>(kv)->text, barrier, pri);
        if (lowestPri < 0 || pri < lowestPri) {
            lowestPri = pri;
            onlyBarrier = true;
        }

        if (!barrier && pri == lowestPri) {
            onlyBarrier = false;
        }
    }

    if (hasSleepOpsEnabled) onlyBarrier = false;

    bool queueBarrier = (lowestPri >= 0 && onlyBarrier && _opQueue.size() == 0); 
    if (queueBarrier) {
        // push all barriers into queue to run together if there are only them
        _logger->LogFormat(VerboseLevel::VERBOSE_INFO, "<I> Trigger barrier ops with priority %d", lowestPri);
    }
    
    for (auto && kv : _internalEnabledOps) {
        bool barrier;
        int pri;
        if (_sleepOps.find(get<0>(kv)) != end(_sleepOps)) {
            barrier = false;
            pri = lowestPri;
        }
        GetPriorityInfo(get<1>(kv)->text, barrier, pri);
        if (pri == lowestPri && barrier == onlyBarrier) {
            // ops to enabled, no mask
            if (queueBarrier) {
                _opQueue.push_back(get<0>(kv));
            }
        }
        else {
            _maskedOps.insert(get<0>(kv));
        }
    }
    
    _logger->LogFormat(
        VerboseLevel::VERBOSE_INFO, "[ProceedSourceStep %d] => stateType = %s, internalAllOps.size = %d",
        choice, StateTypeStringify(_stateType), _internalAllOps.size());
}

void ModularTransformer::ProceedSource(int choice) {

    if (_stateType == StateType::Terminated)
        return;
    
    ProceedSourceStep(choice);

    set<int> skippedOps;

    while (_stateType == StateType::Operation) {
        
        if (_opQueue.size() > 0) {
            int choice = _opQueue.front();
            _opQueue.pop_front();

            // Skip those that are internally disabled
            if (_internalEnabledOps.find(choice) == end(_internalEnabledOps))
                continue;

            ProceedSourceStep(choice);
            continue;
        }

        if (_predicate != nullptr && _internalEnabledOps.size() > 0) {

            bool moreToSkip = false;
            bool hasSkippedEnabled = false;
            vector<int> choices;
            for (auto && kv : _internalEnabledOps) {
                if (_maskedOps.find(get<0>(kv)) != end(_maskedOps))
                    continue;
                if (skippedOps.find(get<0>(kv)) != end(skippedOps)) {
                    hasSkippedEnabled = true;
                    continue;
                }
                if ((*_predicate)(get<1>(kv)->text))
                    moreToSkip = true;
                choices.push_back(get<0>(kv));
            };

            if (!moreToSkip && hasSkippedEnabled) {
                // This condition hold iff all exposed operations is choose to skipped 
                // and there is at least one such opeations. 
                // So in the exposed enabled operation where is at least one to do, no fake deadlock
                break;
            }

            int choice = choices[(*_currentRand)() % choices.size()];

            if ((*_predicate)(_internalAllOps[choice]->text)) {
                skippedOps.insert(choice);
            }
            else {
                ProceedSourceStep(choice);
            }
        }
        else {
            // No filtering or no more enabled actions
            break;
        }
    }

    // Expose data out

    // Path info is already forwarded in ProceedSourceStep

    // _newOps, _toggledOps, _cancelledOps

    set<int> exposed_enabled;
    vector<tuple<int, bool>> newOps;
    vector<int> cancelledOps;
    bool toRemoveSleepOps = false;

    for (auto && kv : _internalAllOps) {
        if ((_internalEnabledOps.find(get<0>(kv)) != end(_internalEnabledOps)) && 
            (_maskedOps.find(get<0>(kv)) == end(_maskedOps)) &&
            (_predicate == nullptr || (*_predicate)(get<1>(kv)->text)) // internal operations are always disabled
            ) {
            // remove sleep ops if there is at least one enabled op to do
            if (_sleepOps.find(get<0>(kv)) == end(_sleepOps))
                toRemoveSleepOps = true;
            exposed_enabled.insert(get<0>(kv));
        }
    }

    if (toRemoveSleepOps) {
        for (int id : _sleepOps)
            exposed_enabled.erase(id);
    }
    else {
        _logger->LogFormat(VerboseLevel::VERBOSE_INFO, "<I> Only sleep ops are enabled");
    }

    for (auto && kv : _internalAllOps) {
        auto it = _lastOps.find(get<0>(kv));
        bool enabled = exposed_enabled.find(get<0>(kv)) != end(exposed_enabled); 
        if (it == end(_lastOps)) {
            // New operation
            if (_newOps != nullptr) {
                // Allocate new object to simplify management
                _newOps->emplace(get<0>(kv), new OperationDescription(*get<1>(kv)));
            }

            if (enabled) {
                newOps.push_back(make_tuple(get<0>(kv), true));
            }
            else {
                // But disabled, add it to toggleOps
                if (_toggledOps != nullptr) {
                    _toggledOps->insert(get<0>(kv));
                }

                newOps.push_back(make_tuple(get<0>(kv), false));
            }
        }
        else {
            if (get<1>(*it) != enabled) {
                // Get toggled
                if (_toggledOps != nullptr) {
                    _toggledOps->insert(get<0>(kv));
                }
                get<1>(*it) = enabled;
            }
        }
    }

    // Cancelled ops
    for (auto && kv : _lastOps) {
        if (_internalAllOps.find(get<0>(kv)) == end(_internalAllOps)) {
            if (_cancelledOps != nullptr) {
                _cancelledOps->insert(get<0>(kv));
            }
            // Delay the deletion of elements in _lastExposed.
            cancelledOps.push_back(get<0>(kv));
        }
    }

    // Apply the changes to _lastExposed.
    for (auto op : cancelledOps) {
        _lastOps.erase(op);
    }
    for (auto && tp : newOps) {
        _lastOps[get<0>(tp)] = get<1>(tp);
    }

    _logger->LogFormat(
        VerboseLevel::VERBOSE_INFO, "[ProceedSource %d] => stateType = %s, newOps.size = %d",
        choice, StateTypeStringify(_stateType), _newOps == nullptr ? -1 : _newOps->size());
}

StateType ModularTransformer::Setup() {
    _logger->Log(VerboseLevel::VERBOSE_INFO, __FUNCTION__);

    _currentSeed = _initialSeed;
    _currentRand = new minstd_rand(_currentSeed);
    _stateType = StateType::Operation;

    ProceedSource(-1);

    return _stateType;
}

StateType ModularTransformer::Proceed(int choice) {
    _logger->Log(VerboseLevel::VERBOSE_INFO, __FUNCTION__);

    ProceedSource(choice);

    return _stateType;
}

void ModularTransformer::CleanUp() {
    _logger->Log(VerboseLevel::VERBOSE_INFO, "Cleanup with allOps = {{");
    for (auto && kv : _internalAllOps) {
        _logger->LogFormat(
            VerboseLevel::VERBOSE_INFO, "    %d - %d: %s (%d)",
            get<0>(kv), get<1>(kv)->node, get<1>(kv)->text.c_str(), get<1>(kv)->isWeak);
    }
    _logger->Log(VerboseLevel::VERBOSE_INFO, "}}");

    if (_waitOnCleanup) {    
        _logger->Log(VerboseLevel::VERBOSE_INFO, "Wait on cleanup for debugging ...");
        while (1) this_thread::sleep_for(chrono::milliseconds(1000));
    }

    ClearInternal();
    delete _currentRand;
    _currentRand = nullptr;
    for (auto && kv : _internalAllOps) {
        delete get<1>(kv);
    }
    _internalAllOps.clear();
    _internalEnabledOps.clear();
    _lastOps.clear();
    _opQueue.clear();
    _maskedOps.clear();
    _sleepOps.clear();
    _source->CleanUp();
}
