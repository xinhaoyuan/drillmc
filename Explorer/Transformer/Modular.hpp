#ifndef __DMC_TRANSFORMER_MODULAR_HPP__
#define __DMC_TRANSFORMER_MODULAR_HPP__

#include "Utils/cpp/LWN.hpp"
#include "Explorer/Core/Testcase.hpp"
#include "Explorer/Core/Operation.hpp"
#include "Utils/cpp/Logger.hpp"
#include "Utils/cpp/Option.hpp"
#include <map>
#include <set>
#include <vector>
#include <list>
#include <deque>
#include <random>
#include <functional>
#include <regex>

LWN_START(DMC);

using namespace std;
using namespace XYHelper;

LWN_EXPORT;


class ModularTransformer : public ITarget {

    ITarget * _source;
    Logger * _logger;

    StateType _stateType;

    function<bool(const string &)> * _predicate;
    unsigned long _initialSeed;
    bool _waitOnCleanup;

    static const regex _barrierRegEx;
    static void GetPriorityInfo(const string & name, bool & barrier, int & priority);
    static bool IsSleepOp(const string & name);

    // Upper layer tracking info
    map<int, OperationDescription *> * _newOps;
    set<int> * _toggledOps;
    set<int> * _cancelledOps;
    DependencyRecord ** _dep;
    vector<MessageRecord *> * _msgs;
    vector<AnnotationRecord *> * _ants;

    // Internal bookkeeping
    unsigned long _currentSeed;
    minstd_rand * _currentRand;
    map<int, bool> _lastOps;
    map<int, OperationDescription *> _internalAllOps;
    map<int, OperationDescription *> _internalEnabledOps;
    map<int, OperationDescription *> _internalNewOps;
    set<int> _maskedOps;
    set<int> _sleepOps;
    deque<int> _opQueue;
    set<int> _internalToggledOps;
    set<int> _internalCancelledOps;
    DependencyRecord * _internalDep;
    vector<MessageRecord *> _internalMsgs;
    vector<AnnotationRecord *> _internalAnts;

    void ClearInternal();
    void ProceedSource(int choice);
    void ProceedSourceStep(int choice);

    static const Option kOptionModularFocus;
    static const Option kOptionModularInitSeed;
    static const Option kOptionModularWaitOnCleanup;
    
public:

    static void Register(list<Option> & options);
    
    ModularTransformer(ITarget * source, Logger * logger);
    ~ModularTransformer() override;

    void SetTracking(
        map<int, OperationDescription *> * newOps,
        set<int> * toggledOps,
        set<int> * cancelledOps,
        DependencyRecord ** dep,
        vector<MessageRecord *> * msgs,
        vector<AnnotationRecord *> * ants) override;

    int GetInputNode() override;
    int GetInputRange() override;

    void SetOptions(const map<string, string> & options) override; 

    StateType Setup() override;
    StateType Proceed(int choice) override;
    void CleanUp() override;
    
};

LWN_END;

#endif
