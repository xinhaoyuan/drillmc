#include "Identical.hpp"
#include "Explorer/Core/ExplContext.hpp"

using namespace std;
using namespace DMC;

IdenticalTransformer::IdenticalTransformer(ITarget * source)
    : _source(source) {
}

void IdenticalTransformer::SetTracking(
    map<int, OperationDescription *> * newOps,
    set<int> * toggledOps,
    set<int> * cancelledOps,
    DependencyRecord ** dep,
    vector<MessageRecord *> * msgs,
    vector<AnnotationRecord *> * ants) {

    _newOps = newOps;
    _toggledOps = toggledOps;
    _cancelledOps = cancelledOps;
    _dep = dep;
    _msgs = msgs;
    _ants = ants;
}

int IdenticalTransformer::GetInputNode() {
    return _source->GetInputNode();
}

int IdenticalTransformer::GetInputRange() {
    return _source->GetInputRange();
}

void IdenticalTransformer::ProceedSource(int choice) {
    if (_stateType == StateType::Terminated) return;

    map<int, OperationDescription *> newOps;
    set<int> toggledOps;
    set<int> cancelledOps;
    DependencyRecord * dep = nullptr;
    vector<MessageRecord *> msgs;
    vector<AnnotationRecord *> ants;

    _source->SetTracking(&newOps, &toggledOps, &cancelledOps, &dep, &msgs, &ants);
    _stateType = choice >= 0 ? _source->Proceed(choice) : _source->Setup();
    _source->SetTracking(nullptr, nullptr, nullptr, nullptr, nullptr, nullptr);

    ArrangeOpSets(_allOps, _enabledOps, newOps, toggledOps, cancelledOps, true);

    if (_newOps != nullptr) {
        for (auto && kv : newOps) {
            _newOps->emplace(get<0>(kv), new OperationDescription(*get<1>(kv)));
        }
    }
    else {
        for (auto && kv : newOps) delete get<1>(kv);
    }
    if (_toggledOps != nullptr) *_toggledOps = toggledOps;
    if (_cancelledOps != nullptr) *_cancelledOps = cancelledOps;
    if (_dep != nullptr) *_dep = dep;
    else {
        delete dep;
    }
    if (_msgs != nullptr) *_msgs = msgs;
    else {
        for (auto msg : msgs) delete msg;
    }
    if (_ants != nullptr) *_ants = ants;
    else {
        for (auto ant : ants) delete ant;
    }
}

StateType IdenticalTransformer::Setup() {
    ProceedSource(-1);
    return _stateType;
}

StateType IdenticalTransformer::Proceed(int choice) {
    ProceedSource(choice);
    return _stateType;
}

void IdenticalTransformer::CleanUp() {
    for (auto && kv : _allOps) {
        delete get<1>(kv);
    }
    _allOps.clear();
    _enabledOps.clear();
    _source->CleanUp();
}

