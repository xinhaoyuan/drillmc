#ifndef __DMC_OPERATION_HPP__
#define __DMC_OPERATION_HPP__

#include <set>
#include <string>
#include <vector>
#include <iostream>
#include <map>

#include "Utils/cpp/LWN.hpp"

LWN_START(DMC);

using namespace std;

LWN_EXPORT;

struct DependencyRecord {
    set<string> readSet;
    set<string> writeSet;
};

struct MessageRecord {
    int    target;
    string message;
};

struct AnnotationRecord {
    int    node;
    string type;
    string value;
};

struct OperationDescription {
    int    node;
    string text;
    bool   isWeak;

    OperationDescription()
        : node(-1), text(), isWeak(false) { }
    
    OperationDescription(const OperationDescription & origin)
        : node(origin.node)
        , text(origin.text)
        , isWeak(origin.isWeak)
    { }

    bool operator ==(const OperationDescription & a) {
        return node == a.node && text == a.text && isWeak == a.isWeak;
    }
};

bool IsDescriptionConsistent(const OperationDescription & a, const OperationDescription & b);
bool IsDependencyConsistent(const DependencyRecord & a, const DependencyRecord & b);
bool IsDependent(const DependencyRecord & a, const DependencyRecord & b);

ostream & operator << (ostream & os, const OperationDescription & desc);

LWN_END;

#endif
