#include "SleepSetExplorer.hpp"
#include "ExplContext.hpp"
#include "Utils/cpp/Formatted.hpp"

using namespace std;
using namespace DMC;
using namespace XYHelper;

SleepSetExplorer::SleepSetExplorer(ITarget * target, IPathCollector * pathCollector)
    : _target(target)
    , _pathCollector(pathCollector)
{ }

SleepSetExplorer::~SleepSetExplorer() {
    DeleteStateNodeRecur(_root.node);
}

static void ConstructSleepSet(map<StateNode *, map<int, DependencyRecord *>> & sleepSet, StateNode * node) {
    // Construct the sleep set
    StateNode * parent = node->parent;

    if (parent == nullptr) {
        sleepSet[node] = {};
        return;
    }

    auto && ssParent = sleepSet[parent];
    auto && ssChild = sleepSet[node];

    // Inherit from parent
    ssChild = ssParent;
    if (parent->type == StateType::Operation) {
        // Add all explored siblings into sleep set
        for (auto && kv : parent->children) {
            if (get<0>(kv) < node->parentChoice)
                ssChild.emplace(get<0>(kv), get<1>(kv).node->dependency);
        }
    }
    // Release all operations that is dependent with current operations
    set<int> activatedOperation;
    for (auto && kv : ssChild) {
        if (IsDependent(*node->dependency, *get<1>(kv)))
            activatedOperation.insert(get<0>(kv));
    }

    for (int i : activatedOperation)
        ssChild.erase(i);

    // cerr << "SleepSet of " << endl;
    // PrintStateNode(cerr, node);
    // cerr << " = " << ssChild << endl;
}
                   
void SleepSetExplorer::ExploreAll(Logger * logger) {
    ExplContext * expl = new ExplContext();
    StateNode * cur;
    int choice;
    StateType st;
    map<StateNode *, map<int, DependencyRecord *>> sleepSet;

    st = expl->Setup(_target, _root);
    cur = expl->GetCurrentNode();
    choice = -1;

    while (true) {

        if (choice >= 0) {
            // go to backtracking point and proceed one step
            st = expl->Forward(_target, cur);
            if (st == StateType::Terminated || st == StateType::Error) {
                ReportPathFromNode(
                    _pathCollector,
                    Formatted("Unexpected termination at backtracking").CString(),
                    cur, choice);
                goto DoBacktrack;
            }
            st = expl->Proceed(choice);
        }

        // Go to the bottom using first choice
        while (st != StateType::Terminated && st != StateType::Error) {
            cur = expl->GetCurrentNode();
            ConstructSleepSet(sleepSet, cur);

            if (cur->type == StateType::Operation) {
                auto && ss = sleepSet[cur];
                choice = -1;
                for (auto && kv : cur->enabledOps) {
                    if (ss.find(get<0>(kv)) == end(ss)) {
                        choice = get<0>(kv);
                        break;
                    }
                }

                if (choice >= 0) {
                    st = expl->Proceed(choice);
                }
                else if (cur->enabledOps.size() > 0) {
                    logger->Log(-1, "Cut by POR");
                    break;
                }
            }
            else if (cur->type == StateType::Input) {
                st = expl->Proceed(0);
            }
            else {
                ReportPathFromNode(
                    _pathCollector,
                    Formatted("Unexpected StateType at proceeding").CString(),
                    cur, -1);
                goto DoBacktrack;
            }

            if (st == StateType::Error) {
                ReportPathFromNode(
                    _pathCollector,
                    ExplContext::EndReasonToCStr(expl->GetEndReason()),
                    expl->GetCurrentNode(), -1);
            }
        }

    DoBacktrack:
        cur = expl->GetCurrentNode();
        expl->CleanUp();
        choice = -1;

        // Backtrack
        while (cur->parent != nullptr) {
            if (cur->parent->type == StateType::Operation) {
                for (auto && kv : cur->parent->enabledOps) {
                    if (get<0>(kv) > cur->parentChoice) {
                        choice = get<0>(kv);
                        break;
                    }
                }
            }
            else if (cur->parent->type == StateType::Input) {
                if (cur->parentChoice + 1 < cur->parent->inputRange) {
                    choice = cur->parentChoice + 1;
                }
            }
            else {
                // This is impossible.
                break;
            }

            cur = cur->parent;
            if (choice >= 0) break;
        }

        // No backtrack point found, the tree is over
        if (choice < 0) break;
    }

    delete expl;
}
