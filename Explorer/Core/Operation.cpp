#include "Operation.hpp"

using namespace DMC;
using namespace std;

template<typename T>
static bool IsSame(const set<T> & a, const set<T> & b) {
    if (a.size() != b.size()) return false;
    for (auto && ele : a)
        if (b.find(ele) == end(b))
            return false;
    return true;
}

bool LWN(DMC)::IsDescriptionConsistent(const OperationDescription & a, const OperationDescription & b) {
    if (a.node != b.node || a.text != b.text) return false;
    return true;
}

bool LWN(DMC)::IsDependencyConsistent(const DependencyRecord & a, const DependencyRecord & b) {
    if (!IsSame(a.readSet, b.readSet)) return false;
    if (!IsSame(a.writeSet, b.writeSet)) return false;
    
    return true;
}

ostream & LWN(DMC)::operator << (ostream & os, const OperationDescription & desc) {
    os << "[" << desc.node << "|" << desc.text << "]";
    return os;
}

bool LWN(DMC)::IsDependent(const DependencyRecord & a, const DependencyRecord & b) {
    for (auto && ele : b.writeSet) {
        if (a.readSet.find(ele) != end(a.readSet)) return true;
        if (a.writeSet.find(ele) != end(a.writeSet)) return true;
    }

    for (auto && ele : a.writeSet) {
        if (b.readSet.find(ele) != end(b.readSet)) return true;
    }

    return false;
}