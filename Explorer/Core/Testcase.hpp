#ifndef __DMC_TESTCASE_HPP__
#define __DMC_TESTCASE_HPP__

#include <vector>
#include <tuple>
#include <string>
#include "Utils/cpp/LWN.hpp"
#include "Operation.hpp"

LWN_START(DMC);

using namespace std;

LWN_EXPORT;

enum StateType {
    Operation,
    Input,
    Terminated,
    Error,
};

inline const char * StateTypeStringify(StateType st) {
    if (st == StateType::Operation) return "Operation";
    else if (st == StateType::Input) return "Input";
    else if (st == StateType::Terminated) return "Terminated";
    else if (st == StateType::Error) return "Error";
    else return "<Unknown>";
}

class ITarget {
public:
    virtual ~ITarget() = default;
    virtual void SetTracking(
        map<int, OperationDescription *> * newOps,
        set<int> * toggledOps,
        set<int> * cancelledOps,
        DependencyRecord ** dep,
        vector<MessageRecord *> * msgs,
        vector<AnnotationRecord *> * ants) = 0;

    virtual int GetInputNode() = 0;
    virtual int GetInputRange() = 0;

    virtual void SetOptions(const map<string, string> & options) = 0;

    virtual StateType Setup() = 0;
    virtual StateType Proceed(int choice) = 0;
    virtual void CleanUp() = 0;
};

LWN_END;

#endif
