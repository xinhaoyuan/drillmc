#ifndef __DMC_CORE_EXHAUSTIVE_EXPLORER_HPP__
#define __DMC_CORE_EXHAUSTIVE_EXPLORER_HPP__

#include "Testcase.hpp"
#include "Operation.hpp"
#include "ExplNode.hpp"
#include "PathCollector.hpp"
#include "Utils/cpp/LWN.hpp"
#include "Utils/cpp/Logger.hpp"

LWN_START(DMC);

using namespace std;
using namespace XYHelper;

LWN_EXPORT;

class ExhaustiveExplorer {
private:
    ITarget * _target;
    ExploredLink _root;
    IPathCollector * _pathCollector;
    
public:

    ExhaustiveExplorer(ITarget * target, IPathCollector * pathCollector);
    ~ExhaustiveExplorer();
    
    void ExploreAll(Logger * logger);
    
};

LWN_END;

#endif
