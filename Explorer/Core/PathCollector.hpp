#ifndef __DMC_CORE_PATH_COLLECTOR_HPP__
#define __DMC_CORE_PATH_COLLECTOR_HPP__

#include "Utils/cpp/LWN.hpp"
#include "Operation.hpp"
#include "ExplNode.hpp"
#include <vector>
#include <string>

LWN_START(DMC);

using namespace std;

LWN_EXPORT;

class IPathCollector {
public:
    virtual ~IPathCollector() = default;
    virtual void ReportPath(const string & comment, const vector<int> & choices, const vector<OperationDescription *> & desc) = 0;
};

void ReportPathFromNode(IPathCollector * collector, const string & comment, StateNode * node, int choice = -1);

LWN_END;

#endif
