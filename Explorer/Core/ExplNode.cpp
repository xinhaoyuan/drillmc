#include "ExplNode.hpp"
#include <algorithm>
#include <exception>
#include <deque>
#include <sstream>

using namespace std;
using namespace DMC;

void LWN(DMC)::DeleteStateNodeRecur(StateNode * node) {
    deque<StateNode *> deleteQueue;
    deleteQueue.push_back(node);

    while (!deleteQueue.empty()) {
        node = deleteQueue.front();
        deleteQueue.pop_front();
        if (node == nullptr) continue;

        for (auto && kv : node->newOps) {
            delete get<1>(kv);
        }
        for (auto msg : node->messages) {
            delete msg;
        }
        for (auto ant : node->annotations) {
            delete ant;
        }
        for (auto && kv : node->children) {
            if (!get<1>(kv).weakLink)
                deleteQueue.push_back(get<1>(kv).node);
        }
        delete node->dependency;
        delete node;
    }
}

ostream & LWN(DMC)::PrintStateNodeTrace(ostream & os, StateNode * trace) {
    if (trace == nullptr) return os;

    map<int, vector<tuple<string, vector<tuple<int, int>>>>> traceContainer;
    vector<int> path;
    StateNode * node = trace;

    while (node->parent != nullptr) {
        path.push_back(node->parentChoice);
        node = node->parent;
    }

    reverse(begin(path), end(path));
    
    for (int choice : path) {
        ostringstream descStream;
        int fromNode = node->currentNode;
        switch (node->type) {
        case StateType::Operation: {
            auto desc = node->allOps[choice];
            descStream << '@' << *desc;
            fromNode = desc->node;
            break;
        }
        case StateType::Input:
            descStream << '#' << choice;
            break;
        case StateType::Terminated:
            descStream << '!';
            break;
        }

        string descStr = descStream.str();
        vector<tuple<int, int>> messageTimes;
        for (MessageRecord * msg : node->messages) {
            messageTimes.push_back(forward_as_tuple(
                msg->target,
                (int)traceContainer[msg->target].size()));
        }

        traceContainer[node->currentNode].push_back(forward_as_tuple(descStr, messageTimes));
        node = node->children[choice].node;
    }

    for (auto && kv : traceContainer) {
        os << "Node " << kv.first << '{' << endl;
        int opIndex = 0;
        for (auto && tp : kv.second) {
            os << "  " << opIndex << ':' << get<0>(tp);
            if (get<1>(tp).size() > 0) {
                os << "=>[";
                bool first = true;
                for (auto && innerTp : get<1>(tp)) {
                    if (first == true)
                        first = false;
                    else os << ',';
                    os << '(' << get<0>(innerTp) << ',' << get<1>(innerTp) << ')';
                }
                os << ']';
            }
            os << endl;
        }
        os << "}" << endl;
    }

    return os;
}
