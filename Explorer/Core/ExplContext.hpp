#ifndef __DMC_CORE_EXPL_CONTEXT_HPP__
#define __DMC_CORE_EXPL_CONTEXT_HPP__

#include "Utils/cpp/LWN.hpp"
#include "Testcase.hpp"
#include "Operation.hpp"
#include "ExplNode.hpp"

LWN_START(DMC);

using namespace std;

LWN_EXPORT;

class ExplContext {
public:

    enum EndReason {
        Impossible,
        Deadlock,
        Forced,
        StateMismatch,
        InvalidChoice,
        FromTarget,
        ErrorFromTarget,
    };

    static const char * EndReasonToCStr(EndReason r);

private:

    bool        _setup;
    bool        _sealed;
    EndReason   _reason;
    StateNode * _node;
    ITarget *   _target;
    bool        _checkMismatch;

public:

    inline StateNode * GetCurrentNode() const { return _node; }
    inline EndReason GetEndReason() { return _reason; }
    inline void SetCheckMismatch(bool flag) { _checkMismatch = flag; }

    ExplContext();
    ~ExplContext();

    StateType Setup(ITarget * target, ExploredLink & rootLink);
    StateType Proceed(int choice);
    StateType ProceedSequence(const vector<int> & seq);
    void End(EndReason reason);
    void CleanUp();

    StateType Forward(ITarget * target, StateNode * node);
};

void ArrangeOpSets(
    map<int, OperationDescription *> & all,
    map<int, OperationDescription *> & enabled,
    const map<int, OperationDescription *> & newOps,
    const set<int> & toggled, const set<int> & cancelled,
    bool deleteCancelled);

bool MatchTrackingInfo(StateNode * a, StateNode * b);

LWN_END;

#endif
