#ifndef __DMC_EXPL_NODE_HPP__
#define __DMC_EXPL_NODE_HPP__

#include "Utils/cpp/LWN.hpp"
#include "Operation.hpp"
#include "Testcase.hpp"

#include <map>
#include <iostream>

LWN_START(DMC);

using namespace std;

LWN_EXPORT;

struct ExploredLink {
    // A link is weak <=> its not the tree link in the trace tree.
    bool weakLink;
    struct StateNode * node;

    ExploredLink() : weakLink(false), node(nullptr) {}
};

struct StateNode {
    StateType type;

    StateNode * parent;
    int         parentChoice;

    // Operation changes
    map<int, OperationDescription *> newOps;
    set<int> toggledOps;
    set<int> cancelledOps;

    // Path information
    // For simplicity, dependency == nullptr || messages.size() == 0 must hold.
    // Dependencies are aggregative
    DependencyRecord *         dependency;
    // Messages and annotations are not
    vector<MessageRecord *>    messages;
    vector<AnnotationRecord *> annotations;

    // If type == Input, take a input from [0, inputRange - 1]
    int inputRange;

    // Aggregated information cached for performance
    map<int, OperationDescription *> allOps;
    map<int, OperationDescription *> enabledOps;
    int currentNode;

    // Explored state space
    map<int, ExploredLink> children;
};

void DeleteStateNodeRecur(StateNode * node);
ostream & PrintStateNodeTrace(ostream & os, StateNode * trace);

LWN_END;

#endif
