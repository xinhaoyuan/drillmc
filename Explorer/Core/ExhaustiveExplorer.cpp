#include "ExhaustiveExplorer.hpp"
#include "ExplContext.hpp"
#include "Utils/cpp/Formatted.hpp"

using namespace std;
using namespace DMC;
using namespace XYHelper;

ExhaustiveExplorer::ExhaustiveExplorer(ITarget * target, IPathCollector * pathCollector)
    : _target(target)
    , _pathCollector(pathCollector)
{ }

ExhaustiveExplorer::~ExhaustiveExplorer() {
    DeleteStateNodeRecur(_root.node);
}

void ExhaustiveExplorer::ExploreAll(Logger * logger) {
    ExplContext * expl = new ExplContext();
    StateNode * cur;
    int choice;
    StateType st;

    st = expl->Setup(_target, _root);
    cur = expl->GetCurrentNode();
    choice = -1;
    
    while (true) {

        if (choice >= 0) {
            // go to backtracking point and proceed one step
            st = expl->Forward(_target, cur);
            if (st == StateType::Terminated || st == StateType::Error) {
                ReportPathFromNode(
                    _pathCollector,
                    Formatted("Unexpected termination at backtracking").CString(),
                    cur, choice);
                goto DoBacktrack;
            }
            st = expl->Proceed(choice);
        }

        // Go to the bottom using first choice
        while (st != StateType::Terminated && st != StateType::Error) {
            cur = expl->GetCurrentNode();

            if (cur->type == StateType::Operation) {
                st = expl->Proceed(get<0>(*cur->enabledOps.begin()));
            }
            else if (cur->type == StateType::Input) {
                st = expl->Proceed(0);
            }
            else {
                ReportPathFromNode(
                    _pathCollector,
                    Formatted("Unexpected StateType at proceeding").CString(),
                    cur, -1);
                goto DoBacktrack;
            }

            if (st == StateType::Error) {
                ReportPathFromNode(
                    _pathCollector,
                    ExplContext::EndReasonToCStr(expl->GetEndReason()),
                    expl->GetCurrentNode(), -1);
            }
        }

    DoBacktrack:
        cur = expl->GetCurrentNode();
        expl->CleanUp();
        choice = -1;

        // Backtrack
        while (cur->parent != nullptr) {
            if (cur->parent->type == StateType::Operation) {
                for (auto && kv : cur->parent->enabledOps) {
                    if (get<0>(kv) > cur->parentChoice) {
                        choice = get<0>(kv);
                        break;
                    }
                }
            }
            else if (cur->parent->type == StateType::Input) {
                if (cur->parentChoice + 1 < cur->parent->inputRange) {
                    choice = cur->parentChoice + 1;
                }
            }
            else {
                // This is impossible.
                break;
            }

            cur = cur->parent;
            if (choice >= 0) break;
        }

        // No backtrack point found, the tree is over
        if (choice < 0) break;
    }

    delete expl;
}
