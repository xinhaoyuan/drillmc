#ifndef __DMC_CORE_LOCAL_PATH_COLLECTOR_HPP__
#define __DMC_CORE_LOCAL_PATH_COLLECTOR_HPP__

#include "Utils/cpp/LWN.hpp"
#include "PathCollector.hpp"
#include <iostream>
#include <string>

LWN_START(DMC);

using namespace std;

LWN_EXPORT;

class LocalPathCollector : public IPathCollector {
private:
    bool      _managed;
    ostream * _stream;
    
public:

    LocalPathCollector(const string & path);
    LocalPathCollector(ostream * stream);
    
    ~LocalPathCollector() override;
    
    void ReportPath(const string & comment, const vector<int> & choices, const vector<OperationDescription *> & desc) override;
};

LWN_END;

#endif
