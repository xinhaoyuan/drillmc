#ifndef __DMC_CORE_SLEEP_SET_EXPLORER_HPP__
#define __DMC_CORE_SLEEP_SET_EXPLORER_HPP__

#include "Testcase.hpp"
#include "Operation.hpp"
#include "ExplNode.hpp"
#include "PathCollector.hpp"
#include "Utils/cpp/LWN.hpp"
#include "Utils/cpp/Logger.hpp"

LWN_START(DMC);

using namespace std;
using namespace XYHelper;

LWN_EXPORT;

class SleepSetExplorer {
private:
    ITarget * _target;
    ExploredLink _root;
    IPathCollector * _pathCollector;
    
public:

    SleepSetExplorer(ITarget * target, IPathCollector * pathCollector);
    ~SleepSetExplorer();
    
    void ExploreAll(Logger * logger);
    
};

LWN_END;

#endif
