#include "LocalPathCollector.hpp"
#include <cassert>
#include <fstream>

using namespace std;
using namespace DMC;

LocalPathCollector::LocalPathCollector(const string & path)
    : _stream(nullptr), _managed(false) {
    _stream = new ofstream(path, ofstream::out | ofstream::app);
    _managed = true;
}

LocalPathCollector::LocalPathCollector(ostream * stream)
    : _stream(stream)
    , _managed(false)
{ }

LocalPathCollector::~LocalPathCollector() {
    if (_managed)
        delete _stream;
}

void LocalPathCollector::ReportPath(const string & comment,
                                    const vector<int> & path,
                                    const vector<OperationDescription *> & pathDesc) {
    if (_stream == nullptr) return;
    assert(path.size() == pathDesc.size());

    *_stream << "ErrorPath{" << endl;
    *_stream << "Raw:";
    for (int step = 0; step < path.size(); ++step)
        *_stream << ' ' << path[step];
    *_stream << endl << "Comment: " << comment << endl;
    bool first = true;
    for (int step = 0; step < path.size(); ++step) {
        int choice = path[step];
        OperationDescription * desc = pathDesc[step];

        *_stream << choice;
        if (desc == nullptr) {
            *_stream << endl;
        }
        else {
            *_stream << " " << *desc << endl;
        }
    }
    *_stream << "}ErrorPath" << endl;
    _stream->flush();
}
