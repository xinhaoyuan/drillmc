#include "PathCollector.hpp"
#include <algorithm>

using namespace std;
using namespace DMC;

void LWN(DMC)::ReportPathFromNode(
    IPathCollector * collector,
    const string & comment,
    StateNode * node,
    int choice)
{
    vector<int> path;
    vector<OperationDescription *> desc;

    if (choice >= 0) {
        path.push_back(choice);
        if (node->type == StateType::Operation) {
            desc.push_back(node->allOps.at(choice));
        }
        else {
            desc.push_back(nullptr);
        }
    }

    while (node->parent != nullptr) {
        path.push_back(node->parentChoice);
        if (node->parent->type == StateType::Operation) {
            desc.push_back(node->parent->allOps.at(node->parentChoice));
        }
        else {
            desc.push_back(nullptr);
        }
        node = node->parent;
    }

    reverse(begin(path), end(path));
    reverse(begin(desc), end(desc));

    collector->ReportPath(comment, path, desc);
}

