#include "ExplContext.hpp"
#include <algorithm>
#include <iterator>
#include <cassert>
#include <fstream>

using namespace std;
using namespace DMC;

const char * ExplContext::EndReasonToCStr(ExplContext::EndReason r) {
    switch (r) {
    case EndReason::Deadlock:
        return "Deadlock";
    case EndReason::ErrorFromTarget:
        return "ErrorFromTarget";
    case EndReason::Forced:
        return "Forced";
    case EndReason::FromTarget:
        return "FromTarget";
    case EndReason::Impossible:
        return "Impossible";
    case EndReason::InvalidChoice:
        return "InvalidChoice";
    case EndReason::StateMismatch:
        return "StateMismatch";
    }
}

ExplContext::ExplContext()
    : _setup(false)
    , _sealed(false)
    , _reason(Impossible)
    , _node(nullptr)
    , _target(nullptr)
    , _checkMismatch(false)
{ }

void LWN(DMC)::ArrangeOpSets(
    map<int, OperationDescription *> & all, 
    map<int, OperationDescription *> & enabled,
    const map<int, OperationDescription *> & newOps,
    const set<int> & toggled, const set<int> & cancelled, bool deleteCancelled) {

    all.insert(begin(newOps), end(newOps));
    enabled.insert(begin(newOps), end(newOps));

    //cerr << "all before: " << all << endl;
    //cerr << "enabled before: " << enabled << endl;
    //cerr << "toggled: " << toggled << endl;
    //cerr << "cancelled: " << cancelled << endl;

    for (int id : toggled) {
        auto it = enabled.find(id);
        if (it != end(enabled))
            enabled.erase(it);
        else {
            it = all.find(id);
            if (it != end(all)) {
                enabled.insert(*it);
            }
        }
    }


    for (int id : cancelled) {
        if (deleteCancelled)
            delete all[id];
        all.erase(id);
        enabled.erase(id);
    }
}

bool LWN(DMC)::MatchTrackingInfo(StateNode * a, StateNode * b) {
    if (a->newOps.size() != b->newOps.size()) return false;
    for (auto && kv : a->newOps) {
        auto it = b->newOps.find(get<0>(kv));
        if (it == end(b->newOps)) return false;
        auto op = it->second;
        if (!(*get<1>(kv) == *it->second)) return false;
    }
    if (a->toggledOps != b->toggledOps) return false;
    if (a->cancelledOps != b->cancelledOps) return false; 
    return true;
}

StateType ExplContext::Setup(ITarget * target, ExploredLink & rootLink) {
    assert("Already setup" && !_setup);
    
    _setup = true;
    _sealed = false;

    if (rootLink.node == nullptr) {
        assert("Cannot setup without target nor valid rootLink" && target != nullptr);
        
        _target = target;

        _node = new StateNode();
        
        _node->parent = nullptr;
        _node->parentChoice = -1;
        _node->dependency = nullptr;
        _node->currentNode = 0;
        _target->SetTracking(
            &_node->newOps, &_node->toggledOps, &_node->cancelledOps,
            &_node->dependency, &_node->messages, &_node->annotations);
        StateType st = target->Setup();
        _target->SetTracking(nullptr, nullptr, nullptr, nullptr, nullptr, nullptr);

        ArrangeOpSets(_node->allOps, _node->enabledOps, _node->newOps, _node->toggledOps, _node->cancelledOps, false);
        
        switch ((_node->type = st)) {
        case StateType::Input:
            _node->currentNode = _target->GetInputNode();
            _node->inputRange = _target->GetInputRange();
            break;
        default:
            break;
        }

        rootLink.node = _node;
        rootLink.weakLink = false;
    }
    else {
        _node = rootLink.node;
        
        if (target != nullptr) {
            _target = target;
            if (_checkMismatch) {
                auto cpNode = new StateNode();
                _target->SetTracking(
                    &cpNode->newOps, &cpNode->toggledOps, &cpNode->cancelledOps,
                    &cpNode->dependency, &cpNode->messages, &cpNode->annotations);
                _target->Setup();
                _target->SetTracking(nullptr, nullptr, nullptr, nullptr, nullptr, nullptr);

                if (!MatchTrackingInfo(cpNode, _node)) {
                    End(EndReason::StateMismatch);
                    return StateType::Error;
                }

                DeleteStateNodeRecur(cpNode);
            }
            else {
                _target->Setup();
            }
        }
    }

    if (_node->type == StateType::Terminated) {
        End(EndReason::FromTarget);
    }
    else if (_node->type == StateType::Error) {
        End(EndReason::FromTarget);
    }
    else if (_node->type == StateType::Operation &&
             _node->enabledOps.size() == 0) {
        bool isError = false;
        for (auto && kv : _node->allOps) {
            if (!get<1>(kv)->isWeak) {
                isError = true;
                break;
            }
        }

        if (isError) {
            End(EndReason::Deadlock);
            return StateType::Error;
        }
        else {
            End(EndReason::FromTarget);
            return StateType::Terminated;
        }
    }

    return _node->type;
}

StateType ExplContext::Proceed(int choice) {
    assert("Not set up or already sealed" && _setup && !_sealed);

    if (_node->children.find(choice) == end(_node->children)) {
        assert(_target != nullptr && "Cannot proceed without target nor valid link");

        switch (_node->type) {
        case StateType::Operation:
            if (_node->enabledOps.find(choice) == end(_node->enabledOps)) {
                End(EndReason::InvalidChoice);
                return StateType::Error;
            }
            break;
        case StateType::Input:
            if (!(choice >= 0 && choice < _node->inputRange)) {
                End(EndReason::InvalidChoice);
                return StateType::Error;
            }
            break;
        default:
            assert(!"Cannot proceed with StateType of other than Operation and Input");
            break;
        }

        auto child = new StateNode();
        child->parent = _node;
        child->parentChoice = choice;

        _target->SetTracking(
            &child->newOps, &child->toggledOps, &child->cancelledOps,
            &child->dependency, &child->messages, &child->annotations);
        StateType st = _target->Proceed(choice);
        _target->SetTracking(nullptr, nullptr, nullptr, nullptr, nullptr, nullptr);

        child->allOps.insert(begin(_node->allOps), end(_node->allOps));
        child->enabledOps.insert(begin(_node->enabledOps), end(_node->enabledOps));

        ArrangeOpSets(child->allOps, child->enabledOps, child->newOps, child->toggledOps, child->cancelledOps, false);

        switch (child->type = st) {
        case StateType::Input:
            child->currentNode = _target->GetInputNode();
            child->inputRange = _target->GetInputRange();
            break;
        case StateType::Operation:
            if (_node->type == StateType::Input) {
                child->currentNode = _node->currentNode;
            }
            else {
                auto desc = _node->allOps[choice];
                child->currentNode = desc->node;
            }
            break;
        case StateType::Terminated:
        case StateType::Error:
            child->currentNode = -1;
            break;
        }

        _node->children[choice].weakLink = false;
        _node->children[choice].node = child;
        
        _node = child;
    }
    else {
        _node = _node->children[choice].node;

        if (_target != nullptr) {
            if (_checkMismatch) {
                auto cpNode = new StateNode();
                _target->SetTracking(
                    &cpNode->newOps, &cpNode->toggledOps, &cpNode->cancelledOps,
                    &cpNode->dependency, &cpNode->messages, &cpNode->annotations);
                _target->Proceed(choice);
                _target->SetTracking(nullptr, nullptr, nullptr, nullptr, nullptr, nullptr);

                if (!MatchTrackingInfo(cpNode, _node)) {
                    End(EndReason::StateMismatch);
                    return StateType::Error;
                }

                DeleteStateNodeRecur(cpNode);
            }
            else {
                _target->Proceed(choice);
            }
        }
    }

    if (_node->type == StateType::Terminated) {
        End(EndReason::FromTarget);
    }
    else if (_node->type == StateType::Error) {
        End(EndReason::ErrorFromTarget);
    }
    else if (_node->type == StateType::Operation && 
             _node->enabledOps.size() == 0) {
        bool isError = false;
        for (auto && kv : _node->allOps) {
            if (!get<1>(kv)->isWeak) {
                isError = true;
                break;
            }
        }

        if (isError) {
            End(EndReason::Deadlock);
            return StateType::Error;
        }
        else {
            End(EndReason::FromTarget);
            return StateType::Terminated;
        }
    }
    
    return _node->type;
}

StateType ExplContext::ProceedSequence(const vector<int> & seq) {
    StateType st = _node->type;
    for (int choice : seq) {
        if (_sealed) break;
        st = Proceed(choice);
    }
    return st;
}

void ExplContext::End(EndReason reason) {
    assert("Must have setup but not seal" && _setup && !_sealed);

    _reason = reason;
    _sealed = true;
}

void ExplContext::CleanUp() {
    assert("Must have setup" && _setup);
    if (!_sealed) {
        // The use of error here is a bit confusing ...
        End(EndReason::Forced);
    }
    _setup = false;
    _node = nullptr;
    _reason = Impossible;
    if (_target != nullptr) {
        _target->CleanUp();
        _target = nullptr;
    }
}

ExplContext::~ExplContext() {
    if (_setup) CleanUp();
}

StateType ExplContext::Forward(ITarget * target, StateNode * node) {
    vector<int> path;
    while (node->parent != nullptr) {
        path.push_back(node->parentChoice);
        node = node->parent;
    }
    reverse(begin(path), end(path));

    ExploredLink link;
    link.node = node;
    StateType st = Setup(target, link);
    for (int i = 0; st != StateType::Terminated && i < path.size(); ++i) {
        st = Proceed(path[i]);
    }
    return st;
}
