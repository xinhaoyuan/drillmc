#include "Explorer/Core/ExplContext.hpp"
#include "Utils/cpp/Formatted.hpp"
#include "RandomExplorer.hpp"
#include <iostream>
#include <fstream>
#include <sstream>
#include <random>
#include <tuple>

#ifdef _MSVC
#include <process.h>
#else
#include <unistd.h>
#include <sys/types.h>
#endif

using namespace std;
using namespace DMC;
using namespace XYHelper;

RandomExplorer::RandomExplorer(ITarget * target, IPathCollector * pathCollector, const vector<int> & setupPrefix)
    : _target(target)
    , _pathCollector(pathCollector)
    , _setupPrefix(setupPrefix)
    , _checkMismatch(false)
{ }

RandomExplorer::~RandomExplorer() {
    DeleteStateNodeRecur(_root.node);
}

void RandomExplorer::StartExplore(int repeat, const string & logPrefix, unsigned long sSeed, Logger * baseLogger, Logger * itLogger) {

    if (itLogger != nullptr)
        baseLogger->SetVerboseLevel(-1);
    baseLogger->LogFormat(-1, "Start exploring with sSeed %lu", sSeed);

    string pidTag;
    {
        ostringstream os;
#ifdef _MSVC
        os << _getpid();
#else
        os << getpid();
#endif
        pidTag = os.str();
    }

    minstd_rand randEngine(sSeed);

    while (repeat != 0) {
        if (repeat > 0) --repeat;
        
        unsigned long explSeed = randEngine();

        if (itLogger != nullptr) {
            string targetName;
            {
                ostringstream os;
                os << logPrefix << pidTag << "-" << explSeed << ".log";
                targetName = os.str();
            }

            // If file exists, skip to next
            if (!!ifstream(targetName.c_str())) {
                continue;
            }

            auto output = new ofstream(targetName.c_str());
            itLogger->ResetStream(output, true);
        }

        baseLogger->LogFormat(-1, "Exploring with seed %lu started", explSeed);

        ExploreWithSeed(explSeed, itLogger == nullptr ? baseLogger : itLogger);

        baseLogger->LogFormat(-1, "Exploring with seed %lu finished", explSeed);

        DeleteStateNodeRecur(_root.node);
        _root = ExploredLink();

        if (itLogger != nullptr) {
            itLogger->ResetStream(nullptr, false);
        }
    }
}

void RandomExplorer::ExploreWithSeed(unsigned long seed, Logger * logger) {
    minstd_rand explRand(seed);
    ExplContext * expl = new ExplContext();
    expl->SetCheckMismatch(_checkMismatch);

    map<string, string> localOptions;
    localOptions["LocalRandomSeed"] = to_string(explRand());
    _target->SetOptions(localOptions);
    
    StateType st = expl->Setup(_target, _root);
    if (st != StateType::Terminated && st != StateType::Error && _setupPrefix.size() > 0) {
        logger->LogFormat(-1, "Executing setup prefix");
        st = expl->ProceedSequence(_setupPrefix);
        logger->LogFormat(-1, "Finished setup prefix");
    }
    
    while (st != StateType::Terminated && st != StateType::Error) {
        auto cur = expl->GetCurrentNode();
        int choice;

        if (st == StateType::Operation) {
            vector<int> choices;
            for (auto kv : cur->enabledOps)
                choices.push_back(get<0>(kv));
            choice = choices[explRand() % choices.size()];
        }
        else if (st == StateType::Input) {
            if (cur->inputRange > 0) {
                choice = explRand() % cur->inputRange;
            }
            else {
                choice = 0;
            }
        }
        else {
            // Unknown state, report an error?
        }

        st = expl->Proceed(choice);
    }

    if (st == StateType::Error) {
        ReportPathFromNode(
            _pathCollector,
            ExplContext::EndReasonToCStr(expl->GetEndReason()),
            expl->GetCurrentNode(), -1);
        // logger->LogFormat(-1, "Spin after path reported ...");
        // while (1) sleep(1);
    }
    
    expl->CleanUp();
    delete expl;
}
