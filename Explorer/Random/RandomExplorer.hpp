#ifndef __DMC_RANDOM_EXPLORER_HPP__
#define __DMC_RANDOM_EXPLORER_HPP__

#include "Utils/cpp/LWN.hpp"
#include "Explorer/Core/Testcase.hpp"
#include "Explorer/Core/PathCollector.hpp"
#include "Explorer/Core/ExplNode.hpp"
#include "Utils/cpp/Logger.hpp"

LWN_START(DMC);

using namespace std;
using namespace XYHelper;

LWN_EXPORT;

class RandomExplorer {
private:

    ITarget *        _target;
    IPathCollector * _pathCollector;
    ExploredLink     _root;
    vector<int>      _setupPrefix;

    bool _checkMismatch;

public:

    RandomExplorer(ITarget * target, IPathCollector * pathCollector, const vector<int> & setupPrefix);
    ~RandomExplorer();

    inline void SetCheckMismatch(bool flag) { _checkMismatch = flag; }

    // If itLogger is nullptr, RandomExplorer will print log
    // into base logger instead of using seperate files for logging.
    void StartExplore(int repeat, const string & logBaseDir, unsigned long sSeed, Logger * baseLogger, Logger * itLogger);
    void ExploreWithSeed(unsigned long seed, Logger * logger);
};


LWN_END;

#endif
