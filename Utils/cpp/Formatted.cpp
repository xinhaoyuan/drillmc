#include "Formatted.hpp"
#include <cstdarg>
#include <cstdlib>
#include <cassert>
#include <cstring>
#include <cstdio>

using namespace std;
using namespace XYHelper;

#define PREALLOCATED_BUF_SIZE 80

Formatted::Formatted() {
    _buf = NULL;
    _buf_size = 0;
}

Formatted::Formatted(const char * format_cstr, ...) {
    va_list ap;

    char stack_buf[PREALLOCATED_BUF_SIZE];
    _buf_size = PREALLOCATED_BUF_SIZE;
    _buf = stack_buf;

    while (true) {
        va_start(ap, format_cstr);
        int result = vsnprintf(_buf, _buf_size, format_cstr, ap);
        va_end(ap);

        if (result >= _buf_size) {
            _buf_size = (result + 1 > _buf_size * 2) ?
                (result + 1) : (_buf_size * 2);
            _buf = (char *)realloc(_buf == stack_buf ? NULL : _buf, _buf_size);
            assert(_buf != NULL);
        }
        else if (_buf == stack_buf) {
            _buf = strdup(_buf);
            assert(_buf != NULL);
            _buf_size = result + 1;
            break;
        }
        else break;
    }

    assert(_buf != stack_buf);
}

Formatted &Formatted::operator()(const char * format_cstr, ...) {
    va_list ap;
    
    char* stack_buf = NULL;

    if (_buf == NULL) {
        _buf_size = PREALLOCATED_BUF_SIZE;
        _buf = stack_buf = (char *)alloca(_buf_size);
        if (_buf == NULL) return *this;
    }

    while (true) {
        va_start(ap, format_cstr);
        int result = vsnprintf(_buf, _buf_size, format_cstr, ap);
        va_end(ap);
        
        if (result >= _buf_size) {
            _buf_size = (result + 1 > _buf_size * 2) ?
                (result + 1) : (_buf_size * 2);
            _buf = (char *)realloc(_buf == stack_buf ? NULL : _buf, _buf_size);
            assert(_buf != NULL);
        }
        else if (_buf == stack_buf) {
            _buf = strdup(_buf);
            assert(_buf != NULL);
            _buf_size = result + 1;
            break;
        }
        else break;
    }

    assert(_buf == NULL || _buf != stack_buf);
    
    return *this;
}

Formatted::~Formatted() {
    free(_buf);
}

const char * Formatted::CString() const {
    return _buf;
}

ostream & LWN(XYHelper)::operator<<(ostream & os, const Formatted & f) {
    os << f.CString();
    return os;
}
