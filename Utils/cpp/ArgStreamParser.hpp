#ifndef __ARG_STREAM_PARSER_HPP__
#define __ARG_STREAM_PARSER_HPP__

#include <iostream>
#include <string>
#include <functional>

void ParseArgStream(std::istream & in, const std::function<void(const std::string &)> & outFunc);

#endif
