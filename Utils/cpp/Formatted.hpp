#ifndef __FORMATTED_HPP__
#define __FORMATTED_HPP__

#include <iostream>
#include <sstream>
#include <string>
#include <cstdarg>

#include "LWN.hpp"

LWN_START(XYHelper);

using namespace std;

LWN_EXPORT;

class Formatted {
private:
    int    _buf_size;
    char * _buf;
public:
    Formatted();
    Formatted(const char * format_cstr, ...); // __attribute__((format(printf, 2, 3)));
    Formatted & operator()(const char * format_cstr, ...); // __attribute__((format(printf, 2, 3)));
    ~Formatted();
    const char * CString() const;
};

ostream & operator<<(ostream & os, const Formatted & f);

template<typename Sink>
inline void DumpTo(Sink & sink) { }

template<typename Sink, typename First, typename ... Args>
inline void DumpTo(Sink & sink, First && first, Args && ... args) {
    sink << first;
    DumpTo(sink, args ...);
}

template<typename... Args>
inline std::string GenString(Args && ... args) {
    std::ostringstream os;
    DumpTo(os, args ...);
    return os.str();
}

LWN_END;

#endif
