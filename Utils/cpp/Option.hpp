#ifndef __DMC_ARBITER_OPTION_HPP__
#define __DMC_ARBITER_OPTION_HPP__

#include "Utils/cpp/LWN.hpp"
#include <string>

LWN_START(DMC);

using namespace std;

LWN_EXPORT;

class Option {

    string _name;
    string _description;
    
public:

    Option(const string & name, const string & description)
        : _name(name), _description(description) { }

    inline const string & GetName() const { return _name; }
    inline const string & GetDescription() const { return _description; }
};

LWN_END;

#endif
