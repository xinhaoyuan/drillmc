#ifndef __CLOSURE_HPP__
#define __CLOSURE_HPP__

#include <functional>

namespace XYHelper {
    
    template<typename T>
    struct FuncSignature
    {
        using type = void;
    };

    template<typename R, typename C, typename... T>
    struct FuncSignature<R(C::*)(T...) const>
    {
        using type = std::function<R(T...)>;
    };

    template<typename F>
    typename FuncSignature<decltype(&F::operator())>::type *
    GetClosure(F const &func)
    {
        return new typename FuncSignature<decltype(&F::operator())>::type(func);
    }
}

#endif
