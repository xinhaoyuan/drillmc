#include "ArgStreamParser.hpp"
#include <sstream>

using namespace std;

void ParseArgStream(std::istream & in, const std::function<void(const std::string &)> & outFunc) {
    string line;
    ostringstream buf;
    bool hasBuf = false;
    bool writeToBuf = false;
    
    while (getline(in, line)) {
        if (line.length() > 0 && line[0] == '#') 
            continue;
        int writeIdx = 0;
        writeToBuf = false;
        for (int i = 0; i < line.length(); ++i) {
            if (line[i] == '\\') {
                if (i + 1 >= line.length()) {
                    writeToBuf = true;
                    break;
                }
                
                ++i;
                switch (line[i]) {
                case 'n':
                    line[writeIdx] = '\n';
                    break;
                case 'r':
                    line[writeIdx] = '\r';
                    break;
                default:
                    line[writeIdx] = line[i];
                }
            }
            else if (line[i] == '#') {
                break;
            }
            else if (writeIdx != i) {
                line[writeIdx] = line[i];
            }
            ++writeIdx;
        }
        line.resize(writeIdx);
        
        if (writeToBuf) {
            hasBuf = true;
            buf << line;
            continue;
        }

        // logical line has finished

        if (hasBuf) {
            buf << line;
            outFunc(buf.str());
            buf.str("");
            hasBuf = false;
        }
        else {
            outFunc(line);
        }
    }

    if (hasBuf) {
        outFunc(buf.str());
    }
}
