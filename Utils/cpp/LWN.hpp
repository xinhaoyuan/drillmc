// Importing other namespace without polluting outside includer
#ifndef __LWN__
#define __LWN__

#define LWN_START(name) namespace name { namespace _ { 
#define LWN_EXPORT namespace _export {
#define LWN_END } } using namespace _::_export; }
#define LWN(name) name::_::_export

#endif
