#ifndef __CONTAINER_HPP__
#define __CONTAINER_HPP__

#include "LWN.hpp"
#include <cassert>
#include <utility>
#include <vector>
#include <iostream>
#include <typeinfo>
#include <type_traits>

LWN_START(XYHelper);

using namespace std;

LWN_EXPORT;

template<typename T>
class TIIteratorSource {
public:
    virtual bool HasNext() = 0;
    virtual const T &Next() = 0;
    virtual ~TIIteratorSource() = default;
};

template<typename T, typename S>
class TIteratorSourceConverter : public TIIteratorSource<T> {
private:
    TIIteratorSource<S>* _source;
public:
    TIteratorSourceConverter(TIIteratorSource<S>* source) : _source(source) { }
    bool HasNext() override { return _source->HasNext(); }
    const T &Next() override { return (T &)_source->Next(); }
    ~TIteratorSourceConverter() override { delete _source; }
};

template<typename P>
class TIteratorSourceConverter<typename P::second_type, P> : public TIIteratorSource<typename P::second_type> {
private:
    TIIteratorSource<P>* _source;
public:
    TIteratorSourceConverter(TIIteratorSource<P>* source) : _source(source) { }
    bool HasNext() override { return _source->HasNext(); }
    const typename P::second_type &Next() override { return _source->Next().second; }
    ~TIteratorSourceConverter() override { delete _source; }
};

template<typename T, typename S>
inline typename enable_if<!is_same<T, S>::value, TIIteratorSource<T>*>::type IteratorSourceConvert(TIIteratorSource<S>* source) {
    return new TIteratorSourceConverter<T, S>(source);
}

template<typename T, typename S>
inline typename enable_if<is_same<T, S>::value, TIIteratorSource<T>*>::type IteratorSourceConvert(TIIteratorSource<S>* source) {
    return source;
}

template<typename T>
class TIterator {
private:
    TIIteratorSource<T> *_source;
    // Not copyable 
    TIterator(const TIterator &) = default;
    TIterator &operator=(const TIterator &) = default;
    
    template<typename X>
    friend class TIterator;
    
public:
    // But movable and convertable 
    template<typename S>
    inline TIterator(TIterator<S> &&it) : _source(IteratorSourceConvert<T, S>(it._source)) { it._source = nullptr; }
    template<typename S>
    inline TIterator &operator=(TIterator<S> &&it) { delete _source; _source = it._source; it._source = nullptr; return *this; }
    inline TIterator(TIIteratorSource<T>* source)
        : _source(source) { }
    inline bool HasNext() const { return _source->HasNext(); } 
    inline const T& Next() const { return _source->Next(); }
    inline ~TIterator() { if (_source) delete _source; }
};

template<typename I, typename T>
class TStdIteratorSource : public TIIteratorSource<T> {
    typedef typename I::value_type V;
private:
    I _cur, _end;
public:
    TStdIteratorSource(const I &begin, const I &end)
        : _cur(begin), _end(end) {
    }
    
    bool HasNext() override {
        return _cur != _end;
    }

    const T& Next() override {
        assert(HasNext());
        const T &v = (const T &)*_cur;
        ++_cur;
        return v;
    }
};

// Handle pair iterator for map
template<typename I>
class TStdIteratorSource<I, typename I::value_type::second_type> : public TIIteratorSource<typename I::value_type::second_type> {
    typedef typename I::value_type::second_type T;
private:
    I _cur, _end;
public:
    TStdIteratorSource(const I &begin, const I &end)
        : _cur(begin), _end(end) {
    }
    
    bool HasNext() override {
        return _cur != _end;
    }

    const T& Next() override {
        assert(HasNext());
        const T &v = _cur->second;
        ++_cur;
        return v;
    }
};

template<typename I>
class TStdIteratorEgg {
private:
    I _begin, _end;
public:
    template<typename T>
    inline operator TIterator<T>() const {
        return TIterator<T>(new TStdIteratorSource<I, T>(_begin, _end));
    }

    inline TStdIteratorEgg(const I &begin, const I &end) : _begin(begin), _end(end) { }
};

template<typename C>  
TStdIteratorEgg<typename C::const_iterator> GetAutoIterator(const C &container) {
    return TStdIteratorEgg<typename C::const_iterator>(begin(container), end(container));
}

// ========================================

template<typename T>
class TIContainerSource {
public:
    virtual TIterator<T> GetIterator() const = 0;
    virtual int GetSize() const = 0;
    virtual ~TIContainerSource() = default;
};

template<typename C, typename T = typename C::value_type>
class TStdContainerSource : public TIContainerSource<T> {
private:
    const C *_c;
public:
    TStdContainerSource(const C &stdContainer) : _c(&stdContainer) { }
    
    TIterator<T> GetIterator() const override {
        return TIterator<T>(new TStdIteratorSource<typename C::const_iterator, T>(_c->begin(), _c->end()));
    }

    int GetSize() const override {
        return _c->size();
    }
};

template<typename T>
class TContainer;

template<typename C>
class TStdContainerEgg {
    const C &_c;
public:
    TStdContainerEgg(const C &c) : _c(c) { }
    
    template<typename T>
    inline operator TContainer<T>() const {
        return TContainer<T>(new TStdContainerSource<C, T>(_c));
    }
};

template<typename T>
class TCachedContainerSourceConverter : public TIContainerSource<T> {
private:
    vector<T> _cache;
    
public:
    template <typename S>
    TCachedContainerSourceConverter(TIContainerSource<S>* source) {
        auto &&it = source->GetIterator();
        while (it.HasNext()) {
            auto &&v = it.Next();
            _cache.push_back(v);
        }
    }

    TIterator<T> GetIterator() const override {
        return GetAutoIterator(_cache);
    }

    int GetSize() const override {
        return _cache.size();
    }
};

template<typename T, typename S>
TIContainerSource<T>* ContainerSourceConvert(TIContainerSource<S>* source) {
    return new TCachedContainerSourceConverter<T>(source);
}

template<typename T>
class TContainer {
private:
    TIContainerSource<T> *_source;
    
    template<typename TT>
    friend class TContainer;
    
public:
    
    template<typename TT>
    inline TContainer(const TContainer<TT> &container) : _source(ContainerSourceConvert<T, TT>(container._source)) {}
    
    template<typename TT>
    inline operator TContainer<TT>() { return TContainer<TT>(ContainerSourceConvert<TT, T>(_source)); } 

    inline TContainer(TIContainerSource<T>* source) : _source(source) { }
    inline TIterator<T> GetIterator() const { return _source->GetIterator(); }
    inline int GetSize() const { return _source->GetSize(); }
    inline ~TContainer() { delete _source; }
};

template<typename C>
inline TStdContainerEgg<C> GetAutoContainer(const C& stdContainer) {
    return TStdContainerEgg<C>(stdContainer);
}

LWN_END;

#endif
