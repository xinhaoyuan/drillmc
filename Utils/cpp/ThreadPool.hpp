#ifndef __THREAD_POOL__
#define __THREAD_POOL__

#include "LWN.hpp"
#include <thread>
#include <vector>
#include <mutex>
#include <condition_variable>
#include <set>
#include <tuple>

LWN_START(XYHelper);

using namespace std;

LWN_EXPORT;

class ThreadPool {

    struct JobControl {
        mutex _lock;
        condition_variable _cv;
        void(* _func)(void *);
        void * _data;
        thread * _thread;
    };

    void(* _cbBeforeJob)(void *);
    void(* _cbAfterJob)(void *);
    void * _cbData;

    mutex _lock;
    vector<JobControl *> _idleControl;
    set<JobControl *> _busyControl;
    int _idleLimit;
    bool _exiting;

    static void JobThreadEntry(tuple<ThreadPool *, JobControl *> * data);
    
public:

    inline ThreadPool(void(*cbBeforeJob)(void *), void(*cbAfterJob)(void *), void * cbData, int idleLimit = 16)
        : _cbBeforeJob(cbBeforeJob)
        , _cbAfterJob(cbAfterJob)
        , _cbData(cbData)
        , _idleLimit(idleLimit), _exiting(false) {}
    
    void Start(void(* func)(void *), void * data);
    void Join();
};

LWN_END;

#endif