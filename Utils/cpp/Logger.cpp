#include "Logger.hpp"
#include <cstdio>
#include <cstdlib>
#include <cstring>

using namespace XYHelper;
using namespace std;

Logger::Logger(ostream * output, bool managed)
    : _output(output)
    , _managed(managed)
{
    _verboseLevel = -1;
    _autoFlush = true;
}

Logger::~Logger() {
    if (_managed) {
        delete _output;
    }
}

void Logger::SetVerboseLevel(int verboseLevel) {
    lock_guard<mutex> lock(_lock);
    _verboseLevel = verboseLevel;
}

int Logger::GetVerboseLevel() {
    lock_guard<mutex> lock(_lock);
    return _verboseLevel;
}

void Logger::SetAutoFlush(bool autoFlush) {
    lock_guard<mutex> lock(_lock);
    _autoFlush = autoFlush;
}

bool Logger::GetAutoFlush() {
    lock_guard<mutex> lock(_lock);
    return _autoFlush;
}

void Logger::Flush() {
    lock_guard<mutex> lock(_lock);
    _output->flush();
}

void Logger::ResetStream(ostream * output, bool managed) {
    lock_guard<mutex> lock(_lock);

    if (_managed)
        delete _output;

    _output = output;
    _managed = managed;
}

#define PREALLOCATED_BUF_SIZE 1024

void Logger::LogFormat(int level, const char * format, ...) {
    lock_guard<mutex> lock(_lock);

    if (_output == nullptr) return;

    if (_verboseLevel > 0 && level > _verboseLevel) {
        return;
    }

    va_list ap;

    char   stack_buf[PREALLOCATED_BUF_SIZE];
    size_t buf_size = PREALLOCATED_BUF_SIZE;
    char * buf = stack_buf;

    while (true) {
        va_start(ap, format);
        int result = vsnprintf(buf, buf_size, format, ap);
        va_end(ap);

        if (result >= buf_size) {
            buf_size = (result + 1 > buf_size * 2) ?
                (result + 1) : (buf_size * 2);
            buf = (char *)realloc(buf == stack_buf ? NULL : buf, buf_size);
            if (buf == nullptr) {
                *_output << "Log failed due to realloc" << endl;
                return;
            }
        }
        else break;
    }

    *_output << buf << endl;
    if (_autoFlush)
        _output->flush();

    if (buf != stack_buf) free(buf);
}

void Logger::Log(int level, const char * line) {
    lock_guard<mutex> lock(_lock);

    if (_output == nullptr) return;

    if (_verboseLevel > 0 && level > _verboseLevel) {
        return;
    }
    
    *_output << line << endl;
    if (_autoFlush)
        _output->flush();
}
