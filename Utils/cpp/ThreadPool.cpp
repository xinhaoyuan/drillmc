#include "ThreadPool.hpp"
#include <iostream>
using namespace XYHelper;
using namespace std;

void ThreadPool::JobThreadEntry(tuple<ThreadPool *, JobControl *> * data) {
    auto tp = get<0>(*data);
    auto jc = get<1>(*data);

    unique_lock<mutex> lock(jc->_lock, defer_lock);

    while (true) {
        lock.lock();
        while (jc->_func == nullptr && jc->_data == nullptr) {
            jc->_cv.wait(lock);
        }
        lock.unlock();

        if (jc->_func == nullptr)
            break;

        if (tp->_cbBeforeJob != nullptr)
            tp->_cbBeforeJob(tp->_cbData);

        jc->_func(jc->_data);
        
        jc->_func = nullptr;
        jc->_data = nullptr;

        {
            lock_guard<mutex> lock(tp->_lock);
            tp->_busyControl.erase(jc);
            if (tp->_exiting)
                break;
            if (tp->_idleLimit < 0 || tp->_idleControl.size() < tp->_idleLimit)
                tp->_idleControl.push_back(jc);
            else break;
        }
        
        if (tp->_cbAfterJob != nullptr)
            tp->_cbAfterJob(tp->_cbData);
    }

    delete jc;
}

void ThreadPool::Start(void(*func)(void *), void * data) {
    JobControl * jc = nullptr;
    
    {
        lock_guard<mutex> lock(_lock);
        if (_idleControl.size() > 0) {
            jc = _idleControl.back();
            _idleControl.pop_back();
            _busyControl.insert(jc);
        }
    }

    if (jc == nullptr) {
        jc = new JobControl();
        jc->_func = nullptr;
        jc->_data = nullptr;
        jc->_thread = nullptr;
        {
            lock_guard<mutex> lock(_lock);
            _busyControl.insert(jc);
        }
    }
    
    {
        lock_guard<mutex> lock(jc->_lock);
        jc->_func = func;
        jc->_data = data;
        if (jc->_thread == nullptr) 
            jc->_thread = new thread(JobThreadEntry, new tuple<ThreadPool *, JobControl *>(this, jc));
        else jc->_cv.notify_one();
    }
}

void ThreadPool::Join() {
    vector<thread *> busyThreads;

    {
        lock_guard<mutex> lock(_lock);
        _exiting = true;
        for (auto jc : _busyControl) {
            busyThreads.push_back(jc->_thread);
        }
        for (auto jc : _idleControl) {
            lock_guard<mutex> lock(jc->_lock);
            jc->_data = (void *)1;
            jc->_cv.notify_all();
            busyThreads.push_back(jc->_thread);
        }
    }

    for (thread * t : busyThreads) {
        t->join();
    }
}