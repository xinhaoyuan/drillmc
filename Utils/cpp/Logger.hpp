#ifndef __LOGGER_HPP__
#define __LOGGER_HPP__

#include "Utils/cpp/LWN.hpp"
#include <iostream>
#include <mutex>
#include <cstdarg>

LWN_START(XYHelper);

using namespace std;

LWN_EXPORT;

class Logger {
private:
    ostream * _output;
    bool      _managed;

    mutex     _lock;
    int       _verboseLevel;
    bool      _autoFlush;

public:

    Logger(ostream * output, bool managed = false);
    ~Logger();

    // Following functions are thread-safe

    void SetVerboseLevel(int verboseLevel);
    int  GetVerboseLevel();

    void SetAutoFlush(bool autoFlush);
    bool GetAutoFlush();

    void Flush();

    void ResetStream(ostream * output, bool managed = false);

    void LogFormat(int level, const char * format, ...);
    void Log(int level, const char * line);
};

LWN_END;

#endif
