#!/usr/bin/env python3

import sys
import getopt
import json
import ast

USAGE = """\
Usage: {0} -f input.json
""".format(sys.argv[0])

def handle_func_entry(output, global_options, func_entry):
    if not isinstance(func_entry, dict):
        raise Exception('Except a dict as the function entry')

    if 'prototype' not in func_entry or\
         not isinstance(func_entry['prototype'], list):
        raise Exception('prototype is required list in func_entry')
    elif len(func_entry['prototype']) < 1:
        raise Exception('prototype should contain at least the function itself')
    elif 'opts' in func_entry and\
         not isinstance(func_entry['opts'], list):
        raise Exception('opts in func_entry should be a list')
    elif 'incl' in func_entry and\
         not isinstance(func_entry['incl'], list):
        raise Exception('incl in func_entry should be a list')

    if 'incl' in func_entry:
        for i in func_entry['incl']:
            if not isinstance(i, str):
                raise Exception('incl in func_entry should contains strings')
            output['interposition_incl_set'].add(i)

    opts = set()
    if 'opts' in func_entry:
        for o in func_entry['opts']:
            if not isinstance(o, str):
                raise Exception('opts in func_entry should contains strings')
            opts.add(o)

    args_decl = ''
    args_invk = ''
    for i, p in enumerate(func_entry['prototype']):
        if not isinstance(p, list) or \
           len(p) != 2 or \
           not isinstance(p[0], str) or\
           not isinstance(p[1], str):
            raise Exception('item of args should be [ type, name ]')

        if i == 0:
            ret_type = p[0]
            name = p[1]
            continue

        if '(*)' in p[0]:
            args_decl += '{0}{1}'.format(', ' if i > 1 else '', p[0].replace('(*)', '(*{})'.format(p[1]), 1))
        else:
            args_decl += '{0}{1} {2}'.format(', ' if i > 1 else '', p[0], p[1])
        args_invk += '{0}{2}'.format(', ' if i > 1 else '', p[0], p[1])

    if '(*)' in ret_type:
        func_header = '{0}({1}) {{'.format(ret_type.replace('(*)', '(*{})'.format(name), 1),  args_decl)
        ret_var_decl = ret_type.replace('(*)', '(*{})'.format('r'), 1);
    else:
        func_header = '{0} {1}({2}) {{'.format(ret_type, name, args_decl)
        ret_var_decl = '{0} {1}'.format(ret_type, 'r')

    if "nodef" not in opts:
        output['interposition_source_body'] += """{1}
    {0}_inst_try_init();""".format(global_options['namespace'], func_header)

        if (ret_type == "void"):
            output['interposition_source_body'] += \
                                               """
    if ({0}_flag_inst_on) {{
        {0}_flag_inst_on = 0; {0}_inst_{1}({2}); {0}_flag_inst_on = 1;
    }}
    else
        {0}_orig_{1}({2});
}}
""".format(global_options['namespace'], name, args_invk, func_header)
        else:
            output['interposition_source_body'] += \
                                               """
    if ({0}_flag_inst_on) {{
        {0}_flag_inst_on = 0; {4} = {0}_inst_{1}({2}); {0}_flag_inst_on = 1;
        return r;
    }}
    else
        return {0}_orig_{1}({2});
}}
""".format(global_options['namespace'], name, args_invk, func_header, ret_var_decl)

    output['interposition_header_body'] += \
        'extern {2} (*{0}_orig_{1})({3});\n'.format(
            global_options['namespace'],
            name,
            ret_type,
            args_decl)
    if 'nodef' not in opts:
        output['interposition_header_body'] += \
            '{2} {0}_inst_{1}({3});\n'.format(
                global_options['namespace'],
                name,
                ret_type,
                args_decl)

    output['interposition_source_header'] += \
        '{2} (*{0}_orig_{1})({3}) = (void *)0;\n'.format(
            global_options['namespace'],
            name,
            ret_type,
            args_decl)
    
    output['interposition_source_init_func_body'] += \
        '    {0}_orig_{1} = dlsym(RTLD_NEXT, "{1}");\n'.format(
            global_options['namespace'],
            name)
    

def handle_main_entry(output_base, output, main_entry):
    if not isinstance(main_entry, dict):
        raise Exception('expect a list as the main entry')
    elif 'functions' not in main_entry:
        raise Exception('functions list is required in the main entry')
    elif 'namespace' in main_entry and \
       not isinstance(main_entry['namespace'], str):
        raise Exception('except namespace in main entry to be a string')
    elif 'incl' in main_entry and\
         not isinstance(main_entry['incl'], list):
        raise Exception('incl in main_entry should be a list')
    elif 'header_filename' not in main_entry or\
         not isinstance(main_entry['header_filename'], str):
        raise Exception('header_filename in main_entry should be a string')
    elif 'source_filename' not in main_entry or\
         not isinstance(main_entry['source_filename'], str):
        raise Exception('source_filename in main_entry should be a string')

    global_options = {}
    global_options['namespace'] \
        = main_entry['namespace'] if 'namespace' in main_entry else ''
    global_options['header_filename'] = main_entry['header_filename']
    global_options['source_filename'] = main_entry['source_filename']

    output['interposition_header_body'] = """\
void {0}_inst_on(void);
void {0}_inst_off(void);
int  {0}_inst_save(int);
void {0}_inst_restore(int);
void {0}_inst_try_init(void);
void {0}_inst_init(void);
void {0}_inst_thread_init(void);

""".format(global_options['namespace'])

    output['interposition_incl_set'] = set()
    if 'incl' in main_entry:
        for i in main_entry['incl']:
            if not isinstance(i, str):
                raise Exception('incl in main_entry should contains strings')
            output['interposition_incl_set'].add(i)

    output['interposition_source_header'] = ''
    output['interposition_source_init_func_body'] = ''
    output['interposition_source_body'] = ''

    for func_entry in main_entry['functions']:
        handle_func_entry(output, global_options, func_entry)

    header_output  = """\
#ifndef __{0}_INTERPOSITION_H__
#define __{0}_INTERPOSITION_H__

#if __cplusplus
extern "C" {{
#endif
""".format(global_options['namespace'])
    header_output += '\n'.join(
        [ '#include {0}'.format(i) for i in output['interposition_incl_set'] ])
    header_output += '\n\n' + output['interposition_header_body']
    header_output += """
#if __cplusplus
}
#endif
#endif
"""

    source_output  = """\
#define _GNU_SOURCE
#include <dlfcn.h>
#include <pthread.h>
#include "{0}"
""".format(global_options['header_filename'])
    source_output += """
{1}
static          volatile int {0}_flag_init = 0;
static __thread volatile int {0}_flag_thread_init = 0;
static __thread volatile int {0}_flag_inst_on = 0;

void {0}_inst_on(void) {{ {0}_flag_inst_on = 1; }}
void {0}_inst_off(void) {{ {0}_flag_inst_on = 0; }}
int  {0}_inst_save(int nr) {{ int r = {0}_flag_inst_on; {0}_flag_inst_on = nr; return r; }}
void {0}_inst_restore(int r) {{ {0}_flag_inst_on = r; }}
void {0}_inst_try_init(void) {{
    int r = {0}_inst_save(0);
    
    int _{0}_flag_init;
    __atomic_load(&{0}_flag_init, &_{0}_flag_init, __ATOMIC_ACQUIRE); 
    
    while (_{0}_flag_init != 2) {{
      asm volatile("pause\\n": : :"memory");
      _{0}_flag_init = 0; 
      int cas = __atomic_compare_exchange_n(&{0}_flag_init, &_{0}_flag_init, 1, 1, __ATOMIC_ACQ_REL, __ATOMIC_ACQUIRE);
      if (cas) break;
    }}

    if (_{0}_flag_init == 2) 
      goto skip_global;

{2}
    {0}_inst_init();
    __atomic_store_n(&{0}_flag_init, 2, __ATOMIC_RELEASE);
    
skip_global:

    if ({0}_flag_thread_init == 0) {{
      {0}_flag_thread_init = 1;
      {0}_inst_thread_init();
    }}
    {0}_inst_restore(r);
}}

{3}""".format(global_options['namespace'],
            output['interposition_source_header'],
            output['interposition_source_init_func_body'],
            output['interposition_source_body'])

    with open(output_base + '/' + global_options['header_filename'], 'w') as f:
        f.write(header_output)
    
    with open(output_base + '/' + global_options['source_filename'], 'w') as f:
        f.write(source_output)
    

def main(argv):
    opts, args = getopt.getopt(argv[1:], 'hf:ljo:')
    input_file = sys.stdin
    output_base = "."
    parser = "json"
    
    try:
        for name, value in opts:
            if name == '-h':
                sys.stderr.write(USAGE)
            elif name == '-f':
                input_file = open(value)
            elif name == '-l':
                parser = "literal"
            elif name == '-j':
                parser = "json"
            elif name == '-o':
                output_base = value 
    except Exception as x:
        sys.stderr.write('Error while handling options: {}\n'.format(x))
        return

    try:
        if parser == "literal":
            data = ast.literal_eval(input_file.read())
        elif parser == "json":
            data = json.load(input_file)
        else:
            raise Exception("Unknown parser")
    except Exception as x:
        sys.stderr.write('Error while parse input as {}: {}\n'.format(parser, x))
        return

    output = {}
    handle_main_entry(output_base, output, data)
    
if __name__ == '__main__':
    main(sys.argv)
