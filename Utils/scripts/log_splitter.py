#!/usr/bin/env python3

import re
import os, sys

def main(args):
	
	if len(args) != 2:
		print("Arguments: in-file prefix")
		sys.exit(0);
		
	inFile = open(args[0])
	prefix = args[1]
	
	counter = 0
	outFile = open("{}{}".format(prefix, counter), 'w')
	
	for line in inFile:
		outFile.write(line)
		if re.search("Cleanup finished", line):
			outFile.close()
			counter = counter + 1
			outFile = open("{}{}".format(prefix, counter), 'w')
	
	outFile.close()

if __name__ == '__main__':
	main(sys.argv[1:])
