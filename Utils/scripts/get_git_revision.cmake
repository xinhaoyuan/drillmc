execute_process(
  COMMAND git log -1 "--format=format:#define GIT_REVISION \"%H %ai\"%n" HEAD 
  OUTPUT_FILE ${CMAKE_CURRENT_BINARY_DIR}/gen/git_revision.h.tmp
  RESULT_VARIABLE git_log_result
  )

if (git_log_result)
  message("Cannot get git revision properly. Generate a dummy definition file.")
  file(WRITE ${CMAKE_CURRENT_BINARY_DIR}/gen/git_revision.h.tmp "#define GIT_REVISION \"UNKNOWN\"")
endif (git_log_result)

execute_process(
  COMMAND ${CMAKE_COMMAND} -E compare_files ${CMAKE_CURRENT_BINARY_DIR}/gen/git_revision.h ${CMAKE_CURRENT_BINARY_DIR}/gen/git_revision.h.tmp
  OUTPUT_QUIET
  ERROR_QUIET
  RESULT_VARIABLE git_revision_diff
  )

if (git_revision_diff)
  message("Updating the git revision definition file.")
  file(RENAME ${CMAKE_CURRENT_BINARY_DIR}/gen/git_revision.h.tmp ${CMAKE_CURRENT_BINARY_DIR}/gen/git_revision.h)
else (git_revision_diff)
  file(REMOVE ${CMAKE_CURRENT_BINARY_DIR}/gen/git_revision.h.tmp)
endif (git_revision_diff)
