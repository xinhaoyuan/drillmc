namespace cpp DMC.Thrift
namespace java dmc.thrift

struct OperationDescription {
    1:required i32    node;
    2:required bool   isWeak;
    3:required string text;
}

struct OperationDependency {
    1:required set<string> readSet;
    2:required set<string> writeSet;
}

struct Message {
    1:required i32 target;
    2:required string text;
}

struct Annotation {
    1:required string type;
    2:required string value;
}