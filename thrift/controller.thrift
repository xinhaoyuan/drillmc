include "arbiter.thrift"

namespace cpp DMC.Thrift
namespace java dmc.thrift

service Controller {
        void Schedule(1:arbiter.ArbiterChoice choice);
        void ExecuteCommand(1:i32 fromNodeId, 2:string command);
}
