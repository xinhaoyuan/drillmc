include "common.thrift"

namespace cpp DMC.Thrift
namespace java dmc.thrift

struct OperationDescriptionWithId {
    1:required i32 id;
    2:required common.OperationDescription desc;
}

struct MessageWithId {
    1:required i32 id;
    2:required common.Message message;
}

enum ChoiceSource {
    INPUT,
    LOCAL,
    REMOTE,
    NOTIFY,
    TO_EXIT
}

struct InitConfiguration {
    1:required bool   success;
    2:required string cookie;
    3:required bool   forceExit;
    4:required bool   strongAuth;
    5:required bool   printMissedLog;
    6:required i32    logLevel;
    7:required bool   verboseStackInfo;
}

struct ArbiterChoice {
    1:required ChoiceSource source;
    2:required i32 choice;
    3:required i32 currentMessageTokenCount;
    4:required i32 currentNodeCount;
    5:required i32 currentIdCount;
    6:i32 externalWeight;
    7:i64 localRandomSeed;
    8:string localFilter;
}

struct SyncData {
    // If > 0, it requires arbiter to feed an integer data input from [0, inputRange - 1] immediately
    // If == 0, normal sync
    // If < 0, newOps is empty, and cancelledOps have all existing events from this node.
    //   Arbiter will disconnect with the current node, and allow client with the same id to connect again.
    //   This simulate the node crash case.
    1:i32 inputRange;
    // Operations changes
    2:list<OperationDescriptionWithId> newOps;
    3:list<i32> toggledOps;
    4:list<i32> cancelledOps;
    // Aggregated data dependency
    5:common.OperationDependency dep;
    // Auxilary annotations
    6:list<common.Annotation> annotations;
    // Remote messages
    7:list<MessageWithId> messages;
    8:list<i32> notify; 
    // Updated global counter
    9:i32 updatedMessageTokenCount;
    10:i32 updatedNodeCount;
    11:i32 updatedIdCount;
}

struct ReportData {
    1:required bool isEnd;
    2:required bool isError;
    3:required string description;
    4:list<string> info;
}

struct BufferInfo {
    1:required bool isOpen;
    2:required i32  length; 
}  

service Arbiter {
    InitConfiguration InitNode(1:string cookie, 2:i32 nodeId, 3:string controllerEndpoint);
    void Sync(1:SyncData syncData);
    void Report(1:ReportData reportData);
    ArbiterChoice WaitSchedule();
    void SetVariables(1:map<string, string> kv);
    list<string> GetVariables(1:list<string> names);

    BufferInfo BufferGetInfo(1:i32 id);
    bool BufferOpen(1:i32 id);
    bool BufferClose(1:i32 id);
    i32  BufferWrite(1:i32 id, 2:binary data);
    binary BufferRead(1:i32 id, 2:i32 length);
}
